unit qtx.smart.controls.toolbar;

//############################################################################
//
// Type: QTX Toolbar class
// Framework: QTX framework
// Platform:  Smart Mobile Studio (Smart Pascal @ HTML5)
// Author: Jon Lennart Aasenden
//
// License: This is a part of the QTX library (LGPL). It can be freely used
// by Smart Mobile Studio users, but it is prohibited to add QTX software
// to other libraries, frameworks or RTLs.
//
//############################################################################

interface

uses
  W3C.DOM,
  System.Types,
  System.Types.Graphics,
  System.colors,
  System.Types.Convert,

  SmartCL.Theme,
  SmartCL.Css.Classes,
  SmartCL.Css.Stylesheet,
  SmartCL.System,
  SmartCL.Components,
  SmartCL.Controls.Image;

type
  // Forward declarations
  TQTXToolbarElement    = class;
  TQTXToolbarButton     = class;
  TQTXToolbarSeparator  = class;
  TQTXToolbar           = class;

  // Exception classes
  EQTXToolbarError = class(EW3Exception);

  // All visual elements that can exist on the toolbar inherit from
  // this class. You can add whatever elements you wish to a toolbar,
  // but only those inherited from this base-class will be managed.
  TQTXToolbarElement = class(TW3CustomControl)
  private
    // Returns the index-number for the element in the toolbar
    function  GetIndex: integer;
  protected
    // Override TW3CustomControl behavior
    procedure CBClick(EventObj: JEvent); override;

    // We override this to validate that the parent is a QTX Toolbar
    function  AcceptOwner(const CandidateObject: TObject): boolean; override;

    // This function returns the host toolbar
    function  GetContainer: TQTXToolbar; virtual;

    // And we override the constructor
    procedure InitializeObject; override;
  public
    property  Index: integer read GetIndex;
    property  Container: TQTXToolbar read GetContainer;
  end;

  // We want the toolbar to handle any control, as long as
  // it inherits from TQTXToolbarElement
  TQTXToolbarElementClass = class of TQTXToolbarElement;

  // Used when processing elements in bulk, see the function:
  // GetChildElements() in TQTXToolbar
  TQTXToolbarElementList  = array of TQTXToolbarElement;

  // Toolbutton text layout modes
  TQTXToolbarTextLayout = (
    telLeft,
    telCenter,
    telRight
  );

  // Toolbar layout of elements
  TQTXToolbarElementAlignment = (
    celAlignLeft    = 0,
    celAlignTop     = 1,
    celAlignRight   = 2,
    celAlignBottom  = 3
  );

  // Toolbar orientation option
  TQTXToolbarOrientation = (
    qtoHorizontal,
    qtoVertical
  );

  // Even declarations
  TQTXToolbarElementClickedEvent    = procedure (Sender: TObject; Element: TQTXToolbarElement);
  TQTXToolbarElementDownChangeEvent = procedure (Sender: TObject; Element: TQTXToolbarElement; var Cancel: boolean);
  TQTXToolbarElementClickAllowEvent = procedure (Sender: TObject; Element: TQTXToolbarElement; var Cancel: boolean);

  // This class implements a normal toolbar button.
  // Notice that the class also supports GroupId. Meaning that you can
  // group buttons and ensure that only one button is down at any given time.
  // This is typical toolbar behavior for Windows and Linux.
  TQTXToolbarButton = class(TQTXToolbarElement)
  private
    FGlyph:       TW3Image;
    FCaption:     string;
    FDown:        boolean;
    FLayout:      TW3GlyphLayout = glGlyphTop;
    FTextLayout:  TQTXToolbarTextLayout = telCenter;
    procedure     HandleGlyphReady(Sender: TObject);
  protected
    procedure     SetDownState(NewDownState: boolean);

    // We have two setcaption routines, because the caption can be set
    // during the creating-phase of a form. If that happens then the
    // caption is kept, but not assigned to the element until it's finished
    // constructing, injected into the DOM and attached to the toolbar
    procedure   InternalSetCaption(const NewCaption: string); virtual;
    procedure   SetCaption(NewCaption: string); virtual;

    // Glyph layout + text alignment
    procedure   SetLayout(const NewLayout: TW3GlyphLayout); virtual;
    procedure   SetTextLayout(const NewTextLayout: TQTXToolbarTextLayout); virtual;

    // We override RTL click behavior, since we support groups we have to
    // intercept clicks and potentially cancel them
    procedure   CBClick(EventObj: JEvent); override;

    // Constructor + Destructor
    procedure   InitializeObject; override;
    procedure   FinalizeObject; override;

    // We want to use the tool-contrast of a theme, so we override and set that here
    procedure   StyleTagObject; override;

    // By default, Smart's RTL defaults to DIV elements for TW3CustomControl.
    // In this particular case we want the BUTTON element type, so we override
    // the RTL function that creates the visual element
    function    MakeElementTagObj: THandle; override;

  published
    property    TextLayout: TQTXToolbarTextLayout read FTextLayout write SetTextLayout;
    property    Layout: TW3GlyphLayout read FLayout write SetLayout;
    property    Glyph: TW3Image read FGlyph;
    property    Caption: string read FCaption write SetCaption;
    property    AllowAllUp: boolean;
    property    Down: boolean read FDown write SetDownState;
    property    GroupIndex: integer;
  end;

  // All toolbars have separators. In our case this is a blank, transparent
  // region. Separators are just that, a visual means to create space between
  // groups of buttons or elements
  TQTXToolbarSeparator = class(TQTXToolbarElement)
  protected
    procedure  StyleTagObject; override;
  end;

  TQTXToolbarLabel = class(TQTXToolbarButton)
  protected
    procedure StyleTagObject; override;
  end;

  TQTXToolbar = class(TW3CustomControl)
  private
    class var __IdNumber: integer = 1;
    FButtonWidth:   integer  = 89;
    FButtonHeight:  integer  = 36;
    FSeperatorSize: integer  = 32;
    FSpacing:       integer  = 4;
    FAlignment:     TQTXToolbarElementAlignment = celAlignLeft;
    FOrientation:   TQTXToolbarOrientation = qtoHorizontal;
  protected
    procedure   SetSpacing(NewSpacing: integer); virtual;
    procedure   SetAlignment(const NewAlignment: TQTXToolbarElementAlignment); virtual;
    procedure   SetOrientation(const NewOrientation: TQTXToolbarOrientation); virtual;
    procedure   SetSeperatorSize(const NewSeperatorSize: integer); virtual;
    procedure   SetButtonWidth(const NewWidth: integer); virtual;
    procedure   SetButtonHeight(const NewHeight: integer); virtual;

    procedure   InitializeObject; override;
    procedure   StyleTagObject; override;
    procedure   Resize; override;

    function    GenerateButtonCaption: string;
    procedure   AdjustButtonsToChanges;
    function    GetChildElements: TQTXToolbarElementList;

    procedure   ChildElementClicked(const Child: TQTXToolbarElement); virtual;
    function    ChildElementDown(const Child: TQTXToolbarElement): boolean; virtual;
    function    ChildElementAllowClick(const Child: TQTXToolbarElement): boolean; virtual;

    procedure   ChildAdded(const NewChild: TW3TagContainer); override;
    procedure   ChildRemoved(const OldChild: TW3TagContainer); override;
  public
    property    Count: integer read (GetChildCount);
    property    Items[const Index: integer]: TQTXToolbarElement
                read  ( TQTXToolbarElement(GetChildObject(Index)) ); default;

    function    GetSelectedInGroup(const Group: integer): TQTXToolbarButton;
    procedure   ClearGroupSelected(const GroupIndex: integer);

    function    CreationFlags: TW3CreationFlags; override;
    procedure   Remove(const Element: TQTXToolbarElement);

    function    Add: TQTXToolbarButton; overload;
    function    Add(Caption: string): TQTXToolbarButton; overload;
    function    Add(const ItemClass: TQTXToolbarElementClass): TQTXToolbarElement; overload;
    function    AddSeparator: TQTXToolbarSeparator;

    function    AddLabel(Caption: string): TQTXToolbarLabel; overload;
    function    AddLabel: TQTXToolbarLabel; overload;
    function    AddLabel(Caption: string; InitialWidth: integer): TQTXToolbarLabel; overload;
    function    AddLabel(InitialWidth: integer): TQTXToolbarLabel; overload;

  published
    property    OnElementClicked: TQTXToolbarElementClickedEvent;
    property    OnElementDown: TQTXToolbarElementDownChangeEvent;
    property    OnElementAllowClick: TQTXToolbarElementClickAllowEvent;

    // Toolbar orientation: horizontal, vertical layout
    property    Orientation: TQTXToolbarOrientation read FOrientation write SetOrientation;

    // Toolbar button alignment: left, top, right, bottom
    property    Alignment: TQTXToolbarElementAlignment read FAlignment write SetAlignment;

    property    Spacing: integer read FSpacing write SetSpacing;

    // Ordinary size control. These ensures a minimal size of elements
    property    SeparatorSize: integer read FSeperatorSize write SetSeperatorSize;
    property    ButtonWidth: integer read FButtonWidth write SetButtonWidth;
    property    ButtonHeight: integer read FButtonHeight write SetButtonHeight;
  end;

implementation


//#############################################################################
// TW3ToolbarLabel
//#############################################################################

procedure TQTXToolbarLabel.StyleTagObject;
begin
  inherited;
  ThemeReset();
  ThemeBackground := TW3ThemeBackground.bsToolControlBackground;
  ThemeBorder := TW3ThemeBorder.btToolButtonBorder;
  Font.Color := clWhite;
end;

//#############################################################################
// TQTXToolbarSeparator
//#############################################################################

procedure TQTXToolbarSeparator.StyleTagObject;
begin
  inherited;
  ThemeReset();
  ThemeBackground := TW3ThemeBackground.bsNone;
  ThemeBorder := TW3ThemeBorder.btNone;

  // We want to remain compatible with the Smart Mobile Studio theme-engine
  TagStyle.Add('TW3ToolbarSeparator');
end;

//#############################################################################
// TQTXToolbarElement
//#############################################################################

procedure TQTXToolbarElement.InitializeObject;
begin
  inherited;
end;

function TQTXToolbarElement.GetIndex: integer;
begin
  var tb := GetContainer();
  if tb <> nil then
    result := tb.IndexOfChild(self)
  else
    result := -1;
end;

function TQTXToolbarElement.AcceptOwner(const CandidateObject: TObject): boolean;
begin
  // Elements can only be created on its host
  result := (CandidateObject <> nil) and (CandidateObject is TQTXToolbar);
end;

function TQTXToolbarElement.GetContainer: TQTXToolbar;
begin
  if Parent <> nil then
    result := Parent as TQTXToolbar;
end;

procedure TQTXToolbarElement.CBClick(EventObj: JEvent);
begin
  var Temp := GetContainer();
  if Temp <> nil then
  begin
    if temp.ChildElementAllowClick(self) then
    begin
      inherited CBClick(EventObj);
      Temp.ChildElementClicked(self);
    end;
  end else
  inherited CBClick(EventObj);
end;

//#############################################################################
// TQTXToolbarButton
//#############################################################################

procedure TQTXToolbarButton.InitializeObject;
begin
  inherited;
  FGlyph := TW3Image.Create(nil);
  FGlyph.OnLoad := HandleGlyphReady;
  FDown := false;
end;

procedure TQTXToolbarButton.FinalizeObject;
begin
  FGlyph.OnLoad := nil;
  FGlyph.Free;
  inherited;
end;

procedure TQTXToolbarButton.StyleTagObject;
begin
  inherited;
  ThemeBackground := bsToolButtonBackground;
  ThemeBorder := btToolButtonBorder;

  // We want to remain compatible with the Smart Mobile Studio theme-engine
  self.TagStyle.Add('TW3ToolbarButton');
end;

procedure TQTXToolbarButton.CBClick(EventObj: JEvent);
begin
  // Only do this if we have a parent
  var Temp := GetContainer();
  if Temp <> nil then
  begin
    if temp.ChildElementAllowClick(self) then
    begin
      inherited CBClick(EventObj);

      if GroupIndex > 0 then
      begin
        if not FDown then
          SetDownState( not FDown)
        else
          exit;
      end;

    end;
  end else
  inherited CBClick(EventObj);
end;

procedure TQTXToolbarButton.SetDownState(NewDownState: boolean);
begin
  if not (csDestroying in ComponentState) then
  begin
    if NewDownState <> FDown then
    begin
      var tb := GetContainer();

      // Signal toolbar of change
      if tb <> nil then
      begin
        var LCancel := tb.ChildElementDown(self);
        if LCancel then
          exit;
      end;

      if GroupIndex > 0 then
      begin
        if not Down then
        begin
          if tb <> nil then
            tb.ClearGroupSelected(GroupIndex);
        end else
        if AllowAllUp then
          FDown := false;
      end;

      case FDown of
      false:
        begin
          ThemeBackground := TW3ThemeBackground.bsDecorativeBackground;
          ThemeBorder := TW3ThemeBorder.btDecorativeBorder;
          FDown := true;
        end;
      true:
        begin
          ThemeBackground := TW3ThemeBackground.bsToolButtonBackground;
          ThemeBorder := TW3ThemeBorder.btToolButtonBorder;
          FDown := false;
        end;
      end;

    end;
  end;
end;

function TQTXToolbarButton.MakeElementTagObj: THandle;
begin
  result := w3_createHtmlElement('button');
end;

procedure TQTXToolbarButton.SetTextLayout(const NewTextLayout: TQTXToolbarTextLayout);
begin
  FTextLayout := NewTextLayout;
  if not (csDestroying in ComponentState) then
    InternalSetCaption(FCaption);
end;

procedure TQTXToolbarButton.SetLayout(const NewLayout: TW3GlyphLayout);
begin
  if NewLayout <> FLayout then
  begin
    FLayout := NewLayout;
    if (csReady in ComponentState) then
      InternalSetCaption(FCaption);
  end;
end;

procedure TQTXToolbarButton.SetCaption(NewCaption: string);
begin
  if not (csDestroying in ComponentState) then
  begin
    NewCaption := NewCaption.trim();
    if (NewCaption <> FCaption) then
    begin
      FCaption := NewCaption;
      InternalSetCaption(NewCaption)
    end;
  end;
end;

procedure TQTXToolbarButton.InternalSetCaption(const NewCaption: string);

  function __IMGTAG(const src: string; const wd, hd: integer): string;
  begin
    // Do not alter, this optimizes well by the JSVM
    result := '<img src="' + src + '"';
    result += ' width=';
    result += TInteger.ToPxStr(wd);
    result += ' height=';
    result += TInteger.ToPxStr(hd);
    result += '>';
  end;

begin
  var Html := '';

  if FGlyph.Ready then
  begin
    var wd := FGlyph.PixelWidth;
    var hd := FGlyph.PixelHeight;

    Html := '<table width="100%" height="100%" class="TQTXToolButtonTable" border=0>';
    case FLayout of
    glGlyphTop:
      begin
        Html += '<tr>';
        html += '<td>';
        Html += __IMGTAG(FGlyph.Url, wd, hd);
        Html += '</td>';
        Html += '</tr>';

        Html += '<tr>';
        case FTextLayout of
        telLeft:    Html += '<td align="left">' + NewCaption + '</td>';
        telCenter:  Html += '<td align="center">' + NewCaption + '</td>';
        telRight:   Html += '<td align="right">' + NewCaption + '</td>';
        end;
        Html += '</tr>';
      end;
    glGlyphLeft:
      begin
        Html += '<tr>';
        Html += '<td>';
        Html += __IMGTAG(FGlyph.Url, wd, hd);
        Html += '</td>';
        case FTextLayout of
        telLeft:    Html += '<td align="left">' + NewCaption + '</td>';
        telCenter:  Html += '<td align="center">' + NewCaption + '</td>';
        telRight:   Html += '<td align="right">' + NewCaption + '</td>';
        end;
        Html += '</tr>';
      end;
    glGlyphRight:
      begin
        Html += '<tr>';
        case FTextLayout of
        telLeft:    Html += '<td align="left">' + NewCaption + '</td>';
        telCenter:  Html += '<td align="center">' + NewCaption + '</td>';
        telRight:   Html += '<td align="right">' + NewCaption + '</td>';
        end;
        Html += '<td>';
        Html += __IMGTAG(FGlyph.Url, wd, hd);
        Html += '</td>';
        Html += '</tr>';
      end;
    glGlyphBottom:
      begin
        Html += '<tr>';
        case FTextLayout of
        telLeft:    Html += '<td align="left">' + NewCaption + '</td>';
        telCenter:  Html += '<td align="center">' + NewCaption + '</td>';
        telRight:   Html += '<td align="right">' + NewCaption + '</td>';
        end;
        Html += '</tr>';
        Html += '<tr>';
        Html += '<td>';
        Html += __IMGTAG(FGlyph.Url, wd, hd);
        Html += '</td>';
        Html += '</tr>';
      end;
    end;
    Html += '</table>';
  end else
  begin
    Html := '<table width=100% class="TQTXToolButtonTable" border=0>';
    Html += '<tr>';
    case FTextLayout of
    telLeft:    Html += '<td align="left">' + NewCaption + '</td>';
    telCenter:  Html += '<td align="center">' + NewCaption + '</td>';
    telRight:   Html += '<td align="right">' + NewCaption + '</td>';
    end;
    Html += '</tr>';
    Html += '</table>';
  end;
  Handle.innerHTML := html;
end;

procedure TQTXToolbarButton.HandleGlyphReady(Sender: TObject);
begin
  // By triggering this, the layout for the button is re-generated
  InternalSetCaption(FCaption);
end;

//############################################################################
// TQTXToolbar
//############################################################################

procedure TQTXToolbar.InitializeObject;
begin
  inherited;
  SimulateMouseEvents := true;
  TransparentEvents   := true;

  SetSize(200, 48);

  FButtonWidth        := 100;
  FButtonHeight       := 32;
end;

procedure TQTXToolbar.StyleTagObject;
begin
  inherited;
  ThemeBackground := bsToolContainerBackground;
  ThemeBorder := btToolContainerBorder;
end;

function TQTXToolbar.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();

  // Generally not set, but we make sure these are excluded
  exclude(result, cfAllowSelection);
  exclude(result, cfKeyCapture);

  // And make sure these are included
  Include(result, cfReportChildAddition);
  Include(result, cfReportChildRemoval);
end;

procedure TQTXToolbar.ClearGroupSelected(const GroupIndex: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    try
      var LElements := GetChildElements();

      for var el in LElements do
      begin
        if (el is TQTXToolbarButton) then
        begin
          var LButton := TQTXToolbarButton(el);
          if LButton.GroupIndex = GroupIndex then
          begin
            if LButton.Down then
              LButton.Down := false;
          end;
        end;
      end;

    finally
      EndUpdate();
    end;
  end;
end;

function TQTXToolbar.ChildElementAllowClick(const Child: TQTXToolbarElement): boolean;
begin
  // Allow the click
  result := true;
  if assigned(OnElementAllowClick) then
    OnElementAllowClick(self, Child, result);
end;

function TQTXToolbar.ChildElementDown(const Child: TQTXToolbarElement): boolean;
begin
  // We default to false here since that means "allow".
  // True would cancel the down operation
  result := false;
  if assigned(OnElementDown) then
    OnElementDown(self, Child, result);
end;

procedure TQTXToolbar.ChildElementClicked(const Child: TQTXToolbarElement);
begin
  if Child <> nil then
  begin
    if assigned(OnElementClicked) then
      OnElementClicked(self, Child);
  end;
end;

procedure TQTXToolbar.SetOrientation(const NewOrientation: TQTXToolbarOrientation);
begin
  if not (csDestroying in ComponentState) then
  begin
    if NewOrientation <> FOrientation then
    begin
      BeginUpdate();
      FOrientation := NewOrientation;
      AddToComponentState([csSized]);
      EndUpdate;
    end;
  end;
end;

procedure TQTXToolbar.SetAlignment(const NewAlignment: TQTXToolbarElementAlignment);
begin
  if not (csDestroying in ComponentState) then
  begin
    if NewAlignment <> FAlignment then
    begin
      BeginUpdate();
      FAlignment := NewAlignment;
      AddToComponentState([csSized]);
      EndUpdate;
    end;
  end;
end;

procedure TQTXToolbar.SetSpacing(NewSpacing: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    NewSpacing := if NewSpacing < 2 then 2 else NewSpacing;
    if NewSpacing <> FSpacing then
    begin
      BeginUpdate;
      try
        FSpacing := NewSpacing;
        AddToComponentState([csSized]);
      finally
        EndUpdate;
      end;

    end;
  end;
end;

procedure TQTXToolbar.SetSeperatorSize(const NewSeperatorSize: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    var Temp := if NewSeperatorSize >16 then NewSeperatorSize else 16;
    if Temp <> FSeperatorSize then
    begin
      BeginUpdate;
      try
        FSeperatorSize := Temp;
        AddToComponentState([csSized]);
      finally
        EndUpdate;
      end;
    end;
  end;
end;

procedure TQTXToolbar.SetButtonWidth(const NewWidth: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    var Temp := if NewWidth >16 then NewWidth else 16;
    if Temp <> FButtonWidth then
    begin
      BeginUpdate;
      FButtonWidth := Temp;
      AddToComponentState([csSized]);
      AdjustButtonsToChanges();
      EndUpdate;
    end;
  end;
end;

procedure TQTXToolbar.SetButtonHeight(const NewHeight: integer);
var
  Temp: integer;
begin
  if not (csDestroying in ComponentState) then
  begin
    Temp := if NewHeight >16 then NewHeight else 16;
    if Temp <> FButtonHeight then
    begin
      BeginUpdate;
      FButtonHeight := Temp;
      AddToComponentState([csSized]);
      AdjustButtonsToChanges();
      EndUpdate;
    end;
  end;
end;

// If the default-button-width is changed on a populated toolbar,
// we traverse the elements and manually adjust the sizes before
// invoking a resize.
// *** NOTE: Only buttons that are smaller than the default-size will be
//           adjusted. Larger buttons that have been manually sized will
//           not be affected!
procedure TQTXToolbar.AdjustButtonsToChanges;
begin
  var LChanged := 0;
  var LElements := GetChildElements();

  if LElements.Count > 0 then
  begin
    BeginUpdate();
    try
      for var el in LElements do
      begin
        if (el is TQTXToolbarButton) then
        begin
          var LButton := TQTXToolbarButton(el);

          // Adjust width
          if LButton.Width < FButtonWidth then
          begin
            LButton.Width := FButtonWidth;
            inc(LChanged);
          end;

          // Adjust height
          if LButton.Height < FButtonHeight then
          begin
            LButton.Height := FButtonHeight;
            inc(LChanged);
          end;

        end;
      end;
    finally
      if LChanged > 0 then
        self.AddToComponentState([csSized]);
      EndUpdate();
    end;
  end;
end;

// If the separator size is changed, we loop through all elements
// and adjust the size of each separator directly before invoking
// a resize. ** NOTE: Separator size is based on minimum, so separators
// that are larger than the default-size set here, will not be adjusted !
{procedure TQTXToolbar.AdjustSeparatorsToChanges;
begin
  var LChanged := 0;
  var LElements := GetChildElements();
  var LBounds := AdjustClientRect(ClientRect);

  if LElements.Count > 0 then
  begin
    BeginUpdate();
    try
      for var el in LElements do
      begin
        if (el is TQTXToolbarSeparator) then
        begin
          var LSep := TQTXToolbarSeparator(el);
          if LSep.Width < FSeperatorSize then
          begin
            LSep.Width := FSeperatorSize;
            inc(LChanged);
          end;

          // Make sure height matches
          if LSep.height <> LBounds.Height then
            LSep.Height := LBounds.Height;

        end;
      end;
    finally
      if LChanged > 0 then
        self.AddToComponentState([csSized]);
      EndUpdate();
    end;
  end;
end; }

function TQTXToolbar.GetChildElements: TQTXToolbarElementList;
begin
  if not (csDestroying in ComponentState) then
  begin
    var LObjs := GetChildren();
    for var el in LObjs do
    begin
      if (el is TQTXToolbarElement) then
        result.add( TQTXToolbarElement(el) );
    end;
  end;
end;

procedure TQTXToolbar.Resize;
begin
  inherited;

  // Mass typecast + collect of valid elements
  var LElements := GetChildElements();
  var LBounds := AdjustClientRect(ClientRect);

  case FOrientation of
  qtoHorizontal:
    begin
    case ord(FAlignment) of
    celAlignLeft, celAlignTop:
      begin
        var dx := LBounds.left;
        for var LItem in LElements do
        begin
          if LItem is TQTXToolbarSeparator then
          begin
            var LTotal := LBounds.Height - 4;
            LItem.Width := if LItem.Width < FSeperatorSize then FSeperatorSize else LItem.Width;
            LItem.Height := if LItem.Height > LTotal then LTotal else LItem.Height;
          end;

          var dy := (LBounds.height div 2) - (LItem.Height div 2);
          inc(dy, LBounds.top);
          LItem.MoveTo(dx, dy);
          inc(dx, LItem.Width + FSpacing);
        end;
      end;

    celAlignRight,celAlignBottom:
      begin
        var dx := LBounds.right;
        for var LItem in LElements do
        begin
          if LItem is TQTXToolbarSeparator then
          begin
            var LTotal := LBounds.Height - 4;
            LItem.Width := if LItem.Width < FSeperatorSize then FSeperatorSize else LItem.Width;
            LItem.Height := if LItem.Height > LTotal then LTotal else LItem.Height;
          end;

          var dy := (LBounds.height div 2) - (LItem.Height div 2);
          inc(dy, LBounds.top);
          LItem.MoveTo(dx - LItem.width, dy);
          dec(dx, LItem.Width + FSpacing);
        end;
      end;
    end;
    end;
  qtoVertical:
    begin
      case ord(FAlignment) of
      celAlignLeft, celAlignTop:
        begin
        end;
      celAlignRight, celAlignBottom:
        begin
        end;
      end;
    end;
  end;

end;

function TQTXToolbar.Add(const ItemClass: TQTXToolbarElementClass): TQTXToolbarElement;
begin
  if ItemClass <> nil then
  begin
    BeginUpdate;
    try
      result := ItemClass.Create(self);
      result.SetSize(FButtonWidth, FButtonHeight);
    finally
      AddToComponentState([csSized]);
      EndUpdate;
    end;
  end else
  raise EQTXToolbarError.Create('Failed to add item, classtype was nil error');
end;

function TQTXToolbar.GenerateButtonCaption: string;
begin
  inc(__IdNumber);
  result := 'Button ' + __IdNumber.ToString();
end;

function TQTXToolbar.Add(Caption: string): TQTXToolbarButton;
begin
  BeginUpdate();
  try
    result := TQTXToolbarButton.Create(self);
    result.SetSize(FButtonWidth, FButtonHeight);
    result.Caption := Caption;
  finally
    AddToComponentState([csSized]);
    EndUpdate();
  end;
end;

function TQTXToolbar.Add: TQTXToolbarButton;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    try
      result := TQTXToolbarButton.Create(self);
      result.SetSize(FButtonWidth, FButtonHeight);
      result.Caption := GenerateButtonCaption();
    finally
      AddToComponentState([csSized]);
      EndUpdate();
    end;
  end;
end;

function TQTXToolbar.AddLabel(Caption: string; InitialWidth: integer): TQTXToolbarLabel;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    try
      result := TQTXToolbarLabel.Create(self);
      result.Caption := Caption.trim();
      result.SetSize(
        if InitialWidth > 0 then InitialWidth else FButtonWidth,
        FButtonHeight);
    finally
      AddToComponentState([csSized]);
      EndUpdate();
    end;
  end;
end;

function TQTXToolbar.AddLabel(InitialWidth: integer): TQTXToolbarLabel;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    try
      result := TQTXToolbarLabel.Create(self);
      result.Caption := GenerateButtonCaption();
      result.SetSize(
        if InitialWidth > 0 then InitialWidth else FButtonWidth,
        FButtonHeight);
    finally
      AddToComponentState([csSized]);
      EndUpdate();
    end;
  end;
end;

function TQTXToolbar.AddLabel(Caption: string): TQTXToolbarLabel;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    try
      result := TQTXToolbarLabel.Create(self);
      result.Caption := Caption.trim();
      result.SetSize(FButtonWidth, FButtonHeight);
    finally
      AddToComponentState([csSized]);
      EndUpdate();
    end;
  end;
end;

function TQTXToolbar.AddLabel: TQTXToolbarLabel;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    try
      result := TQTXToolbarLabel.Create(self);
      result.SetSize(FButtonWidth, FButtonHeight);
    finally
      AddToComponentState([csSized]);
      EndUpdate();
    end;
  end;
end;

function TQTXToolbar.AddSeparator: TQTXToolbarSeparator;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    try
      result := TQTXToolbarSeparator.Create(self);
      result.SetSize(FSeperatorSize, FButtonHeight);
    finally
      AddToComponentState([csSized]);
      EndUpdate();
    end;
  end;
end;

function TQTXToolbar.GetSelectedInGroup(const Group: integer): TQTXToolbarButton;
begin
  if not (csDestroying in ComponentState) then
  begin
    var LElements := GetChildElements();
    for var el in LElements do
    begin
      if (el is TQTXToolbarButton) then
      begin
        var btn := TQTXToolbarButton(el);
        if btn.GroupIndex = Group then
        begin
          if btn.Down then
          begin
            result := btn;
            break;
          end;
        end;
      end;
    end;
  end;
end;

procedure TQTXToolbar.ChildAdded(const NewChild: TW3TagContainer);
begin
  if (NewChild is TQTXToolbarElement) then
  begin
    if (csReady in ComponentState) then
      Invalidate();
  end;
  inherited ChildAdded(NewChild);
end;

procedure TQTXToolbar.ChildRemoved(const OldChild: TW3TagContainer);
begin
  if (OldChild is TQTXToolbarElement) then
  begin
    if (csReady in ComponentState) then
      Invalidate();
  end;
  inherited ChildRemoved(OldChild);
end;

procedure TQTXToolbar.Remove(const Element: TQTXToolbarElement);
begin
  if not (csDestroying in ComponentState) then
  begin
    var Temp := IndexOfChild(Element);
    if Temp >= 0 then
    begin
      BeginUpdate();
      try
        Element.free;
      except
        on e: exception do;
      end;
      self.AddToComponentState([csSized]);
      EndUpdate();
    end;
  end;
end;


initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var StyleCode := #'

    .TQTXToolbar {
      padding: 2px;
      -webkit-box-shadow: 0px -1px 2px #000000, 0.4)?>;
         -moz-box-shadow: 0px -1px 2px #000000, 0.4)?>;
          -ms-box-shadow: 0px -1px 2px #000000, 0.4)?>;
           -o-box-shadow: 0px -1px 2px #000000, 0.4)?>;
              box-shadow: 0px -1px 2px #000000, 0.4)?>;
    }

    .TQTXToolButtonTable {
      border-spacing: 0px;
      table-layout: fixed;
      border-collapse: collapse;
      padding: 0px;
      border: 0px;
      margin: 0px;
    }

    .TQTXToolButtonTable > th, td {
      padding: 0px;
    }

    ';
  Sheet.Append(StyleCode);
end;



end.
