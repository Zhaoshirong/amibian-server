unit editor.clickblocker;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,
  System.Device.Storage,
  SmartCL.Device.Storage,
  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System;

type

  TClickBlocker = class(TW3Blockbox)
  public
    property  Id: string;
  end;

  TClickBlockerAPI = static class
  private
    class var FBlockers: array of TClickBlocker;
  public
    class function ShowClickBlocker(Id: string): TClickBlocker;
    class procedure CloseClickBlocker(Id: string);
  end;

implementation


class function TClickBlockerAPI.ShowClickBlocker(Id: string): TClickBlocker;
begin
  // Create blocker element
  result := TClickBlocker.Create( Application.Display );
  result.Id := ID;

  // Add to list of blockers [modal views active]
  FBlockers.add(result);

  // some fine tuning
  result.SizeToParent();
end;

class procedure TClickBlockerAPI.CloseClickBlocker(Id: string);
begin
  if FBlockers.Count > 0 then
  begin
    var x := FBlockers.low;
    while x <= FBlockers.high do
    begin
      if FBlockers[x].Id.toLower() = ID.ToLower() then
      begin
        var LInstance := FBlockers[x];
        FBlockers.delete(x, 1);
        LInstance.freeAfter(100);
        LInstance := nil;
        continue;
      end;
      inc(x);
    end;
  end;
end;


end.
