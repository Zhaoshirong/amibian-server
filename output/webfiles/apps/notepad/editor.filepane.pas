unit editor.filepane;

interface

uses
  W3C.DOM,
  System.Types,
  System.Types.Graphics,
  System.Colors,
  SmartCL.Css.Stylesheet,
  SmartCL.System,
  SmartCL.Graphics,
  SmartCL.Components,
  SmartCL.Fonts,
  SmartCL.Theme,
  SmartCL.Borders,
  SmartCL.Controls.Label;

type

  TEdFilePane = class(TW3CustomControl)
  private
    FHeader:  TW3Label;
  protected
    procedure InitializeObject; override;
    procedure StyleTagObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;
  public
    property  Header: TW3Label read FHeader;
  end;

implementation

//#############################################################################
// TEdFilePane
//#############################################################################

procedure TEdFilePane.InitializeObject;
begin
  inherited;
  FHeader := TW3Label.Create(self);
  Border.Margin := 0;
  Border.Padding := 2;
end;

procedure TEdFilePane.ObjectReady;
begin
  inherited;
  TW3Dispatch.WaitFor([FHeader], procedure ()
  begin

    FHeader.Caption := 'Files';
    FHeader.AlignText := TTextAlign.taCenter;
    FHeader.VAlign := TTextVAlign.tvCenter;
    FHeader.Font.Color := clWhite;
    FHeader.ThemeBorder := TW3ThemeBorder.btFlatBorder;
    Fheader.SizeToTextContent();

    Resize();
  end);
end;

procedure TEdFilePane.StyleTagObject;
begin
  inherited;
  ThemeReset();
  ThemeBackground := TW3ThemeBackground.bsToolContainerBackground;
  ThemeBorder := TW3ThemeBorder.btToolContainerBorder;
  { Border.Top.Width := 1;
  border.Top.Color := clBlack;
  Border.Top.Style := TW3BorderEdgeStyle.besSolid; }
end;

procedure TEdFilePane.Resize;
begin
  inherited;
  var LBounds := AdjustClientRect(ClientRect);
  LBounds.bottom := LBounds.top + Fheader.height;
  FHeader.SetBounds(LBounds);
end;


end.
