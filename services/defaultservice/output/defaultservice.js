var $R = [
	"Method %s in class %s threw exception [%s]",
	"Procedure %s threw exception [%s]",
	"Host classtype was rejected as unsuitable",
	"Invalid handle for operation, reference was null error",
	"Invalid stream style for operation, expected memorystream",
	"Method not implemented",
	"stream operation failed, system threw exception: %s",
	"write failed, system threw exception: %s",
	"read failed, system threw exception: %s",
	"operation failed, invalid handle error",
	"Invalid length, %s bytes exceeds storage medium error",
	"Read failed, invalid signature error [%s]",
	"Invalid length, %s bytes exceeds storage boundaries error",
	"Write failed, invalid signature error [%s]",
	"Write failed, invalid datasize [%d] error",
	"File operation [%s] failed, system threw exception: %s",
	"Structure %s error, method %s.%s threw exception [%s] error",
	"Structure storage failed, structure contains function reference error",
	"Structure storage failed, structure contains symbol reference error",
	"Structure storage failed, structure contains uknown datatype error",
	"Failed to read structure, method %s.%s threw exception [%s] error",
	"Failed to write structure, method %s.%s threw exception [%s] error",
	"Structure data contains invalid or damaged data signature error",
	"Failed to register message, classtype was nil error",
	"Failed to register message, handler as nil error",
	"Failed to register message, a handler for that message already exists error",
	"Failed to register message, classtype was nil error",
	"Failed to register message, handler as nil error",
	"Failed to register message, a handler for that message already exists error",
	"Invalid dictionary key",
	"Invalid array of dictionary keys",
	"Database not active error",
	"Prepare failed, database was nil error",
	"Prepare failed, database not active error",
	"Failed to alter value, field is read-only error",
	"Operation failed, dataset is not active error",
	"Invalid state for operation error",
	"Failed to match field class for datatype error",
	"Field definition cannot be altered in a live dataset error",
	"Failed to read field value, invalid field signature error",
	"A field with that name already exists error",
	"Failed to add field definition, invalid name error",
	"Delete operation failed, reference was NIL error",
	"Read of field-definitions failed, invalid header signature error",
	"Loading dataset failed, invalid signature error",
	"Loading dataset failed, invalid header identity error",
	"Failed to store dataset, system threw exception: %s",
	"Failed to load dataset, system threw exception: %s",
	"Failed to load dataset, invalid header signature error",
	"Failed to load dataset, invalid source error",
	"Failed to create dataset, no field definitions error",
	"Delete failed, dataset is empty error",
	"Delete failed, misaligned RecNo [%s]",
	"Unknown field datatype, table structure is corrupt or damaged error",
	"Invalid table structure, expected field signature error",
	"Failed to read dataset fields, invalid signature error",
	"Unknown datatype, binary extraction failed error",
	"Buffer is empty error",
	"Offset at BOF error",
	"Offset at EOF error",
	"Comment not closed error",
	"Expected EOF error",
	"Invalid length error",
	"Failed to write value, property [%s] not found error",
	"Failed to read value, property [%s] not found error",
	"Failed to locate object, property [%s] not found error",
	"Read failed, invalid offset [%d], expected %d..%d",
	"Write operation failed, target stream is nil error",
	"Read operation failed, source stream is nil error",
	"'Invalid handle for object (%s), reference rejected error",
	"Failed to convert typed-array: expected %d bytes, read %d. Insufficient data error",
	"Failed to process data, reference value was nil or unassigned error",
	"0123456789",
	"0123456789ABCDEF"];
function Trim$_String_(s) { return s.replace(/^\s\s*/, "").replace(/\s\s*$/, "") }
var TObject={
	$ClassName: "TObject",
	$Parent: null,
	ClassName: function (s) { return s.$ClassName },
	ClassType: function (s) { return s },
	ClassParent: function (s) { return s.$Parent },
	$Init: function (s) {},
	Create: function (s) { return s },
	Destroy: function (s) { for (var prop in s) if (s.hasOwnProperty(prop)) delete s[prop] },
	Destroy$: function(s) { return s.ClassType.Destroy(s) },
	Free: function (s) { if (s!==null) s.ClassType.Destroy(s) }
}
function StrEndsWith(s,e) { return s.substr(s.length-e.length)==e }
function StrBeginsWith(s,b) { return s.substr(0, b.length)==b }
function SetLength(s,n) { if (s.v.length>n) s.v=s.v.substring(0,n);else while (s.v.length<n) s.v+=" "; }
function SameText(a,b) { return a.toUpperCase()==b.toUpperCase() }
function RightStr(s,n) { return s.substr(s.length-n) }
function Now() { var d=new Date(); var utcnow=d.getTime()/864e5+25569; var dt = new Date(); var n = dt.getTimezoneOffset(); return utcnow-n/1440 }
function IntToHex2(v) { var r=v.toString(16); return (r.length==1)?"0"+r:r }
/**
sprintf() for JavaScript 0.7-beta1
http://www.diveintojavascript.com/projects/javascript-sprintf

Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of sprintf() for JavaScript nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

var sprintf = (function() {
	function get_type(variable) {
		return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
	}
	function str_repeat(input, multiplier) {
		for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */}
		return output.join('');
	}

	var str_format = function() {
		if (!str_format.cache.hasOwnProperty(arguments[0])) {
			str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
		}
		return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
	};

	str_format.format = function(parse_tree, argv) {
		var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
		for (i = 0; i < tree_length; i++) {
			node_type = get_type(parse_tree[i]);
			if (node_type === 'string') {
				output.push(parse_tree[i]);
			}
			else if (node_type === 'array') {
				match = parse_tree[i]; // convenience purposes only
				if (match[2]) { // keyword argument
					arg = argv[cursor];
					for (k = 0; k < match[2].length; k++) {
						if (!arg.hasOwnProperty(match[2][k])) {
							throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
						}
						arg = arg[match[2][k]];
					}
				}
				else if (match[1]) { // positional argument (explicit)
					arg = argv[match[1]];
				}
				else { // positional argument (implicit)
					arg = argv[cursor++];
				}

				if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
					throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
				}
				switch (match[8]) {
					case 'b': arg = arg.toString(2); break;
					case 'c': arg = String.fromCharCode(arg); break;
					case 'd': arg = String(parseInt(arg, 10)); if (match[7]) { arg = str_repeat('0', match[7]-arg.length)+arg } break;
					case 'e': arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential(); break;
					case 'f': arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg); break;
					case 'o': arg = arg.toString(8); break;
					case 's': arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg); break;
					case 'u': arg = Math.abs(arg); break;
					case 'x': arg = arg.toString(16); break;
					case 'X': arg = arg.toString(16).toUpperCase(); break;
				}
				arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+'+ arg : arg);
				pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
				pad_length = match[6] - String(arg).length;
				pad = match[6] ? str_repeat(pad_character, pad_length) : '';
				output.push(match[5] ? arg + pad : pad + arg);
			}
		}
		return output.join('');
	};

	str_format.cache = {};

	str_format.parse = function(fmt) {
		var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
		while (_fmt) {
			if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
				parse_tree.push(match[0]);
			}
			else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
				parse_tree.push('%');
			}
			else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
				if (match[2]) {
					arg_names |= 1;
					var field_list = [], replacement_field = match[2], field_match = [];
					if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
						field_list.push(field_match[1]);
						while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
							if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else {
								throw('[sprintf] huh?');
							}
						}
					}
					else {
						throw('[sprintf] huh?');
					}
					match[2] = field_list;
				}
				else {
					arg_names |= 2;
				}
				if (arg_names === 3) {
					throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
				}
				parse_tree.push(match);
			}
			else {
				throw('[sprintf] huh?');
			}
			_fmt = _fmt.substring(match[0].length);
		}
		return parse_tree;
	};

	return str_format;
})();
function Format(f,a) { a.unshift(f); return sprintf.apply(null,a) }
function FloatToStr$_Float_(i) { return i.toString() }
function FloatToStr$_Float_Integer_(i,p) { return (p==99)?i.toString():i.toFixed(p) }
var Exception={
	$ClassName: "Exception",
	$Parent: TObject,
	$Init: function (s) { FMessage="" },
	Create: function (s,Msg) { s.FMessage=Msg; return s }
}
function Delete(s,i,n) { var v=s.v; if ((i<=0)||(i>v.length)||(n<=0)) return; s.v=v.substr(0,i-1)+v.substr(i+n-1); }
function ClampInt(v,mi,ma) { return v<mi ? mi : v>ma ? ma : v }
function $W(e) { return e.ClassType?e:Exception.Create($New(Exception),e.constructor.name+", "+e.message) }
// inspired from 
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/String/charCodeAt
function $uniCharAt(str, idx) {
    var c = str.charCodeAt(idx);
    if (0xD800 <= c && c <= 0xDBFF) { // High surrogate
        return str.substr(idx, 2);
    }
    if (0xDC00 <= c && c <= 0xDFFF) { // Low surrogate
        return null;
    }
    return str.charAt(idx);
}function $ToClassType(o) {
	if (o===null) return o;
	return o.ClassType
}
;
function $SetInc(s,v,m,n) { v-=m; if (v>=0 && v<n) s[v>>5]|=1<<(v&31) }
function $SetIn(s,v,m,n) { v-=m; return (v<0 && v>=n)?false:(s[v>>5]&(1<<(v&31)))!=0 }
function $SetExc(s,v,m,n) { v-=m; if (v>=0 && v<n) s[v>>5]&=~(1<<(v&31)) }
Array.prototype.pusha = function (e) { this.push.apply(this, e); return this }
function $NewDyn(c,z) {
	if (c==null) throw Exception.Create($New(Exception),"ClassType is nil"+z);
	var i={ClassType:c};
	c.$Init(i);
	return i
}
function $New(c) { var i={ClassType:c}; c.$Init(i); return i }
function $Is(o,c) {
	if (o===null) return false;
	return $Inh(o.ClassType,c);
}
;
function $Inh(s,c) {
	if (s===null) return false;
	while ((s)&&(s!==c)) s=s.$Parent;
	return (s)?true:false;
}
;
function $Extend(base, sub, props) {
	function F() {};
	F.prototype = base.prototype;
	sub.prototype = new F();
	sub.prototype.constructor = sub;
	for (var n in props) {
		if (props.hasOwnProperty(n)) {
			sub.prototype[n]=props[n];
		}
	}
}
function $Event3(i,f) {
	var li=i,lf=f;
	return function(a,b,c) {
		return lf.call(li,li,a,b,c)
	}
}
function $Event2(i,f) {
	var li=i,lf=f;
	return function(a,b) {
		return lf.call(li,li,a,b)
	}
}
function $Event1(i,f) {
	var li=i,lf=f;
	return function(a) {
		return lf.call(li,li,a)
	}
}
function $Event0(i,f) {
	var li=i,lf=f;
	return function() {
		return lf.call(li,li)
	}
}
function $Div(a,b) { var r=a/b; return (r>=0)?Math.floor(r):Math.ceil(r) }
function $AsIntf(o,i) {
	if (o===null) return null;
	var r = o.ClassType.$Intf[i].map(function (e) {
		return function () {
			var arg=Array.prototype.slice.call(arguments);
			arg.splice(0,0,o);
			return e.apply(o, arg);
		}
	});
	r.O = o;
	return r;
}
;
function $As(o,c) {
	if ((o===null)||$Is(o,c)) return o;
	throw Exception.Create($New(Exception),"Cannot cast instance of type \""+o.ClassType.$ClassName+"\" to class \""+c.$ClassName+"\"");
}
function $ArraySetLenC(a,n,d) {
	var o=a.length;
	if (o==n) return;
	if (o>n) a.length=n; else for (;o<n;o++) a.push(d());
}
/// TNodeProgram = class (TObject)
///  [line: 23, column: 3, file: Unit1]
var TNodeProgram = {
   $ClassName:"TNodeProgram",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FService = null;
   }
   /// procedure TNodeProgram.Execute()
   ///  [line: 50, column: 24, file: Unit1]
   ,Execute:function(Self) {
      try {
         TRagnarokService.Start(Self.FService,0,function (Error$1) {
            if (Error$1!==null) {
               WriteLn("Service failed to start");
               WriteLnF("Error: %s",[Error$1.FMessage]);
               throw Error$1;
            }
            WriteLnF("Service started, listening on port: %d",[Self.FService.FPort]);
         });
      } catch ($e) {
         var e = $W($e);
         WriteLn(e.FMessage)      }
   }
   /// constructor TNodeProgram.Create()
   ///  [line: 35, column: 26, file: Unit1]
   ,Create$3:function(Self) {
      TObject.Create(Self);
      Self.FService = TRagnarokService.Create$69($New(TSessionService));
      TRagnarokService.SetPort(Self.FService,1882);
      return Self
   }
   /// destructor TNodeProgram.Destroy()
   ///  [line: 42, column: 25, file: Unit1]
   ,Destroy:function(Self) {
      if (TRagnarokService.GetActive$1(Self.FService)) {
         TRagnarokService.Stop(Self.FService,null);
      }
      TObject.Free(Self.FService);
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// function TW3VariantHelper.DataType(const Self: Variant) : TW3VariantDataType
///  [line: 1944, column: 27, file: System.Types]
function TW3VariantHelper$DataType$1(Self$1) {
   var Result = 1;
   var LType = "";
   if (TW3VariantHelper$Valid$2(Self$1)) {
      LType = typeof(Self$1);
      {var $temp1 = (LType).toLocaleLowerCase();
         if ($temp1=="object") {
            if (!Self$1.length) {
               Result = 8;
            } else {
               Result = 9;
            }
         }
          else if ($temp1=="function") {
            Result = 7;
         }
          else if ($temp1=="symbol") {
            Result = 6;
         }
          else if ($temp1=="boolean") {
            Result = 2;
         }
          else if ($temp1=="string") {
            Result = 5;
         }
          else if ($temp1=="number") {
            if (Math.round(Number(Self$1))!=Self$1) {
               Result = 4;
            } else {
               Result = 3;
            }
         }
          else if ($temp1=="array") {
            Result = 9;
         }
          else {
            Result = 1;
         }
      }
   } else if (Self$1==null) {
      Result = 10;
   } else {
      Result = 1;
   }
   return Result
}
/// function TW3VariantHelper.IsObject(const Self: Variant) : Boolean
///  [line: 1991, column: 27, file: System.Types]
function TW3VariantHelper$IsObject(Self$2) {
   var Result = false;
   Result = ((Self$2) !== undefined)
      && (Self$2 !== null)
      && (typeof Self$2  === "object")
      && ((Self$2).length === undefined);
   return Result
}
/// function TW3VariantHelper.IsUInt8Array(const Self: Variant) : Boolean
///  [line: 1930, column: 27, file: System.Types]
function TW3VariantHelper$IsUInt8Array(Self$3) {
   var Result = false;
   var LTypeName = "";
   Result = false;
   if (TW3VariantHelper$Valid$2(Self$3)) {
      LTypeName = Object.prototype.toString.call(Self$3);
      Result = LTypeName=="[object Uint8Array]";
   }
   return Result
}
/// function TW3VariantHelper.Valid(const Self: Variant) : Boolean
///  [line: 1902, column: 27, file: System.Types]
function TW3VariantHelper$Valid$2(Self$4) {
   var Result = false;
   Result = !( (Self$4 == undefined) || (Self$4 == null) );
   return Result
}
/// TW3VariantDataType enumeration
///  [line: 574, column: 3, file: System.Types]
var TW3VariantDataType = { 1:"vdUnknown", 2:"vdBoolean", 3:"vdinteger", 4:"vdfloat", 5:"vdstring", 6:"vdSymbol", 7:"vdFunction", 8:"vdObject", 9:"vdArray", 10:"vdVariant" };
/// TW3OwnedObject = class (TObject)
///  [line: 373, column: 3, file: System.Types]
var TW3OwnedObject = {
   $ClassName:"TW3OwnedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FOwner = null;
   }
   /// function TW3OwnedObject.GetOwner() : TObject
   ///  [line: 1306, column: 26, file: System.Types]
   ,GetOwner:function(Self) {
      return Self.FOwner;
   }
   /// procedure TW3OwnedObject.SetOwner(const NewOwner: TObject)
   ///  [line: 1316, column: 26, file: System.Types]
   ,SetOwner:function(Self, NewOwner) {
      if (NewOwner!==Self.FOwner) {
         if (TW3OwnedObject.AcceptOwner$(Self,NewOwner)) {
            Self.FOwner = NewOwner;
         } else {
            throw EW3Exception.CreateFmt($New(EW3OwnedObject),$R[0],["TW3OwnedObject.SetOwner", TObject.ClassName(Self.ClassType), $R[2]]);
         }
      }
   }
   /// function TW3OwnedObject.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 1311, column: 25, file: System.Types]
   ,AcceptOwner:function(Self, CandidateObject) {
      return true;
   }
   /// constructor TW3OwnedObject.Create(const AOwner: TObject)
   ///  [line: 1300, column: 28, file: System.Types]
   ,Create$16:function(Self, AOwner) {
      TObject.Create(Self);
      TW3OwnedObject.SetOwner(Self,AOwner);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16$:function($){return $.ClassType.Create$16.apply($.ClassType, arguments)}
};
TW3OwnedObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3OwnedLockedObject = class (TW3OwnedObject)
///  [line: 387, column: 3, file: System.Types]
var TW3OwnedLockedObject = {
   $ClassName:"TW3OwnedLockedObject",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked = 0;
   }
   /// procedure TW3OwnedLockedObject.DisableAlteration()
   ///  [line: 1262, column: 32, file: System.Types]
   ,DisableAlteration:function(Self) {
      ++Self.FLocked;
      if (Self.FLocked==1) {
         TW3OwnedLockedObject.ObjectLocked$(Self);
      }
   }
   /// procedure TW3OwnedLockedObject.EnableAlteration()
   ///  [line: 1269, column: 32, file: System.Types]
   ,EnableAlteration:function(Self) {
      if (Self.FLocked>0) {
         --Self.FLocked;
         if (!Self.FLocked) {
            TW3OwnedLockedObject.ObjectUnLocked$(Self);
         }
      }
   }
   /// function TW3OwnedLockedObject.GetLockState() : Boolean
   ///  [line: 1279, column: 31, file: System.Types]
   ,GetLockState:function(Self) {
      return Self.FLocked>0;
   }
   /// procedure TW3OwnedLockedObject.ObjectLocked()
   ///  [line: 1284, column: 32, file: System.Types]
   ,ObjectLocked:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3OwnedLockedObject.ObjectUnLocked()
   ///  [line: 1290, column: 32, file: System.Types]
   ,ObjectUnLocked:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked$:function($){return $.ClassType.ObjectLocked($)}
   ,ObjectUnLocked$:function($){return $.ClassType.ObjectUnLocked($)}
};
TW3OwnedLockedObject.$Intf={
   IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3LockedObject = class (TObject)
///  [line: 405, column: 3, file: System.Types]
var TW3LockedObject = {
   $ClassName:"TW3LockedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$1 = 0;
   }
   /// procedure TW3LockedObject.DisableAlteration()
   ///  [line: 1223, column: 27, file: System.Types]
   ,DisableAlteration$1:function(Self) {
      ++Self.FLocked$1;
      if (Self.FLocked$1==1) {
         TW3LockedObject.ObjectLocked$1$(Self);
      }
   }
   /// procedure TW3LockedObject.EnableAlteration()
   ///  [line: 1230, column: 27, file: System.Types]
   ,EnableAlteration$1:function(Self) {
      if (Self.FLocked$1>0) {
         --Self.FLocked$1;
         if (!Self.FLocked$1) {
            TW3LockedObject.ObjectUnLocked$1$(Self);
         }
      }
   }
   /// function TW3LockedObject.GetLockState() : Boolean
   ///  [line: 1240, column: 26, file: System.Types]
   ,GetLockState$1:function(Self) {
      return Self.FLocked$1>0;
   }
   /// procedure TW3LockedObject.ObjectLocked()
   ///  [line: 1245, column: 27, file: System.Types]
   ,ObjectLocked$1:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3LockedObject.ObjectUnLocked()
   ///  [line: 1251, column: 27, file: System.Types]
   ,ObjectUnLocked$1:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TObject.Destroy
   ,ObjectLocked$1$:function($){return $.ClassType.ObjectLocked$1($)}
   ,ObjectUnLocked$1$:function($){return $.ClassType.ObjectUnLocked$1($)}
};
TW3LockedObject.$Intf={
   IW3LockObject:[TW3LockedObject.DisableAlteration$1,TW3LockedObject.EnableAlteration$1,TW3LockedObject.GetLockState$1]
}
/// TW3Identifiers = class (TObject)
///  [line: 294, column: 3, file: System.Types]
var TW3Identifiers = {
   $ClassName:"TW3Identifiers",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TW3Identifiers.GenerateUniqueObjectId() : String
   ///  [line: 1830, column: 31, file: System.Types]
   ,GenerateUniqueObjectId:function(Self) {
      var Result = "";
      ++__UNIQUE;
      Result = "OBJ"+__UNIQUE.toString();
      return Result
   }
   /// function TW3Identifiers.GenerateUniqueNumber() : Integer
   ///  [line: 1818, column: 31, file: System.Types]
   ,GenerateUniqueNumber:function(Self) {
      var Result = 0;
      ++__UNIQUE;
      Result = __UNIQUE;
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TW3FilePermissionMask enumeration
///  [line: 80, column: 3, file: System.Types]
var TW3FilePermissionMask = { 0:"fpNone", 111:"fpExecute", 222:"fpWrite", 333:"fpWriteExecute", 444:"fpRead", 555:"fpReadExecute", 666:"fpDefault", 777:"fpReadWriteExecute", 740:"fpRWEGroupReadOnly" };
/// TVariant = class (TObject)
///  [line: 525, column: 3, file: System.Types]
var TVariant = {
   $ClassName:"TVariant",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TVariant.AsBool(const aValue: Variant) : Boolean
   ///  [line: 2350, column: 25, file: System.Types]
   ,AsBool:function(aValue) {
      var Result = false;
      if (aValue!=undefined&&aValue!=null) {
         Result = (aValue?true:false);
      }
      return Result
   }
   /// function TVariant.AsFloat(const aValue: Variant) : Float
   ///  [line: 2333, column: 25, file: System.Types]
   ,AsFloat:function(aValue$1) {
      var Result = 0;
      if (aValue$1!=undefined&&aValue$1!=null) {
         Result = Number(aValue$1);
      }
      return Result
   }
   /// function TVariant.AsInteger(const aValue: Variant) : Integer
   ///  [line: 2319, column: 25, file: System.Types]
   ,AsInteger:function(aValue$2) {
      var Result = 0;
      if (aValue$2!=undefined&&aValue$2!=null) {
         Result = parseInt(aValue$2,10);
      }
      return Result
   }
   /// function TVariant.AsObject(const aValue: Variant) : TObject
   ///  [line: 2340, column: 25, file: System.Types]
   ,AsObject:function(aValue$3) {
      var Result = null;
      if (aValue$3!=undefined&&aValue$3!=null) {
         Result = aValue$3;
      }
      return Result
   }
   /// function TVariant.AsString(const aValue: Variant) : String
   ///  [line: 2326, column: 25, file: System.Types]
   ,AsString:function(aValue$4) {
      var Result = "";
      if (aValue$4!=undefined&&aValue$4!=null) {
         Result = String(aValue$4);
      }
      return Result
   }
   /// function TVariant.CreateObject() : Variant
   ///  [line: 2371, column: 25, file: System.Types]
   ,CreateObject:function() {
      var Result = undefined;
      Result = new Object();
      return Result
   }
   /// procedure TVariant.ForEachProperty(const Data: Variant; const CallBack: TW3ObjectKeyCallback)
   ///  [line: 2454, column: 26, file: System.Types]
   ,ForEachProperty:function(Data$3, CallBack) {
      var LObj,
         Keys$1 = [],
         a$452 = 0;
      var LName = "";
      if (CallBack) {
         Keys$1 = TVariant.Properties(Data$3);
         var $temp2;
         for(a$452=0,$temp2=Keys$1.length;a$452<$temp2;a$452++) {
            LName = Keys$1[a$452];
            LObj = Keys$1[LName];
            if ((~CallBack(LName,LObj))==1) {
               break;
            }
         }
      }
   }
   /// function TVariant.GetKeys(const aValue: Variant) : TStrArray
   ///  [line: 2424, column: 25, file: System.Types]
   ,GetKeys:function(aValue$5) {
      var Result = [];
      if (Object.keys) {
      Result = Object.keys(aValue$5);
    } else {
      Result = [];
    }
      return Result
   }
   /// function TVariant.Properties(const Data: Variant) : TStrArray
   ///  [line: 2473, column: 25, file: System.Types]
   ,Properties:function(Data$4) {
      var Result = [];
      if (Data$4) {
         if (!(Object.keys === undefined)) {
        Result = Object.keys(Data$4);
        return Result;
      }
         if (!(Object.getOwnPropertyNames === undefined)) {
          Result = Object.getOwnPropertyNames(Data$4);
          return Result;
      }
         for (var qtxenum in Data$4) {
        if ( (Data$4).hasOwnProperty(qtxenum) == true )
          (Result).push(qtxenum);
      }
      return Result;
      }
      return Result
   }
   /// function TVariant.ValidRef(const aValue: Variant) : Boolean
   ///  [line: 2314, column: 25, file: System.Types]
   ,ValidRef:function(aValue$6) {
      return aValue$6!=undefined&&aValue$6!=null;
   }
   ,Destroy:TObject.Destroy
};
/// TTextFormation enumeration
///  [line: 216, column: 3, file: System.Types]
var TTextFormation = { 256:"tfHex", 257:"tfOrdinal", 258:"tfFloat", 259:"tfQuote" };
/// function TStringHelper.ContainsHex(const Self: String) : Boolean
///  [line: 1769, column: 24, file: System.Types]
function TStringHelper$ContainsHex(Self$5) {
   var Result = false;
   var x$1 = 0;
   var LStart = 0;
   var LItem = "";
   var LLen = 0;
   Result = false;
   LLen = Self$5.length;
   if (LLen>=1) {
      LStart = 1;
      if (Self$5.charAt(0)=="$") {
         ++LStart;
         --LLen;
      } else {
         LItem = (Self$5.substr(0,1)).toLocaleUpperCase();
         Result = ($R[73].indexOf(LItem)+1)>0;
         if (!Result) {
            return Result;
         }
      }
      if (LLen>=1) {
         var $temp3;
         for(x$1=LStart,$temp3=Self$5.length;x$1<=$temp3;x$1++) {
            LItem = (Self$5.charAt(x$1-1)).toLocaleUpperCase();
            Result = ($R[73].indexOf(LItem)+1)>0;
            if (!Result) {
               break;
            }
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsOrdinal(const Self: String) : Boolean
///  [line: 1753, column: 24, file: System.Types]
function TStringHelper$ContainsOrdinal(Self$6) {
   var Result = false;
   var LLen$1 = 0,
      x$2 = 0;
   var LItem$1 = "";
   Result = false;
   LLen$1 = Self$6.length;
   if (LLen$1>=1) {
      var $temp4;
      for(x$2=1,$temp4=LLen$1;x$2<=$temp4;x$2++) {
         LItem$1 = Self$6.charAt(x$2-1);
         Result = ($R[72].indexOf(LItem$1)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsFloat(const Self: String) : Boolean
///  [line: 1698, column: 24, file: System.Types]
function TStringHelper$ContainsFloat(Self$7) {
   var Result = false;
   var x$3 = 0;
   var LItem$2 = "";
   var LLen$2 = 0;
   var LLine = false;
   Result = false;
   LLen$2 = Self$7.length;
   if (LLen$2>=1) {
      LLine = false;
      var $temp5;
      for(x$3=1,$temp5=LLen$2;x$3<=$temp5;x$3++) {
         LItem$2 = Self$7.charAt(x$3-1);
         if (LItem$2==".") {
            if (x$3==1&&LLen$2==1) {
               break;
            }
            if (x$3==1&&LLen$2>1) {
               LLine = true;
               continue;
            }
            if (x$3>1&&x$3<LLen$2) {
               if (LLine) {
                  break;
               } else {
                  LLine = true;
                  continue;
               }
            } else {
               break;
            }
         }
         Result = ("0123456789".indexOf(LItem$2)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsQuote(const Self: String) : Boolean
///  [line: 1617, column: 24, file: System.Types]
function TStringHelper$ContainsQuote(Self$8) {
   var Result = false;
   var LLen$3 = 0;
   var LStart$1 = 0;
   var LFound = false;
   var LQuote = ["",""];
   Result = false;
   LLen$3 = Self$8.length;
   if (LLen$3>=2) {
      LStart$1 = 1;
      while (LStart$1<=LLen$3) {
         if (Self$8.charAt(LStart$1-1)==" ") {
            ++LStart$1;
            continue;
         } else {
            break;
         }
      }
      LQuote[false?1:0] = "'";
      LQuote[true?1:0] = "\"";
      if (Self$8.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$8.charAt(LStart$1-1)!=LQuote[false?1:0]) {
         return Result;
      }
      if (LStart$1>=LLen$3) {
         return Result;
      }
      ++LStart$1;
      LFound = false;
      while (LStart$1<=LLen$3) {
         if (Self$8.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$8.charAt(LStart$1-1)!=LQuote[false?1:0]) {
            LFound = true;
         }
         ++LStart$1;
      }
      if (!LFound) {
         return Result;
      }
      if (LStart$1==LLen$3) {
         Result = true;
         return Result;
      }
      while (LStart$1<=LLen$3) {
         if (Self$8.charAt(LStart$1-1)!=" ") {
            LFound = false;
            break;
         } else {
            ++LStart$1;
         }
      }
      Result = LFound;
   }
   return Result
}
/// TString = class (TObject)
///  [line: 235, column: 3, file: System.Types.Convert]
var TString = {
   $ClassName:"TString",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TString.BinaryStrToInt(BinStr: String) : Integer
   ///  [line: 1578, column: 24, file: System.Types.Convert]
   ,BinaryStrToInt:function(BinStr) {
      var Result = {v:0};
      try {
         if (!TString.ExamineBinary(BinStr,Result)) {
            throw EW3Exception.CreateFmt($New(EConvertBinaryStringInvalid),"Failed to convert binary string (%s)",[BinStr]);
         }
      } finally {return Result.v}
   }
   /// function TString.CharCodeFor(const Character: Char) : Integer
   ///  [line: 1369, column: 24, file: System.Types]
   ,CharCodeFor:function(Self, Character) {
      var Result = 0;
      Result = (Character).charCodeAt(0);
      return Result
   }
   /// function TString.CreateGUID() : String
   ///  [line: 1954, column: 24, file: System.Types.Convert]
   ,CreateGUID:function(Self) {
      var Result = "";
      var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";

    Result = s.join("");
      Result = (Result).toUpperCase();
      return Result
   }
   /// function TString.DecodeBase64(TextToDecode: String) : String
   ///  [line: 1977, column: 24, file: System.Types.Convert]
   ,DecodeBase64:function(Self, TextToDecode) {
      return TBase64EncDec.Base64ToString(TBase64EncDec,TextToDecode);
   }
   /// function TString.DecodeURI(const Text: String) : String
   ///  [line: 1520, column: 24, file: System.Types]
   ,DecodeURI:function(Self, Text$1) {
      var Result = "";
      Result = (Text$1).replace(new RegExp('\\+','g'),' ');
    Result = unescape(Result);
      return Result
   }
   /// function TString.DecodeUTF8(const BytesToDecode: TByteArray) : String
   ///  [line: 1428, column: 24, file: System.Types]
   ,DecodeUTF8:function(Self, BytesToDecode) {
      var Result = "";
      var LCodec = null;
      LCodec = TCustomCodec.Create$28($New(TUTF8Codec));
      try {
         Result = TUTF8Codec.Decode(LCodec,BytesToDecode);
      } finally {
         TObject.Free(LCodec);
      }
      return Result
   }
   /// function TString.EncodeBase64(TextToEncode: String) : String
   ///  [line: 1972, column: 24, file: System.Types.Convert]
   ,EncodeBase64:function(Self, TextToEncode) {
      return TBase64EncDec.StringToBase64(TBase64EncDec,TextToEncode);
   }
   /// function TString.EncodeURI(const Text: String) : String
   ///  [line: 1504, column: 24, file: System.Types]
   ,EncodeURI:function(Self, Text$2) {
      var Result = "";
      Result = escape(Text$2);
    Result = (Result).replace(new RegExp('\\+','g'),'%2B');
    Result = (Result).replace(new RegExp('%20','g'),'+');
      return Result
   }
   /// function TString.EncodeUTF8(TextToEncode: String) : TByteArray
   ///  [line: 1418, column: 24, file: System.Types]
   ,EncodeUTF8:function(Self, TextToEncode$1) {
      var Result = {v:[]};
      try {
         var LCodec$1 = null;
         LCodec$1 = TCustomCodec.Create$28($New(TUTF8Codec));
         try {
            Result.v = TUTF8Codec.Encode(LCodec$1,TextToEncode$1);
         } finally {
            TObject.Free(LCodec$1);
         }
      } finally {return Result.v}
   }
   /// function TString.ExamineBinary(Text: String; var value: Longword) : Boolean
   ///  [line: 1717, column: 24, file: System.Types.Convert]
   ,ExamineBinary:function(Text$3, value) {
      var Result = false;
      var BitIndex = 0,
         x$4 = 0;
      value.v = TDataTypeConverter.InitUint32(0);
      if ((Text$3.charAt(0)=="%")) {
         Text$3 = (Text$3).substring(1);
      } else if ((Text$3.substr(0,2)=="0b")) {
         Text$3 = (Text$3).substring(2);
      }
      if (!TString.ValidBinChars(Text$3)) {
         Result = false;
         return Result;
      }
      BitIndex = 0;
      for(x$4=Text$3.length;x$4>=1;x$4--) {
         if (Text$3.charAt(x$4-1)=="1") {
            TInteger.SetBit(BitIndex,true,value);
         }
         ++BitIndex;
         if (BitIndex>31) {
            break;
         }
      }
      Result = true;
      return Result
   }
   /// function TString.ExamineBoolean(Text: String; var Value: Boolean) : Boolean
   ///  [line: 1906, column: 24, file: System.Types.Convert]
   ,ExamineBoolean:function(Text$4, Value$9) {
      var Result = false;
      Text$4 = Trim$_String_(Text$4);
      {var $temp6 = (Text$4).toLocaleLowerCase();
         if ($temp6=="true") {
            Result = true;
            Value$9.v = true;
         }
          else if ($temp6=="yes") {
            Result = true;
            Value$9.v = true;
         }
          else if ($temp6=="false") {
            Result = true;
            Value$9.v = false;
         }
          else if ($temp6=="no") {
            Result = true;
            Value$9.v = false;
         }
      }
      return Result
   }
   /// function TString.ExamineFloat(Text: String; var Value: Float) : Boolean
   ///  [line: 1754, column: 24, file: System.Types.Convert]
   ,ExamineFloat:function(Text$5, Value$10) {
      var Result = false;
      var TextLen = 0,
         scan = false,
         offset = 0,
         character = "";
      Text$5 = Trim$_String_(Text$5);
      TextLen = Text$5.length;
      if (TextLen>=1) {
         scan = false;
         offset = 0;
         for (var $temp7=0;$temp7<Text$5.length;$temp7++) {
            character=$uniCharAt(Text$5,$temp7);
            if (!character) continue;
            ++offset;
            if (character==".") {
               if (offset==1&&TextLen==1) {
                  break;
               }
               if (offset==1&&TextLen>1) {
                  scan = true;
                  continue;
               }
               if (offset>1&&offset<TextLen) {
                  if (scan) {
                     break;
                  } else {
                     scan = true;
                     continue;
                  }
               } else {
                  break;
               }
            }
            Result = ((character>="0")&&(character<="9"));
            if (!Result) {
               break;
            }
         }
         if (Result) {
            Value$10.v = parseFloat(Text$5);
         }
      }
      return Result
   }
   /// function TString.ExamineInteger(Text: String; var Value: Integer) : Boolean
   ///  [line: 1839, column: 24, file: System.Types.Convert]
   ,ExamineInteger:function(Text$6, Value$11) {
      var Result = false;
      var TextLen$1 = 0,
         Prefix = 0;
      Text$6 = Trim$_String_(Text$6);
      TextLen$1 = Text$6.length;
      if (TextLen$1>0) {
         Prefix = TString.ExaminePrefixType(Text$6);
         if (Prefix) {
            switch (Prefix) {
               case 1 :
                  --TextLen$1;
                  Text$6 = RightStr(Text$6,TextLen$1);
                  Result = TString.ValidHexChars(Text$6);
                  if (Result) {
                     Value$11.v = parseInt("0x"+Text$6,16);
                  }
                  break;
               case 2 :
                  (TextLen$1-= 2);
                  Text$6 = RightStr(Text$6,TextLen$1);
                  Result = TString.ValidHexChars(Text$6);
                  if (Result) {
                     Value$11.v = parseInt("0x"+Text$6,16);
                  }
                  break;
               case 3 :
                  --TextLen$1;
                  Text$6 = RightStr(Text$6,TextLen$1);
                  Result = TString.ValidBinChars(Text$6);
                  if (Result) {
                     Value$11.v = TString.BinaryStrToInt(Text$6);
                  }
                  break;
               case 4 :
                  (TextLen$1-= 2);
                  Text$6 = RightStr(Text$6,TextLen$1);
                  Result = TString.ValidBinChars(Text$6);
                  if (Result) {
                     Value$11.v = TString.BinaryStrToInt(Text$6);
                  }
                  break;
               case 5 :
                  return Result;
                  break;
               default :
                  Result = TString.ValidDecChars(Text$6);
                  if (Result) {
                     Value$11.v = parseInt(Text$6,10);
                  }
            }
         } else {
            Result = TString.ValidDecChars(Text$6);
            if (Result) {
               Value$11.v = parseInt(Text$6,10);
            }
         }
      }
      return Result
   }
   /// function TString.ExaminePrefixType(const Text: String) : TValuePrefixType
   ///  [line: 1590, column: 24, file: System.Types.Convert]
   ,ExaminePrefixType:function(Text$7) {
      var Result = 0;
      Result = 0;
      if (Text$7.length>0) {
         if ((Text$7.charAt(0)=="$")) {
            Result = 1;
         } else if ((Text$7.substr(0,2)=="0x")) {
            Result = 2;
         } else if ((Text$7.charAt(0)=="%")) {
            Result = 3;
         } else if ((Text$7.substr(0,2)=="0b")) {
            Result = 4;
         } else if ((Text$7.charAt(0)=="\"")) {
            Result = 5;
         }
      }
      return Result
   }
   /// function TString.FromCharCode(const CharCode: Byte) : Char
   ///  [line: 1392, column: 24, file: System.Types]
   ,FromCharCode:function(Self, CharCode) {
      var Result = "";
      Result = String.fromCharCode(CharCode);
      return Result
   }
   /// function TString.GetStringIsQuoted(const Text: String) : Boolean
   ///  [line: 1924, column: 24, file: System.Types.Convert]
   ,GetStringIsQuoted:function(Text$8) {
      var Result = false;
      var TextLen$2 = 0;
      TextLen$2 = Text$8.length;
      if (TextLen$2>0) {
         Result = (Text$8.charAt(0)=="\"")&&StrEndsWith(Text$8,"\"");
      }
      return Result
   }
   /// function TString.UnQuoteString(const Text: String) : String
   ///  [line: 1939, column: 24, file: System.Types.Convert]
   ,UnQuoteString:function(Text$9) {
      var Result = "";
      var TextLen$3 = 0;
      TextLen$3 = Text$9.length;
      if (TextLen$3>0) {
         if ((Text$9.charAt(0)=="\"")&&StrEndsWith(Text$9,"\"")) {
            (TextLen$3-= 2);
            Result = Text$9.substr(1,TextLen$3);
         } else {
            Result = Text$9;
         }
      }
      return Result
   }
   /// function TString.ValidBinChars(const Text: String) : Boolean
   ///  [line: 1829, column: 24, file: System.Types.Convert]
   ,ValidBinChars:function(Text$10) {
      var Result = false;
      var character$1 = "";
      for (var $temp8=0;$temp8<Text$10.length;$temp8++) {
         character$1=$uniCharAt(Text$10,$temp8);
         if (!character$1) continue;
         Result = ((character$1=="0")||(character$1=="1"));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   /// function TString.ValidDecChars(const Text: String) : Boolean
   ///  [line: 1819, column: 24, file: System.Types.Convert]
   ,ValidDecChars:function(Text$11) {
      var Result = false;
      var character$2 = "";
      for (var $temp9=0;$temp9<Text$11.length;$temp9++) {
         character$2=$uniCharAt(Text$11,$temp9);
         if (!character$2) continue;
         Result = ((character$2>="0")&&(character$2<="9"));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   /// function TString.ValidHexChars(const Text: String) : Boolean
   ///  [line: 1809, column: 24, file: System.Types.Convert]
   ,ValidHexChars:function(Text$12) {
      var Result = false;
      var character$3 = "";
      for (var $temp10=0;$temp10<Text$12.length;$temp10++) {
         character$3=$uniCharAt(Text$12,$temp10);
         if (!character$3) continue;
         Result = (((character$3>="0")&&(character$3<="9"))||((character$3>="a")&&(character$3<="f"))||((character$3>="A")&&(character$3<="F")));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TInteger = class (TObject)
///  [line: 474, column: 3, file: System.Types]
var TInteger = {
   $ClassName:"TInteger",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TInteger.Diff(const Primary: Integer; const Secondary: Integer) : Integer
   ///  [line: 2203, column: 25, file: System.Types]
   ,Diff:function(Primary, Secondary) {
      var Result = 0;
      if (Primary!=Secondary) {
         if (Primary>Secondary) {
            Result = Primary-Secondary;
         } else {
            Result = Secondary-Primary;
         }
         if (Result<0) {
            Result = (Result-1)^(-1);
         }
      } else {
         Result = 0;
      }
      return Result
   }
   /// function TInteger.EnsureRange(const aValue: Integer; const aMin: Integer; const aMax: Integer) : Integer
   ///  [line: 2157, column: 25, file: System.Types]
   ,EnsureRange:function(aValue$7, aMin, aMax) {
      return ClampInt(aValue$7,aMin,aMax);
   }
   /// procedure TInteger.SetBit(index: Integer; aValue: Boolean; var buffer: Integer)
   ///  [line: 2072, column: 26, file: System.Types]
   ,SetBit:function(index, aValue$8, buffer$1) {
      if (index>=0&&index<=31) {
         if (aValue$8) {
            buffer$1.v = buffer$1.v|(1<<index);
         } else {
            buffer$1.v = buffer$1.v&(~(1<<index));
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid bit index, expected 0..31");
      }
   }
   /// function TInteger.SubtractSmallest(const First: Integer; const Second: Integer) : Integer
   ///  [line: 2129, column: 25, file: System.Types]
   ,SubtractSmallest:function(First$2, Second) {
      var Result = 0;
      if (First$2<Second) {
         Result = Second-First$2;
      } else {
         Result = First$2-Second;
      }
      return Result
   }
   /// function TInteger.ToNearest(const Value: Integer; const Factor: Integer) : Integer
   ///  [line: 2188, column: 25, file: System.Types]
   ,ToNearest:function(Value$12, Factor) {
      var Result = 0;
      var FTemp = 0;
      Result = Value$12;
      FTemp = Value$12%Factor;
      if (FTemp>0) {
         (Result+= (Factor-FTemp));
      }
      return Result
   }
   /// function TInteger.WrapRange(const aValue: Integer; const aLowRange: Integer; const aHighRange: Integer) : Integer
   ///  [line: 2171, column: 25, file: System.Types]
   ,WrapRange:function(aValue$9, aLowRange, aHighRange) {
      var Result = 0;
      if (aValue$9>aHighRange) {
         Result = aLowRange+TInteger.Diff(aHighRange,(aValue$9-1));
         if (Result>aHighRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else if (aValue$9<aLowRange) {
         Result = aHighRange-TInteger.Diff(aLowRange,(aValue$9+1));
         if (Result<aLowRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else {
         Result = aValue$9;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TFileAccessMode enumeration
///  [line: 135, column: 3, file: System.Types]
var TFileAccessMode = [ "fmOpenRead", "fmOpenWrite", "fmOpenReadWrite" ];
/// TEnumState enumeration
///  [line: 129, column: 3, file: System.Types]
var TEnumState = { 1:"esContinue", 0:"esAbort" };
/// TEnumResult enumeration
///  [line: 110, column: 3, file: System.Types]
var TEnumResult = { 160:"erContinue", 16:"erBreak" };
/// TDataTypeMap = record
///  [line: 515, column: 3, file: System.Types]
function Copy$TDataTypeMap(s,d) {
   d.Boolean=s.Boolean;
   d.Number$1=s.Number$1;
   d.String$1=s.String$1;
   d.Object$2=s.Object$2;
   d.Undefined=s.Undefined;
   d.Function$1=s.Function$1;
   return d;
}
function Clone$TDataTypeMap($) {
   return {
      Boolean:$.Boolean,
      Number$1:$.Number$1,
      String$1:$.String$1,
      Object$2:$.Object$2,
      Undefined:$.Undefined,
      Function$1:$.Function$1
   }
}
function GetIsRunningInBrowser() {
   var Result = false;
   Result = (!(typeof window === 'undefined'));
   return Result
};
/// EW3Exception = class (Exception)
///  [line: 245, column: 3, file: System.Types]
var EW3Exception = {
   $ClassName:"EW3Exception",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   /// constructor EW3Exception.CreateFmt(aText: String; const aValues: array of const)
   ///  [line: 1884, column: 26, file: System.Types]
   ,CreateFmt:function(Self, aText, aValues) {
      Exception.Create(Self,Format(aText,aValues.slice(0)));
      return Self
   }
   /// constructor EW3Exception.Create(const MethodName: String; const Instance: TObject; const ErrorText: String)
   ///  [line: 1889, column: 26, file: System.Types]
   ,Create$27:function(Self, MethodName, Instance$5, ErrorText) {
      var LCallerName = "";
      LCallerName = (Instance$5)?TObject.ClassName(Instance$5.ClassType):"Anonymous";
      EW3Exception.CreateFmt(Self,$R[0],[MethodName, LCallerName, ErrorText]);
      return Self
   }
   ,Destroy:Exception.Destroy
};
/// EW3OwnedObject = class (EW3Exception)
///  [line: 364, column: 3, file: System.Types]
var EW3OwnedObject = {
   $ClassName:"EW3OwnedObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EW3LockError = class (EW3Exception)
///  [line: 356, column: 3, file: System.Types]
var EW3LockError = {
   $ClassName:"EW3LockError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SetupTypeLUT() {
   __TYPE_MAP.Boolean = typeof(true);
   __TYPE_MAP.Number$1 = typeof(0);
   __TYPE_MAP.String$1 = typeof("");
   __TYPE_MAP.Object$2 = typeof(TVariant.CreateObject());
   __TYPE_MAP.Undefined = typeof(undefined);
   __TYPE_MAP.Function$1 = typeof(function () {
      /* null */
   });
};
/// TValuePrefixType enumeration
///  [line: 52, column: 3, file: System.Types.Convert]
var TValuePrefixType = [ "vpNone", "vpHexPascal", "vpHexC", "vpBinPascal", "vpBinC", "vpString" ];
/// TSystemEndianType enumeration
///  [line: 66, column: 3, file: System.Types.Convert]
var TSystemEndianType = [ "stDefault", "stLittleEndian", "stBigEndian" ];
function TryStrToInt(Data$5, Value$13) {
   return TString.ExamineInteger(Data$5,Value$13);
};
function TryStrToFloat(Data$6, Value$14) {
   return TString.ExamineFloat(Data$6,Value$14);
};
function TryStrToBool(Data$7, Value$15) {
   return TString.ExamineBoolean(Data$7,Value$15);
};
/// TRTLDatatype enumeration
///  [line: 37, column: 3, file: System.Types.Convert]
var TRTLDatatype = [ "itUnknown", "itBoolean", "itByte", "itChar", "itWord", "itLong", "itInt16", "itInt32", "itFloat32", "itFloat64", "itString" ];
/// TDataTypeConverter = class (TObject)
///  [line: 73, column: 3, file: System.Types.Convert]
var TDataTypeConverter = {
   $ClassName:"TDataTypeConverter",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnEndianChanged = null;
      $.FBuffer = $.FView = null;
      $.FEndian = 0;
      $.FTyped = null;
   }
   /// function TDataTypeConverter.Base64ToBytes(const Base64: String) : TByteArray
   ///  [line: 1240, column: 35, file: System.Types.Convert]
   ,Base64ToBytes:function(Self, Base64) {
      return TBase64EncDec.Base64ToBytes$2(TBase64EncDec,Base64);
   }
   /// function TDataTypeConverter.BooleanToBytes(const Value: Boolean) : TByteArray
   ///  [line: 1041, column: 35, file: System.Types.Convert]
   ,BooleanToBytes:function(Self, Value$16) {
      var Result = [];
      Result.push((Value$16)?1:0);
      return Result
   }
   /// function TDataTypeConverter.BytesToBase64(const Bytes: TByteArray) : String
   ///  [line: 1269, column: 35, file: System.Types.Convert]
   ,BytesToBase64:function(Self, Bytes$1) {
      return TBase64EncDec.BytesToBase64$2(TBase64EncDec,Bytes$1);
   }
   /// function TDataTypeConverter.BytesToBoolean(const Data: TByteArray) : Boolean
   ///  [line: 1274, column: 29, file: System.Types.Convert]
   ,BytesToBoolean:function(Self, Data$8) {
      return Data$8[0]>0;
   }
   /// function TDataTypeConverter.BytesToFloat32(const Data: TByteArray) : Float
   ///  [line: 1219, column: 29, file: System.Types.Convert]
   ,BytesToFloat32:function(Self, Data$9) {
      var Result = 0;
      Self.FView.setUint8(0,Data$9[0]);
      Self.FView.setUint8(1,Data$9[1]);
      Self.FView.setUint8(2,Data$9[2]);
      Self.FView.setUint8(3,Data$9[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat32(0);
            break;
         case 1 :
            Result = Self.FView.getFloat32(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToFloat64(const Data: TByteArray) : Float
   ///  [line: 990, column: 29, file: System.Types.Convert]
   ,BytesToFloat64:function(Self, Data$10) {
      var Result = 0;
      Self.FView.setUint8(0,Data$10[0]);
      Self.FView.setUint8(1,Data$10[1]);
      Self.FView.setUint8(2,Data$10[2]);
      Self.FView.setUint8(3,Data$10[3]);
      Self.FView.setUint8(4,Data$10[4]);
      Self.FView.setUint8(5,Data$10[5]);
      Self.FView.setUint8(6,Data$10[6]);
      Self.FView.setUint8(7,Data$10[7]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat64(0);
            break;
         case 1 :
            Result = Self.FView.getFloat64(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat64(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt16(const Data: TByteArray) : SmallInt
   ///  [line: 1008, column: 29, file: System.Types.Convert]
   ,BytesToInt16:function(Self, Data$11) {
      var Result = 0;
      Self.FView.setUint8(0,Data$11[0]);
      Self.FView.setUint8(1,Data$11[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt16(0);
            break;
         case 1 :
            Result = Self.FView.getInt16(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt32(const Data: TByteArray) : Integer
   ///  [line: 946, column: 29, file: System.Types.Convert]
   ,BytesToInt32:function(Self, Data$12) {
      var Result = 0;
      Self.FView.setUint8(0,Data$12[0]);
      Self.FView.setUint8(1,Data$12[1]);
      Self.FView.setUint8(2,Data$12[2]);
      Self.FView.setUint8(3,Data$12[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt32(0);
            break;
         case 1 :
            Result = Self.FView.getInt32(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToString(const Data: TByteArray) : String
   ///  [line: 1307, column: 29, file: System.Types.Convert]
   ,BytesToString:function(Self, Data$13) {
      var Result = "";
      Result = String.fromCharCode.apply(String, Data$13);
      return Result
   }
   /// function TDataTypeConverter.BytesToTypedArray(const Values: TByteArray) : TMemoryHandle
   ///  [line: 1020, column: 29, file: System.Types.Convert]
   ,BytesToTypedArray:function(Self, Values$9) {
      var Result = undefined;
      var LLen$4 = 0;
      LLen$4 = Values$9.length;
      Result = new Uint8Array(LLen$4);
      (Result).set(Values$9, 0);
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt16(const Data: TByteArray) : Word
   ///  [line: 1090, column: 29, file: System.Types.Convert]
   ,BytesToUInt16:function(Self, Data$14) {
      var Result = 0;
      Self.FView.setUint8(0,Data$14[0]);
      Self.FView.setUint8(1,Data$14[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint16(0);
            break;
         case 1 :
            Result = Self.FView.getUint16(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt32(const Data: TByteArray) : Longword
   ///  [line: 892, column: 29, file: System.Types.Convert]
   ,BytesToUInt32:function(Self, Data$15) {
      var Result = 0;
      Self.FView.setUint8(0,Data$15[0]);
      Self.FView.setUint8(1,Data$15[1]);
      Self.FView.setUint8(2,Data$15[2]);
      Self.FView.setUint8(3,Data$15[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint32(0);
            break;
         case 1 :
            Result = Self.FView.getUint32(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToVariant(Data: TByteArray) : Variant
   ///  [line: 1115, column: 29, file: System.Types.Convert]
   ,BytesToVariant:function(Self, Data$16) {
      var Result = undefined;
      var LType$1 = 0;
      LType$1 = Data$16[0];
      Data$16.shift();
      switch (LType$1) {
         case 161 :
            Result = TDataTypeConverter.BytesToBoolean(Self,Data$16);
            break;
         case 162 :
            Result = Data$16[0];
            break;
         case 168 :
            Result = TDataTypeConverter.BytesToUInt16(Self,Data$16);
            break;
         case 169 :
            Result = TDataTypeConverter.BytesToUInt32(Self,Data$16);
            break;
         case 163 :
            Result = TDataTypeConverter.BytesToInt16(Self,Data$16);
            break;
         case 164 :
            Result = TDataTypeConverter.BytesToInt32(Self,Data$16);
            break;
         case 165 :
            Result = TDataTypeConverter.BytesToFloat32(Self,Data$16);
            break;
         case 166 :
            Result = TDataTypeConverter.BytesToFloat64(Self,Data$16);
            break;
         case 167 :
            Result = TString.DecodeUTF8(TString,Data$16);
            break;
         default :
            throw EW3Exception.CreateFmt($New(EDatatype),"Failed to convert bytes[] to intrinsic type, unknown identifier [%s] error",[IntToHex2(LType$1)]);
      }
      return Result
   }
   /// function TDataTypeConverter.ByteToChar(const Value: Byte) : Char
   ///  [line: 1314, column: 35, file: System.Types.Convert]
   ,ByteToChar:function(Self, Value$17) {
      var Result = "";
      Result = String.fromCharCode(Value$17);
      return Result
   }
   /// function TDataTypeConverter.CharToByte(const Value: Char) : Word
   ///  [line: 1064, column: 35, file: System.Types.Convert]
   ,CharToByte:function(Self, Value$18) {
      var Result = 0;
      Result = (Value$18).charCodeAt(0);
      return Result
   }
   /// constructor TDataTypeConverter.Create()
   ///  [line: 630, column: 32, file: System.Types.Convert]
   ,Create$15:function(Self) {
      TObject.Create(Self);
      Self.FBuffer = new ArrayBuffer(16);
    Self.FView   = new DataView(Self.FBuffer);
      Self.FTyped = new Uint8Array(Self.FBuffer,0,15);
      return Self
   }
   /// destructor TDataTypeConverter.Destroy()
   ///  [line: 640, column: 31, file: System.Types.Convert]
   ,Destroy:function(Self) {
      Self.FTyped = null;
      Self.FView = null;
      Self.FBuffer = null;
      TObject.Destroy(Self);
   }
   /// function TDataTypeConverter.Float32ToBytes(const Value: float32) : TByteArray
   ///  [line: 1102, column: 29, file: System.Types.Convert]
   ,Float32ToBytes:function(Self, Value$19) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat32(0,Value$19);
            break;
         case 1 :
            Self.FView.setFloat32(0,Value$19,true);
            break;
         case 2 :
            Self.FView.setFloat32(0,Value$19,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.Float64ToBytes(const Value: float64) : TByteArray
   ///  [line: 918, column: 29, file: System.Types.Convert]
   ,Float64ToBytes:function(Self, Value$20) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat64(0,Number(Value$20));
            break;
         case 1 :
            Self.FView.setFloat64(0,Number(Value$20),true);
            break;
         case 2 :
            Self.FView.setFloat64(0,Number(Value$20),false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 8 );
      return Result
   }
   /// function TDataTypeConverter.InitFloat32(const Value: float32) : float32
   ///  [line: 714, column: 35, file: System.Types.Convert]
   ,InitFloat32:function(Value$21) {
      var Result = 0;
      var temp = null;
      temp = new Float32Array(1);
      temp[0]=Value$21;
      Result = temp[0];
      temp = null;
      return Result
   }
   /// function TDataTypeConverter.InitFloat64(const Value: float64) : float64
   ///  [line: 722, column: 35, file: System.Types.Convert]
   ,InitFloat64:function(Value$22) {
      var Result = undefined;
      var temp$1 = null;
      temp$1 = new Float64Array(1);
      temp$1[0]=(Number(Value$22));
      Result = temp$1[0];
      temp$1 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt08(const Value: Byte) : Byte
   ///  [line: 746, column: 35, file: System.Types.Convert]
   ,InitInt08:function(Value$23) {
      var Result = 0;
      var temp$2 = null;
      temp$2 = new Int8Array(1);
      temp$2[0]=((Value$23<-128)?-128:(Value$23>127)?127:Value$23);
      Result = temp$2[0];
      temp$2 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt16(const Value: SmallInt) : SmallInt
   ///  [line: 738, column: 35, file: System.Types.Convert]
   ,InitInt16:function(Value$24) {
      var Result = 0;
      var temp$3 = null;
      temp$3 = new Int16Array(1);
      temp$3[0]=((Value$24<-32768)?-32768:(Value$24>32767)?32767:Value$24);
      Result = temp$3[0];
      temp$3 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt32(const Value: Integer) : Integer
   ///  [line: 730, column: 35, file: System.Types.Convert]
   ,InitInt32:function(Value$25) {
      var Result = 0;
      var temp$4 = null;
      temp$4 = new Int32Array(1);
      temp$4[0]=((Value$25<-2147483648)?-2147483648:(Value$25>2147483647)?2147483647:Value$25);
      Result = temp$4[0];
      temp$4 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint08(const Value: Byte) : Byte
   ///  [line: 770, column: 35, file: System.Types.Convert]
   ,InitUint08:function(Value$26) {
      var Result = 0;
      var LTemp = null;
      LTemp = new Uint8Array(1);
      LTemp[0]=((Value$26<0)?0:(Value$26>255)?255:Value$26);
      Result = LTemp[0];
      LTemp = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint16(const Value: Word) : Word
   ///  [line: 762, column: 35, file: System.Types.Convert]
   ,InitUint16:function(Value$27) {
      var Result = 0;
      var temp$5 = null;
      temp$5 = new Uint16Array(1);
      temp$5[0]=((Value$27<0)?0:(Value$27>65536)?65536:Value$27);
      Result = temp$5[0];
      temp$5 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint32(const Value: Longword) : Longword
   ///  [line: 754, column: 35, file: System.Types.Convert]
   ,InitUint32:function(Value$28) {
      var Result = 0;
      var temp$6 = null;
      temp$6 = new Uint32Array(1);
      temp$6[0]=((Value$28<0)?0:(Value$28>4294967295)?4294967295:Value$28);
      Result = temp$6[0];
      temp$6 = null;
      return Result
   }
   /// function TDataTypeConverter.Int16ToBytes(const Value: SmallInt) : TByteArray
   ///  [line: 1245, column: 29, file: System.Types.Convert]
   ,Int16ToBytes:function(Self, Value$29) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt16(0,Value$29);
            break;
         case 1 :
            Self.FView.setInt16(0,Value$29,true);
            break;
         case 2 :
            Self.FView.setInt16(0,Value$29,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.Int32ToBytes(const Value: Integer) : TByteArray
   ///  [line: 867, column: 29, file: System.Types.Convert]
   ,Int32ToBytes:function(Self, Value$30) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt32(0,Value$30);
            break;
         case 1 :
            Self.FView.setInt32(0,Value$30,true);
            break;
         case 2 :
            Self.FView.setInt32(0,Value$30,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// procedure TDataTypeConverter.SetEndian(const NewEndian: TSystemEndianType)
   ///  [line: 648, column: 30, file: System.Types.Convert]
   ,SetEndian:function(Self, NewEndian) {
      if (NewEndian!=Self.FEndian) {
         Self.FEndian = NewEndian;
         if (Self.OnEndianChanged) {
            Self.OnEndianChanged(Self);
         }
      }
   }
   /// function TDataTypeConverter.SizeOfType(const Kind: TRTLDatatype) : Integer
   ///  [line: 684, column: 35, file: System.Types.Convert]
   ,SizeOfType:function(Self, Kind) {
      return __SIZES[Kind];
   }
   /// function TDataTypeConverter.StringToBytes(const Value: String) : TByteArray
   ///  [line: 1298, column: 29, file: System.Types.Convert]
   ,StringToBytes:function(Self, Value$31) {
      var Result = [];
      Result = (Value$31).split("").map( function( val ) {
        return val.charCodeAt( 0 );
    } );
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToBytes(const Value: TMemoryHandle) : TByteArray
   ///  [line: 906, column: 29, file: System.Types.Convert]
   ,TypedArrayToBytes:function(Self, Value$32) {
      var Result = [];
      if (!TVariant.ValidRef(Value$32)) {
         throw EW3Exception.Create$27($New(EConvertError),"TDataTypeConverter.TypedArrayToBytes",Self,$R[71]);
      }
      Result = Array.prototype.slice.call(Value$32);
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToStr(const Value: TMemoryHandle) : String
   ///  [line: 930, column: 29, file: System.Types.Convert]
   ,TypedArrayToStr:function(Self, Value$33) {
      var Result = "";
      var x$5 = 0;
      if (TVariant.ValidRef(Value$33)) {
         if (Value$33.length>0) {
            var $temp11;
            for(x$5=0,$temp11=Value$33.length-1;x$5<=$temp11;x$5++) {
               Result += String.fromCharCode((Value$33)[x$5]);
            }
         }
      }
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToUInt32(const Value: TMemoryHandle) : Longword
   ///  [line: 831, column: 29, file: System.Types.Convert]
   ,TypedArrayToUInt32:function(Self, Value$34) {
      var Result = 0;
      var LBuffer = null,
         LBytes = 0,
         LTypeSize = 0,
         LView = null;
      if (!TVariant.ValidRef(Value$34)) {
         throw EW3Exception.Create$27($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,$R[71]);
      }
      LBuffer = Value$34.buffer;
      LBytes = LBuffer.byteLength;
      LTypeSize = TDataTypeConverter.SizeOfType(Self.ClassType,7);
      if (LBytes<LTypeSize) {
         throw EW3Exception.Create$27($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,Format($R[70],["Int32", LTypeSize, LBytes]));
      }
      if (LBytes>LTypeSize) {
         LBytes = LTypeSize;
      }
      LView = new DataView(LBuffer);
      switch (Self.FEndian) {
         case 0 :
            Result = LView.getUint32(0);
            break;
         case 1 :
            Result = LView.getUint32(0,true);
            break;
         case 2 :
            Result = LView.getUint32(0,false);
            break;
      }
      LView = null;
      return Result
   }
   /// function TDataTypeConverter.UInt16ToBytes(const Value: Word) : TByteArray
   ///  [line: 1257, column: 29, file: System.Types.Convert]
   ,UInt16ToBytes:function(Self, Value$35) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint16(0,Value$35);
            break;
         case 1 :
            Self.FView.setUint16(0,Value$35,true);
            break;
         case 2 :
            Self.FView.setUint16(0,Value$35,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.UInt32ToBytes(const Value: Longword) : TByteArray
   ///  [line: 880, column: 29, file: System.Types.Convert]
   ,UInt32ToBytes:function(Self, Value$36) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint32(0,Value$36);
            break;
         case 1 :
            Self.FView.setUint32(0,Value$36,true);
            break;
         case 2 :
            Self.FView.setUint32(0,Value$36,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.VariantToBytes(Value: Variant) : TByteArray
   ///  [line: 1138, column: 29, file: System.Types.Convert]
   ,VariantToBytes:function(Self, Value$37) {
      var Result = [];
      var LType$2 = 0;
      function GetUnSignedIntType() {
         var Result = 0;
         if (Value$37<=255) {
            return 162;
         }
         if (Value$37<=65536) {
            return 168;
         }
         if (Value$37<=2147483647) {
            Result = 169;
         }
         return Result
      };
      function GetSignedIntType() {
         var Result = 0;
         if (Value$37>-32768) {
            Result = 163;
            return Result;
         }
         if (Value$37>-2147483648) {
            Result = 164;
         }
         return Result
      };
      function IsFloat32(x$6) {
         var Result = false;
         Result = isFinite(x$6) && x$6 == Math.fround(x$6);
         return Result
      };
      switch (TW3VariantHelper$DataType$1(Value$37)) {
         case 2 :
            Result = [161].slice();
            Result.pusha(TDataTypeConverter.BooleanToBytes(Self.ClassType,(Value$37?true:false)));
            break;
         case 3 :
            if (Value$37<0) {
               LType$2 = GetSignedIntType();
            } else {
               LType$2 = GetUnSignedIntType();
            }
            if (LType$2) {
               Result = [LType$2].slice();
               switch (LType$2) {
                  case 162 :
                     Result.push(TDataTypeConverter.InitInt08(parseInt(Value$37,10)));
                     break;
                  case 168 :
                     Result.pusha(TDataTypeConverter.UInt16ToBytes(Self,TDataTypeConverter.InitUint16(parseInt(Value$37,10))));
                     break;
                  case 169 :
                     Result.pusha(TDataTypeConverter.UInt32ToBytes(Self,TDataTypeConverter.InitUint32(parseInt(Value$37,10))));
                     break;
                  case 163 :
                     Result.pusha(TDataTypeConverter.Int16ToBytes(Self,TDataTypeConverter.InitInt16(parseInt(Value$37,10))));
                     break;
                  case 164 :
                     Result.pusha(TDataTypeConverter.Int32ToBytes(Self,TDataTypeConverter.InitInt32(parseInt(Value$37,10))));
                     break;
               }
            } else {
               throw Exception.Create($New(EDatatype),"Invalid datatype, failed to identify number [integer] type error");
            }
            break;
         case 4 :
            if (IsFloat32(Value$37)) {
               Result = [165].slice();
               Result.pusha(TDataTypeConverter.Float32ToBytes(Self,(Number(Value$37))));
            } else {
               Result = [166].slice();
               Result.pusha(TDataTypeConverter.Float64ToBytes(Self,(Number(Value$37))));
            }
            break;
         case 5 :
            Result = [167].slice();
            Result.pusha(TString.EncodeUTF8(TString,String(Value$37)));
            break;
         default :
            throw Exception.Create($New(EDatatype),"Invalid datatype, byte conversion failed error");
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
};
/// TDatatype = class (TObject)
///  [line: 158, column: 3, file: System.Types.Convert]
var TDatatype = {
   $ClassName:"TDatatype",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TDatatype.Base64ToBytes(const Base64: String) : TByteArray
   ///  [line: 1437, column: 26, file: System.Types.Convert]
   ,Base64ToBytes$1:function(Self, Base64$1) {
      return TDataTypeConverter.Base64ToBytes(__Def_Converter.ClassType,Base64$1);
   }
   /// function TDatatype.BytesToBase64(const Bytes: TByteArray) : String
   ///  [line: 1432, column: 26, file: System.Types.Convert]
   ,BytesToBase64$1:function(Self, Bytes$2) {
      return TDataTypeConverter.BytesToBase64(__Def_Converter.ClassType,Bytes$2);
   }
   /// function TDatatype.BytesToString(const Data: TByteArray) : String
   ///  [line: 1452, column: 26, file: System.Types.Convert]
   ,BytesToString$1:function(Self, Data$17) {
      return TDataTypeConverter.BytesToString(__Def_Converter,Data$17);
   }
   /// function TDatatype.BytesToTypedArray(const Values: TByteArray) : TMemoryHandle
   ///  [line: 1442, column: 26, file: System.Types.Convert]
   ,BytesToTypedArray$1:function(Self, Values$10) {
      return TDataTypeConverter.BytesToTypedArray(__Def_Converter,Values$10);
   }
   /// function TDatatype.BytesToVariant(const Data: TByteArray) : Variant
   ///  [line: 1493, column: 26, file: System.Types.Convert]
   ,BytesToVariant$1:function(Self, Data$18) {
      return TDataTypeConverter.BytesToVariant(__Def_Converter,Data$18);
   }
   /// function TDatatype.StringToBytes(const Value: String) : TByteArray
   ///  [line: 1447, column: 26, file: System.Types.Convert]
   ,StringToBytes$1:function(Self, Value$38) {
      return TDataTypeConverter.StringToBytes(__Def_Converter,Value$38);
   }
   /// function TDatatype.TypedArrayToBytes(const Value: TDefaultBufferType) : TByteArray
   ///  [line: 1340, column: 26, file: System.Types.Convert]
   ,TypedArrayToBytes$1:function(Self, Value$39) {
      return TDataTypeConverter.TypedArrayToBytes(__Def_Converter,Value$39);
   }
   /// function TDatatype.TypedArrayToUInt32(const Value: TDefaultBufferType) : Longword
   ///  [line: 1345, column: 26, file: System.Types.Convert]
   ,TypedArrayToUInt32$1:function(Self, Value$40) {
      return TDataTypeConverter.TypedArrayToUInt32(__Def_Converter,Value$40);
   }
   /// function TDatatype.VariantToBytes(const Value: Variant) : TByteArray
   ///  [line: 1498, column: 26, file: System.Types.Convert]
   ,VariantToBytes$1:function(Self, Value$41) {
      return TDataTypeConverter.VariantToBytes(__Def_Converter,Value$41);
   }
   ,Destroy:TObject.Destroy
};
/// TBase64EncDec = class (TObject)
///  [line: 291, column: 3, file: System.Types.Convert]
var TBase64EncDec = {
   $ClassName:"TBase64EncDec",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TBase64EncDec.Base64ToBytes(const b64: String) : TByteArray
   ///  [line: 461, column: 30, file: System.Types.Convert]
   ,Base64ToBytes$2:function(Self, b64) {
      var Result = [];
      var ASeg = 0;
      var BSeg = 0;
      var CSeg = 0;
      var DSeg = 0;
      var LTextLen = 0,
         LPlaceholderCount = 0,
         BufferSize = 0,
         xpos = 0,
         idx = 0,
         temp$7 = 0,
         temp$8 = 0,
         temp$9 = 0;
      LTextLen = b64.length;
      if (LTextLen>0) {
         LPlaceholderCount = 0;
         if (LTextLen%4<1) {
            LPlaceholderCount = (b64.charAt((LTextLen-1)-1)=="=")?2:(b64.charAt(LTextLen-1)=="=")?1:0;
         }
         BufferSize = ($Div(LTextLen*3,4))-LPlaceholderCount;
         $ArraySetLenC(Result,BufferSize,function (){return 0});
         if (LPlaceholderCount>0) {
            (LTextLen-= 4);
         }
         xpos = 1;
         idx = 0;
         while (xpos<LTextLen) {
            ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos-1))]<<18;
            BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos))]<<12;
            CSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos+1))]<<6;
            DSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos+2))];
            temp$7 = ((ASeg|BSeg)|CSeg)|DSeg;
            Result[idx]=(temp$7>>>16)&255;
            ++idx;
            Result[idx]=(temp$7>>>8)&255;
            ++idx;
            Result[idx]=temp$7&255;
            ++idx;
            (xpos+= 4);
         }
         switch (LPlaceholderCount) {
            case 1 :
               ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos-1))]<<2;
               BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos))]>>>4;
               temp$8 = ASeg|BSeg;
               Result[idx]=temp$8&255;
               break;
            case 2 :
               ASeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos-1))]<<10;
               BSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos))]<<4;
               CSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos+1))]>>>2;
               temp$9 = (ASeg|BSeg)|CSeg;
               Result[idx]=(temp$9>>>8)&255;
               ++idx;
               Result[idx]=temp$9&255;
               break;
         }
      }
      return Result
   }
   /// function TBase64EncDec.Base64ToString(const b64: String) : String
   ///  [line: 409, column: 30, file: System.Types.Convert]
   ,Base64ToString:function(Self, b64$1) {
      var Result = "";
      Result = atob(b64$1);
      return Result
   }
   /// function TBase64EncDec.BytesToBase64(const Data: TByteArray) : String
   ///  [line: 514, column: 30, file: System.Types.Convert]
   ,BytesToBase64$2:function(Self, Data$19) {
      var Result = "";
      var LLen$5 = 0,
         LExtra = 0,
         LStrideLen = 0,
         LMaxChunkLength = 0,
         i = 0,
         Ahead = 0,
         SegSize = 0,
         output = "",
         LTemp$1 = 0,
         LTemp$2 = 0;
      LLen$5 = Data$19.length;
      if (LLen$5>0) {
         LExtra = Data$19.length%3;
         LStrideLen = LLen$5-LExtra;
         LMaxChunkLength = 16383;
         i = 0;
         while (i<LStrideLen) {
            Ahead = i+LMaxChunkLength;
            SegSize = (Ahead>LStrideLen)?LStrideLen:Ahead;
            Result+=TBase64EncDec.EncodeChunk(Self,Data$19,i,SegSize);
            (i+= LMaxChunkLength);
         }
         if (LExtra>0) {
            --LLen$5;
         }
         output = "";
         switch (LExtra) {
            case 1 :
               LTemp$1 = Data$19[LLen$5];
               output+=__B64_Lookup[LTemp$1>>>2];
               output+=__B64_Lookup[(LTemp$1<<4)&63];
               output+="==";
               break;
            case 2 :
               LTemp$2 = (Data$19[LLen$5-1]<<8)+Data$19[LLen$5];
               output+=__B64_Lookup[LTemp$2>>>10];
               output+=__B64_Lookup[(LTemp$2>>>4)&63];
               output+=__B64_Lookup[(LTemp$2<<2)&63];
               output+="=";
               break;
         }
         Result+=output;
      }
      return Result
   }
   /// function TBase64EncDec.EncodeChunk(const Data: TByteArray; startpos: Integer; endpos: Integer) : String
   ///  [line: 424, column: 30, file: System.Types.Convert]
   ,EncodeChunk:function(Self, Data$20, startpos, endpos) {
      var Result = "";
      var temp$10 = 0;
      while (startpos<endpos) {
         temp$10 = (Data$20[startpos]<<16)+(Data$20[startpos+1]<<8)+Data$20[startpos+2];
         Result+=__B64_Lookup[(temp$10>>>18)&63]+__B64_Lookup[(temp$10>>>12)&63]+__B64_Lookup[(temp$10>>>6)&63]+__B64_Lookup[temp$10&63];
         (startpos+= 3);
      }
      return Result
   }
   /// function TBase64EncDec.StringToBase64(const Text: String) : String
   ///  [line: 402, column: 30, file: System.Types.Convert]
   ,StringToBase64:function(Self, Text$13) {
      var Result = "";
      Result = btoa(Text$13);
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// EDatatype = class (EW3Exception)
///  [line: 22, column: 3, file: System.Types.Convert]
var EDatatype = {
   $ClassName:"EDatatype",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EConvertError = class (EW3Exception)
///  [line: 23, column: 3, file: System.Types.Convert]
var EConvertError = {
   $ClassName:"EConvertError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EConvertBinaryStringInvalid = class (EConvertError)
///  [line: 26, column: 3, file: System.Types.Convert]
var EConvertBinaryStringInvalid = {
   $ClassName:"EConvertBinaryStringInvalid",$Parent:EConvertError
   ,$Init:function ($) {
      EConvertError.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SetupConversionLUT() {
   try {
      __CONV_BUFFER = new ArrayBuffer(16);
      __CONV_VIEW   = new DataView(__CONV_BUFFER);
      __CONV_ARRAY = new Uint8Array(__CONV_BUFFER,0,15);
   } catch ($e) {
      var e$1 = $W($e);
      /* null */
   }
};
function SetupBase64() {
   var i$1 = 0;
   var $temp12;
   for(i$1=1,$temp12=CNT_B64_CHARSET.length;i$1<=$temp12;i$1++) {
      __B64_Lookup[i$1-1] = CNT_B64_CHARSET.charAt(i$1-1);
      __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,CNT_B64_CHARSET.charAt(i$1-1))] = i$1-1;
   }
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"-")] = 62;
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"_")] = 63;
};
/// TUnManaged = class (TObject)
///  [line: 106, column: 3, file: System.Memory]
var TUnManaged = {
   $ClassName:"TUnManaged",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TUnManaged.AllocMemA(const Size: Integer) : TMemoryHandle
   ///  [line: 245, column: 27, file: System.Memory]
   ,AllocMemA:function(Self, Size$5) {
      var Result = undefined;
      if (Size$5>0) {
         Result = new Uint8Array(Size$5);
      } else {
         Result = null;
      }
      return Result
   }
   /// function TUnManaged.ReAllocMemA(const Memory: TMemoryHandle; Size: Integer) : TMemoryHandle
   ///  [line: 264, column: 27, file: System.Memory]
   ,ReAllocMemA:function(Self, Memory$1, Size$6) {
      var Result = undefined;
      if (Memory$1) {
         if (Size$6>0) {
            Result = new Uint8Array(Size$6);
            TMarshal.Move$1(TMarshal,Memory$1,0,Result,0,Size$6);
         }
      } else {
         Result = TUnManaged.AllocMemA(Self,Size$6);
      }
      return Result
   }
   /// function TUnManaged.ReadMemoryA(const Memory: TMemoryHandle; const Offset: Integer; Size: Integer) : TMemoryHandle
   ///  [line: 347, column: 27, file: System.Memory]
   ,ReadMemoryA:function(Self, Memory$2, Offset$1, Size$7) {
      var Result = undefined;
      var LTotal = 0;
      if (Memory$2&&Offset$1>=0) {
         LTotal = Offset$1+Size$7;
         if (LTotal>Memory$2.length) {
            Size$7 = parseInt((Memory$2.length-LTotal),10);
         }
         if (Size$7>0) {
            Result = new Uint8Array(Memory$2.buffer.slice(Offset$1,Size$7));
         }
      }
      return Result
   }
   /// function TUnManaged.WriteMemoryA(const Memory: TMemoryHandle; const Offset: Integer; const Data: TMemoryHandle) : Integer
   ///  [line: 313, column: 27, file: System.Memory]
   ,WriteMemoryA:function(Self, Memory$3, Offset$2, Data$21) {
      var Result = 0;
      var mTotal,
         mChunk = null,
         mTemp = null;
      if (Memory$3) {
         if (Data$21) {
            mTotal = Offset$2+Data$21.length;
            if (mTotal>Memory$3.length) {
               Result = parseInt((Memory$3.length-mTotal),10);
            } else {
               Result = parseInt(Data$21.length,10);
            }
            if (Result>0) {
               if (Offset$2+Data$21.length<=Memory$3.length) {
                  Memory$3.set(Data$21,Offset$2);
               } else {
                  mChunk = Data$21.buffer.slice(0,Result-1);
                  mTemp = new Uint8Array(mChunk);
                  Memory$3.set(mTemp,Offset$2);
               }
            }
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// function TMemoryHandleHelper.Valid(const Self: TMemoryHandle) : Boolean
///  [line: 212, column: 30, file: System.Memory]
function TMemoryHandleHelper$Valid$3(Self$9) {
   var Result = false;
   Result = !( (Self$9 == undefined) || (Self$9 == null) );
   return Result
}
/// function TMemoryHandleHelper.Defined(const Self: TMemoryHandle) : Boolean
///  [line: 219, column: 30, file: System.Memory]
function TMemoryHandleHelper$Defined(Self$10) {
   var Result = false;
   Result = !(Self$10 == undefined);
   return Result
}
/// TMarshal = class (TObject)
///  [line: 133, column: 3, file: System.Memory]
var TMarshal = {
   $ClassName:"TMarshal",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TMarshal.Move(const Source: TMemoryHandle; const SourceStart: Integer; const Target: TMemoryHandle; const TargetStart: Integer; const Size: Integer)
   ///  [line: 553, column: 26, file: System.Memory]
   ,Move$1:function(Self, Source$1, SourceStart, Target, TargetStart, Size$8) {
      var LRef = null;
      if (TMemoryHandleHelper$Defined(Source$1)&&TMemoryHandleHelper$Valid$3(Source$1)&&SourceStart>=0) {
         if (TMemoryHandleHelper$Defined(Target)&&TMemoryHandleHelper$Valid$3(Target)&&TargetStart>=0) {
            LRef = Source$1.subarray(SourceStart,SourceStart+Size$8);
            Target.set(LRef,TargetStart);
         }
      }
   }
   /// procedure TMarshal.Move(const Source: TAddress; const Target: TAddress; const Size: Integer)
   ///  [line: 570, column: 26, file: System.Memory]
   ,Move:function(Self, Source$2, Target$1, Size$9) {
      if (Source$2!==null) {
         if (Target$1!==null) {
            if (Size$9>0) {
               TMarshal.Move$1(Self,Source$2.FBuffer$2,Source$2.FOffset$1,Target$1.FBuffer$2,Target$1.FOffset$1,Size$9);
            }
         }
      }
   }
   ,Destroy:TObject.Destroy
};
/// function TBufferHandleHelper.Valid(const Self: TBufferHandle) : Boolean
///  [line: 180, column: 30, file: System.Memory]
function TBufferHandleHelper$Valid$4(Self$11) {
   var Result = false;
   Result = !( (Self$11 == undefined) || (Self$11 == null) );
   return Result
}
/// TAddress = class (TObject)
///  [line: 71, column: 3, file: System.Streams]
var TAddress = {
   $ClassName:"TAddress",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FBuffer$2 = undefined;
      $.FOffset$1 = 0;
   }
   /// constructor TAddress.Create(const Memory: TBinaryData)
   ///  [line: 259, column: 22, file: System.Memory.Buffer]
   ,Create$35:function(Self, Memory$4) {
      if (Memory$4!==null) {
         if (TAllocation.GetSize$3(Memory$4)>0) {
            TAddress.Create$33(Self,TAllocation.GetHandle(Memory$4),0);
         } else {
            throw Exception.Create($New(Exception),"Invalid memory object error");
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid memory object error");
      }
      return Self
   }
   /// constructor TAddress.Create(const Stream: TStream)
   ///  [line: 228, column: 22, file: System.Streams]
   ,Create$34:function(Self, Stream) {
      var LRef$1 = undefined;
      if ($Is(Stream,TMemoryStream)) {
         LRef$1 = TAllocation.GetHandle($As(Stream,TMemoryStream).FBuffer$1);
         if (LRef$1) {
            TAddress.Create$33(Self,LRef$1,0);
         } else {
            throw Exception.Create($New(EAddress),$R[3]);
         }
      } else {
         throw Exception.Create($New(EAddress),$R[4]);
      }
      return Self
   }
   /// constructor TAddress.Create(const Segment: TBufferHandle; const Offset: Integer)
   ///  [line: 621, column: 22, file: System.Memory]
   ,Create$33:function(Self, Segment$1, Offset$3) {
      TObject.Create(Self);
      if (Segment$1&&TBufferHandleHelper$Valid$4(Segment$1)) {
         Self.FBuffer$2 = Segment$1;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid segment error");
      }
      if (Offset$3>=0) {
         Self.FOffset$1 = Offset$3;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid offset error");
      }
      return Self
   }
   /// destructor TAddress.Destroy()
   ///  [line: 635, column: 21, file: System.Memory]
   ,Destroy:function(Self) {
      Self.FBuffer$2 = null;
      Self.FOffset$1 = 0;
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// EAddress = class (EW3Exception)
///  [line: 83, column: 3, file: System.Memory]
var EAddress = {
   $ClassName:"EAddress",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3OwnedErrorObject = class (TW3OwnedObject)
///  [line: 78, column: 3, file: System.Objects]
var TW3OwnedErrorObject = {
   $ClassName:"TW3OwnedErrorObject",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FLastError = "";
      $.FOptions$3 = null;
   }
   /// procedure TW3OwnedErrorObject.ClearLastError()
   ///  [line: 279, column: 31, file: System.Objects]
   ,ClearLastError:function(Self) {
      Self.FLastError = "";
   }
   /// constructor TW3OwnedErrorObject.Create(const AOwner: TObject)
   ///  [line: 227, column: 33, file: System.Objects]
   ,Create$16:function(Self, AOwner$1) {
      TW3OwnedObject.Create$16(Self,AOwner$1);
      Self.FOptions$3 = TW3ErrorObjectOptions.Create$44($New(TW3ErrorObjectOptions));
      return Self
   }
   /// destructor TW3OwnedErrorObject.Destroy()
   ///  [line: 233, column: 32, file: System.Objects]
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions$3);
      TObject.Destroy(Self);
   }
   /// function TW3OwnedErrorObject.GetFailed() : Boolean
   ///  [line: 244, column: 30, file: System.Objects]
   ,GetFailed:function(Self) {
      return Self.FLastError.length>0;
   }
   /// function TW3OwnedErrorObject.GetLastError() : String
   ///  [line: 239, column: 30, file: System.Objects]
   ,GetLastError:function(Self) {
      return Self.FLastError;
   }
   /// procedure TW3OwnedErrorObject.SetLastError(const ErrorText: String)
   ///  [line: 249, column: 31, file: System.Objects]
   ,SetLastError:function(Self, ErrorText$1) {
      Self.FLastError = Trim$_String_(ErrorText$1);
      if (Self.FLastError.length>0) {
         if (Self.FOptions$3.AutoWriteToConsole) {
            if (console) {
          console.log( (Self.FLastError) );
        }
         }
         if (Self.FOptions$3.ThrowExceptions) {
            throw Exception.Create($New(EW3ErrorObject),Self.FLastError);
         }
      }
   }
   /// procedure TW3OwnedErrorObject.SetLastErrorF(const ErrorText: String; const Values: array of const)
   ///  [line: 273, column: 31, file: System.Objects]
   ,SetLastErrorF:function(Self, ErrorText$2, Values$11) {
      Self.FLastError = Format(ErrorText$2,Values$11.slice(0));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16$:function($){return $.ClassType.Create$16.apply($.ClassType, arguments)}
};
TW3OwnedErrorObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3OwnedLockedErrorObject = class (TW3OwnedErrorObject)
///  [line: 96, column: 3, file: System.Objects]
var TW3OwnedLockedErrorObject = {
   $ClassName:"TW3OwnedLockedErrorObject",$Parent:TW3OwnedErrorObject
   ,$Init:function ($) {
      TW3OwnedErrorObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$2 = 0;
   }
   /// procedure TW3OwnedLockedErrorObject.DisableAlteration()
   ///  [line: 189, column: 37, file: System.Objects]
   ,DisableAlteration$2:function(Self) {
      ++Self.FLocked$2;
      if (Self.FLocked$2==1) {
         TW3OwnedLockedErrorObject.ObjectLocked$2(Self);
      }
   }
   /// procedure TW3OwnedLockedErrorObject.EnableAlteration()
   ///  [line: 196, column: 37, file: System.Objects]
   ,EnableAlteration$2:function(Self) {
      if (Self.FLocked$2>0) {
         --Self.FLocked$2;
         if (!Self.FLocked$2) {
            TW3OwnedLockedErrorObject.ObjectUnLocked$2(Self);
         }
      }
   }
   /// function TW3OwnedLockedErrorObject.GetLockState() : Boolean
   ///  [line: 206, column: 36, file: System.Objects]
   ,GetLockState$2:function(Self) {
      return Self.FLocked$2>0;
   }
   /// procedure TW3OwnedLockedErrorObject.ObjectLocked()
   ///  [line: 211, column: 37, file: System.Objects]
   ,ObjectLocked$2:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3OwnedLockedErrorObject.ObjectUnLocked()
   ///  [line: 217, column: 37, file: System.Objects]
   ,ObjectUnLocked$2:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TW3OwnedErrorObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
};
TW3OwnedLockedErrorObject.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3HandleBasedObject = class (TObject)
///  [line: 115, column: 3, file: System.Objects]
var TW3HandleBasedObject = {
   $ClassName:"TW3HandleBasedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FHandle$1 = undefined;
   }
   /// procedure TW3HandleBasedObject.ObjectHandleChanged(const PreviousHandle: THandle; const NewHandle: THandle)
   ///  [line: 160, column: 32, file: System.Objects]
   ,ObjectHandleChanged:function(Self, PreviousHandle, NewHandle) {
      /* null */
   }
   /// function TW3HandleBasedObject.AcceptObjectHandle(const CandidateHandle: THandle) : Boolean
   ///  [line: 154, column: 31, file: System.Objects]
   ,AcceptObjectHandle:function(Self, CandidateHandle) {
      return true;
   }
   /// procedure TW3HandleBasedObject.SetObjectHandle(const NewHandle: THandle)
   ///  [line: 173, column: 32, file: System.Objects]
   ,SetObjectHandle:function(Self, NewHandle$1) {
      var LTemp$3 = undefined;
      if (TW3HandleBasedObject.AcceptObjectHandle(Self,NewHandle$1)) {
         LTemp$3 = Self.FHandle$1;
         Self.FHandle$1 = NewHandle$1;
         TW3HandleBasedObject.ObjectHandleChanged(Self,LTemp$3,Self.FHandle$1);
      } else {
         throw EW3Exception.CreateFmt($New(EW3HandleBasedObject),$R[69],["TW3HandleBasedObject.SetObjectHandle"]);
      }
   }
   /// function TW3HandleBasedObject.GetObjectHandle() : THandle
   ///  [line: 168, column: 31, file: System.Objects]
   ,GetObjectHandle:function(Self) {
      return Self.FHandle$1;
   }
   ,Destroy:TObject.Destroy
};
/// TW3ErrorObjectOptions = class (TObject)
///  [line: 27, column: 3, file: System.Objects]
var TW3ErrorObjectOptions = {
   $ClassName:"TW3ErrorObjectOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.AutoWriteToConsole = $.ThrowExceptions = $.AutoResetError = false;
   }
   /// constructor TW3ErrorObjectOptions.Create()
   ///  [line: 139, column: 35, file: System.Objects]
   ,Create$44:function(Self) {
      Self.AutoResetError = true;
      Self.AutoWriteToConsole = false;
      Self.ThrowExceptions = false;
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TW3ErrorObject = class (TObject)
///  [line: 56, column: 3, file: System.Objects]
var TW3ErrorObject = {
   $ClassName:"TW3ErrorObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLastError$1 = "";
      $.FOptions$4 = null;
   }
   /// procedure TW3ErrorObject.ClearLastError()
   ///  [line: 351, column: 26, file: System.Objects]
   ,ClearLastError$1:function(Self) {
      Self.FLastError$1 = "";
   }
   /// constructor TW3ErrorObject.Create()
   ///  [line: 288, column: 28, file: System.Objects]
   ,Create$45:function(Self) {
      TObject.Create(Self);
      Self.FOptions$4 = TW3ErrorObjectOptions.Create$44($New(TW3ErrorObjectOptions));
      return Self
   }
   /// destructor TW3ErrorObject.Destroy()
   ///  [line: 294, column: 27, file: System.Objects]
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions$4);
      TObject.Destroy(Self);
   }
   /// function TW3ErrorObject.GetExceptionClass() : TW3ExceptionClass
   ///  [line: 356, column: 25, file: System.Objects]
   ,GetExceptionClass:function(Self) {
      return EW3ErrorObject;
   }
   /// function TW3ErrorObject.GetFailed() : Boolean
   ///  [line: 305, column: 25, file: System.Objects]
   ,GetFailed$1:function(Self) {
      return Self.FLastError$1.length>0;
   }
   /// function TW3ErrorObject.GetLastError() : String
   ///  [line: 300, column: 25, file: System.Objects]
   ,GetLastError$1:function(Self) {
      return Self.FLastError$1;
   }
   /// procedure TW3ErrorObject.SetLastError(const ErrorText: String)
   ///  [line: 310, column: 26, file: System.Objects]
   ,SetLastError$1:function(Self, ErrorText$3) {
      var ErrClass = null;
      Self.FLastError$1 = Trim$_String_(ErrorText$3);
      if (Self.FLastError$1.length>0) {
         if (Self.FOptions$4.AutoWriteToConsole) {
            if (console) {
          console.log( (Self.FLastError$1) );
       }
         }
         if (Self.FOptions$4.ThrowExceptions) {
            ErrClass = TW3ErrorObject.GetExceptionClass$(Self);
            if (!ErrClass) {
               ErrClass = EW3ErrorObject;
            }
            throw Exception.Create($NewDyn(ErrClass,""),Self.FLastError$1);
         }
      }
   }
   /// procedure TW3ErrorObject.SetLastErrorF(const ErrorText: String; const Values: array of const)
   ///  [line: 345, column: 26, file: System.Objects]
   ,SetLastErrorF$1:function(Self, ErrorText$4, Values$12) {
      TW3ErrorObject.SetLastError$1(Self,Format(ErrorText$4,Values$12.slice(0)));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45$:function($){return $.ClassType.Create$45($)}
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
};
TW3ErrorObject.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// EW3HandleBasedObject = class (EW3Exception)
///  [line: 113, column: 3, file: System.Objects]
var EW3HandleBasedObject = {
   $ClassName:"EW3HandleBasedObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EW3ErrorObject = class (EW3Exception)
///  [line: 21, column: 3, file: System.Objects]
var EW3ErrorObject = {
   $ClassName:"EW3ErrorObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3DirectoryParser = class (TW3ErrorObject)
///  [line: 49, column: 3, file: System.IOUtils]
var TW3DirectoryParser = {
   $ClassName:"TW3DirectoryParser",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
   }
   /// function TW3DirectoryParser.GetErrorObject() : IW3ErrorAccess
   ///  [line: 176, column: 29, file: System.IOUtils]
   ,GetErrorObject:function(Self) {
      return $AsIntf(Self,"IW3ErrorAccess");
   }
   /// function TW3DirectoryParser.IsPathRooted(FilePath: String) : Boolean
   ///  [line: 181, column: 29, file: System.IOUtils]
   ,IsPathRooted:function(Self, FilePath) {
      var Result = false;
      var LMoniker = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      FilePath = (Trim$_String_(FilePath)).toLocaleLowerCase();
      if (FilePath.length>0) {
         LMoniker = TW3DirectoryParser.GetRootMoniker$(Self);
         Result = StrBeginsWith(FilePath,LMoniker);
      }
      return Result
   }
   /// function TW3DirectoryParser.IsRelativePath(FilePath: String) : Boolean
   ///  [line: 194, column: 29, file: System.IOUtils]
   ,IsRelativePath:function(Self, FilePath$1) {
      var Result = false;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TW3DirectoryParser.IsValidPath$(Self,FilePath$1)) {
         Result = !StrBeginsWith(FilePath$1,TW3DirectoryParser.GetRootMoniker$(Self));
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3DirectoryParser.GetPathSeparator,TW3DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3DirectoryParser.IsValidPath,TW3DirectoryParser.HasValidPathChars,TW3DirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3DirectoryParser.GetFileNameWithoutExtension,TW3DirectoryParser.GetPathName,TW3DirectoryParser.GetDevice,TW3DirectoryParser.GetFileName,TW3DirectoryParser.GetExtension,TW3DirectoryParser.GetDirectoryName,TW3DirectoryParser.IncludeTrailingPathDelimiter,TW3DirectoryParser.IncludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeTrailingPathDelimiter,TW3DirectoryParser.ChangeFileExt]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TW3UnixDirectoryParser = class (TW3DirectoryParser)
///  [line: 81, column: 3, file: System.IOUtils]
var TW3UnixDirectoryParser = {
   $ClassName:"TW3UnixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   /// function TW3UnixDirectoryParser.ChangeFileExt(const FilePath: String; NewExt: String) : String
   ///  [line: 670, column: 33, file: System.IOUtils]
   ,ChangeFileExt:function(Self, FilePath$2, NewExt) {
      NewExt={v:NewExt};
      var Result = "";
      var Separator$1 = "",
         x$7 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      Separator$1 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$2,Separator$1)) {
         TW3ErrorObject.SetLastError$1(Self,"Failed to change extension, path has no filename error");
         Result = FilePath$2;
         return Result;
      }
      NewExt.v = Trim$_String_(NewExt.v);
      while ((NewExt.v.charAt(0)==".")) {
         Delete(NewExt,1,1);
         if (NewExt.v.length<1) {
            break;
         }
      }
      if (NewExt.v.length>0) {
         NewExt.v = "."+NewExt.v;
      }
      for(x$7=FilePath$2.length;x$7>=1;x$7--) {
         {var $temp13 = FilePath$2.charAt(x$7-1);
            if ($temp13==".") {
               Result = FilePath$2.substr(0,(x$7-1))+NewExt.v;
               break;
            }
             else if ($temp13==Separator$1) {
               Result = FilePath$2+NewExt.v;
               break;
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 723, column: 33, file: System.IOUtils]
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$3) {
      var Result = "";
      if (StrBeginsWith(FilePath$3,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = FilePath$3.substr(1);
      } else {
         Result = FilePath$3;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 739, column: 33, file: System.IOUtils]
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$4) {
      var Result = "";
      if (StrEndsWith(FilePath$4,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = (FilePath$4).substr(0,(FilePath$4.length-1));
      } else {
         Result = FilePath$4;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetDevice(const FilePath: String) : String
   ///  [line: 517, column: 33, file: System.IOUtils]
   ,GetDevice:function(Self, FilePath$5) {
      var Result = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$5.length>0) {
         if (StrBeginsWith(FilePath$5,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetDirectoryName(const FilePath: String) : String
   ///  [line: 604, column: 33, file: System.IOUtils]
   ,GetDirectoryName:function(Self, FilePath$6) {
      var Result = "";
      var Separator$2 = "",
         NameParts = [];
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$6.length>0) {
         Separator$2 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(FilePath$6,Separator$2)) {
            Result = FilePath$6;
            return Result;
         }
         NameParts = (FilePath$6).split(Separator$2);
         NameParts.splice((NameParts.length-1),1)
         ;
         Result = (NameParts).join(Separator$2)+Separator$2;
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract directory, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetExtension(const Filename: String) : String
   ///  [line: 627, column: 33, file: System.IOUtils]
   ,GetExtension:function(Self, Filename$3) {
      var Result = "";
      var Separator$3 = "",
         x$8 = 0;
      var dx = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Filename$3.length>0) {
         Separator$3 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Filename$3,Separator$3)) {
            TW3ErrorObject.SetLastError$1(Self,"Failed to extract extension, path contains no filename error");
         } else {
            for(x$8=Filename$3.length;x$8>=1;x$8--) {
               {var $temp14 = Filename$3.charAt(x$8-1);
                  if ($temp14==".") {
                     dx = Filename$3.length;
                     (dx-= x$8);
                     ++dx;
                     Result = RightStr(Filename$3,dx);
                     break;
                  }
                   else if ($temp14==Separator$3) {
                     break;
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract extension, filename was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetFileName(const FilePath: String) : String
   ///  [line: 585, column: 33, file: System.IOUtils]
   ,GetFileName:function(Self, FilePath$7) {
      var Result = "";
      var Temp$1 = "",
         Separator$4 = "",
         Parts = [];
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      Temp$1 = Trim$_String_(FilePath$7);
      if (Temp$1.length>0) {
         Separator$4 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$1,Separator$4)) {
            TW3ErrorObject.SetLastError$1(Self,"No filename part in path error");
         } else {
            Parts = (Temp$1).split(Separator$4);
            Result = Parts[(Parts.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract filename, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetFileNameWithoutExtension(const Filename: String) : String
   ///  [line: 575, column: 33, file: System.IOUtils]
   ,GetFileNameWithoutExtension:function(Self, Filename$4) {
      var Result = "";
      var temp$11 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      temp$11 = TW3DirectoryParser.GetFileName$(Self,Filename$4);
      if (!TW3ErrorObject.GetFailed$1(Self)) {
         Result = TW3DirectoryParser.ChangeFileExt$(Self,temp$11,"");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetPathName(const FilePath: String) : String
   ///  [line: 532, column: 33, file: System.IOUtils]
   ,GetPathName:function(Self, FilePath$8) {
      var Result = "";
      var Temp$2 = "",
         Parts$1 = [],
         Separator$5 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      Temp$2 = Trim$_String_(FilePath$8);
      if (Temp$2.length>0) {
         Separator$5 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$2,Separator$5)) {
            if (Temp$2==TW3DirectoryParser.GetRootMoniker$(Self)) {
               TW3ErrorObject.SetLastError$1(Self,"Failed to get directory name, path is root");
               return "";
            }
            Temp$2 = (Temp$2).substr(0,(Temp$2.length-1));
            Parts$1 = (Temp$2).split(Separator$5);
            Result = Parts$1[(Parts$1.length-1)];
            return Result;
         }
         Parts$1 = (Temp$2).split(Separator$5);
         if (Parts$1.length>1) {
            Result = Parts$1[(Parts$1.length-1)-1];
         } else {
            Result = Parts$1[(Parts$1.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract directory name, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetPathSeparator() : Char
   ///  [line: 398, column: 33, file: System.IOUtils]
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   /// function TW3UnixDirectoryParser.GetRootMoniker() : String
   ///  [line: 403, column: 33, file: System.IOUtils]
   ,GetRootMoniker:function(Self) {
      return "~\/";
   }
   /// function TW3UnixDirectoryParser.HasValidFileNameChars(FileName: String) : Boolean
   ///  [line: 408, column: 33, file: System.IOUtils]
   ,HasValidFileNameChars:function(Self, FileName) {
      var Result = false;
      var el = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FileName.length>0) {
         if ((FileName.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \" \" in filename \"%s\" error",[FileName]);
         } else {
            for (var $temp15=0;$temp15<FileName.length;$temp15++) {
               el=$uniCharAt(FileName,$temp15);
               if (!el) continue;
               Result = (((el>="A")&&(el<="Z"))||((el>="a")&&(el<="z"))||((el>="0")&&(el<="9"))||(el=="-")||(el=="_")||(el==".")||(el==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el, FileName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.HasValidPathChars(FolderName: String) : Boolean
   ///  [line: 437, column: 33, file: System.IOUtils]
   ,HasValidPathChars:function(Self, FolderName) {
      var Result = false;
      var el$1 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FolderName.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \" \" in foldername \"%s\" error",[FolderName]);
      } else {
         if (FolderName.length>0) {
            for (var $temp16=0;$temp16<FolderName.length;$temp16++) {
               el$1=$uniCharAt(FolderName,$temp16);
               if (!el$1) continue;
               Result = (((el$1>="A")&&(el$1<="Z"))||((el$1>="a")&&(el$1<="z"))||((el$1>="0")&&(el$1<="9"))||(el$1=="-")||(el$1=="_")||(el$1==".")||(el$1==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$1, FolderName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IncludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 714, column: 33, file: System.IOUtils]
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$9) {
      var Result = "";
      var Separator$6 = "";
      Separator$6 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$9,Separator$6)) {
         Result = FilePath$9;
      } else {
         Result = Separator$6+FilePath$9;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IncludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 731, column: 33, file: System.IOUtils]
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$10) {
      var Result = "";
      var LSeparator = "";
      LSeparator = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$10;
      if (!StrEndsWith(Result,LSeparator)) {
         Result+=LSeparator;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IsValidPath(FilePath: String) : Boolean
   ///  [line: 466, column: 33, file: System.IOUtils]
   ,IsValidPath:function(Self, FilePath$11) {
      var Result = false;
      var Separator$7 = "",
         PathParts = [],
         Index = 0,
         a$453 = 0;
      var part = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FilePath$11.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \" \" in path \"%s\" error",[FilePath$11]);
      } else {
         if (FilePath$11.length>0) {
            Separator$7 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts = (FilePath$11).split(Separator$7);
            Index = 0;
            var $temp17;
            for(a$453=0,$temp17=PathParts.length;a$453<$temp17;a$453++) {
               part = PathParts[a$453];
               {var $temp18 = part;
                  if ($temp18=="") {
                     TW3ErrorObject.SetLastErrorF$1(Self,"Path has multiple separators (%s) error",[FilePath$11]);
                     return false;
                  }
                   else if ($temp18=="~") {
                     if (Index>0) {
                        TW3ErrorObject.SetLastErrorF$1(Self,"Path has misplaced root moniker (%s) error",[FilePath$11]);
                        return false;
                     }
                  }
                   else {
                     if (Index==(PathParts.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part)) {
                           return false;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part)) {
                        return false;
                     }
                  }
               }
               Index+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3UnixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3UnixDirectoryParser.GetPathSeparator,TW3UnixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TPath = class (TObject)
///  [line: 107, column: 3, file: System.IOUtils]
var TPath = {
   $ClassName:"TPath",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TPath.GetDevice(const FilePath: String) : String
   ///  [line: 268, column: 22, file: System.IOUtils]
   ,GetDevice$2:function(FilePath$12) {
      var Result = "";
      var Access$2 = null,
         Error$2 = null;
      Access$2 = GetDirectoryParser();
      Error$2 = Access$2[2]();
      Result = Access$2[10](FilePath$12);
      if (Error$2[0]()) {
         throw Exception.Create($New(EPathError),Error$2[1]());
      }
      return Result
   }
   /// function TPath.IsPathRooted(const FilePath: String) : Boolean
   ///  [line: 257, column: 22, file: System.IOUtils]
   ,IsPathRooted$1:function(FilePath$13) {
      var Result = false;
      var Access$3 = null,
         Error$3 = null;
      Access$3 = GetDirectoryParser();
      Error$3 = Access$3[2]();
      Result = Access$3[7](FilePath$13);
      if (Error$3[0]()) {
         throw Exception.Create($New(EPathError),Error$3[1]());
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
function InstallDirectoryParser(NewParser) {
   if (__Parser!==null) {
      TObject.Free(__Parser);
      __Parser = null;
   }
   __Parser = NewParser;
};
function GetDirectoryParser() {
   var Result = null;
   if (__Parser===null) {
      if (GetIsRunningInBrowser()) {
         __Parser = TW3ErrorObject.Create$45$($New(TW3UnixDirectoryParser));
      }
   }
   if (__Parser!==null) {
      Result = $AsIntf(__Parser,"IW3DirectoryParser");
   } else {
      Result = null;
   }
   return Result
};
/// EPathError = class (EW3Exception)
///  [line: 105, column: 3, file: System.IOUtils]
var EPathError = {
   $ClassName:"EPathError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TQTXPropertyDataType enumeration
///  [line: 34, column: 3, file: Ragnarok.JSON]
var TQTXPropertyDataType = [ "qdtInvalid", "qdtBoolean", "qdtinteger", "qdtfloat", "qdtstring", "qdtSymbol", "qdtFunction", "qdtObject", "qdtArray", "qdtVariant" ];
/// TQTXJSONObject = class (TObject)
///  [line: 104, column: 3, file: Ragnarok.JSON]
var TQTXJSONObject = {
   $ClassName:"TQTXJSONObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FInstance$1 = undefined;
   }
   /// function TQTXJSONObject.Branch(const ObjectName: String) : TQTXJSONObject
   ///  [line: 526, column: 25, file: Ragnarok.JSON]
   ,Branch:function(Self, ObjectName) {
      var Result = null;
      if (Self.FInstance$1.hasOwnProperty(ObjectName)) {
         Result = TQTXJSONObject.Create$100($New(TQTXJSONObject),Self.FInstance$1[ObjectName]);
         return Result;
      }
      Self.FInstance$1[ObjectName] = TVariant.CreateObject();
      Result = TQTXJSONObject.Create$100($New(TQTXJSONObject),Self.FInstance$1[ObjectName]);
      return Result
   }
   /// procedure TQTXJSONObject.Clear()
   ///  [line: 466, column: 26, file: Ragnarok.JSON]
   ,Clear$18:function(Self) {
      Self.FInstance$1 = TVariant.CreateObject();
   }
   /// constructor TQTXJSONObject.Create(const Instance: THandle)
   ///  [line: 313, column: 28, file: Ragnarok.JSON]
   ,Create$100:function(Self, Instance$6) {
      TObject.Create(Self);
      if (Instance$6) {
         if (TW3VariantHelper$IsObject(Instance$6)) {
            Self.FInstance$1 = Instance$6;
         } else {
            throw Exception.Create($New(EQTXJSONObject),"Failed to attach to JSON instance, reference is not an object");
         }
      } else {
         throw Exception.Create($New(EQTXJSONObject),"Failed to attach to JSON instance, reference was NIL error");
      }
      return Self
   }
   /// constructor TQTXJSONObject.Create()
   ///  [line: 307, column: 28, file: Ragnarok.JSON]
   ,Create$99:function(Self) {
      TObject.Create(Self);
      Self.FInstance$1 = TVariant.CreateObject();
      return Self
   }
   /// procedure TQTXJSONObject.Delete(const PropertyName: String)
   ///  [line: 333, column: 26, file: Ragnarok.JSON]
   ,Delete$10:function(Self, PropertyName) {
      var LRef$2 = undefined;
      if (Self.FInstance$1.hasOwnProperty(PropertyName)) {
         LRef$2 = Self.FInstance$1;
         delete LRef$2[PropertyName];
      }
   }
   /// destructor TQTXJSONObject.Destroy()
   ///  [line: 327, column: 27, file: Ragnarok.JSON]
   ,Destroy:function(Self) {
      Self.FInstance$1 = null;
      TObject.Destroy(Self);
   }
   /// function TQTXJSONObject.Examine(const PropertyName: String) : TQTXPropertyDataType
   ///  [line: 344, column: 25, file: Ragnarok.JSON]
   ,Examine$5:function(Self, PropertyName$1) {
      var Result = 0;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$1)) {
         Result = TQTXJSONDataTypeResolver.QueryDataType(TQTXJSONDataTypeResolver,Self.FInstance$1[PropertyName$1]);
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),"Failed to examine datatype, property [%s] not found error",[PropertyName$1]);
      }
      return Result
   }
   /// function TQTXJSONObject.Exists(const PropertyName: String) : Boolean
   ///  [line: 626, column: 25, file: Ragnarok.JSON]
   ,Exists$4:function(Self, PropertyName$2) {
      var Result = false;
      if (Self.FInstance$1) {
         Result = (Self.FInstance$1.hasOwnProperty(PropertyName$2)?true:false);
      } else {
         Result = false;
      }
      return Result
   }
   /// function TQTXJSONObject.ForEach(const CB: TQTXJSONEnumerator) : TQTXJSONObject
   ///  [line: 378, column: 25, file: Ragnarok.JSON]
   ,ForEach$7:function(Self, CB) {
      var Result = null;
      var a$454 = 0;
      var el$2 = "",
         LData;
      Result = Self;
      if (CB) {
         var a$455 = [];
         a$455 = TQTXJSONObject.GetInstanceKeys(Self);
         var $temp19;
         for(a$454=0,$temp19=a$455.length;a$454<$temp19;a$454++) {
            el$2 = a$455[a$454];
            LData.v = TQTXJSONObject.Read$8(Self,el$2);
            if (CB(el$2,LData)==1) {
               TQTXJSONObject.Write$10(Self,el$2,LData.v);
            } else {
               break;
            }
         }
      }
      return Result
   }
   /// procedure TQTXJSONObject.FromJSON(const Text: String)
   ///  [line: 409, column: 26, file: Ragnarok.JSON]
   ,FromJSON$1:function(Self, Text$14) {
      Self.FInstance$1 = JSON.parse(Text$14);
   }
   /// function TQTXJSONObject.GetInstanceKeys() : TStrArray
   ///  [line: 444, column: 25, file: Ragnarok.JSON]
   ,GetInstanceKeys:function(Self) {
      var Result = [];
      var LRef$3 = undefined;
      LRef$3 = Self.FInstance$1;
      if (!(Object.keys === undefined)) {
      Result = Object.keys(LRef$3);
      return Result;
    }

    if (!(Object.getOwnPropertyNames === undefined)) {
        Result = Object.getOwnPropertyNames(LRef$3);
        return Result;
    }

    for (var qtxenum in LRef$3) {
      if ( (LRef$3).hasOwnProperty(qtxenum) == true )
        (Result).push(qtxenum);
    }
    return Result;
      return Result
   }
   /// function TQTXJSONObject.GetProperty(const Index: Integer) : Variant
   ///  [line: 362, column: 25, file: Ragnarok.JSON]
   ,GetProperty:function(Self, Index$1) {
      var Result = undefined;
      var LRef$4 = undefined;
      LRef$4 = Self.FInstance$1;
      Result = Object.keys(LRef$4)[Index$1];
      return Result
   }
   /// function TQTXJSONObject.GetPropertyByName(const PropertyName: String) : Variant
   ///  [line: 353, column: 25, file: Ragnarok.JSON]
   ,GetPropertyByName:function(Self, PropertyName$3) {
      var Result = undefined;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$3)) {
         Result = Self.FInstance$1[PropertyName$3];
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[64],[PropertyName$3]);
      }
      return Result
   }
   /// function TQTXJSONObject.GetPropertyCount() : Integer
   ///  [line: 370, column: 25, file: Ragnarok.JSON]
   ,GetPropertyCount$1:function(Self) {
      var Result = 0;
      var LRef$5 = undefined;
      LRef$5 = Self.FInstance$1;
      Result = Object.keys(LRef$5).length;
      return Result
   }
   /// function TQTXJSONObject.Locate(const ObjectName: String; const AllowCreate: Boolean) : TQTXJSONObject
   ///  [line: 538, column: 25, file: Ragnarok.JSON]
   ,Locate:function(Self, ObjectName$1, AllowCreate) {
      var Result = null;
      if (Self.FInstance$1.hasOwnProperty(ObjectName$1)) {
         Result = TQTXJSONObject.Create$100($New(TQTXJSONObject),Self.FInstance$1[ObjectName$1]);
      } else {
         if (AllowCreate) {
            Self.FInstance$1[ObjectName$1] = TVariant.CreateObject();
            Result = TQTXJSONObject.Create$100($New(TQTXJSONObject),Self.FInstance$1[ObjectName$1]);
         }
      }
      return Result
   }
   /// function TQTXJSONObject.Read(const PropertyName: String) : Variant
   ///  [line: 472, column: 25, file: Ragnarok.JSON]
   ,Read$8:function(Self, PropertyName$4) {
      var Result = undefined;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$4)) {
         Result = Self.FInstance$1[PropertyName$4];
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[64],[PropertyName$4]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadBoolean(const PropertyName: String) : Boolean
   ///  [line: 499, column: 25, file: Ragnarok.JSON]
   ,ReadBoolean$2:function(Self, PropertyName$5) {
      var Result = false;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$5)) {
         Result = TVariant.AsBool(Self.FInstance$1[PropertyName$5]);
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[64],[PropertyName$5]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadDateTime(const PropertyName: String) : TDateTime
   ///  [line: 517, column: 25, file: Ragnarok.JSON]
   ,ReadDateTime$3:function(Self, PropertyName$6) {
      var Result = undefined;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$6)) {
         Result = JDateToDateTime(Self.FInstance$1[PropertyName$6]);
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[64],[PropertyName$6]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadFloat(const PropertyName: String) : Float
   ///  [line: 508, column: 25, file: Ragnarok.JSON]
   ,ReadFloat$2:function(Self, PropertyName$7) {
      var Result = 0;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$7)) {
         Result = TVariant.AsFloat(Self.FInstance$1[PropertyName$7]);
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[64],[PropertyName$7]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadInteger(const PropertyName: String) : Integer
   ///  [line: 490, column: 25, file: Ragnarok.JSON]
   ,ReadInteger$2:function(Self, PropertyName$8) {
      var Result = 0;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$8)) {
         Result = TVariant.AsInteger(Self.FInstance$1[PropertyName$8]);
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[64],[PropertyName$8]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadString(const PropertyName: String) : String
   ///  [line: 481, column: 25, file: Ragnarok.JSON]
   ,ReadString$4:function(Self, PropertyName$9) {
      var Result = "";
      if (Self.FInstance$1.hasOwnProperty(PropertyName$9)) {
         Result = TVariant.AsString(Self.FInstance$1[PropertyName$9]);
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[64],[PropertyName$9]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadVariant(const PropertyName: String) : Variant
   ///  [line: 601, column: 25, file: Ragnarok.JSON]
   ,ReadVariant$1:function(Self, PropertyName$10) {
      var Result = undefined;
      var LBytes$1 = [];
      if (TQTXJSONObject.Exists$4(Self,PropertyName$10)) {
         LBytes$1 = TDatatype.Base64ToBytes$1(TDatatype,TQTXJSONObject.ReadString$4(Self,PropertyName$10));
         Result = TDatatype.BytesToVariant$1(TDatatype,LBytes$1);
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[64],[PropertyName$10]);
      }
      return Result
   }
   /// function TQTXJSONObject.ToJSON(const Indent: Integer) : String
   ///  [line: 399, column: 25, file: Ragnarok.JSON]
   ,ToJSON$4:function(Self, Indent) {
      return JSON.stringify(Self.FInstance$1,null,Indent);
   }
   /// function TQTXJSONObject.ToJSON() : String
   ///  [line: 394, column: 25, file: Ragnarok.JSON]
   ,ToJSON$3:function(Self) {
      return JSON.stringify(Self.FInstance$1,null,4);
   }
   /// procedure TQTXJSONObject.Write(const PropertyName: String; const Data: Variant)
   ///  [line: 593, column: 26, file: Ragnarok.JSON]
   ,Write$10:function(Self, PropertyName$11, Data$22) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$11)) {
         Self.FInstance$1[PropertyName$11] = Data$22;
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[63],[PropertyName$11]);
      }
   }
   /// procedure TQTXJSONObject.WriteBoolean(const PropertyName: String; const Data: Boolean)
   ///  [line: 569, column: 26, file: Ragnarok.JSON]
   ,WriteBoolean$1:function(Self, PropertyName$12, Data$23) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$12)) {
         Self.FInstance$1[PropertyName$12] = Data$23;
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[63],[PropertyName$12]);
      }
   }
   /// procedure TQTXJSONObject.WriteDateTime(const PropertyName: String; const Data: TDateTime)
   ///  [line: 577, column: 26, file: Ragnarok.JSON]
   ,WriteDateTime$2:function(Self, PropertyName$13, Data$24) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$13)) {
         Self.FInstance$1[PropertyName$13] = DateTimeToJDate(Data$24);
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[63],[PropertyName$13]);
      }
   }
   /// procedure TQTXJSONObject.WriteFloat(const PropertyName: String; const Data: Float)
   ///  [line: 585, column: 26, file: Ragnarok.JSON]
   ,WriteFloat$1:function(Self, PropertyName$14, Data$25) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$14)) {
         Self.FInstance$1[PropertyName$14] = FloatToStr$_Float_(Data$25);
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[63],[PropertyName$14]);
      }
   }
   /// procedure TQTXJSONObject.WriteInteger(const PropertyName: String; const Data: Integer)
   ///  [line: 561, column: 26, file: Ragnarok.JSON]
   ,WriteInteger$1:function(Self, PropertyName$15, Data$26) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$15)) {
         Self.FInstance$1[PropertyName$15] = Data$26.toString();
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[63],[PropertyName$15]);
      }
   }
   /// procedure TQTXJSONObject.WriteOrAdd(const PropertyName: String; const Data: Variant)
   ///  [line: 621, column: 26, file: Ragnarok.JSON]
   ,WriteOrAdd:function(Self, PropertyName$16, Data$27) {
      Self.FInstance$1[PropertyName$16] = Data$27;
   }
   /// procedure TQTXJSONObject.WriteString(const PropertyName: String; const Data: String)
   ///  [line: 553, column: 26, file: Ragnarok.JSON]
   ,WriteString$3:function(Self, PropertyName$17, Data$28) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$17)) {
         Self.FInstance$1[PropertyName$17] = Data$28;
      } else {
         throw EW3Exception.CreateFmt($New(EQTXJSONObject),$R[63],[PropertyName$17]);
      }
   }
   /// procedure TQTXJSONObject.WriteVariant(const PropertyName: String; const Value: Variant)
   ///  [line: 611, column: 26, file: Ragnarok.JSON]
   ,WriteVariant$1:function(Self, PropertyName$18, Value$42) {
      var LBytes$2 = [];
      if (Value$42) {
         LBytes$2 = TDatatype.VariantToBytes$1(TDatatype,Value$42);
         Self.FInstance$1[PropertyName$18] = TDatatype.BytesToBase64$1(TDatatype,LBytes$2);
      } else {
         Self.FInstance$1[PropertyName$18] = Value$42;
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
TQTXJSONObject.$Intf={
   IQTXJSONStorage:[TQTXJSONObject.GetPropertyCount$1,TQTXJSONObject.GetProperty,TQTXJSONObject.GetPropertyByName,TQTXJSONObject.GetInstanceKeys,TQTXJSONObject.ForEach$7,TQTXJSONObject.Exists$4,TQTXJSONObject.Examine$5,TQTXJSONObject.Read$8,TQTXJSONObject.ReadString$4,TQTXJSONObject.ReadInteger$2,TQTXJSONObject.ReadBoolean$2,TQTXJSONObject.ReadDateTime$3,TQTXJSONObject.ReadFloat$2,TQTXJSONObject.ReadVariant$1,TQTXJSONObject.WriteVariant$1,TQTXJSONObject.Write$10,TQTXJSONObject.WriteOrAdd,TQTXJSONObject.WriteString$3,TQTXJSONObject.WriteInteger$1,TQTXJSONObject.WriteBoolean$1,TQTXJSONObject.WriteDateTime$2,TQTXJSONObject.WriteFloat$1,TQTXJSONObject.Branch,TQTXJSONObject.Locate,TQTXJSONObject.Delete$10]
}
/// TQTXJSONDataTypeResolver = class (TObject)
///  [line: 56, column: 3, file: Ragnarok.JSON]
var TQTXJSONDataTypeResolver = {
   $ClassName:"TQTXJSONDataTypeResolver",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TQTXJSONDataTypeResolver.QueryDataType(const Reference: THandle) : TQTXPropertyDataType
   ///  [line: 180, column: 41, file: Ragnarok.JSON]
   ,QueryDataType:function(Self, Reference) {
      var Result = 0;
      var LType$3 = "";
      if (Reference) {
         LType$3 = typeof(Reference);
         {var $temp20 = (LType$3).toLocaleLowerCase();
            if ($temp20=="object") {
               if (!Reference.length) {
                  Result = 7;
               } else {
                  Result = 8;
               }
            }
             else if ($temp20=="function") {
               Result = 6;
            }
             else if ($temp20=="symbol") {
               Result = 5;
            }
             else if ($temp20=="boolean") {
               Result = 1;
            }
             else if ($temp20=="string") {
               Result = 4;
            }
             else if ($temp20=="number") {
               if (Math.round(Number(Reference))!=Reference) {
                  Result = 3;
               } else {
                  Result = 2;
               }
            }
             else if ($temp20=="array") {
               Result = 8;
            }
             else {
               Result = 9;
            }
         }
      } else {
         Result = 0;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// EQTXJSONObject = class (EW3Exception)
///  [line: 27, column: 3, file: Ragnarok.JSON]
var EQTXJSONObject = {
   $ClassName:"EQTXJSONObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function JDateToDateTime(Obj) {
   return Obj.getTime()/86400000+25569;
};
function DateTimeToJDate(Present) {
   var Result = null;
   Result = new Date();
   Result.setTime(Math.round((Present-25569)*86400000));
   return Result
};
var CNT_DaysInMonthData = [[31,28,31,30,31,30,31,31,30,31,30,31],[31,29,31,30,31,30,31,31,30,31,30,31]];
/// TAllocation = class (TDataTypeConverter)
///  [line: 51, column: 3, file: System.Memory.Allocation]
var TAllocation = {
   $ClassName:"TAllocation",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FHandle = undefined;
      $.FOptions$1 = null;
      $.FSize = 0;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 73, column: 37, file: System.Memory.Allocation]
   ,a$28:function(Self) {
      return ((!Self.FHandle)?true:false);
   }
   /// procedure TAllocation.Allocate(const NumberOfBytes: Integer)
   ///  [line: 258, column: 23, file: System.Memory.Allocation]
   ,Allocate:function(Self, NumberOfBytes) {
      var NewSize = 0;
      if (Self.FHandle) {
         TAllocation.Release$1(Self);
      }
      if (NumberOfBytes>0) {
         NewSize = 0;
         if (Self.FOptions$1.FUseCache) {
            NewSize = TInteger.ToNearest(NumberOfBytes,Self.FOptions$1.FCacheSize);
         } else {
            NewSize = NumberOfBytes;
         }
         Self.FHandle = TUnManaged.AllocMemA(TUnManaged,NewSize);
         Self.FSize = NumberOfBytes;
         TAllocation.HandleAllocated$(Self);
      }
   }
   /// constructor TAllocation.Create(const ByteSize: Integer)
   ///  [line: 179, column: 25, file: System.Memory.Allocation]
   ,Create$38:function(Self, ByteSize) {
      TDataTypeConverter.Create$15$(Self);
      if (ByteSize>0) {
         TAllocation.Allocate(Self,ByteSize);
      }
      return Self
   }
   /// constructor TAllocation.Create()
   ///  [line: 173, column: 25, file: System.Memory.Allocation]
   ,Create$15:function(Self) {
      TDataTypeConverter.Create$15(Self);
      Self.FOptions$1 = TAllocationOptions.Create$36($New(TAllocationOptions),Self);
      return Self
   }
   /// function TAllocation.DataGetSize() : Integer
   ///  [line: 230, column: 22, file: System.Memory.Allocation]
   ,DataGetSize$1:function(Self) {
      return TAllocation.GetSize$3(Self);
   }
   /// function TAllocation.DataOffset() : Integer
   ///  [line: 224, column: 22, file: System.Memory.Allocation]
   ,DataOffset$1:function(Self) {
      return 0;
   }
   /// function TAllocation.DataRead(const Offset: Integer; const ByteCount: Integer) : TByteArray
   ///  [line: 236, column: 22, file: System.Memory.Allocation]
   ,DataRead$1:function(Self, Offset$4, ByteCount) {
      return TDataTypeConverter.TypedArrayToBytes(Self,TUnManaged.ReadMemoryA(TUnManaged,TAllocation.GetHandle(Self),Offset$4,ByteCount));
   }
   /// procedure TAllocation.DataWrite(const Offset: Integer; const Bytes: TByteArray)
   ///  [line: 243, column: 23, file: System.Memory.Allocation]
   ,DataWrite$1:function(Self, Offset$5, Bytes$3) {
      TUnManaged.WriteMemoryA(TUnManaged,TAllocation.GetHandle(Self),Offset$5,TDataTypeConverter.BytesToTypedArray(Self,Bytes$3));
   }
   /// destructor TAllocation.Destroy()
   ///  [line: 186, column: 24, file: System.Memory.Allocation]
   ,Destroy:function(Self) {
      if (Self.FHandle) {
         TAllocation.Release$1(Self);
      }
      TObject.Free(Self.FOptions$1);
      TDataTypeConverter.Destroy(Self);
   }
   /// function TAllocation.GetBufferHandle() : TBufferHandle
   ///  [line: 430, column: 22, file: System.Memory.Allocation]
   ,GetBufferHandle:function(Self) {
      var Result = undefined;
      if (Self.FHandle) {
         Result = Self.FHandle.buffer;
      }
      return Result
   }
   /// function TAllocation.GetHandle() : TMemoryHandle
   ///  [line: 425, column: 22, file: System.Memory.Allocation]
   ,GetHandle:function(Self) {
      return Self.FHandle;
   }
   /// function TAllocation.GetSize() : Integer
   ///  [line: 420, column: 22, file: System.Memory.Allocation]
   ,GetSize$3:function(Self) {
      return Self.FSize;
   }
   /// function TAllocation.GetTotalSize() : Integer
   ///  [line: 414, column: 22, file: System.Memory.Allocation]
   ,GetTotalSize$1:function(Self) {
      var Result = 0;
      if (Self.FHandle) {
         Result = parseInt(Self.FHandle.length,10);
      }
      return Result
   }
   /// function TAllocation.GetTransport() : IBinaryTransport
   ///  [line: 195, column: 22, file: System.Memory.Allocation]
   ,GetTransport:function(Self) {
      return $AsIntf(Self,"IBinaryTransport");
   }
   /// procedure TAllocation.Grow(const NumberOfBytes: Integer)
   ///  [line: 300, column: 23, file: System.Memory.Allocation]
   ,Grow:function(Self, NumberOfBytes$1) {
      var ExactNewSize = 0,
         TotalNewSize = 0;
      if (Self.FHandle) {
         ExactNewSize = Self.FSize+NumberOfBytes$1;
         if (Self.FOptions$1.FUseCache) {
            if (NumberOfBytes$1<TAllocationOptions.GetCacheFree(Self.FOptions$1)) {
               (Self.FSize+= NumberOfBytes$1);
            } else {
               TotalNewSize = TInteger.ToNearest(ExactNewSize,Self.FOptions$1.FCacheSize);
               TAllocation.ReAllocate(Self,TotalNewSize);
               Self.FSize = ExactNewSize;
            }
         } else {
            TAllocation.ReAllocate(Self,ExactNewSize);
         }
      } else {
         TAllocation.Allocate(Self,NumberOfBytes$1);
      }
   }
   /// procedure TAllocation.HandleAllocated()
   ///  [line: 248, column: 23, file: System.Memory.Allocation]
   ,HandleAllocated:function(Self) {
      /* null */
   }
   /// procedure TAllocation.HandleReleased()
   ///  [line: 253, column: 23, file: System.Memory.Allocation]
   ,HandleReleased:function(Self) {
      /* null */
   }
   /// procedure TAllocation.ReAllocate(const NewSize: Integer)
   ///  [line: 379, column: 23, file: System.Memory.Allocation]
   ,ReAllocate:function(Self, NewSize$1) {
      var LSizeToSet = 0;
      if (NewSize$1>0) {
         if (Self.FHandle) {
            if (NewSize$1!=Self.FSize) {
               TAllocation.HandleReleased$(Self);
               LSizeToSet = 0;
               if (Self.FOptions$1.FUseCache) {
                  LSizeToSet = TInteger.ToNearest(NewSize$1,Self.FOptions$1.FCacheSize);
               } else {
                  LSizeToSet = TInteger.ToNearest(NewSize$1,16);
               }
               Self.FHandle = TUnManaged.ReAllocMemA(TUnManaged,Self.FHandle,LSizeToSet);
               Self.FSize = NewSize$1;
            }
         } else {
            TAllocation.Allocate(Self,NewSize$1);
         }
         TAllocation.HandleAllocated$(Self);
      } else {
         TAllocation.Release$1(Self);
      }
   }
   /// procedure TAllocation.Release()
   ///  [line: 287, column: 23, file: System.Memory.Allocation]
   ,Release$1:function(Self) {
      if (Self.FHandle) {
         try {
            Self.FHandle = null;
            Self.FSize = 0;
         } finally {
            TAllocation.HandleReleased$(Self);
         }
      }
   }
   /// procedure TAllocation.Shrink(const NumberOfBytes: Integer)
   ///  [line: 332, column: 23, file: System.Memory.Allocation]
   ,Shrink:function(Self, NumberOfBytes$2) {
      var ExactNewSize$1 = 0,
         Spare = 0,
         AlignedSize = 0;
      if (Self.FHandle) {
         ExactNewSize$1 = TInteger.EnsureRange((Self.FSize-NumberOfBytes$2),0,2147483647);
         if (Self.FOptions$1.FUseCache) {
            if (ExactNewSize$1>0) {
               Spare = ExactNewSize$1%Self.FOptions$1.FCacheSize;
               if (Spare>0) {
                  AlignedSize = ExactNewSize$1;
                  (AlignedSize+= (Self.FOptions$1.FCacheSize-Spare));
                  TAllocation.ReAllocate(Self,AlignedSize);
                  Self.FSize = ExactNewSize$1;
               } else {
                  Self.FSize = ExactNewSize$1;
               }
            } else {
               TAllocation.Release$1(Self);
            }
         } else if (ExactNewSize$1>0) {
            TAllocation.ReAllocate(Self,ExactNewSize$1);
         } else {
            TAllocation.Release$1(Self);
         }
      }
   }
   /// procedure TAllocation.Transport(const Target: IBinaryTransport)
   ///  [line: 200, column: 23, file: System.Memory.Allocation]
   ,Transport:function(Self, Target$2) {
      var Data$29 = [];
      if (Target$2===null) {
         throw Exception.Create($New(EAllocation),"Invalid transport interface, reference was NIL error");
      } else {
         if (!TAllocation.a$28(Self)) {
            try {
               Data$29 = TDataTypeConverter.TypedArrayToBytes(Self,TAllocation.GetHandle(Self));
               Target$2[3](Target$2[0](),Data$29);
            } catch ($e) {
               var e$2 = $W($e);
               throw EW3Exception.CreateFmt($New(EAllocation),"Data transport failed, mechanism threw exception %s with error [%s]",[TObject.ClassName(e$2.ClassType), e$2.FMessage]);
            }
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
};
TAllocation.$Intf={
   IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$1,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
   ,IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
}
/// TBinaryData = class (TAllocation)
///  [line: 158, column: 3, file: System.Memory.Buffer]
var TBinaryData = {
   $ClassName:"TBinaryData",$Parent:TAllocation
   ,$Init:function ($) {
      TAllocation.$Init($);
      $.FDataView = null;
   }
   /// procedure TBinaryData.AppendBuffer(const Raw: TMemoryHandle)
   ///  [line: 1201, column: 23, file: System.Memory.Buffer]
   ,AppendBuffer:function(Self, Raw) {
      var LOffset = 0;
      if (Raw) {
         if (Raw.length>0) {
            LOffset = TAllocation.GetSize$3(Self);
            TAllocation.Grow(Self,Raw.length);
            TBinaryData.WriteBuffer$2(Self,LOffset,Raw);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, invalid source handle error");
      }
   }
   /// procedure TBinaryData.AppendBytes(const Bytes: TByteArray)
   ///  [line: 1257, column: 23, file: System.Memory.Buffer]
   ,AppendBytes:function(Self, Bytes$4) {
      var LLen$6 = 0,
         LOffset$1 = 0;
      LLen$6 = Bytes$4.length;
      if (LLen$6>0) {
         LOffset$1 = TAllocation.GetSize$3(Self);
         TAllocation.Grow(Self,LLen$6);
         TAllocation.GetHandle(Self).set(Bytes$4,LOffset$1);
      }
   }
   /// procedure TBinaryData.AppendFloat32(const Value: float32)
   ///  [line: 1187, column: 23, file: System.Memory.Buffer]
   ,AppendFloat32:function(Self, Value$43) {
      var LOffset$2 = 0;
      LOffset$2 = TAllocation.GetSize$3(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,8));
      TBinaryData.WriteFloat32(Self,LOffset$2,Value$43);
   }
   /// procedure TBinaryData.AppendFloat64(const Value: float64)
   ///  [line: 1194, column: 23, file: System.Memory.Buffer]
   ,AppendFloat64:function(Self, Value$44) {
      var LOffset$3 = 0;
      LOffset$3 = TAllocation.GetSize$3(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,9));
      TBinaryData.WriteFloat64(Self,LOffset$3,Value$44);
   }
   /// procedure TBinaryData.AppendMemory(const Buffer: TBinaryData; const ReleaseBufferOnExit: Boolean)
   ///  [line: 1215, column: 23, file: System.Memory.Buffer]
   ,AppendMemory:function(Self, Buffer$2, ReleaseBufferOnExit) {
      var LOffset$4 = 0;
      if (Buffer$2!==null) {
         try {
            if (TAllocation.GetSize$3(Buffer$2)>0) {
               LOffset$4 = TAllocation.GetSize$3(Self);
               TAllocation.Grow(Self,TAllocation.GetSize$3(Buffer$2));
               TBinaryData.WriteBinaryData(Self,LOffset$4,Buffer$2);
            }
         } finally {
            if (ReleaseBufferOnExit) {
               TObject.Free(Buffer$2);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, Invalid source buffer error");
      }
   }
   /// procedure TBinaryData.AppendStr(const Text: String)
   ///  [line: 1235, column: 23, file: System.Memory.Buffer]
   ,AppendStr:function(Self, Text$15) {
      var LLen$7 = 0,
         LOffset$5 = 0,
         LTemp$4 = [],
         x$9 = 0;
      LLen$7 = Text$15.length;
      if (LLen$7>0) {
         LOffset$5 = TAllocation.GetSize$3(Self);
         LTemp$4 = TString.EncodeUTF8(TString,Text$15);
         TAllocation.Grow(Self,LTemp$4.length);
         var $temp21;
         for(x$9=0,$temp21=LTemp$4.length;x$9<$temp21;x$9++) {
            Self.FDataView.setInt8(LOffset$5,LTemp$4[x$9]);
            ++LOffset$5;
         }
      }
   }
   /// function TBinaryData.Clone() : TBinaryData
   ///  [line: 1160, column: 22, file: System.Memory.Buffer]
   ,Clone:function(Self) {
      return TBinaryData.Create$42($New(TBinaryData),TBinaryData.ToTypedArray(Self));
   }
   /// function TBinaryData.Copy(const Offset: Integer; const ByteLen: Integer) : TByteArray
   ///  [line: 480, column: 22, file: System.Memory.Buffer]
   ,Copy$1:function(Self, Offset$6, ByteLen) {
      var Result = [];
      var LSize = 0,
         LTemp$5 = null;
      LSize = TAllocation.GetSize$3(Self);
      if (LSize>0) {
         if (Offset$6>=0&&Offset$6<LSize) {
            if (Offset$6+ByteLen<=LSize) {
               LTemp$5 = new Uint8Array(Self.FDataView.buffer,Offset$6,ByteLen);
               Result = Array.prototype.slice.call(LTemp$5);
               LTemp$5.buffer = null;
               LTemp$5 = null;
            }
         }
      }
      return Result
   }
   /// procedure TBinaryData.CopyFrom(const Buffer: TBinaryData; const Offset: Integer; const ByteLen: Integer)
   ///  [line: 1165, column: 23, file: System.Memory.Buffer]
   ,CopyFrom$2:function(Self, Buffer$3, Offset$7, ByteLen$1) {
      if (Buffer$3!==null) {
         TBinaryData.CopyFromMemory(Self,TAllocation.GetHandle(Buffer$3),Offset$7,ByteLen$1);
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, source instance was NIL error");
      }
   }
   /// procedure TBinaryData.CopyFromMemory(const Raw: TMemoryHandle; Offset: Integer; ByteLen: Integer)
   ///  [line: 1173, column: 23, file: System.Memory.Buffer]
   ,CopyFromMemory:function(Self, Raw$1, Offset$8, ByteLen$2) {
      if (TMemoryHandleHelper$Valid$3(Raw$1)) {
         if (TBinaryData.OffsetInRange(Self,Offset$8)) {
            if (ByteLen$2>0) {
               TMarshal.Move$1(TMarshal,Raw$1,0,TAllocation.GetHandle(Self),Offset$8,ByteLen$2);
            }
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$8]);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, invalid source handle error");
      }
   }
   /// constructor TBinaryData.Create(aHandle: TMemoryHandle)
   ///  [line: 283, column: 25, file: System.Memory.Buffer]
   ,Create$42:function(Self, aHandle) {
      var LSignature;
      TDataTypeConverter.Create$15$(Self);
      if (TMemoryHandleHelper$Defined(aHandle)&&TMemoryHandleHelper$Valid$3(aHandle)) {
         if (aHandle.toString) {
            LSignature = aHandle.toString();
            if (SameText(String(LSignature),"[object Uint8Array]")||SameText(String(LSignature),"[object Uint8ClampedArray]")) {
               TAllocation.Allocate(Self,parseInt(aHandle.length,10));
               TMarshal.Move$1(TMarshal,aHandle,0,TAllocation.GetHandle(Self),0,parseInt(aHandle.length,10));
            } else {
               throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
         }
      }
      return Self
   }
   /// function TBinaryData.CutBinaryData(const Offset: Integer; const ByteLen: Integer) : TBinaryData
   ///  [line: 1139, column: 22, file: System.Memory.Buffer]
   ,CutBinaryData:function(Self, Offset$9, ByteLen$3) {
      var Result = null;
      var LSize$1 = 0,
         LNewBuffer = null;
      if (ByteLen$3>0) {
         LSize$1 = TAllocation.GetSize$3(Self);
         if (LSize$1>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$9)) {
               LNewBuffer = TAllocation.GetHandle(Self).subarray(Offset$9,Offset$9+ByteLen$3-1);
               Result = TBinaryData.Create$42($New(TBinaryData),LNewBuffer);
            } else {
               throw EW3Exception.CreateFmt($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$9]);
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Cut memory failed, buffer is empty error");
         }
      } else {
         Result = TBinaryData.Create$42($New(TBinaryData),null);
      }
      return Result
   }
   /// function TBinaryData.CutStream(const Offset: Integer; const ByteLen: Integer) : TStream
   ///  [line: 1117, column: 22, file: System.Memory.Buffer]
   ,CutStream:function(Self, Offset$10, ByteLen$4) {
      return TBinaryData.ToStream(TBinaryData.CutBinaryData(Self,Offset$10,ByteLen$4));
   }
   /// function TBinaryData.CutTypedArray(Offset: Integer; ByteLen: Integer) : TMemoryHandle
   ///  [line: 1122, column: 22, file: System.Memory.Buffer]
   ,CutTypedArray:function(Self, Offset$11, ByteLen$5) {
      var Result = undefined;
      var LTemp$6 = null;
      if (ByteLen$5>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$11)) {
            if (TAllocation.GetSize$3(Self)-Offset$11>0) {
               LTemp$6 = Self.FDataView.buffer.slice(Offset$11,Offset$11+ByteLen$5);
               Result = new Uint8Array(LTemp$6);
            }
         }
      }
      return Result
   }
   /// procedure TBinaryData.FromBase64(FileData: String)
   ///  [line: 639, column: 23, file: System.Memory.Buffer]
   ,FromBase64:function(Self, FileData) {
      var LBytes$3 = [];
      TAllocation.Release$1(Self);
      if (FileData.length>0) {
         LBytes$3 = TBase64EncDec.Base64ToBytes$2(TBase64EncDec,FileData);
         if (LBytes$3.length>0) {
            TBinaryData.AppendBytes(Self,LBytes$3);
         }
      }
   }
   /// procedure TBinaryData.FromNodeBuffer(const NodeBuffer: JNodeBuffer)
   ///  [line: 93, column: 23, file: SmartNJ.Streams]
   ,FromNodeBuffer:function(Self, NodeBuffer) {
      var LTypedAccess = undefined;
      if (!TAllocation.a$28(Self)) {
         TAllocation.Release$1(Self);
      }
      if (NodeBuffer!==null) {
         LTypedAccess = new Uint8Array(NodeBuffer);
         TBinaryData.AppendBuffer(Self,LTypedAccess);
      }
   }
   /// function TBinaryData.GetBit(const BitIndex: Integer) : Boolean
   ///  [line: 357, column: 22, file: System.Memory.Buffer]
   ,GetBit$1:function(Self, BitIndex$1) {
      var Result = false;
      var LOffset$6 = 0;
      LOffset$6 = BitIndex$1>>>3;
      if (TBinaryData.OffsetInRange(Self,LOffset$6)) {
         Result = TBitAccess.Get(TBitAccess,(BitIndex$1%8),TBinaryData.GetByte(Self,LOffset$6));
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, BitIndex$1]);
      }
      return Result
   }
   /// function TBinaryData.GetBitCount() : Integer
   ///  [line: 317, column: 22, file: System.Memory.Buffer]
   ,GetBitCount:function(Self) {
      return TAllocation.GetSize$3(Self)<<3;
   }
   /// function TBinaryData.GetByte(const Index: Integer) : Byte
   ///  [line: 677, column: 22, file: System.Memory.Buffer]
   ,GetByte:function(Self, Index$2) {
      var Result = 0;
      if (TAllocation.GetHandle(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index$2)) {
            Result = Self.FDataView.getUint8(Index$2);
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index$2]);
         }
      }
      return Result
   }
   /// procedure TBinaryData.HandleAllocated()
   ///  [line: 309, column: 23, file: System.Memory.Buffer]
   ,HandleAllocated:function(Self) {
      var LRef$6 = undefined;
      LRef$6 = TAllocation.GetBufferHandle(Self);
      (Self.FDataView) = new DataView(LRef$6);
   }
   /// procedure TBinaryData.HandleReleased()
   ///  [line: 322, column: 23, file: System.Memory.Buffer]
   ,HandleReleased:function(Self) {
      Self.FDataView = null;
   }
   /// procedure TBinaryData.LoadFromStream(const Stream: TStream)
   ///  [line: 561, column: 23, file: System.Memory.Buffer]
   ,LoadFromStream:function(Self, Stream$1) {
      var BytesToRead = 0;
      TAllocation.Release$1(Self);
      if (Stream$1!==null) {
         if (TStream.GetSize$(Stream$1)>0) {
            BytesToRead = TStream.GetSize$(Stream$1)-TStream.GetPosition$(Stream$1);
            if (BytesToRead>0) {
               TBinaryData.AppendBytes(Self,TStream.ReadBuffer$(Stream$1,TStream.GetPosition$(Stream$1),BytesToRead));
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),$R[68]);
      }
   }
   /// function TBinaryData.OffsetInRange(const Offset: Integer) : Boolean
   ///  [line: 850, column: 22, file: System.Memory.Buffer]
   ,OffsetInRange:function(Self, Offset$12) {
      var Result = false;
      var LSize$2 = 0;
      LSize$2 = TAllocation.GetSize$3(Self);
      if (LSize$2>0) {
         Result = Offset$12>=0&&Offset$12<=LSize$2;
      } else {
         Result = (Offset$12==0);
      }
      return Result
   }
   /// function TBinaryData.ReadBool(Offset: Integer) : Boolean
   ///  [line: 842, column: 22, file: System.Memory.Buffer]
   ,ReadBool:function(Self, Offset$13) {
      var Result = false;
      if (TBinaryData.OffsetInRange(Self,Offset$13)) {
         Result = Self.FDataView.getUint8(Offset$13)>0;
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[66],[Offset$13, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadBytes(Offset: Integer; ByteLen: Integer) : TByteArray
   ///  [line: 825, column: 22, file: System.Memory.Buffer]
   ,ReadBytes:function(Self, Offset$14, ByteLen$6) {
      var Result = [];
      var LSize$3 = 0,
         x$10 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$14)) {
         LSize$3 = TAllocation.GetSize$3(Self);
         if (Offset$14+ByteLen$6<=LSize$3) {
            var $temp22;
            for(x$10=0,$temp22=ByteLen$6;x$10<$temp22;x$10++) {
               Result.push(Self.FDataView.getUint8(Offset$14+x$10));
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[66],[Offset$14, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadFloat32(Offset: Integer) : float32
   ///  [line: 719, column: 22, file: System.Memory.Buffer]
   ,ReadFloat32:function(Self, Offset$15) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$15)) {
         if (Offset$15+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat32(Offset$15);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat32(Offset$15,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat32(Offset$15,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[66],[Offset$15, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadFloat64(Offset: Integer) : float64
   ///  [line: 701, column: 22, file: System.Memory.Buffer]
   ,ReadFloat64:function(Self, Offset$16) {
      var Result = undefined;
      if (TBinaryData.OffsetInRange(Self,Offset$16)) {
         if (Offset$16+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat64(Offset$16);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat64(Offset$16,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat64(Offset$16,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[66],[Offset$16, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadInt(Offset: Integer) : Integer
   ///  [line: 787, column: 22, file: System.Memory.Buffer]
   ,ReadInt:function(Self, Offset$17) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$17)) {
         if (Offset$17+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt32(Offset$17);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt32(Offset$17,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt32(Offset$17,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[66],[Offset$17, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadInt16(Offset: Integer) : SmallInt
   ///  [line: 736, column: 22, file: System.Memory.Buffer]
   ,ReadInt16:function(Self, Offset$18) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$18)) {
         if (Offset$18+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt16(Offset$18);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt16(Offset$18,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt16(Offset$18,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[66],[Offset$18, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadLong(Offset: Integer) : Longword
   ///  [line: 770, column: 22, file: System.Memory.Buffer]
   ,ReadLong$1:function(Self, Offset$19) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$19)) {
         if (Offset$19+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint32(Offset$19);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint32(Offset$19,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint32(Offset$19,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[66],[Offset$19, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadStr(Offset: Integer; ByteLen: Integer) : String
   ///  [line: 804, column: 22, file: System.Memory.Buffer]
   ,ReadStr$1:function(Self, Offset$20, ByteLen$7) {
      var Result = "";
      var LSize$4 = 0,
         LFetch = [],
         x$11 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$20)) {
         LSize$4 = TAllocation.GetSize$3(Self);
         if (Offset$20+ByteLen$7<=LSize$4) {
            var $temp23;
            for(x$11=0,$temp23=ByteLen$7;x$11<$temp23;x$11++) {
               LFetch.push(TBinaryData.GetByte(Self,(Offset$20+x$11)));
            }
            Result = TString.DecodeUTF8(TString,LFetch);
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[66],[Offset$20, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadWord(Offset: Integer) : Word
   ///  [line: 753, column: 22, file: System.Memory.Buffer]
   ,ReadWord$1:function(Self, Offset$21) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$21)) {
         if (Offset$21+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint16(Offset$21);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint16(Offset$21,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint16(Offset$21,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[66],[Offset$21, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// procedure TBinaryData.SetBit(const BitIndex: Integer; const value: Boolean)
   ///  [line: 352, column: 23, file: System.Memory.Buffer]
   ,SetBit$1:function(Self, BitIndex$2, value$1) {
      TBinaryData.SetByte(Self,(BitIndex$2>>>3),TBitAccess.Set$4(TBitAccess,(BitIndex$2%8),TBinaryData.GetByte(Self,(BitIndex$2>>>3)),value$1));
   }
   /// procedure TBinaryData.SetByte(const Index: Integer; const Value: Byte)
   ///  [line: 689, column: 23, file: System.Memory.Buffer]
   ,SetByte:function(Self, Index$3, Value$45) {
      if (TAllocation.GetHandle(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index$3)) {
            Self.FDataView.setUint8(Index$3,Value$45);
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index$3]);
         }
      }
   }
   /// function TBinaryData.ToBase64() : String
   ///  [line: 655, column: 22, file: System.Memory.Buffer]
   ,ToBase64:function(Self) {
      return TDataTypeConverter.BytesToBase64(Self.ClassType,TBinaryData.ToBytes(Self));
   }
   /// function TBinaryData.ToBytes() : TByteArray
   ///  [line: 510, column: 22, file: System.Memory.Buffer]
   ,ToBytes:function(Self) {
      var Result = [];
      var LSize$5 = 0,
         LTemp$7 = null;
      LSize$5 = TAllocation.GetSize$3(Self);
      if (LSize$5>0) {
         LTemp$7 = new Uint8Array(Self.FDataView.buffer,0,LSize$5);
         Result = Array.prototype.slice.call(LTemp$7);
         LTemp$7.buffer = null;
         LTemp$7 = null;
      } else {
         Result = [];
      }
      return Result
   }
   /// function TBinaryData.ToHexDump(BytesPerRow: Integer; Options: TBufferHexDumpOptions) : String
   ///  [line: 367, column: 22, file: System.Memory.Buffer]
   ,ToHexDump:function(Self, BytesPerRow, Options$4) {
      var Result = "";
      var mDump = [],
         mCount = 0,
         x$12 = 0;
      var LByte = 0,
         mPad = 0,
         z = 0;
      var mPad$1 = 0,
         z$1 = 0;
      function SliceToText(DataSlice) {
         var Result = "";
         var y = 0;
         var LChar = "",
            LOff = 0;
         if (DataSlice.length>0) {
            var $temp24;
            for(y=0,$temp24=DataSlice.length;y<$temp24;y++) {
               LChar = TDataTypeConverter.ByteToChar(TDataTypeConverter,DataSlice[y]);
               if ((((LChar>="A")&&(LChar<="Z"))||((LChar>="a")&&(LChar<="z"))||((LChar>="0")&&(LChar<="9"))||(LChar==",")||(LChar==";")||(LChar=="<")||(LChar==">")||(LChar=="{")||(LChar=="}")||(LChar=="[")||(LChar=="]")||(LChar=="-")||(LChar=="_")||(LChar=="#")||(LChar=="$")||(LChar=="%")||(LChar=="&")||(LChar=="\/")||(LChar=="(")||(LChar==")")||(LChar=="!")||(LChar=="§")||(LChar=="^")||(LChar==":")||(LChar==",")||(LChar=="?"))) {
                  Result+=LChar;
               } else {
                  Result+="_";
               }
            }
         }
         LOff = BytesPerRow-DataSlice.length;
         while (LOff>0) {
            Result+="_";
            --LOff;
         }
         Result+="\r\n";
         return Result
      };
      if (TAllocation.GetHandle(Self)) {
         mCount = 0;
         BytesPerRow = TInteger.EnsureRange(BytesPerRow,2,64);
         var $temp25;
         for(x$12=0,$temp25=TAllocation.GetSize$3(Self);x$12<$temp25;x$12++) {
            LByte = TBinaryData.GetByte(Self,x$12);
            mDump.push(LByte);
            if ($SetIn(Options$4,0,0,2)) {
               Result+="$"+(IntToHex2(LByte)).toLocaleUpperCase();
            } else {
               Result+=(IntToHex2(LByte)).toLocaleUpperCase();
            }
            ++mCount;
            if (mCount>=BytesPerRow) {
               if ($SetIn(Options$4,1,0,2)&&mCount>0) {
                  Result+=" ";
                  mPad = BytesPerRow-mCount;
                  var $temp26;
                  for(z=1,$temp26=mPad;z<=$temp26;z++) {
                     if ($SetIn(Options$4,0,0,2)) {
                        Result+="$";
                     }
                     Result+="00";
                     ++mCount;
                     if (mCount>=BytesPerRow) {
                        mCount = 0;
                        break;
                     } else {
                        Result+=" ";
                     }
                  }
               }
               if (mDump.length>0) {
                  Result+=SliceToText(mDump);
                  mDump.length=0;
                  mCount = 0;
               }
            } else {
               Result+=" ";
            }
         }
         if (mDump.length>0) {
            if ($SetIn(Options$4,1,0,2)&&mCount>0) {
               mPad$1 = BytesPerRow-mCount;
               var $temp27;
               for(z$1=1,$temp27=mPad$1;z$1<=$temp27;z$1++) {
                  if ($SetIn(Options$4,0,0,2)) {
                     Result+="$";
                  }
                  Result+="00";
                  ++mCount;
                  if (mCount>=BytesPerRow) {
                     break;
                  } else {
                     Result+=" ";
                  }
               }
            }
            Result+=" "+SliceToText(mDump);
            mCount = 0;
            mDump.length=0;
         }
      }
      return Result
   }
   /// function TBinaryData.ToNodeBuffer() : JNodeBuffer
   ///  [line: 83, column: 22, file: SmartNJ.Streams]
   ,ToNodeBuffer:function(Self) {
      var Result = null;
      if (TAllocation.a$28(Self)) {
         Result = new Buffer();
      } else {
         Result = new Buffer(TBinaryData.ToTypedArray(Self));
      }
      return Result
   }
   /// function TBinaryData.ToStream() : TStream
   ///  [line: 583, column: 22, file: System.Memory.Buffer]
   ,ToStream:function(Self) {
      var Result = null;
      var LSize$6 = 0,
         LCache = [],
         LTemp$8 = null;
      Result = TDataTypeConverter.Create$15$($New(TMemoryStream));
      LSize$6 = TAllocation.GetSize$3(Self);
      if (LSize$6>0) {
         LTemp$8 = new Uint8Array(Self.FDataView.buffer,0,LSize$6);
         LCache = Array.prototype.slice.call(LTemp$8);
         LTemp$8 = null;
         if (LCache.length>0) {
            try {
               TStream.Write$1(Result,LCache);
               TStream.SetPosition$(Result,0);
            } catch ($e) {
               var e$3 = $W($e);
               TObject.Free(Result);
               Result = null;
               throw $e;
            }
         }
      }
      return Result
   }
   /// function TBinaryData.ToString() : String
   ///  [line: 660, column: 22, file: System.Memory.Buffer]
   ,ToString$2:function(Self) {
      var Result = "";
      var CHUNK_SIZE = 32768;
      var LRef$7 = undefined;
      if (TAllocation.GetHandle(Self)) {
         LRef$7 = TAllocation.GetHandle(Self);
         var c = [];
    for (var i=0; i < (LRef$7).length; i += CHUNK_SIZE) {
      c.push(String.fromCharCode.apply(null, (LRef$7).subarray(i, i + CHUNK_SIZE)));
    }
    Result = c.join("");
      }
      return Result
   }
   /// function TBinaryData.ToTypedArray() : TMemoryHandle
   ///  [line: 619, column: 22, file: System.Memory.Buffer]
   ,ToTypedArray:function(Self) {
      var Result = undefined;
      var LLen$8 = 0,
         LTemp$9 = null;
      LLen$8 = TAllocation.GetSize$3(Self);
      if (LLen$8>0) {
         LTemp$9 = Self.FDataView.buffer.slice(0,LLen$8);
         Result = new Uint8Array(LTemp$9);
      }
      return Result
   }
   /// procedure TBinaryData.WriteBinaryData(const Offset: Integer; const Data: TBinaryData)
   ///  [line: 882, column: 23, file: System.Memory.Buffer]
   ,WriteBinaryData:function(Self, Offset$22, Data$30) {
      var LGrowth = 0;
      if (Data$30!==null) {
         if (TAllocation.GetSize$3(Data$30)>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$22)) {
               LGrowth = 0;
               if (Offset$22+TAllocation.GetSize$3(Data$30)>TAllocation.GetSize$3(Self)-1) {
                  LGrowth = Offset$22+TAllocation.GetSize$3(Data$30)-TAllocation.GetSize$3(Self);
               }
               if (LGrowth>0) {
                  TAllocation.Grow(Self,LGrowth);
               }
               TMarshal.Move$1(TMarshal,TAllocation.GetHandle(Data$30),0,TAllocation.GetHandle(Self),0,TAllocation.GetSize$3(Data$30));
            } else {
               throw EW3Exception.CreateFmt($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$22]);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, invalid source buffer [nil] error");
      }
   }
   /// procedure TBinaryData.WriteBool(const Offset: Integer; const Data: Boolean)
   ///  [line: 969, column: 23, file: System.Memory.Buffer]
   ,WriteBool:function(Self, Offset$23, Data$31) {
      var LGrowth$1 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$23)) {
         LGrowth$1 = 0;
         if (Offset$23+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$1 = Offset$23+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$1>0) {
            TAllocation.Grow(Self,LGrowth$1);
         }
         if (Data$31) {
            Self.FDataView.setUint8(Offset$23,1);
         } else {
            Self.FDataView.setUint8(Offset$23,0);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write boolean failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$23]);
      }
   }
   /// procedure TBinaryData.WriteBuffer(const Offset: Integer; const Data: TMemoryHandle)
   ///  [line: 906, column: 23, file: System.Memory.Buffer]
   ,WriteBuffer$2:function(Self, Offset$24, Data$32) {
      var LBuffer$1 = null,
         LGrowth$2 = 0;
      if (Data$32) {
         if (Data$32.buffer) {
            LBuffer$1 = Data$32.buffer;
            if (LBuffer$1.byteLength>0) {
               if (TBinaryData.OffsetInRange(Self,Offset$24)) {
                  LGrowth$2 = 0;
                  if (Offset$24+Data$32.length>TAllocation.GetSize$3(Self)-1) {
                     LGrowth$2 = Offset$24+Data$32.length-TAllocation.GetSize$3(Self);
                  }
                  if (LGrowth$2>0) {
                     TAllocation.Grow(Self,LGrowth$2);
                  }
                  TMarshal.Move$1(TMarshal,Data$32,0,TAllocation.GetHandle(Self),Offset$24,parseInt(TAllocation.GetHandle(Self).length,10));
               } else {
                  throw EW3Exception.CreateFmt($New(EBinaryData),"Write typed-handle failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$24]);
               }
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Write failed, invalid handle or unassigned buffer");
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, source handle was nil error");
      }
   }
   /// procedure TBinaryData.WriteBytes(const Offset: Integer; const Data: TByteArray)
   ///  [line: 863, column: 23, file: System.Memory.Buffer]
   ,WriteBytes:function(Self, Offset$25, Data$33) {
      var LGrowth$3 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$25)) {
         if (Data$33.length>0) {
            LGrowth$3 = 0;
            if (Offset$25+Data$33.length>TAllocation.GetSize$3(Self)-1) {
               LGrowth$3 = Offset$25+Data$33.length-TAllocation.GetSize$3(Self);
            }
            if (LGrowth$3>0) {
               TAllocation.Grow(Self,LGrowth$3);
            }
            TAllocation.GetHandle(Self).set(Data$33,Offset$25);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write bytearray failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$25]);
      }
   }
   /// procedure TBinaryData.WriteFloat32(const Offset: Integer; const Data: float32)
   ///  [line: 989, column: 23, file: System.Memory.Buffer]
   ,WriteFloat32:function(Self, Offset$26, Data$34) {
      var LGrowth$4 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$26)) {
         LGrowth$4 = 0;
         if (Offset$26+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$4 = Offset$26+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$4>0) {
            TAllocation.Grow(Self,LGrowth$4);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat32(Offset$26,Data$34);
               break;
            case 1 :
               Self.FDataView.setFloat32(Offset$26,Data$34,true);
               break;
            case 2 :
               Self.FDataView.setFloat32(Offset$26,Data$34,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$26]);
      }
   }
   /// procedure TBinaryData.WriteFloat64(const Offset: Integer; const Data: float64)
   ///  [line: 1012, column: 23, file: System.Memory.Buffer]
   ,WriteFloat64:function(Self, Offset$27, Data$35) {
      var LGrowth$5 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$27)) {
         LGrowth$5 = 0;
         if (Offset$27+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$5 = Offset$27+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$5>0) {
            TAllocation.Grow(Self,LGrowth$5);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat64(Offset$27,Number(Data$35));
               break;
            case 1 :
               Self.FDataView.setFloat64(Offset$27,Number(Data$35),true);
               break;
            case 2 :
               Self.FDataView.setFloat64(Offset$27,Number(Data$35),false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$27]);
      }
   }
   /// procedure TBinaryData.WriteInt16(const Offset: Integer; const Data: SmallInt)
   ///  [line: 1033, column: 23, file: System.Memory.Buffer]
   ,WriteInt16:function(Self, Offset$28, Data$36) {
      var LGrowth$6 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$28)) {
         LGrowth$6 = 0;
         if (Offset$28+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$6 = Offset$28+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$6>0) {
            TAllocation.Grow(Self,LGrowth$6);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt16(Offset$28,Data$36);
               break;
            case 1 :
               Self.FDataView.setInt16(Offset$28,Data$36,true);
               break;
            case 2 :
               Self.FDataView.setInt16(Offset$28,Data$36,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write int16 failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$28]);
      }
   }
   /// procedure TBinaryData.WriteInt32(const Offset: Integer; const Data: Integer)
   ///  [line: 1096, column: 23, file: System.Memory.Buffer]
   ,WriteInt32:function(Self, Offset$29, Data$37) {
      var LGrowth$7 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$29)) {
         LGrowth$7 = 0;
         if (Offset$29+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$7 = Offset$29+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$7>0) {
            TAllocation.Grow(Self,LGrowth$7);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt32(Offset$29,Data$37);
               break;
            case 1 :
               Self.FDataView.setInt32(Offset$29,Data$37,true);
               break;
            case 2 :
               Self.FDataView.setInt32(Offset$29,Data$37,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write integer failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$29]);
      }
   }
   /// procedure TBinaryData.WriteLong(const Offset: Integer; const Data: Longword)
   ///  [line: 1075, column: 23, file: System.Memory.Buffer]
   ,WriteLong:function(Self, Offset$30, Data$38) {
      var LGrowth$8 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$30)) {
         LGrowth$8 = 0;
         if (Offset$30+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$8 = Offset$30+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$8>0) {
            TAllocation.Grow(Self,LGrowth$8);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint32(Offset$30,Data$38);
               break;
            case 1 :
               Self.FDataView.setUint32(Offset$30,Data$38,true);
               break;
            case 2 :
               Self.FDataView.setUint32(Offset$30,Data$38,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write longword failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$30]);
      }
   }
   /// procedure TBinaryData.WriteStr(const Offset: Integer; const Data: String)
   ///  [line: 943, column: 23, file: System.Memory.Buffer]
   ,WriteStr$1:function(Self, Offset$31, Data$39) {
      var LTemp$10 = [],
         LGrowth$9 = 0,
         x$13 = 0;
      if (Data$39.length>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$31)) {
            LTemp$10 = TString.EncodeUTF8(TString,Data$39);
            LGrowth$9 = 0;
            if (Offset$31+LTemp$10.length>TAllocation.GetSize$3(Self)-1) {
               LGrowth$9 = Offset$31+LTemp$10.length-TAllocation.GetSize$3(Self);
            }
            if (LGrowth$9>0) {
               TAllocation.Grow(Self,LGrowth$9);
            }
            var $temp28;
            for(x$13=0,$temp28=LTemp$10.length;x$13<$temp28;x$13++) {
               Self.FDataView.setUint8(Offset$31+x$13,LTemp$10[x$13]);
            }
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$31]);
         }
      }
   }
   /// procedure TBinaryData.WriteWord(const Offset: Integer; const Data: Word)
   ///  [line: 1054, column: 23, file: System.Memory.Buffer]
   ,WriteWord$1:function(Self, Offset$32, Data$40) {
      var LGrowth$10 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$32)) {
         LGrowth$10 = 0;
         if (Offset$32+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$10 = Offset$32+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$10>0) {
            TAllocation.Grow(Self,LGrowth$10);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint16(Offset$32,Data$40);
               break;
            case 1 :
               Self.FDataView.setUint16(Offset$32,Data$40,true);
               break;
            case 2 :
               Self.FDataView.setUint16(Offset$32,Data$40,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write word [uint16] failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$32]);
      }
   }
   ,Destroy:TAllocation.Destroy
   ,Create$15:TAllocation.Create$15
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
};
TBinaryData.$Intf={
   IBinaryDataWriteAccess:[TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataImport:[TBinaryData.FromBase64]
   ,IBinaryDataReadAccess:[TBinaryData.Copy$1,TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes]
   ,IBinaryDataReadWriteAccess:[TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes,TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataBitAccess:[TBinaryData.GetBitCount,TBinaryData.GetBit$1,TBinaryData.SetBit$1]
   ,IBinaryDataExport:[TBinaryData.Copy$1,TBinaryData.ToBase64,TBinaryData.ToString$2,TBinaryData.ToTypedArray,TBinaryData.ToBytes,TBinaryData.ToHexDump,TBinaryData.ToStream,TBinaryData.Clone]
   ,IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$1,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
   ,IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
}
/// EBinaryData = class (EW3Exception)
///  [line: 157, column: 3, file: System.Memory.Buffer]
var EBinaryData = {
   $ClassName:"EBinaryData",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TBitAccess = class (TObject)
///  [line: 20, column: 3, file: System.Types.Bits]
var TBitAccess = {
   $ClassName:"TBitAccess",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TBitAccess.Get(const index: Integer; const Value: Byte) : Boolean
   ///  [line: 114, column: 27, file: System.Types.Bits]
   ,Get:function(Self, index$1, Value$46) {
      var Result = false;
      var mMask = 0;
      if (index$1>=0&&index$1<8) {
         mMask = 1<<index$1;
         Result = ((Value$46&mMask)!=0);
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),"Invalid bit index, expected 0..7 not %d",[index$1]);
      }
      return Result
   }
   /// function TBitAccess.Set(const Index: Integer; const Value: Byte; const Data: Boolean) : Byte
   ///  [line: 127, column: 27, file: System.Types.Bits]
   ,Set$4:function(Self, Index$4, Value$47, Data$41) {
      var Result = 0;
      var mSet = false;
      var mMask$1 = 0;
      Result = Value$47;
      if (Index$4>=0&&Index$4<8) {
         mMask$1 = 1<<Index$4;
         mSet = ((Value$47&mMask$1)!=0);
         if (mSet!=Data$41) {
            if (Data$41) {
               Result = Result|mMask$1;
            } else {
               Result = (Result&(~mMask$1));
            }
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
var CNT_BitBuffer_ByteTable = [0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8];
/// TStream = class (TDataTypeConverter)
///  [line: 80, column: 3, file: System.Streams]
var TStream = {
   $ClassName:"TStream",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
   }
   /// function TStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ///  [line: 470, column: 18, file: System.Streams]
   ,CopyFrom:function(Self, Source$3, Count$16) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.DataGetSize() : Integer
   ///  [line: 436, column: 18, file: System.Streams]
   ,DataGetSize:function(Self) {
      return TStream.GetSize$(Self);
   }
   /// function TStream.DataOffset() : Integer
   ///  [line: 430, column: 18, file: System.Streams]
   ,DataOffset:function(Self) {
      return TStream.GetPosition$(Self);
   }
   /// function TStream.DataRead(const Offset: Integer; const ByteCount: Integer) : TByteArray
   ///  [line: 442, column: 19, file: System.Streams]
   ,DataRead:function(Self, Offset$33, ByteCount$1) {
      return TStream.ReadBuffer$(Self,Offset$33,ByteCount$1);
   }
   /// procedure TStream.DataWrite(const Offset: Integer; const Bytes: TByteArray)
   ///  [line: 448, column: 19, file: System.Streams]
   ,DataWrite:function(Self, Offset$34, Bytes$5) {
      TStream.WriteBuffer$(Self,Bytes$5,Offset$34);
   }
   /// function TStream.GetPosition() : Integer
   ///  [line: 475, column: 18, file: System.Streams]
   ,GetPosition:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.GetSize() : Integer
   ///  [line: 495, column: 18, file: System.Streams]
   ,GetSize:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Read(const Count: Integer) : TByteArray
   ///  [line: 453, column: 18, file: System.Streams]
   ,Read:function(Self, Count$17) {
      return TStream.ReadBuffer$(Self,TStream.GetPosition$(Self),Count$17);
   }
   /// function TStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ///  [line: 505, column: 18, file: System.Streams]
   ,ReadBuffer:function(Self, Offset$35, Count$18) {
      var Result = [];
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ///  [line: 490, column: 18, file: System.Streams]
   ,Seek:function(Self, Offset$36, Origin) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// procedure TStream.SetPosition(NewPosition: Integer)
   ///  [line: 480, column: 19, file: System.Streams]
   ,SetPosition:function(Self, NewPosition) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   /// procedure TStream.SetSize(NewSize: Integer)
   ///  [line: 485, column: 19, file: System.Streams]
   ,SetSize:function(Self, NewSize$2) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   /// function TStream.Skip(Amount: Integer) : Integer
   ///  [line: 500, column: 18, file: System.Streams]
   ,Skip:function(Self, Amount) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Write(const Buffer: TByteArray) : Integer
   ///  [line: 458, column: 18, file: System.Streams]
   ,Write$1:function(Self, Buffer$4) {
      var Result = 0;
      TStream.WriteBuffer$(Self,Buffer$4,TStream.GetPosition$(Self));
      Result = Buffer$4.length;
      return Result
   }
   /// procedure TStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ///  [line: 510, column: 19, file: System.Streams]
   ,WriteBuffer:function(Self, Buffer$5, Offset$37) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
};
TStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TMemoryStream = class (TStream)
///  [line: 122, column: 3, file: System.Streams]
var TMemoryStream = {
   $ClassName:"TMemoryStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FBuffer$1 = null;
      $.FPos = 0;
   }
   /// function TMemoryStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ///  [line: 258, column: 24, file: System.Streams]
   ,CopyFrom:function(Self, Source$4, Count$19) {
      var Result = 0;
      var LData$1 = [];
      LData$1 = TStream.ReadBuffer$(Source$4,TStream.GetPosition$(Source$4),Count$19);
      TStream.WriteBuffer$(Self,LData$1,TStream.GetPosition$(Self));
      Result = LData$1.length;
      return Result
   }
   /// constructor TMemoryStream.Create()
   ///  [line: 246, column: 27, file: System.Streams]
   ,Create$15:function(Self) {
      TDataTypeConverter.Create$15(Self);
      Self.FBuffer$1 = TDataTypeConverter.Create$15$($New(TAllocation));
      return Self
   }
   /// destructor TMemoryStream.Destroy()
   ///  [line: 252, column: 26, file: System.Streams]
   ,Destroy:function(Self) {
      TObject.Free(Self.FBuffer$1);
      TDataTypeConverter.Destroy(Self);
   }
   /// function TMemoryStream.GetPosition() : Integer
   ///  [line: 265, column: 24, file: System.Streams]
   ,GetPosition:function(Self) {
      return Self.FPos;
   }
   /// function TMemoryStream.GetSize() : Integer
   ///  [line: 340, column: 24, file: System.Streams]
   ,GetSize:function(Self) {
      return TAllocation.GetSize$3(Self.FBuffer$1);
   }
   /// function TMemoryStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ///  [line: 358, column: 24, file: System.Streams]
   ,ReadBuffer:function(Self, Offset$38, Count$20) {
      var Result = [];
      var LLen$9 = 0,
         LBytesToMove = 0,
         LTemp$11 = null;
      LLen$9 = 0;
      if (TStream.GetPosition$(Self)<TStream.GetSize$(Self)) {
         LLen$9 = TStream.GetSize$(Self)-TStream.GetPosition$(Self);
      }
      if (LLen$9>0) {
         try {
            LBytesToMove = Count$20;
            if (LBytesToMove>LLen$9) {
               LBytesToMove = LLen$9;
            }
            LTemp$11 = new Uint8Array(LBytesToMove);
            TMarshal.Move$1(TMarshal,TAllocation.GetHandle(Self.FBuffer$1),Offset$38,LTemp$11,0,LBytesToMove);
            Result = TDataTypeConverter.TypedArrayToBytes(Self,LTemp$11);
            TStream.SetPosition$(Self,Offset$38+Result.length);
         } catch ($e) {
            var e$4 = $W($e);
            throw EW3Exception.CreateFmt($New(EStreamReadError),$R[8],[e$4.FMessage]);
         }
      }
      return Result
   }
   /// function TMemoryStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ///  [line: 312, column: 24, file: System.Streams]
   ,Seek:function(Self, Offset$39, Origin$1) {
      var Result = 0;
      var LSize$7 = 0;
      LSize$7 = TStream.GetSize$(Self);
      if (LSize$7>0) {
         switch (Origin$1) {
            case 0 :
               if (Offset$39>-1) {
                  TStream.SetPosition$(Self,Offset$39);
                  Result = Offset$39;
               }
               break;
            case 1 :
               Result = TInteger.EnsureRange((TStream.GetPosition$(Self)+Offset$39),0,LSize$7);
               TStream.SetPosition$(Self,Result);
               break;
            case 2 :
               Result = TInteger.EnsureRange((TStream.GetSize$(Self)-Math.abs(Offset$39)),0,LSize$7);
               TStream.SetPosition$(Self,Result);
               break;
         }
      }
      return Result
   }
   /// procedure TMemoryStream.SetPosition(NewPosition: Integer)
   ///  [line: 270, column: 25, file: System.Streams]
   ,SetPosition:function(Self, NewPosition$1) {
      var LSize$8 = 0;
      LSize$8 = TStream.GetSize$(Self);
      if (LSize$8>0) {
         Self.FPos = TInteger.EnsureRange(NewPosition$1,0,LSize$8);
      }
   }
   /// procedure TMemoryStream.SetSize(NewSize: Integer)
   ///  [line: 277, column: 25, file: System.Streams]
   ,SetSize:function(Self, NewSize$3) {
      var LSize$9 = 0,
         LDiff = 0,
         LDiff$1 = 0;
      LSize$9 = TStream.GetSize$(Self);
      if (NewSize$3>0) {
         if (NewSize$3==LSize$9) {
            return;
         }
         if (NewSize$3>LSize$9) {
            LDiff = NewSize$3-LSize$9;
            if (TAllocation.GetSize$3(Self.FBuffer$1)+LDiff>0) {
               TAllocation.Grow(Self.FBuffer$1,LDiff);
            } else {
               TAllocation.Release$1(Self.FBuffer$1);
            }
         } else {
            LDiff$1 = LSize$9-NewSize$3;
            if (TAllocation.GetSize$3(Self.FBuffer$1)-LDiff$1>0) {
               TAllocation.Shrink(Self.FBuffer$1,LDiff$1);
            } else {
               TAllocation.Release$1(Self.FBuffer$1);
            }
         }
      } else {
         TAllocation.Release$1(Self.FBuffer$1);
      }
      Self.FPos = TInteger.EnsureRange(Self.FPos,0,TStream.GetSize$(Self));
   }
   /// function TMemoryStream.Skip(Amount: Integer) : Integer
   ///  [line: 345, column: 24, file: System.Streams]
   ,Skip:function(Self, Amount$1) {
      var Result = 0;
      var LSize$10 = 0,
         LTotal$1 = 0;
      LSize$10 = TStream.GetSize$(Self);
      if (LSize$10>0) {
         LTotal$1 = TStream.GetPosition$(Self)+Amount$1;
         if (LTotal$1>LSize$10) {
            LTotal$1 = LSize$10-LTotal$1;
         }
         (Self.FPos+= LTotal$1);
         Result = LTotal$1;
      }
      return Result
   }
   /// procedure TMemoryStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ///  [line: 386, column: 25, file: System.Streams]
   ,WriteBuffer:function(Self, Buffer$6, Offset$40) {
      var mData = undefined,
         mData$1 = undefined;
      try {
         if (TAllocation.a$28(Self.FBuffer$1)&&Offset$40<1) {
            TAllocation.Allocate(Self.FBuffer$1,Buffer$6.length);
            mData = TDataTypeConverter.BytesToTypedArray(Self,Buffer$6);
            TMarshal.Move$1(TMarshal,mData,0,TAllocation.GetHandle(Self.FBuffer$1),0,Buffer$6.length);
         } else {
            if (TStream.GetPosition$(Self)==TStream.GetSize$(Self)) {
               TAllocation.Grow(Self.FBuffer$1,Buffer$6.length);
               mData$1 = TDataTypeConverter.BytesToTypedArray(Self,Buffer$6);
               TMarshal.Move$1(TMarshal,mData$1,0,TAllocation.GetHandle(Self.FBuffer$1),Offset$40,Buffer$6.length);
            } else {
               TMarshal.Move$1(TMarshal,TDataTypeConverter.BytesToTypedArray(Self,Buffer$6),0,TAllocation.GetHandle(Self.FBuffer$1),Offset$40,Buffer$6.length);
            }
         }
         TStream.SetPosition$(Self,Offset$40+Buffer$6.length);
      } catch ($e) {
         var e$5 = $W($e);
         throw EW3Exception.CreateFmt($New(EStreamWriteError),$R[7],[e$5.FMessage]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
};
TMemoryStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TStreamSeekOrigin enumeration
///  [line: 48, column: 3, file: System.Streams]
var TStreamSeekOrigin = [ "soFromBeginning", "soFromCurrent", "soFromEnd" ];
/// TCustomFileStream = class (TStream)
///  [line: 151, column: 3, file: System.Streams]
var TCustomFileStream = {
   $ClassName:"TCustomFileStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FAccess$1 = 0;
      $.FFilename = "";
   }
   /// procedure TCustomFileStream.SetAccessMode(const Value: TFileAccessMode)
   ///  [line: 214, column: 29, file: System.Streams]
   ,SetAccessMode:function(Self, Value$48) {
      Self.FAccess$1 = Value$48;
   }
   /// procedure TCustomFileStream.SetFilename(const Value: String)
   ///  [line: 219, column: 29, file: System.Streams]
   ,SetFilename:function(Self, Value$49) {
      Self.FFilename = Value$49;
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,CopyFrom:TStream.CopyFrom
   ,GetPosition:TStream.GetPosition
   ,GetSize:TStream.GetSize
   ,ReadBuffer:TStream.ReadBuffer
   ,Seek:TStream.Seek
   ,SetPosition:TStream.SetPosition
   ,SetSize:TStream.SetSize
   ,Skip:TStream.Skip
   ,WriteBuffer:TStream.WriteBuffer
   ,Create$32$:function($){return $.ClassType.Create$32.apply($.ClassType, arguments)}
};
TCustomFileStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// EStream = class (EW3Exception)
///  [line: 56, column: 3, file: System.Streams]
var EStream = {
   $ClassName:"EStream",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamWriteError = class (EStream)
///  [line: 58, column: 3, file: System.Streams]
var EStreamWriteError = {
   $ClassName:"EStreamWriteError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamReadError = class (EStream)
///  [line: 57, column: 3, file: System.Streams]
var EStreamReadError = {
   $ClassName:"EStreamReadError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamNotImplemented = class (EStream)
///  [line: 59, column: 3, file: System.Streams]
var EStreamNotImplemented = {
   $ClassName:"EStreamNotImplemented",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TAllocationOptions = class (TW3OwnedObject)
///  [line: 100, column: 3, file: System.Memory.Allocation]
var TAllocationOptions = {
   $ClassName:"TAllocationOptions",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FCacheSize = 0;
      $.FUseCache = false;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 113, column: 41, file: System.Memory.Allocation]
   ,a$27:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TAllocation);
   }
   /// function TAllocationOptions.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 134, column: 29, file: System.Memory.Allocation]
   ,AcceptOwner:function(Self, CandidateObject$1) {
      return CandidateObject$1!==null&&$Is(CandidateObject$1,TAllocation);
   }
   /// constructor TAllocationOptions.Create(const AOwner: TAllocation)
   ///  [line: 127, column: 32, file: System.Memory.Allocation]
   ,Create$36:function(Self, AOwner$2) {
      TW3OwnedObject.Create$16(Self,AOwner$2);
      Self.FCacheSize = 4096;
      Self.FUseCache = true;
      return Self
   }
   /// function TAllocationOptions.GetCacheFree() : Integer
   ///  [line: 140, column: 29, file: System.Memory.Allocation]
   ,GetCacheFree:function(Self) {
      return Self.FCacheSize-TAllocationOptions.GetCacheUsed(Self);
   }
   /// function TAllocationOptions.GetCacheUsed() : Integer
   ///  [line: 145, column: 29, file: System.Memory.Allocation]
   ,GetCacheUsed:function(Self) {
      var Result = 0;
      if (Self.FUseCache) {
         Result = parseInt((Self.FCacheSize-(TAllocation.GetHandle(TAllocationOptions.a$27(Self)).length-TAllocation.GetSize$3(TAllocationOptions.a$27(Self)))),10);
      }
      return Result
   }
   /// procedure TAllocationOptions.SetCacheSize(const NewCacheSize: Integer)
   ///  [line: 156, column: 30, file: System.Memory.Allocation]
   ,SetCacheSize:function(Self, NewCacheSize) {
      Self.FCacheSize = (NewCacheSize<1024)?1024:(NewCacheSize>1024000)?1024000:NewCacheSize;
   }
   /// procedure TAllocationOptions.SetUseCache(const NewUseValue: Boolean)
   ///  [line: 151, column: 30, file: System.Memory.Allocation]
   ,SetUseCache:function(Self, NewUseValue) {
      Self.FUseCache = NewUseValue;
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
};
TAllocationOptions.$Intf={
   IW3OwnedObjectAccess:[TAllocationOptions.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
function a$456(Self) {
   return ((!Self[0]())?true:false);
}/// EAllocation = class (EW3Exception)
///  [line: 22, column: 3, file: System.Memory.Allocation]
var EAllocation = {
   $ClassName:"EAllocation",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3CustomReader = class (TDataTypeConverter)
///  [line: 38, column: 3, file: System.Reader]
var TW3CustomReader = {
   $ClassName:"TW3CustomReader",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess$2 = null;
      $.FOffset$2 = $.FTotalSize$1 = 0;
      $.FOptions$2 = [0];
   }
   /// constructor TW3CustomReader.Create(const Access: IBinaryTransport)
   ///  [line: 113, column: 29, file: System.Reader]
   ,Create$39:function(Self, Access$4) {
      TDataTypeConverter.Create$15(Self);
      Self.FOptions$2 = [1];
      Self.FAccess$2 = Access$4;
      Self.FOffset$2 = Self.FAccess$2[0]();
      Self.FTotalSize$1 = Self.FAccess$2[1]();
      return Self
   }
   /// function TW3CustomReader.GetReadOffset() : Integer
   ///  [line: 156, column: 26, file: System.Reader]
   ,GetReadOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$2,0,0,1)) {
         Result = Self.FOffset$2;
      } else {
         Result = Self.FAccess$2[0]();
      }
      return Result
   }
   /// function TW3CustomReader.GetTotalSize() : Integer
   ///  [line: 148, column: 26, file: System.Reader]
   ,GetTotalSize$2:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$2,0,0,1)) {
         Result = Self.FTotalSize$1;
      } else {
         Result = Self.FAccess$2[1]();
      }
      return Result
   }
   /// function TW3CustomReader.QueryBreachOfBoundary(const NumberOfBytes: Integer) : Boolean
   ///  [line: 164, column: 26, file: System.Reader]
   ,QueryBreachOfBoundary$1:function(Self, NumberOfBytes$3) {
      return TW3CustomReader.GetTotalSize$2(Self)-TW3CustomReader.GetReadOffset(Self)<NumberOfBytes$3;
   }
   /// function TW3CustomReader.Read(const BytesToRead: Integer) : TByteArray
   ///  [line: 169, column: 26, file: System.Reader]
   ,Read$1:function(Self, BytesToRead$1) {
      var Result = [];
      if (BytesToRead$1>0) {
         Result = Self.FAccess$2[2](TW3CustomReader.GetReadOffset(Self),BytesToRead$1);
         if ($SetIn(Self.FOptions$2,0,0,1)) {
            (Self.FOffset$2+= Result.length);
         }
      } else {
         throw EW3Exception.Create$27($New(EW3ReadError),"TW3CustomReader.Read",Self,("Invalid read length ("+BytesToRead$1.toString()+")"));
      }
      return Result
   }
   /// function TW3CustomReader.ReadBoolean() : Boolean
   ///  [line: 184, column: 26, file: System.Reader]
   ,ReadBoolean:function(Self) {
      var Result = false;
      var LBytesToRead = 0;
      LBytesToRead = TDataTypeConverter.SizeOfType(TDataTypeConverter,1);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead)) {
         throw EW3Exception.Create$27($New(EW3ReadError),"TW3CustomReader.ReadBoolean",Self,Format($R[10],[LBytesToRead]));
      } else {
         Result = TDataTypeConverter.BytesToBoolean(Self,TW3CustomReader.Read$1(Self,LBytesToRead));
      }
      return Result
   }
   /// function TW3CustomReader.ReadDouble() : double
   ///  [line: 286, column: 26, file: System.Reader]
   ,ReadDouble:function(Self) {
      var Result = undefined;
      var LBytesToRead$1 = 0;
      LBytesToRead$1 = TDataTypeConverter.SizeOfType(TDataTypeConverter,9);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$1)) {
         throw EW3Exception.Create$27($New(EW3ReadError),"TW3CustomReader.ReadDouble",Self,Format($R[10],[LBytesToRead$1]));
      } else {
         Result = TDataTypeConverter.BytesToFloat64(Self,TW3CustomReader.Read$1(Self,LBytesToRead$1));
      }
      return Result
   }
   /// function TW3CustomReader.ReadInteger() : Integer
   ///  [line: 246, column: 26, file: System.Reader]
   ,ReadInteger:function(Self) {
      var Result = 0;
      var LBytesToRead$2 = 0;
      LBytesToRead$2 = TDataTypeConverter.SizeOfType(TDataTypeConverter,7);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$2)) {
         throw EW3Exception.Create$27($New(EW3ReadError),"TW3CustomReader.ReadInteger",Self,Format($R[10],[LBytesToRead$2]));
      } else {
         Result = TDataTypeConverter.BytesToInt32(Self,TW3CustomReader.Read$1(Self,LBytesToRead$2));
      }
      return Result
   }
   /// function TW3CustomReader.ReadStr(const BytesToRead: Integer) : String
   ///  [line: 306, column: 26, file: System.Reader]
   ,ReadStr:function(Self, BytesToRead$2) {
      var Result = "";
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,BytesToRead$2)) {
         throw EW3Exception.Create$27($New(EW3ReadError),"TW3CustomReader.ReadStr",Self,Format($R[10],[BytesToRead$2]));
      } else if (BytesToRead$2>0) {
         Result = TDataTypeConverter.BytesToString(Self,TW3CustomReader.Read$1(Self,BytesToRead$2));
      }
      return Result
   }
   /// function TW3CustomReader.ReadString() : String
   ///  [line: 317, column: 26, file: System.Reader]
   ,ReadString$1:function(Self) {
      var Result = "";
      var LSignature$1 = 0,
         LLength = 0;
      LSignature$1 = TW3CustomReader.ReadInteger(Self);
      if (LSignature$1==3131756270) {
         LLength = TW3CustomReader.ReadInteger(Self);
         if (LLength>0) {
            Result = TW3CustomReader.ReadStr(Self,LLength);
         }
      } else {
         throw EW3Exception.Create$27($New(EW3ReadError),"TW3CustomReader.ReadString",Self,Format($R[11],["string"]));
      }
      return Result
   }
   /// procedure TW3CustomReader.SetUpdateOption(const NewUpdateMode: TW3ReaderProviderOptions)
   ///  [line: 122, column: 27, file: System.Reader]
   ,SetUpdateOption:function(Self, NewUpdateMode) {
      Self.FOptions$2 = NewUpdateMode.slice(0);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// TReader = class (TW3CustomReader)
///  [line: 82, column: 3, file: System.Reader]
var TReader = {
   $ClassName:"TReader",$Parent:TW3CustomReader
   ,$Init:function ($) {
      TW3CustomReader.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// TStreamReader = class (TReader)
///  [line: 86, column: 3, file: System.Reader]
var TStreamReader = {
   $ClassName:"TStreamReader",$Parent:TReader
   ,$Init:function ($) {
      TReader.$Init($);
   }
   /// constructor TStreamReader.Create(const Stream: TStream)
   ///  [line: 99, column: 27, file: System.Reader]
   ,Create$40:function(Self, Stream$2) {
      TW3CustomReader.Create$39(Self,$AsIntf(Stream$2,"IBinaryTransport"));
      if (!$SetIn(Self.FOptions$2,0,0,1)) {
         TW3CustomReader.SetUpdateOption(Self,[1]);
      }
      return Self
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// EW3ReadError = class (EW3Exception)
///  [line: 34, column: 3, file: System.Reader]
var EW3ReadError = {
   $ClassName:"EW3ReadError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3CustomWriter = class (TDataTypeConverter)
///  [line: 39, column: 3, file: System.Writer]
var TW3CustomWriter = {
   $ClassName:"TW3CustomWriter",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess = null;
      $.FOffset = $.FTotalSize = 0;
      $.FOptions = [0];
   }
   /// constructor TW3CustomWriter.Create(const Access: IBinaryTransport)
   ///  [line: 102, column: 29, file: System.Writer]
   ,Create$29:function(Self, Access$5) {
      TDataTypeConverter.Create$15(Self);
      Self.FAccess = Access$5;
      Self.FOffset = Self.FAccess[0]();
      Self.FTotalSize = Self.FAccess[1]();
      Self.FOptions = [3];
      return Self
   }
   /// function TW3CustomWriter.GetOffset() : Integer
   ///  [line: 125, column: 26, file: System.Writer]
   ,GetOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions,0,0,2)) {
         Result = Self.FOffset;
      } else {
         Result = Self.FAccess[0]();
      }
      return Result
   }
   /// function TW3CustomWriter.GetTotalFree() : Integer
   ///  [line: 146, column: 26, file: System.Writer]
   ,GetTotalFree:function(Self) {
      return Self.FAccess[1]()-TW3CustomWriter.GetOffset(Self);
   }
   /// function TW3CustomWriter.GetTotalSize() : Integer
   ///  [line: 116, column: 26, file: System.Writer]
   ,GetTotalSize:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions,0,0,2)) {
         Result = 2147483647;
      } else {
         Result = Self.FAccess[1]();
      }
      return Result
   }
   /// function TW3CustomWriter.QueryBreachOfBoundary(const BytesToFit: Integer) : Boolean
   ///  [line: 133, column: 26, file: System.Writer]
   ,QueryBreachOfBoundary:function(Self, BytesToFit) {
      var Result = false;
      if (BytesToFit>=1) {
         if ($SetIn(Self.FOptions,1,0,2)) {
            Result = false;
         } else {
            Result = TW3CustomWriter.GetTotalFree(Self)<BytesToFit;
         }
      }
      return Result
   }
   /// procedure TW3CustomWriter.SetAccessOptions(const NewOptions: TW3WriterOptions)
   ///  [line: 111, column: 27, file: System.Writer]
   ,SetAccessOptions:function(Self, NewOptions) {
      Self.FOptions = NewOptions.slice(0);
   }
   /// function TW3CustomWriter.Write(const Data: TByteArray) : Integer
   ///  [line: 151, column: 26, file: System.Writer]
   ,Write:function(Self, Data$42) {
      var Result = 0;
      var LBytesToWrite = 0,
         LBytesLeft = 0,
         LBytesMissing = 0;
      LBytesToWrite = Data$42.length;
      if (LBytesToWrite>0) {
         if ($SetIn(Self.FOptions,1,0,2)) {
            Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$42);
            if ($SetIn(Self.FOptions,0,0,2)) {
               (Self.FOffset+= Data$42.length);
            }
         } else {
            if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite)) {
               LBytesLeft = TW3CustomWriter.GetTotalSize(Self)-TW3CustomWriter.GetOffset(Self);
               LBytesMissing = Math.abs(LBytesLeft-LBytesToWrite);
               (LBytesToWrite-= LBytesMissing);
               $ArraySetLenC(Data$42,LBytesToWrite,function (){return 0});
            }
            if (LBytesToWrite>1) {
               Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$42);
               if ($SetIn(Self.FOptions,0,0,2)) {
                  (Self.FOffset+= Data$42.length);
               }
            } else {
               throw EW3Exception.Create$27($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[12],[Data$42.length]));
            }
         }
         Result = Data$42.length;
      } else {
         throw EW3Exception.Create$27($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[14],[LBytesToWrite]));
      }
      return Result
   }
   /// procedure TW3CustomWriter.WriteBoolean(const Value: Boolean)
   ///  [line: 202, column: 27, file: System.Writer]
   ,WriteBoolean:function(Self, Value$50) {
      var LBytesToWrite$1 = 0;
      LBytesToWrite$1 = TDataTypeConverter.SizeOfType(TDataTypeConverter,1);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$1)) {
         throw EW3Exception.Create$27($New(EW3WriteError),"TW3CustomWriter.WriteBoolean",Self,Format($R[12],[LBytesToWrite$1]));
      } else {
         TW3CustomWriter.Write(Self,TDataTypeConverter.BooleanToBytes(Self.ClassType,Value$50));
      }
   }
   /// procedure TW3CustomWriter.WriteDouble(const Value: double)
   ///  [line: 274, column: 27, file: System.Writer]
   ,WriteDouble:function(Self, Value$51) {
      var LBytesToWrite$2 = 0;
      LBytesToWrite$2 = TDataTypeConverter.SizeOfType(TDataTypeConverter,9);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$2)) {
         throw EW3Exception.Create$27($New(EW3WriteError),"TW3CustomWriter.WriteDouble",Self,Format($R[12],[LBytesToWrite$2]));
      } else {
         TW3CustomWriter.Write(Self,TDataTypeConverter.Float64ToBytes(Self,Value$51));
      }
   }
   /// procedure TW3CustomWriter.WriteInteger(const Value: Integer)
   ///  [line: 294, column: 27, file: System.Writer]
   ,WriteInteger:function(Self, Value$52) {
      var LBytesToWrite$3 = 0;
      LBytesToWrite$3 = TDataTypeConverter.SizeOfType(TDataTypeConverter,7);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$3)) {
         throw EW3Exception.Create$27($New(EW3WriteError),"TW3CustomWriter.WriteInteger",Self,Format($R[12],[LBytesToWrite$3]));
      } else {
         TW3CustomWriter.Write(Self,TDataTypeConverter.Int32ToBytes(Self,Value$52));
      }
   }
   /// procedure TW3CustomWriter.WriteString(const Value: String)
   ///  [line: 325, column: 27, file: System.Writer]
   ,WriteString:function(Self, Value$53) {
      var LTotal$2 = 0,
         LBytes$4 = [];
      LTotal$2 = TDataTypeConverter.SizeOfType(TDataTypeConverter,7);
      (LTotal$2+= TDataTypeConverter.SizeOfType(TDataTypeConverter,7));
      LBytes$4 = TDataTypeConverter.StringToBytes(Self,Value$53);
      (LTotal$2+= LBytes$4.length);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LTotal$2)) {
         throw EW3Exception.Create$27($New(EW3WriteError),"TW3CustomWriter.WriteString",Self,Format($R[12],[LTotal$2]));
      } else {
         try {
            TW3CustomWriter.WriteInteger(Self,3131756270);
            TW3CustomWriter.WriteInteger(Self,LBytes$4.length);
            if (LBytes$4.length>0) {
               TW3CustomWriter.Write(Self,LBytes$4);
            }
         } catch ($e) {
            var e$6 = $W($e);
            throw EW3Exception.Create$27($New(EW3WriteError),"TW3CustomWriter.WriteString",Self,e$6.FMessage);
         }
      }
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// TWriter = class (TW3CustomWriter)
///  [line: 76, column: 3, file: System.Writer]
var TWriter = {
   $ClassName:"TWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// TStreamWriter = class (TW3CustomWriter)
///  [line: 79, column: 3, file: System.Writer]
var TStreamWriter = {
   $ClassName:"TStreamWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   /// constructor TStreamWriter.Create(const Stream: TStream)
   ///  [line: 92, column: 27, file: System.Writer]
   ,Create$30:function(Self, Stream$3) {
      TW3CustomWriter.Create$29(Self,$AsIntf(Stream$3,"IBinaryTransport"));
      TW3CustomWriter.SetAccessOptions(Self,[3]);
      return Self
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
};
/// EW3WriteError = class (EW3Exception)
///  [line: 31, column: 3, file: System.Writer]
var EW3WriteError = {
   $ClassName:"EW3WriteError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TMessageFactory = class (TObject)
///  [line: 22, column: 3, file: ragnarok.messages.factory]
var TMessageFactory = {
   $ClassName:"TMessageFactory",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLUT$1 = null;
   }
   /// procedure TMessageFactory.Register(const MessageClass: TQTXMessageBaseClass)
   ///  [line: 54, column: 27, file: ragnarok.messages.factory]
   ,Register:function(Self, MessageClass$2) {
      if (MessageClass$2) {
         if (!TW3CustomDictionary.Contains(Self.FLUT$1,TObject.ClassName(MessageClass$2))) {
            TW3CustomDictionary.SetItem(Self.FLUT$1,TObject.ClassName(MessageClass$2),MessageClass$2);
         }
      }
   }
   /// function TMessageFactory.Build(MessageClassName: String; var Instance: TQTXBaseMessage) : Boolean
   ///  [line: 63, column: 26, file: ragnarok.messages.factory]
   ,Build:function(Self, MessageClassName, Instance$7) {
      var Result = false;
      var LClass = null;
      var LTemp$12;
      Instance$7.v = null;
      if (MessageClassName.length>0) {
         Result = TW3CustomDictionary.Contains(Self.FLUT$1,MessageClassName);
         if (Result) {
            LTemp$12 = TW3CustomDictionary.GetItem$2(Self.FLUT$1,MessageClassName);
            LClass = LTemp$12;
            Instance$7.v = TQTXBaseMessage.Create$70$($NewDyn(LClass,""));
         }
      }
      return Result
   }
   /// constructor TMessageFactory.Create()
   ///  [line: 41, column: 29, file: ragnarok.messages.factory]
   ,Create$101:function(Self) {
      TObject.Create(Self);
      Self.FLUT$1 = TW3CustomDictionary.Create$87($New(TW3VarDictionary));
      TMessageFactory.RegisterIntrinsic$(Self);
      return Self
   }
   /// destructor TMessageFactory.Destroy()
   ///  [line: 48, column: 28, file: ragnarok.messages.factory]
   ,Destroy:function(Self) {
      TObject.Free(Self.FLUT$1);
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,RegisterIntrinsic$:function($){return $.ClassType.RegisterIntrinsic($)}
};
/// TRagnarokServiceFactory = class (TMessageFactory)
///  [line: 43, column: 3, file: service.base]
var TRagnarokServiceFactory = {
   $ClassName:"TRagnarokServiceFactory",$Parent:TMessageFactory
   ,$Init:function ($) {
      TMessageFactory.$Init($);
   }
   /// procedure TRagnarokServiceFactory.RegisterIntrinsic()
   ///  [line: 100, column: 35, file: service.base]
   ,RegisterIntrinsic:function(Self) {
      /* null */
   }
   ,Destroy:TMessageFactory.Destroy
   ,RegisterIntrinsic$:function($){return $.ClassType.RegisterIntrinsic($)}
};
/// TRagnarokService = class (TObject)
///  [line: 48, column: 3, file: service.base]
var TRagnarokService = {
   $ClassName:"TRagnarokService",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnAfterServerStopped = null;
      $.OnBeforeServerStopped = null;
      $.OnAfterServerStarted = null;
      $.OnBeforeServerStarted = null;
      $.FDispatch = $.FFactory = $.FServer = null;
      $.FPort = 0;
   }
   /// procedure TRagnarokService.AfterServerStarted()
   ///  [line: 309, column: 28, file: service.base]
   ,AfterServerStarted:function(Self) {
      if (Self.OnAfterServerStarted) {
         Self.OnAfterServerStarted(Self);
      }
   }
   /// procedure TRagnarokService.AfterServerStopped()
   ///  [line: 321, column: 28, file: service.base]
   ,AfterServerStopped:function(Self) {
      if (Self.OnAfterServerStopped) {
         Self.OnAfterServerStopped(Self);
      }
   }
   /// procedure TRagnarokService.BeforeServerStarted()
   ///  [line: 303, column: 28, file: service.base]
   ,BeforeServerStarted:function(Self) {
      if (Self.OnBeforeServerStarted) {
         Self.OnBeforeServerStarted(Self);
      }
   }
   /// procedure TRagnarokService.BeforeServerStopped()
   ///  [line: 315, column: 28, file: service.base]
   ,BeforeServerStopped:function(Self) {
      if (Self.OnBeforeServerStopped) {
         Self.OnBeforeServerStopped(Self);
      }
   }
   /// constructor TRagnarokService.Create()
   ///  [line: 108, column: 30, file: service.base]
   ,Create$69:function(Self) {
      TObject.Create(Self);
      Self.FDispatch = TQTXMessageDispatcher.Create$97($New(TQTXMessageDispatcher));
      Self.FPort = 1881;
      return Self
   }
   /// destructor TRagnarokService.Destroy()
   ///  [line: 115, column: 29, file: service.base]
   ,Destroy:function(Self) {
      if (Self.FServer!==null) {
         TObject.Free(Self.FServer);
         Self.FServer = null;
      }
      TObject.Free(Self.FFactory);
      TObject.Free(Self.FDispatch);
      TObject.Destroy(Self);
   }
   /// procedure TRagnarokService.Dispatch(Socket: TNJWebSocketSocket; Message: TQTXBaseMessage)
   ///  [line: 138, column: 28, file: service.base]
   ,Dispatch:function(Self, Socket$1, Message$1) {
      /* null */
   }
   /// function TRagnarokService.GetActive() : Boolean
   ///  [line: 142, column: 27, file: service.base]
   ,GetActive$1:function(Self) {
      return (Self.FServer!==null)&&TNJCustomServer.GetActive$3(Self.FServer);
   }
   /// procedure TRagnarokService.HandleAfterServerStarted(Sender: TObject)
   ///  [line: 167, column: 28, file: service.base]
   ,HandleAfterServerStarted:function(Self, Sender) {
      TRagnarokService.AfterServerStarted$(Self);
   }
   /// procedure TRagnarokService.HandleAfterServerStopped(Sender: TObject)
   ///  [line: 177, column: 28, file: service.base]
   ,HandleAfterServerStopped:function(Self, Sender$1) {
      TRagnarokService.AfterServerStopped(Self);
   }
   /// procedure TRagnarokService.HandleBeforeServerStarted(Sender: TObject)
   ///  [line: 162, column: 28, file: service.base]
   ,HandleBeforeServerStarted:function(Self, Sender$2) {
      TRagnarokService.BeforeServerStarted(Self);
   }
   /// procedure TRagnarokService.HandleBeforeServerStopped(Sender: TObject)
   ///  [line: 172, column: 28, file: service.base]
   ,HandleBeforeServerStopped:function(Self, Sender$3) {
      TRagnarokService.BeforeServerStopped$(Self);
   }
   /// procedure TRagnarokService.HandleMessage(Sender: TObject; Socket: TNJWebSocketSocket; Info: TNJWebsocketMessage)
   ///  [line: 127, column: 28, file: service.base]
   ,HandleMessage:function(Self, Sender$4, Socket$2, Info$2) {
      var LInstance = {};
      LInstance.v = null;
      var LId = "";
      LId = TQTXBaseMessage.QueryIdentifier(TQTXBaseMessage,Info$2.wiText);
      if (TMessageFactory.Build(Self.FFactory,LId,LInstance)) {
         TRagnarokService.Dispatch$(Self,Socket$2,LInstance.v);
      } else {
         throw Exception.Create($New(Exception),"Unknown identifier ["+LId+"] or message format error");
      }
   }
   /// procedure TRagnarokService.SetPort(const NewPort: Integer)
   ///  [line: 147, column: 28, file: service.base]
   ,SetPort:function(Self, NewPort) {
      if (NewPort!=Self.FPort) {
         if (Self.FServer!==null) {
            if (TNJCustomServer.GetActive$3(Self.FServer)) {
               throw Exception.Create($New(Exception),"Port cannot be altered while server is active errror");
            } else {
               Self.FPort = NewPort;
            }
         } else {
            Self.FPort = NewPort;
         }
      }
   }
   /// procedure TRagnarokService.Start(const ServerType: TRagnarokServerType; const CB: TRagnarokServerStartCB)
   ///  [line: 182, column: 28, file: service.base]
   ,Start:function(Self, ServerType, CB$1) {
      var Err = null,
         Err$1 = null;
      if (TRagnarokService.GetActive$1(Self)) {
         Err = Exception.Create($New(Exception),"failed to start server, server is already active error");
         if (CB$1) {
            CB$1(Err);
            return;
         } else {
            throw Err;
         }
      }
      try {
         switch (ServerType) {
            case 0 :
               Self.FServer = TW3ErrorObject.Create$45$($New(TNJWebSocketServer));
               break;
            case 1 :
               Self.FServer = TW3ErrorObject.Create$45$($New(TNJWebSocketServerSecure));
               break;
         }
      } catch ($e) {
         var e$7 = $W($e);
         Self.FServer = null;
         if (CB$1) {
            CB$1(e$7);
            return;
         } else {
            throw e$7;
         }
      }
      TNJCustomServer.SetPort$1$(Self.FServer,Self.FPort);
      Self.FFactory = TMessageFactory.Create$101($New(TRagnarokServiceFactory));
      Self.FServer.OnBeforeServerStarted = $Event1(Self,TRagnarokService.HandleBeforeServerStarted);
      Self.FServer.OnBeforeServerStopped = $Event1(Self,TRagnarokService.HandleBeforeServerStopped);
      Self.FServer.OnAfterServerStarted = $Event1(Self,TRagnarokService.HandleAfterServerStarted);
      Self.FServer.OnAfterServerStopped = $Event1(Self,TRagnarokService.HandleAfterServerStopped);
      TNJRawWebSocketServer.SetTracking(Self.FServer,true);
      Self.FServer.OnTextMessage = $Event3(Self,TRagnarokService.HandleMessage);
      try {
         TNJCustomServer.Start$1(Self.FServer);
      } catch ($e) {
         var e$8 = $W($e);
         Err$1 = Exception.Create($New(Exception),"An error occured starting the server: "+e$8.FMessage);
         if (CB$1) {
            CB$1(Err$1);
            return;
         } else {
            throw Err$1;
         }
      }
      if (CB$1) {
         CB$1(null);
      }
   }
   /// procedure TRagnarokService.Stop(const CB: TRagnarokServerStopCB)
   ///  [line: 252, column: 28, file: service.base]
   ,Stop:function(Self, CB$2) {
      var Err$2 = null;
      if (!TRagnarokService.GetActive$1(Self)) {
         Err$2 = Exception.Create($New(Exception),"Failed to stop server, no instance is active error");
         if (CB$2) {
            CB$2(Err$2);
            return;
         } else {
            throw Err$2;
         }
      }
      try {
         TNJCustomServer.Stop$1(Self.FServer);
      } catch ($e) {
         var e$9 = $W($e);
         /* null */
      }
      try {
         try {
            TObject.Free(Self.FServer);
         } finally {
            Self.FServer = null;
         }
      } catch ($e) {
         var e$10 = $W($e);
         /* null */
      }
      if (Self.FFactory!==null) {
         try {
            try {
               TObject.Free(Self.FFactory);
            } finally {
               Self.FFactory = null;
            }
         } catch ($e) {
            var e$11 = $W($e);
            /* null */
         }
      }
      if (CB$2) {
         CB$2(null);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AfterServerStarted$:function($){return $.ClassType.AfterServerStarted($)}
   ,BeforeServerStopped$:function($){return $.ClassType.BeforeServerStopped($)}
   ,Dispatch$:function($){return $.ClassType.Dispatch.apply($.ClassType, arguments)}
};
/// TRagnarokServerType enumeration
///  [line: 34, column: 3, file: service.base]
var TRagnarokServerType = [ "rsBasic", "rsSecure" ];
/// TW3Component = class (TW3OwnedLockedErrorObject)
///  [line: 19, column: 3, file: System.Widget]
var TW3Component = {
   $ClassName:"TW3Component",$Parent:TW3OwnedLockedErrorObject
   ,$Init:function ($) {
      TW3OwnedLockedErrorObject.$Init($);
      $.FInitialized = false;
   }
   /// constructor TW3Component.Create(AOwner: TW3Component)
   ///  [line: 73, column: 26, file: System.Widget]
   ,Create$46:function(Self, AOwner$3) {
      TW3OwnedErrorObject.Create$16(Self,AOwner$3);
      Self.FInitialized = true;
      TW3Component.InitializeObject$(Self);
      return Self
   }
   /// constructor TW3Component.CreateEx(AOwner: TObject)
   ///  [line: 88, column: 26, file: System.Widget]
   ,CreateEx:function(Self, AOwner$4) {
      TW3OwnedErrorObject.Create$16(Self,AOwner$4);
      Self.FInitialized = false;
      return Self
   }
   /// destructor TW3Component.Destroy()
   ///  [line: 99, column: 25, file: System.Widget]
   ,Destroy:function(Self) {
      if (Self.FInitialized) {
         TW3Component.FinalizeObject$(Self);
      }
      TW3OwnedErrorObject.Destroy(Self);
   }
   /// procedure TW3Component.FinalizeObject()
   ///  [line: 129, column: 24, file: System.Widget]
   ,FinalizeObject:function(Self) {
      /* null */
   }
   /// procedure TW3Component.InitializeObject()
   ///  [line: 119, column: 24, file: System.Widget]
   ,InitializeObject:function(Self) {
      /* null */
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$46$:function($){return $.ClassType.Create$46.apply($.ClassType, arguments)}
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
};
TW3Component.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3Transaction = class (TW3Component)
///  [line: 95, column: 3, file: System.db]
var TW3Transaction = {
   $ClassName:"TW3Transaction",$Parent:TW3Component
   ,$Init:function ($) {
      TW3Component.$Init($);
      $.FDatabase = $.FParams = null;
   }
   /// function TW3Transaction.CanExecute() : Boolean
   ///  [line: 268, column: 25, file: System.db]
   ,CanExecute:function(Self) {
      var Result = false;
      var obj = null;
      obj = TW3Transaction.GetDatabase(Self);
      Result = (obj!==null)&&TW3Database.GetState(obj)==2;
      return Result
   }
   /// procedure TW3Transaction.FinalizeObject()
   ///  [line: 249, column: 26, file: System.db]
   ,FinalizeObject:function(Self) {
      TObject.Free(Self.FParams);
      TW3Component.FinalizeObject(Self);
   }
   /// function TW3Transaction.GetDatabase() : TW3Database
   ///  [line: 255, column: 25, file: System.db]
   ,GetDatabase:function(Self) {
      return Self.FDatabase;
   }
   /// procedure TW3Transaction.InitializeObject()
   ///  [line: 243, column: 26, file: System.db]
   ,InitializeObject:function(Self) {
      TW3Component.InitializeObject(Self);
      Self.FParams = TW3OwnedObject.Create$16$($New(TW3TransactionParameters),Self);
   }
   /// procedure TW3Transaction.SetDatabase(NewDatabase: TW3Database)
   ///  [line: 260, column: 26, file: System.db]
   ,SetDatabase:function(Self, NewDatabase) {
      if (NewDatabase!==Self.FDatabase) {
         Self.FDatabase = NewDatabase;
      }
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$46:TW3Component.Create$46
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
   ,CanExecute$:function($){return $.ClassType.CanExecute($)}
};
TW3Transaction.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3SQLTransaction = class (TW3Transaction)
///  [line: 121, column: 3, file: System.db]
var TW3SQLTransaction = {
   $ClassName:"TW3SQLTransaction",$Parent:TW3Transaction
   ,$Init:function ($) {
      TW3Transaction.$Init($);
      $.FHandle$4 = undefined;
      $.FSQL = "";
   }
   /// function TW3SQLTransaction.CanExecute() : Boolean
   ///  [line: 216, column: 28, file: System.db]
   ,CanExecute:function(Self) {
      return TW3Transaction.CanExecute(Self)&&Trim$_String_(Self.FSQL).length>0;
   }
   /// function TW3SQLTransaction.GetSQL() : String
   ///  [line: 226, column: 28, file: System.db]
   ,GetSQL:function(Self) {
      return Self.FSQL;
   }
   /// procedure TW3SQLTransaction.SetHandle(NewHandle: THandle)
   ///  [line: 221, column: 29, file: System.db]
   ,SetHandle:function(Self, NewHandle$2) {
      Self.FHandle$4 = NewHandle$2;
   }
   /// procedure TW3SQLTransaction.SetSQL(NewSQL: String)
   ///  [line: 231, column: 29, file: System.db]
   ,SetSQL:function(Self, NewSQL) {
      if (NewSQL!=Self.FSQL) {
         Self.FSQL = NewSQL;
      }
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$46:TW3Component.Create$46
   ,FinalizeObject:TW3Transaction.FinalizeObject
   ,InitializeObject:TW3Transaction.InitializeObject
   ,CanExecute$:function($){return $.ClassType.CanExecute($)}
   ,Execute$2$:function($){return $.ClassType.Execute$2.apply($.ClassType, arguments)}
};
TW3SQLTransaction.$Intf={
   ISQLTransaction:[TW3SQLTransaction.SetHandle,TW3SQLTransaction.GetSQL,TW3SQLTransaction.SetSQL,TW3SQLTransaction.CanExecute]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3WriteTransaction = class (TW3SQLTransaction)
///  [line: 141, column: 3, file: System.db]
var TW3WriteTransaction = {
   $ClassName:"TW3WriteTransaction",$Parent:TW3SQLTransaction
   ,$Init:function ($) {
      TW3SQLTransaction.$Init($);
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$46:TW3Component.Create$46
   ,FinalizeObject:TW3Transaction.FinalizeObject
   ,InitializeObject:TW3Transaction.InitializeObject
   ,CanExecute:TW3SQLTransaction.CanExecute
   ,Execute$2:TW3SQLTransaction.Execute$2
};
TW3WriteTransaction.$Intf={
   ISQLTransaction:[TW3SQLTransaction.SetHandle,TW3SQLTransaction.GetSQL,TW3SQLTransaction.SetSQL,TW3SQLTransaction.CanExecute]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3TransactionParameters = class (TW3OwnedLockedObject)
///  [line: 70, column: 3, file: System.db]
var TW3TransactionParameters = {
   $ClassName:"TW3TransactionParameters",$Parent:TW3OwnedLockedObject
   ,$Init:function ($) {
      TW3OwnedLockedObject.$Init($);
      $.FObjects = [];
   }
   /// function TW3TransactionParameters.Add(Name: String; const Value: Variant) : TW3TransactionNamedParameter
   ///  [line: 308, column: 35, file: System.db]
   ,Add:function(Self, Name$10, Value$54) {
      var Result = null;
      Name$10 = Trim$_String_(Name$10);
      if (Name$10.length>0) {
         if (TW3TransactionParameters.IndexOf(Self,Name$10)==-1) {
            Result = TW3OwnedObject.Create$16$($New(TW3TransactionNamedParameter),Self);
            Self.FObjects.push(Result);
            Result.Name = Name$10;
            Result.Value = Value$54;
         } else {
            throw EW3Exception.CreateFmt($New(EW3TransactionParameters),"Method %s failed, could not add parameter (%s), that name already exists",["TW3TransactionParameters.Add", Name$10]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3TransactionParameters),"Method %s failed, could not create parameter, name was empty error",["TW3TransactionParameters.Add"]);
      }
      return Result
   }
   /// function TW3TransactionParameters.AddNameOnly(Name: String) : TW3TransactionNamedParameter
   ///  [line: 330, column: 35, file: System.db]
   ,AddNameOnly:function(Self, Name$11) {
      var Result = null;
      Name$11 = (Trim$_String_(Name$11)).toLocaleLowerCase();
      if (Name$11.length>0) {
         if (TW3TransactionParameters.IndexOf(Self,Name$11)==-1) {
            Result = TW3OwnedObject.Create$16$($New(TW3TransactionNamedParameter),Self);
            Self.FObjects.push(Result);
            Result.Name = Trim$_String_(Name$11);
         } else {
            throw EW3Exception.CreateFmt($New(EW3TransactionParameters),"Method %s failed, could not add parameter (%s), that name already exists",["TW3TransactionParameters.AddNameOnly", Name$11]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3TransactionParameters),"Method %s failed, could not create parameter, name was empty error",["TW3TransactionParameters.AddNameOnly"]);
      }
      return Result
   }
   /// function TW3TransactionParameters.AddValueOnly(const Value: Variant) : TW3TransactionNamedParameter
   ///  [line: 350, column: 35, file: System.db]
   ,AddValueOnly:function(Self, Value$55) {
      var Result = null;
      Result = TW3OwnedObject.Create$16$($New(TW3TransactionNamedParameter),Self);
      Self.FObjects.push(Result);
      Result.Value = Value$55;
      return Result
   }
   /// procedure TW3TransactionParameters.Clear()
   ///  [line: 286, column: 36, file: System.db]
   ,Clear$4:function(Self) {
      var a$457 = 0;
      var item = null;
      try {
         var a$458 = [];
         a$458 = Self.FObjects;
         var $temp29;
         for(a$457=0,$temp29=a$458.length;a$457<$temp29;a$457++) {
            item = a$458[a$457];
            TObject.Free(item);
         }
      } finally {
         Self.FObjects.length=0;
      }
   }
   /// destructor TW3TransactionParameters.Destroy()
   ///  [line: 279, column: 37, file: System.db]
   ,Destroy:function(Self) {
      if (Self.FObjects.length>0) {
         TW3TransactionParameters.Clear$4(Self);
      }
      TObject.Destroy(Self);
   }
   /// function TW3TransactionParameters.GetNames() : TStrArray
   ///  [line: 357, column: 35, file: System.db]
   ,GetNames:function(Self) {
      var Result = [];
      var a$459 = 0;
      var item$1 = null;
      var a$460 = [];
      a$460 = Self.FObjects;
      var $temp30;
      for(a$459=0,$temp30=a$460.length;a$459<$temp30;a$459++) {
         item$1 = a$460[a$459];
         Result.push(item$1.Name);
      }
      return Result
   }
   /// function TW3TransactionParameters.GetParameter(index: Integer) : TW3TransactionNamedParameter
   ///  [line: 298, column: 35, file: System.db]
   ,GetParameter:function(Self, index$2) {
      return Self.FObjects[index$2];
   }
   /// function TW3TransactionParameters.GetParameterCount() : Integer
   ///  [line: 303, column: 35, file: System.db]
   ,GetParameterCount:function(Self) {
      return Self.FObjects.length;
   }
   /// function TW3TransactionParameters.GetValues() : TVarArray
   ///  [line: 365, column: 35, file: System.db]
   ,GetValues:function(Self) {
      var Result = [];
      var a$461 = 0;
      var item$2 = null;
      var a$462 = [];
      a$462 = Self.FObjects;
      var $temp31;
      for(a$461=0,$temp31=a$462.length;a$461<$temp31;a$461++) {
         item$2 = a$462[a$461];
         Result.push(item$2.Value);
      }
      return Result
   }
   /// function TW3TransactionParameters.IndexOf(Name: String) : Integer
   ///  [line: 389, column: 35, file: System.db]
   ,IndexOf:function(Self, Name$12) {
      var Result = 0;
      var x$14 = 0;
      Result = -1;
      Name$12 = (Trim$_String_(Name$12)).toLocaleLowerCase();
      if (Name$12.length>0) {
         var $temp32;
         for(x$14=0,$temp32=Self.FObjects.length;x$14<$temp32;x$14++) {
            if ((Self.FObjects[x$14].Name).toLocaleLowerCase()==Name$12) {
               Result = x$14;
               break;
            }
         }
      }
      return Result
   }
   /// function TW3TransactionParameters.ObjectOf(Name: String) : TW3TransactionNamedParameter
   ///  [line: 373, column: 35, file: System.db]
   ,ObjectOf:function(Self, Name$13) {
      var Result = null;
      var a$463 = 0;
      var item$3 = null;
      Name$13 = (Trim$_String_(Name$13)).toLocaleLowerCase();
      if (Name$13.length>0) {
         var a$464 = [];
         a$464 = Self.FObjects;
         var $temp33;
         for(a$463=0,$temp33=a$464.length;a$463<$temp33;a$463++) {
            item$3 = a$464[a$463];
            if ((item$3.Name).toLocaleLowerCase()==Name$13) {
               Result = item$3;
               break;
            }
         }
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3OwnedLockedObject.ObjectLocked
   ,ObjectUnLocked:TW3OwnedLockedObject.ObjectUnLocked
};
TW3TransactionParameters.$Intf={
   IW3TransactionParameters:[TW3TransactionParameters.GetParameter,TW3TransactionParameters.GetParameterCount,TW3TransactionParameters.Add,TW3TransactionParameters.AddNameOnly,TW3TransactionParameters.AddValueOnly,TW3TransactionParameters.GetValues,TW3TransactionParameters.GetNames,TW3TransactionParameters.ObjectOf,TW3TransactionParameters.IndexOf,TW3TransactionParameters.Clear$4]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3TransactionNamedParameter = class (TW3OwnedObject)
///  [line: 47, column: 3, file: System.db]
var TW3TransactionNamedParameter = {
   $ClassName:"TW3TransactionNamedParameter",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.Name = "";
      $.Value = undefined;
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
};
TW3TransactionNamedParameter.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3ReadTransaction = class (TW3SQLTransaction)
///  [line: 136, column: 3, file: System.db]
var TW3ReadTransaction = {
   $ClassName:"TW3ReadTransaction",$Parent:TW3SQLTransaction
   ,$Init:function ($) {
      TW3SQLTransaction.$Init($);
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$46:TW3Component.Create$46
   ,FinalizeObject:TW3Transaction.FinalizeObject
   ,InitializeObject:TW3Transaction.InitializeObject
   ,CanExecute:TW3SQLTransaction.CanExecute
   ,Execute$2:TW3SQLTransaction.Execute$2
};
TW3ReadTransaction.$Intf={
   ISQLTransaction:[TW3SQLTransaction.SetHandle,TW3SQLTransaction.GetSQL,TW3SQLTransaction.SetSQL,TW3SQLTransaction.CanExecute]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3DatabaseState enumeration
///  [line: 35, column: 3, file: System.db]
var TW3DatabaseState = [ "dbInactive", "dbConnecting", "dbActive", "dbDisconnecting" ];
/// TW3Database = class (TW3Component)
///  [line: 147, column: 3, file: System.db]
var TW3Database = {
   $ClassName:"TW3Database",$Parent:TW3Component
   ,$Init:function ($) {
      TW3Component.$Init($);
      $.FLocation = "";
      $.FObjects$1 = [];
      $.FState = 0;
   }
   /// function TW3Database.GetActive() : Boolean
   ///  [line: 452, column: 22, file: System.db]
   ,GetActive$2:function(Self) {
      return (TW3Database.GetState(Self)!=0);
   }
   /// function TW3Database.GetLocation() : String
   ///  [line: 410, column: 22, file: System.db]
   ,GetLocation:function(Self) {
      return Self.FLocation;
   }
   /// function TW3Database.GetState() : TW3DatabaseState
   ///  [line: 457, column: 22, file: System.db]
   ,GetState:function(Self) {
      return Self.FState;
   }
   /// function TW3Database.RegisterTransaction(Transaction: TW3Transaction) : Boolean
   ///  [line: 420, column: 22, file: System.db]
   ,RegisterTransaction:function(Self, Transaction) {
      var Result = false;
      TW3OwnedErrorObject.ClearLastError(Self);
      if (Transaction!==null) {
         if (Self.FObjects$1.indexOf(Transaction)<0) {
            Self.FObjects$1.push(Transaction);
            Result = true;
         } else {
            TW3OwnedErrorObject.SetLastErrorF(Self,"Method %s failed, transaction uknown error",["TW3Database.RegisterTransaction"]);
         }
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"Method %s failed, transaction was nil error",["TW3Database.RegisterTransaction"]);
      }
      return Result
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$46:TW3Component.Create$46
   ,FinalizeObject:TW3Component.FinalizeObject
   ,InitializeObject:TW3Component.InitializeObject
   ,apiClose$:function($){return $.ClassType.apiClose.apply($.ClassType, arguments)}
   ,apiOpen$:function($){return $.ClassType.apiOpen.apply($.ClassType, arguments)}
   ,CreateReadTransaction$:function($){return $.ClassType.CreateReadTransaction.apply($.ClassType, arguments)}
   ,CreateWriteTransaction$:function($){return $.ClassType.CreateWriteTransaction.apply($.ClassType, arguments)}
};
TW3Database.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// EW3TransactionParameters = class (EW3Exception)
///  [line: 21, column: 3, file: System.db]
var EW3TransactionParameters = {
   $ClassName:"EW3TransactionParameters",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EW3Database = class (EW3Exception)
///  [line: 19, column: 3, file: System.db]
var EW3Database = {
   $ClassName:"EW3Database",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EW3Transaction = class (EW3Database)
///  [line: 20, column: 3, file: System.db]
var EW3Transaction = {
   $ClassName:"EW3Transaction",$Parent:EW3Database
   ,$Init:function ($) {
      EW3Database.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3RepeatResult enumeration
///  [line: 55, column: 3, file: System.Time]
var TW3RepeatResult = { 241:"rrContinue", 242:"rrStop", 243:"rrDispose" };
/// TW3CustomRepeater = class (TObject)
///  [line: 71, column: 3, file: System.Time]
var TW3CustomRepeater = {
   $ClassName:"TW3CustomRepeater",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FActive = false;
      $.FDelay$1 = 0;
      $.FHandle$2 = undefined;
   }
   /// procedure TW3CustomRepeater.AllocTimer()
   ///  [line: 446, column: 29, file: System.Time]
   ,AllocTimer:function(Self) {
      if (Self.FHandle$2) {
         TW3CustomRepeater.FreeTimer(Self);
      }
      Self.FHandle$2 = TW3Dispatch.SetInterval(TW3Dispatch,$Event0(Self,TW3CustomRepeater.CBExecute$),Self.FDelay$1);
   }
   /// destructor TW3CustomRepeater.Destroy()
   ///  [line: 400, column: 30, file: System.Time]
   ,Destroy:function(Self) {
      if (Self.FActive) {
         TW3CustomRepeater.SetActive(Self,false);
      }
      TObject.Destroy(Self);
   }
   /// procedure TW3CustomRepeater.FreeTimer()
   ///  [line: 455, column: 29, file: System.Time]
   ,FreeTimer:function(Self) {
      if (Self.FHandle$2) {
         TW3Dispatch.ClearInterval(TW3Dispatch,Self.FHandle$2);
         Self.FHandle$2 = undefined;
      }
   }
   /// procedure TW3CustomRepeater.SetActive(const NewActive: Boolean)
   ///  [line: 414, column: 29, file: System.Time]
   ,SetActive:function(Self, NewActive) {
      if (NewActive!=Self.FActive) {
         try {
            if (Self.FActive) {
               TW3CustomRepeater.FreeTimer(Self);
            } else {
               TW3CustomRepeater.AllocTimer(Self);
            }
         } finally {
            Self.FActive = NewActive;
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,CBExecute$:function($){return $.ClassType.CBExecute($)}
};
/// TW3Dispatch = class (TObject)
///  [line: 135, column: 3, file: System.Time]
var TW3Dispatch = {
   $ClassName:"TW3Dispatch",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TW3Dispatch.ClearInterval(const Handle: TW3DispatchHandle)
   ///  [line: 234, column: 29, file: System.Time]
   ,ClearInterval:function(Self, Handle$7) {
      clearInterval(Handle$7);
   }
   /// function TW3Dispatch.Execute(const EntryPoint: TProcedureRef; const WaitForInMs: Integer) : TW3DispatchHandle
   ///  [line: 264, column: 28, file: System.Time]
   ,Execute$1:function(Self, EntryPoint$1, WaitForInMs) {
      var Result = undefined;
      Result = setTimeout(EntryPoint$1,WaitForInMs);
      return Result
   }
   /// procedure TW3Dispatch.RepeatExecute(const Entrypoint: TProcedureRef; const RepeatCount: Integer; const IntervalInMs: Integer)
   ///  [line: 272, column: 29, file: System.Time]
   ,RepeatExecute:function(Self, Entrypoint, RepeatCount, IntervalInMs) {
      if (Entrypoint) {
         if (RepeatCount>0) {
            Entrypoint();
            if (RepeatCount>1) {
               TW3Dispatch.Execute$1(Self,function () {
                  TW3Dispatch.RepeatExecute(Self,Entrypoint,(RepeatCount-1),IntervalInMs);
               },IntervalInMs);
            }
         } else {
            Entrypoint();
            TW3Dispatch.Execute$1(Self,function () {
               TW3Dispatch.RepeatExecute(Self,Entrypoint,(-1),IntervalInMs);
            },IntervalInMs);
         }
      }
   }
   /// function TW3Dispatch.SetInterval(const Entrypoint: TProcedureRef; const IntervalDelayInMS: Integer) : TW3DispatchHandle
   ///  [line: 226, column: 28, file: System.Time]
   ,SetInterval:function(Self, Entrypoint$1, IntervalDelayInMS) {
      var Result = undefined;
      Result = setInterval(Entrypoint$1,IntervalDelayInMS);
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TW3StorageObjectType enumeration
///  [line: 36, column: 3, file: System.Device.Storage]
var TW3StorageObjectType = [ "otUnknown", "otFile", "otFolder", "otBlockDevice", "otCharacterDevice", "otSymbolicLink", "otFIFO", "otSocket" ];
/// TW3StorageDevice = class (TW3Component)
///  [line: 145, column: 3, file: System.Device.Storage]
var TW3StorageDevice = {
   $ClassName:"TW3StorageDevice",$Parent:TW3Component
   ,$Init:function ($) {
      TW3Component.$Init($);
      $.FActive$1 = false;
      $.FIdentifier = $.FName = "";
      $.FOptions$5 = [0];
   }
   /// procedure TW3StorageDevice.FinalizeObject()
   ///  [line: 211, column: 28, file: System.Device.Storage]
   ,FinalizeObject:function(Self) {
      if (Self.FActive$1) {
         TW3StorageDevice.UnMount$(Self,null);
      }
      TW3Component.FinalizeObject(Self);
   }
   /// function TW3StorageDevice.GetActive() : Boolean
   ///  [line: 307, column: 27, file: System.Device.Storage]
   ,GetActive:function(Self) {
      return Self.FActive$1;
   }
   /// procedure TW3StorageDevice.GetDeviceId(CB: TW3DeviceIdCallback)
   ///  [line: 261, column: 28, file: System.Device.Storage]
   ,GetDeviceId:function(Self, CB$3) {
      if (CB$3) {
         CB$3(Self.FIdentifier,true);
      }
   }
   /// procedure TW3StorageDevice.GetDeviceOptions(CB: TW3DeviceOptionsCallback)
   ///  [line: 218, column: 28, file: System.Device.Storage]
   ,GetDeviceOptions:function(Self, CB$4) {
      if (CB$4) {
         CB$4(Self.FOptions$5.slice(0),true);
      }
   }
   /// procedure TW3StorageDevice.GetName(CB: TW3DeviceNameCallback)
   ///  [line: 255, column: 28, file: System.Device.Storage]
   ,GetName:function(Self, CB$5) {
      if (CB$5) {
         CB$5(Self.FName,true);
      }
   }
   /// procedure TW3StorageDevice.InitializeObject()
   ///  [line: 203, column: 28, file: System.Device.Storage]
   ,InitializeObject:function(Self) {
      TW3Component.InitializeObject(Self);
      Self.FOptions$5 = [2];
      Self.FIdentifier = TString.CreateGUID(TString);
      Self.FName = "dh0";
   }
   /// procedure TW3StorageDevice.SetActive(const NewActiveState: Boolean)
   ///  [line: 312, column: 28, file: System.Device.Storage]
   ,SetActive$1:function(Self, NewActiveState) {
      Self.FActive$1 = NewActiveState;
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$46:TW3Component.Create$46
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
   ,CdUp$:function($){return $.ClassType.CdUp.apply($.ClassType, arguments)}
   ,ChDir$:function($){return $.ClassType.ChDir.apply($.ClassType, arguments)}
   ,DirExists$:function($){return $.ClassType.DirExists.apply($.ClassType, arguments)}
   ,Examine$1$:function($){return $.ClassType.Examine$1.apply($.ClassType, arguments)}
   ,FileExists$1$:function($){return $.ClassType.FileExists$1.apply($.ClassType, arguments)}
   ,GetFileSize$:function($){return $.ClassType.GetFileSize.apply($.ClassType, arguments)}
   ,GetPath$1$:function($){return $.ClassType.GetPath$1.apply($.ClassType, arguments)}
   ,GetStorageObjectType$:function($){return $.ClassType.GetStorageObjectType.apply($.ClassType, arguments)}
   ,Load$:function($){return $.ClassType.Load.apply($.ClassType, arguments)}
   ,MakeDir$:function($){return $.ClassType.MakeDir.apply($.ClassType, arguments)}
   ,Mount$:function($){return $.ClassType.Mount.apply($.ClassType, arguments)}
   ,RemoveDir$:function($){return $.ClassType.RemoveDir.apply($.ClassType, arguments)}
   ,Save$:function($){return $.ClassType.Save.apply($.ClassType, arguments)}
   ,UnMount$:function($){return $.ClassType.UnMount.apply($.ClassType, arguments)}
};
TW3StorageDevice.$Intf={
   IW3StorageDevice:[TW3StorageDevice.GetActive,TW3StorageDevice.GetName,TW3StorageDevice.GetDeviceOptions,TW3StorageDevice.GetDeviceId,TW3StorageDevice.GetPath$1,TW3StorageDevice.GetFileSize,TW3StorageDevice.FileExists$1,TW3StorageDevice.DirExists,TW3StorageDevice.MakeDir,TW3StorageDevice.RemoveDir,TW3StorageDevice.Examine$1,TW3StorageDevice.ChDir,TW3StorageDevice.CdUp,TW3StorageDevice.GetStorageObjectType,TW3StorageDevice.Load,TW3StorageDevice.Save]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3DeviceAuthenticationData = class (TObject)
///  [line: 67, column: 3, file: System.Device.Storage]
var TW3DeviceAuthenticationData = {
   $ClassName:"TW3DeviceAuthenticationData",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TNJFileItemType enumeration
///  [line: 47, column: 3, file: System.Device.Storage]
var TNJFileItemType = [ "wtFile", "wtFolder" ];
/// TNJFileItemList = class (JObject)
///  [line: 62, column: 3, file: System.Device.Storage]
function TNJFileItemList() {
   this.dlItems = [];
}
$Extend(Object,TNJFileItemList,
   {
      "dlPath" : ""
   });

/// TNJFileItem = class (JObject)
///  [line: 52, column: 3, file: System.Device.Storage]
function TNJFileItem() {
}
$Extend(Object,TNJFileItem,
   {
      "diFileName" : "",
      "diFileType" : 0,
      "diFileSize" : 0,
      "diFileMode" : "",
      "diCreated" : undefined,
      "diModified" : undefined
   });

/// TW3DatasetObject = class (TW3OwnedLockedObject)
///  [line: 181, column: 3, file: System.dataset]
var TW3DatasetObject = {
   $ClassName:"TW3DatasetObject",$Parent:TW3OwnedLockedObject
   ,$Init:function ($) {
      TW3OwnedLockedObject.$Init($);
      $.FReadOnly = false;
   }
   /// procedure TW3DatasetObject.ObjectLocked()
   ///  [line: 1559, column: 28, file: System.dataset]
   ,ObjectLocked:function(Self) {
      TW3DatasetObject.SetReadOnly$(Self,true);
      TW3OwnedLockedObject.ObjectLocked(Self);
   }
   /// procedure TW3DatasetObject.ObjectUnLocked()
   ///  [line: 1565, column: 28, file: System.dataset]
   ,ObjectUnLocked:function(Self) {
      TW3DatasetObject.SetReadOnly$(Self,false);
      TW3OwnedLockedObject.ObjectUnLocked(Self);
   }
   /// procedure TW3DatasetObject.SetReadOnly(const NewState: Boolean)
   ///  [line: 1571, column: 28, file: System.dataset]
   ,SetReadOnly:function(Self, NewState) {
      Self.FReadOnly = NewState;
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked$:function($){return $.ClassType.ObjectLocked($)}
   ,ObjectUnLocked$:function($){return $.ClassType.ObjectUnLocked($)}
   ,SetReadOnly$:function($){return $.ClassType.SetReadOnly.apply($.ClassType, arguments)}
};
TW3DatasetObject.$Intf={
   IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3DatasetField = class (TW3DatasetObject)
///  [line: 190, column: 3, file: System.dataset]
var TW3DatasetField = {
   $ClassName:"TW3DatasetField",$Parent:TW3DatasetObject
   ,$Init:function ($) {
      TW3DatasetObject.$Init($);
      $.FKind = 0;
      $.FName$1 = "";
      $.FValue = undefined;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 239, column: 23, file: System.dataset]
   ,a$171:function(Self, Value$56) {
      Self.FValue = Value$56;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 238, column: 22, file: System.dataset]
   ,a$170:function(Self) {
      return TVariant.AsFloat(Self.FValue);
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 235, column: 23, file: System.dataset]
   ,a$169:function(Self, Value$57) {
      Self.FValue = Value$57;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 234, column: 22, file: System.dataset]
   ,a$168:function(Self) {
      return TVariant.AsBool(Self.FValue);
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 231, column: 23, file: System.dataset]
   ,a$167:function(Self, Value$58) {
      Self.FValue = Value$58;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 230, column: 22, file: System.dataset]
   ,a$166:function(Self) {
      return TVariant.AsInteger(Self.FValue);
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 227, column: 23, file: System.dataset]
   ,a$165:function(Self, Value$59) {
      Self.FValue = Value$59;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 226, column: 22, file: System.dataset]
   ,a$164:function(Self) {
      return TVariant.AsString(Self.FValue);
   }
   /// function TW3DatasetField.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 3078, column: 26, file: System.dataset]
   ,AcceptOwner:function(Self, CandidateObject$2) {
      return CandidateObject$2!==null&&$Is(CandidateObject$2,TW3DatasetFields);
   }
   /// constructor TW3DatasetField.Create(const Owner: TW3DatasetFields)
   ///  [line: 3066, column: 29, file: System.dataset]
   ,Create$72:function(Self, Owner$11) {
      TW3OwnedObject.Create$16(Self,Owner$11);
      Self.FKind = 0;
      Self.FName$1 = "Field"+TW3Identifiers.GenerateUniqueNumber(TW3Identifiers).toString();
      return Self
   }
   // IGNORED: TW3DatasetField.Generate
   // IGNORED: TW3DatasetField.GetGenerated
   /// function TW3DatasetField.GetValue() : Variant
   ///  [line: 3176, column: 26, file: System.dataset]
   ,GetValue:function(Self) {
      return Self.FValue;
   }
   // IGNORED: TW3DatasetField.ReadBinaryValue
   /// procedure TW3DatasetField.SetValue(const NewValue: Variant)
   ///  [line: 3181, column: 27, file: System.dataset]
   ,SetValue:function(Self, NewValue) {
      Self.FValue = NewValue;
   }
   // IGNORED: TW3DatasetField.WriteBinaryValue
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly:TW3DatasetObject.SetReadOnly
   // IGNORED: TW3DatasetField.Generate
   // IGNORED: TW3DatasetField.GetGenerated
   // IGNORED: TW3DatasetField.ReadBinaryValue
   // IGNORED: TW3DatasetField.WriteBinaryValue
};
TW3DatasetField.$Intf={
   IW3OwnedObjectAccess:[TW3DatasetField.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3FilterValue = class (TObject)
///  [line: 102, column: 3, file: System.dataset]
var TW3FilterValue = {
   $ClassName:"TW3FilterValue",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FFilter = null;
   }
   /// constructor TW3FilterValue.Create(const Filter: TW3DatasetFilter)
   ///  [line: 711, column: 28, file: System.dataset]
   ,Create$73:function(Self, Filter$1) {
      TObject.Create(Self);
      Self.FFilter = Filter$1;
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TW3FilterSymbol enumeration
///  [line: 54, column: 3, file: System.dataset]
var TW3FilterSymbol = [ "fsymInvalid", "fsymEquals", "fsymGreater", "fsymLess", "fsymLessGreater", "fsymLike" ];
/// TW3FilterOperator enumeration
///  [line: 67, column: 3, file: System.dataset]
var TW3FilterOperator = [ "fopInvalid", "fopAnd", "fopOr", "fopNot" ];
/// TW3FilterExpression = class (TObject)
///  [line: 135, column: 3, file: System.dataset]
var TW3FilterExpression = {
   $ClassName:"TW3FilterExpression",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.Operator = 0;
      $.Symbol = 0;
      $.FLeft = $.FRight = null;
   }
   /// constructor TW3FilterExpression.Create(const Filter: TW3DatasetFilter)
   ///  [line: 680, column: 33, file: System.dataset]
   ,Create$74:function(Self, Filter$2) {
      TObject.Create(Self);
      Self.FLeft = TW3FilterValue.Create$73($New(TW3FilterValue),Filter$2);
      Self.FRight = TW3FilterValue.Create$73($New(TW3FilterValue),Filter$2);
      Self.Operator = 1;
      Self.Symbol = 0;
      return Self
   }
   /// destructor TW3FilterExpression.Destroy()
   ///  [line: 689, column: 32, file: System.dataset]
   ,Destroy:function(Self) {
      TObject.Free(Self.FLeft);
      TObject.Free(Self.FRight);
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TW3FieldDefs = class (TW3DatasetObject)
///  [line: 371, column: 3, file: System.dataset]
var TW3FieldDefs = {
   $ClassName:"TW3FieldDefs",$Parent:TW3DatasetObject
   ,$Init:function ($) {
      TW3DatasetObject.$Init($);
      $.FFields = [];
      $.FId = 0;
   }
   /// function TW3FieldDefs.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 1597, column: 23, file: System.dataset]
   ,AcceptOwner:function(Self, CandidateObject$3) {
      return CandidateObject$3!==null&&$Is(CandidateObject$3,TW3Dataset);
   }
   /// procedure TW3FieldDefs.Clear()
   ///  [line: 1602, column: 24, file: System.dataset]
   ,Clear$5:function(Self) {
      var a$465 = 0;
      var Field = null;
      if (Self.FFields.length>0) {
         try {
            var a$466 = [];
            a$466 = Self.FFields;
            var $temp34;
            for(a$465=0,$temp34=a$466.length;a$465<$temp34;a$465++) {
               Field = a$466[a$465];
               TObject.Free(Field);
            }
         } finally {
            Self.FFields.length=0;
         }
      }
   }
   /// constructor TW3FieldDefs.Create(const Owner: TW3Dataset)
   ///  [line: 1580, column: 26, file: System.dataset]
   ,Create$75:function(Self, Owner$12) {
      TW3OwnedObject.Create$16(Self,Owner$12);
      return Self
   }
   /// destructor TW3FieldDefs.Destroy()
   ///  [line: 1585, column: 25, file: System.dataset]
   ,Destroy:function(Self) {
      if (Self.FFields.length>0) {
         TW3FieldDefs.Clear$5(Self);
      }
      TObject.Destroy(Self);
   }
   /// function TW3FieldDefs.GetFieldIdentifier() : String
   ///  [line: 1617, column: 23, file: System.dataset]
   ,GetFieldIdentifier:function(Self) {
      var Result = "";
      do {
         ++Self.FId;
         Result = "Field"+Self.FId.toString();
      } while (!(TW3FieldDefs.IndexOf$1(Self,Result)<0));
      return Result
   }
   /// function TW3FieldDefs.IndexOf(FieldName: String) : Integer
   ///  [line: 1681, column: 23, file: System.dataset]
   ,IndexOf$1:function(Self, FieldName) {
      var Result = 0;
      var x$15 = 0;
      var Field$1 = null;
      Result = -1;
      if (Self.FFields.length>0) {
         FieldName = (Trim$_String_(FieldName)).toLocaleLowerCase();
         if (FieldName.length>0) {
            var $temp35;
            for(x$15=0,$temp35=Self.FFields.length;x$15<$temp35;x$15++) {
               Field$1 = Self.FFields[x$15];
               if (Field$1.FName$2==FieldName) {
                  Result = x$15;
                  break;
               }
            }
         }
      }
      return Result
   }
   /// procedure TW3FieldDefs.SetReadOnly(const NewState: Boolean)
   ///  [line: 1625, column: 24, file: System.dataset]
   ,SetReadOnly:function(Self, NewState$1) {
      var a$467 = 0;
      var Item = null;
      if (NewState$1!=TW3OwnedLockedObject.GetLockState(Self)) {
         TW3DatasetObject.SetReadOnly(Self,NewState$1);
         if (Self.FFields.length>0) {
            var a$468 = [];
            a$468 = Self.FFields;
            var $temp36;
            for(a$467=0,$temp36=a$468.length;a$467<$temp36;a$467++) {
               Item = a$468[a$467];
               TW3DatasetObject.SetReadOnly$(Item,NewState$1);
            }
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly$:function($){return $.ClassType.SetReadOnly.apply($.ClassType, arguments)}
};
TW3FieldDefs.$Intf={
   IW3OwnedObjectAccess:[TW3FieldDefs.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3FieldDef = class (TW3DatasetObject)
///  [line: 348, column: 3, file: System.dataset]
var TW3FieldDef = {
   $ClassName:"TW3FieldDef",$Parent:TW3DatasetObject
   ,$Init:function ($) {
      TW3DatasetObject.$Init($);
      $.FName$2 = "";
   }
   /// function TW3FieldDef.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 1878, column: 22, file: System.dataset]
   ,AcceptOwner:function(Self, CandidateObject$4) {
      return CandidateObject$4!==null&&$Is(CandidateObject$4,TW3FieldDefs);
   }
   /// constructor TW3FieldDef.Create(const Owner: TW3FieldDefs)
   ///  [line: 1862, column: 25, file: System.dataset]
   ,Create$76:function(Self, Owner$13) {
      TW3OwnedObject.Create$16(Self,Owner$13);
      if (Owner$13!==null) {
         Self.FName$2 = TW3FieldDefs.GetFieldIdentifier(Owner$13);
      } else {
         throw EW3Exception.CreateFmt($New(EW3FieldDef),"Failed to create field-definition [%s], parent was nil error",[TObject.ClassName(Self.ClassType)]);
      }
      return Self
   }
   /// procedure TW3FieldDef.SetName(DefName: String)
   ///  [line: 1910, column: 23, file: System.dataset]
   ,SetName$3:function(Self, DefName) {
      if (!TW3OwnedLockedObject.GetLockState(Self)) {
         DefName = (Trim$_String_(DefName)).toLocaleLowerCase();
         if (DefName.length>0) {
            Self.FName$2 = DefName;
         } else {
            throw Exception.Create($New(EW3FieldDef),"Failed to set field-def name, string was empty error");
         }
      }
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly:TW3DatasetObject.SetReadOnly
};
TW3FieldDef.$Intf={
   IW3OwnedObjectAccess:[TW3FieldDef.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3DatasetState enumeration
///  [line: 88, column: 3, file: System.dataset]
var TW3DatasetState = [ "dsIdle", "dsInsert", "dsEdit" ];
/// TW3DatasetFilter = class (TW3ErrorObject)
///  [line: 152, column: 3, file: System.dataset]
var TW3DatasetFilter = {
   $ClassName:"TW3DatasetFilter",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.FCache = [];
      $.FDataset = null;
      $.FFilterText = "";
      $.FOperatorStack = [];
      $.FReady = false;
   }
   /// procedure TW3DatasetFilter.Clear()
   ///  [line: 1096, column: 28, file: System.dataset]
   ,Clear$6:function(Self) {
      var x$16 = 0;
      Self.FReady = false;
      Self.FFilterText = "";
      Self.FOperatorStack.length=0;
      try {
         var $temp37;
         for(x$16=0,$temp37=Self.FCache.length;x$16<$temp37;x$16++) {
            if (Self.FCache[x$16]!==null) {
               TObject.Free(Self.FCache[x$16]);
            }
         }
      } finally {
         Self.FCache.length=0;
      }
   }
   /// constructor TW3DatasetFilter.Create(const Dataset: TW3Dataset)
   ///  [line: 1083, column: 30, file: System.dataset]
   ,Create$77:function(Self, Dataset$1) {
      TW3ErrorObject.Create$45(Self);
      Self.FDataset = Dataset$1;
      return Self
   }
   /// destructor TW3DatasetFilter.Destroy()
   ///  [line: 1089, column: 29, file: System.dataset]
   ,Destroy:function(Self) {
      TW3DatasetFilter.Clear$6(Self);
      TW3ErrorObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
};
TW3DatasetFilter.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TW3DatasetFieldType enumeration
///  [line: 75, column: 3, file: System.dataset]
var TW3DatasetFieldType = [ "ftUnknown", "ftboolean", "ftinteger", "ftFloat", "ftString", "ftDateTime", "ftAutoInc", "ftGUID" ];
/// TW3DatasetFields = class (TW3DatasetObject)
///  [line: 310, column: 3, file: System.dataset]
var TW3DatasetFields = {
   $ClassName:"TW3DatasetFields",$Parent:TW3DatasetObject
   ,$Init:function ($) {
      TW3DatasetObject.$Init($);
      $.FFields$1 = [];
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 323, column: 37, file: System.dataset]
   ,a$187:function(Self) {
      return Self.FFields$1.length;
   }
   /// function TW3DatasetFields.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 2756, column: 27, file: System.dataset]
   ,AcceptOwner:function(Self, CandidateObject$5) {
      return CandidateObject$5!==null&&$Is(CandidateObject$5,TW3Dataset);
   }
   /// procedure TW3DatasetFields.Clear()
   ///  [line: 2865, column: 28, file: System.dataset]
   ,Clear$7:function(Self) {
      var a$469 = 0;
      var field = null;
      if (Self.FFields$1.length>0) {
         try {
            var a$470 = [];
            a$470 = Self.FFields$1;
            var $temp38;
            for(a$469=0,$temp38=a$470.length;a$469<$temp38;a$469++) {
               field = a$470[a$469];
               TObject.Free(field);
            }
         } finally {
            Self.FFields$1.length=0;
         }
      }
   }
   /// constructor TW3DatasetFields.Create(const Owner: TW3Dataset)
   ///  [line: 2727, column: 30, file: System.dataset]
   ,Create$78:function(Self, Owner$14) {
      TW3OwnedObject.Create$16(Self,Owner$14);
      return Self
   }
   /// destructor TW3DatasetFields.Destroy()
   ///  [line: 2732, column: 29, file: System.dataset]
   ,Destroy:function(Self) {
      if (TW3DatasetFields.a$187(Self)>0) {
         TW3DatasetFields.Clear$7(Self);
      }
      TObject.Destroy(Self);
   }
   /// procedure TW3DatasetFields.ResetFieldValues()
   ///  [line: 2854, column: 28, file: System.dataset]
   ,ResetFieldValues:function(Self) {
      var a$471 = 0;
      var Field$2 = null;
      if (Self.FFields$1.length>0) {
         var a$472 = [];
         a$472 = Self.FFields$1;
         var $temp39;
         for(a$471=0,$temp39=a$472.length;a$471<$temp39;a$471++) {
            Field$2 = a$472[a$471];
            TW3DatasetField.SetValue(Field$2,null);
         }
      }
   }
   /// procedure TW3DatasetFields.SetReadOnly(const NewState: Boolean)
   ///  [line: 2839, column: 28, file: System.dataset]
   ,SetReadOnly:function(Self, NewState$2) {
      var a$473 = 0;
      var Field$3 = null;
      if (NewState$2!=TW3OwnedLockedObject.GetLockState(Self)) {
         TW3DatasetObject.SetReadOnly(Self,NewState$2);
         if (Self.FFields$1.length>0) {
            var a$474 = [];
            a$474 = Self.FFields$1;
            var $temp40;
            for(a$473=0,$temp40=a$474.length;a$473<$temp40;a$473++) {
               Field$3 = a$474[a$473];
               TW3DatasetObject.SetReadOnly$(Field$3,NewState$2);
            }
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly$:function($){return $.ClassType.SetReadOnly.apply($.ClassType, arguments)}
};
TW3DatasetFields.$Intf={
   IW3OwnedObjectAccess:[TW3DatasetFields.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3Dataset = class (TObject)
///  [line: 415, column: 3, file: System.dataset]
var TW3Dataset = {
   $ClassName:"TW3Dataset",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FActive$2 = $.FDestroying = $.FFiltered = false;
      $.FCache$1 = [];
      $.FDefs = $.FFields$2 = $.FFilter$1 = $.FOnAdd = $.FOnClosed = $.FOnCreated = $.FOnDelete = $.FOnPos = $.FOnState = null;
      $.FDsIndex = 0;
      $.FState$1 = 0;
   }
   /// procedure TW3Dataset.Close()
   ///  [line: 2356, column: 22, file: System.dataset]
   ,Close$1:function(Self) {
      if (Self.FActive$2) {
         TW3Dataset.DoBeforeDatasetClosed(Self);
         try {
            try {
               TW3DatasetFields.Clear$7(Self.FFields$2);
               Self.FCache$1.length=0;
               if (Self.FFilter$1!==null) {
                  Self.FFiltered = false;
                  TObject.Free(Self.FFilter$1);
                  Self.FFilter$1 = null;
               }
            } finally {
               Self.FActive$2 = false;
               Self.FState$1 = 0;
               Self.FDsIndex = -1;
               TW3DatasetFields.ResetFieldValues(Self.FFields$2);
               TW3DatasetObject.SetReadOnly$(Self.FFields$2,false);
               TW3DatasetObject.SetReadOnly$(Self.FDefs,false);
            }
         } finally {
            TW3Dataset.DoAfterDatasetClosed(Self);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[35]);
      }
   }
   /// constructor TW3Dataset.Create()
   ///  [line: 1926, column: 24, file: System.dataset]
   ,Create$79:function(Self) {
      TObject.Create(Self);
      Self.FFields$2 = TW3DatasetFields.Create$78($New(TW3DatasetFields),Self);
      Self.FDefs = TW3FieldDefs.Create$75($New(TW3FieldDefs),Self);
      Self.FState$1 = 0;
      Self.FDestroying = false;
      Self.FDsIndex = -1;
      return Self
   }
   /// destructor TW3Dataset.Destroy()
   ///  [line: 1936, column: 23, file: System.dataset]
   ,Destroy:function(Self) {
      Self.FDestroying = true;
      if (Self.FActive$2) {
         TW3Dataset.Close$1(Self);
      }
      TObject.Free(Self.FFields$2);
      TObject.Free(Self.FDefs);
      TObject.Destroy(Self);
   }
   /// procedure TW3Dataset.DoAfterDatasetClosed()
   ///  [line: 2694, column: 22, file: System.dataset]
   ,DoAfterDatasetClosed:function(Self) {
      if (Self.FOnClosed) {
         if (!Self.FDestroying) {
            Self.FOnClosed(Self);
         }
      }
   }
   /// procedure TW3Dataset.DoBeforeDatasetClosed()
   ///  [line: 2690, column: 22, file: System.dataset]
   ,DoBeforeDatasetClosed:function(Self) {
      /* null */
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// EW3FieldDef = class (EW3Exception)
///  [line: 42, column: 3, file: System.dataset]
var EW3FieldDef = {
   $ClassName:"EW3FieldDef",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EW3DatasetField = class (EW3Exception)
///  [line: 44, column: 3, file: System.dataset]
var EW3DatasetField = {
   $ClassName:"EW3DatasetField",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EW3Dataset = class (EW3Exception)
///  [line: 46, column: 3, file: System.dataset]
var EW3Dataset = {
   $ClassName:"EW3Dataset",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SetupFilterLUT() {
   _OperandLUT = TVariant.CreateObject();
   _OperandLUT["<"] = 3;
   _OperandLUT["="] = 1;
   _OperandLUT[">"] = 2;
   _OperandLUT["<>"] = 4;
   _OperandLUT["like"] = 5;
   _StatementLUT = TVariant.CreateObject();
   _StatementLUT["and"] = 1;
   _StatementLUT["or"] = 2;
   _StatementLUT["not"] = 3;
   _Reserved.push("and");
   _Reserved.push("or");
   _Reserved.push("not");
};
/// TTextBufferBookmark = class (TObject)
///  [line: 40, column: 3, file: System.Text.Parser]
var TTextBufferBookmark = {
   $ClassName:"TTextBufferBookmark",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.bbCol = $.bbRow = $.bbOffset = 0;
   }
   ,Destroy:TObject.Destroy
};
/// TTextBuffer = class (TW3ErrorObject)
///  [line: 61, column: 3, file: System.Text.Parser]
var TTextBuffer = {
   $ClassName:"TTextBuffer",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.FCol = $.FLength = $.FOffset$3 = $.FRow = 0;
      $.FData = "";
   }
   /// function TTextBuffer.BOF() : Boolean
   ///  [line: 1399, column: 22, file: System.Text.Parser]
   ,BOF$4:function(Self) {
      var Result = false;
      if (!TTextBuffer.Empty$1(Self)) {
         Result = Self.FOffset$3<1;
      }
      return Result
   }
   /// function TTextBuffer.Bookmark() : TTextBufferBookmark
   ///  [line: 318, column: 22, file: System.Text.Parser]
   ,Bookmark$1:function(Self) {
      var Result = null;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TTextBuffer.Empty$1(Self)) {
         throw Exception.Create($New(ETextBuffer),"Failed to bookmark location, buffer is empty error");
      } else {
         Result = TObject.Create($New(TTextBufferBookmark));
         Result.bbOffset = Self.FOffset$3;
         Result.bbCol = Self.FCol;
         Result.bbRow = Self.FRow;
      }
      return Result
   }
   /// procedure TTextBuffer.Clear()
   ///  [line: 260, column: 23, file: System.Text.Parser]
   ,Clear$8:function(Self) {
      Self.FData = "";
      Self.FOffset$3 = -1;
      Self.FLength = 0;
      Self.FCol = -1;
      Self.FRow = -1;
   }
   /// function TTextBuffer.Compare(const CompareText: String; const CaseSensitive: Boolean) : Boolean
   ///  [line: 931, column: 22, file: System.Text.Parser]
   ,Compare:function(Self, CompareText$1, CaseSensitive) {
      var Result = false;
      var LenToRead = 0,
         ReadData = {v:""};
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TTextBuffer.Empty$1(Self)) {
         TW3ErrorObject.SetLastError$1(Self,$R[57]);
      } else {
         if (TTextBuffer.BOF$4(Self)) {
            if (!TTextBuffer.First$1(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$4(Self)) {
            TW3ErrorObject.SetLastError$1(Self,$R[59]);
            return Result;
         }
         LenToRead = CompareText$1.length;
         if (LenToRead>0) {
            ReadData.v = "";
            if (TTextBuffer.Peek$1(Self,LenToRead,ReadData)) {
               if (CaseSensitive) {
                  Result = ReadData.v==CompareText$1;
               } else {
                  Result = (ReadData.v).toLocaleLowerCase()==(CompareText$1).toLocaleLowerCase();
               }
            }
         } else {
            TW3ErrorObject.SetLastError$1(Self,$R[62]);
         }
      }
      return Result
   }
   /// procedure TTextBuffer.ConsumeCRLF()
   ///  [line: 1070, column: 23, file: System.Text.Parser]
   ,ConsumeCRLF:function(Self) {
      if (!TTextBuffer.Empty$1(Self)) {
         if (TTextBuffer.BOF$4(Self)) {
            if (!TTextBuffer.First$1(Self)) {
               return;
            }
         }
         if (TTextBuffer.EOF$4(Self)) {
            TW3ErrorObject.SetLastError$1(Self,$R[59]);
            return;
         }
         if (Self.FData.charAt(Self.FOffset$3-1)=="\r") {
            if (Self.FData.charAt(Self.FOffset$3)=="\n") {
               (Self.FOffset$3+= 2);
            } else {
               ++Self.FOffset$3;
            }
            ++Self.FRow;
            Self.FCol = 0;
         }
      }
   }
   /// procedure TTextBuffer.ConsumeJunk()
   ///  [line: 971, column: 23, file: System.Text.Parser]
   ,ConsumeJunk:function(Self) {
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TTextBuffer.Empty$1(Self)) {
         TW3ErrorObject.SetLastError$1(Self,$R[57]);
      } else {
         if (TTextBuffer.BOF$4(Self)) {
            if (!TTextBuffer.First$1(Self)) {
               return;
            }
         }
         if (TTextBuffer.EOF$4(Self)) {
            TW3ErrorObject.SetLastError$1(Self,$R[59]);
            return;
         }
         do {
            {var $temp41 = TTextBuffer.Current(Self);
               if ($temp41==" ") {
                  /* null */
               }
                else if ($temp41=="\"") {
                  break;
               }
                else if ($temp41=="\b") {
                  /* null */
               }
                else if ($temp41=="\t") {
                  /* null */
               }
                else if ($temp41=="\/") {
                  if (TTextBuffer.Compare(Self,"\/*",false)) {
                     if (TTextBuffer.ReadTo$2(Self,"*\/")) {
                        (Self.FOffset$3+= 2);
                        continue;
                     } else {
                        TW3ErrorObject.SetLastError$1(Self,$R[60]);
                     }
                  } else {
                     if (TTextBuffer.Compare(Self,"\/\/",false)) {
                        if (TTextBuffer.ReadToEOL(Self)) {
                           continue;
                        } else {
                           TW3ErrorObject.SetLastError$1(Self,$R[61]);
                        }
                     }
                  }
               }
                else if ($temp41=="(") {
                  if (TTextBuffer.Compare(Self,"(*",false)&&(!TTextBuffer.Compare(Self,"(*)",false))) {
                     if (TTextBuffer.ReadTo$2(Self,"*)")) {
                        (Self.FOffset$3+= 2);
                        continue;
                     } else {
                        TW3ErrorObject.SetLastError$1(Self,$R[60]);
                     }
                  } else {
                     break;
                  }
               }
                else if ($temp41=="\r") {
                  if (Self.FData.charAt(Self.FOffset$3)=="\n") {
                     (Self.FOffset$3+= 2);
                  } else {
                     ++Self.FOffset$3;
                  }
                  continue;
               }
                else if ($temp41=="\n") {
                  ++Self.FOffset$3;
                  continue;
               }
                else {
                  break;
               }
            }
            if (!TTextBuffer.Next$1(Self)) {
               break;
            }
         } while (!TTextBuffer.EOF$4(Self));
      }
   }
   /// constructor TTextBuffer.Create(const BufferText: String)
   ///  [line: 245, column: 25, file: System.Text.Parser]
   ,Create$80:function(Self, BufferText) {
      TW3ErrorObject.Create$45(Self);
      if (BufferText.length>0) {
         TTextBuffer.LoadBufferText(Self,BufferText);
      } else {
         TTextBuffer.Clear$8(Self);
      }
      return Self
   }
   /// function TTextBuffer.CrLf() : Boolean
   ///  [line: 498, column: 22, file: System.Text.Parser]
   ,CrLf:function(Self) {
      return (!TTextBuffer.Empty$1(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData.length&&Self.FData.charAt(Self.FOffset$3-1)=="\r"&&Self.FData.charAt(Self.FOffset$3)=="\n";
   }
   /// function TTextBuffer.Current() : Char
   ///  [line: 1368, column: 22, file: System.Text.Parser]
   ,Current:function(Self) {
      var Result = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TTextBuffer.Empty$1(Self)) {
         TW3ErrorObject.SetLastError$1(Self,$R[57]);
         Result = "";
      } else {
         if (Self.FOffset$3>=1) {
            if (Self.FOffset$3<=Self.FData.length) {
               Result = Self.FData.charAt(Self.FOffset$3-1);
            } else {
               TW3ErrorObject.SetLastError$1(Self,$R[59]);
               Result = "";
            }
         } else {
            TW3ErrorObject.SetLastError$1(Self,$R[58]);
            Result = "";
         }
      }
      return Result
   }
   /// destructor TTextBuffer.Destroy()
   ///  [line: 254, column: 24, file: System.Text.Parser]
   ,Destroy:function(Self) {
      TTextBuffer.Clear$8(Self);
      TW3ErrorObject.Destroy(Self);
   }
   /// function TTextBuffer.Empty() : Boolean
   ///  [line: 1101, column: 22, file: System.Text.Parser]
   ,Empty$1:function(Self) {
      return Self.FLength<1;
   }
   /// function TTextBuffer.EOF() : Boolean
   ///  [line: 1405, column: 22, file: System.Text.Parser]
   ,EOF$4:function(Self) {
      var Result = false;
      if (!TTextBuffer.Empty$1(Self)) {
         Result = Self.FOffset$3>Self.FData.length;
      }
      return Result
   }
   /// function TTextBuffer.First() : Boolean
   ///  [line: 1253, column: 22, file: System.Text.Parser]
   ,First$1:function(Self) {
      var Result = false;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TTextBuffer.Empty$1(Self)) {
         TW3ErrorObject.SetLastError$1(Self,$R[57]);
      } else {
         Self.FOffset$3 = 1;
         Result = true;
      }
      return Result
   }
   /// procedure TTextBuffer.LoadBufferText(const NewBuffer: String)
   ///  [line: 286, column: 23, file: System.Text.Parser]
   ,LoadBufferText:function(Self, NewBuffer) {
      var TempLen = 0;
      TTextBuffer.Clear$8(Self);
      TempLen = NewBuffer.length;
      if (TempLen>0) {
         Self.FData = NewBuffer;
         Self.FOffset$3 = 0;
         Self.FCol = 0;
         Self.FRow = 0;
         Self.FLength = TempLen;
      }
   }
   /// function TTextBuffer.Next() : Boolean
   ///  [line: 1303, column: 22, file: System.Text.Parser]
   ,Next$1:function(Self) {
      var Result = false;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TTextBuffer.Empty$1(Self)) {
         TW3ErrorObject.SetLastError$1(Self,$R[57]);
      } else {
         if (TTextBuffer.BOF$4(Self)) {
            if (!TTextBuffer.First$1(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$4(Self)) {
            TW3ErrorObject.SetLastError$1(Self,$R[59]);
            return Result;
         }
         ++Self.FOffset$3;
         ++Self.FCol;
         if (Self.FOffset$3<Self.FData.length) {
            if (Self.FData.charAt(Self.FOffset$3-1)=="\r") {
               ++Self.FRow;
               Self.FCol = 0;
               if (Self.FData.charAt(Self.FOffset$3)=="\n") {
                  (Self.FOffset$3+= 2);
               } else {
                  ++Self.FOffset$3;
               }
            }
         }
         Result = true;
      }
      return Result
   }
   /// function TTextBuffer.NextNoCrLf() : Boolean
   ///  [line: 1279, column: 22, file: System.Text.Parser]
   ,NextNoCrLf:function(Self) {
      var Result = false;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TTextBuffer.Empty$1(Self)) {
         TW3ErrorObject.SetLastError$1(Self,$R[57]);
      } else {
         Result = Self.FOffset$3<=Self.FData.length;
         if (Result) {
            ++Self.FOffset$3;
            if (!function(v$){return ((v$=="\r")||(v$=="\n"))}(Self.FData.charAt(Self.FOffset$3-1))) {
               ++Self.FCol;
            }
         } else {
            TW3ErrorObject.SetLastError$1(Self,$R[59]);
         }
      }
      return Result
   }
   /// function TTextBuffer.Peek(CharCount: Integer; var TextRead: String) : Boolean
   ///  [line: 1217, column: 22, file: System.Text.Parser]
   ,Peek$1:function(Self, CharCount, TextRead) {
      var Result = false;
      var Mark = null;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      Result = !TTextBuffer.Empty$1(Self);
      if (Result) {
         Result = !TTextBuffer.EOF$4(Self);
         if (Result) {
            Result = false;
            SetLength(TextRead,0);
            Mark = TTextBuffer.Bookmark$1(Self);
            try {
               while (CharCount>0) {
                  TextRead.v = TextRead.v+TTextBuffer.Current(Self);
                  if (!TTextBuffer.Next$1(Self)) {
                     break;
                  }
                  --CharCount;
               }
            } finally {
               TTextBuffer.Restore(Self,Mark);
            }
            Result = TextRead.v.length>0;
         } else {
            TW3ErrorObject.SetLastError$1(Self,$R[59]);
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,$R[57]);
      }
      return Result
   }
   /// function TTextBuffer.ReadCommaList(var List: TStrArray) : Boolean
   ///  [line: 1642, column: 22, file: System.Text.Parser]
   ,ReadCommaList:function(Self, List) {
      var Result = false;
      var LTemp$13 = "";
      var LValue = "";
      List.v.length=0;
      if (!TTextBuffer.Empty$1(Self)) {
         TTextBuffer.ConsumeJunk(Self);
         while (!TTextBuffer.EOF$4(Self)) {
            {var $temp42 = TTextBuffer.Current(Self);
               if ($temp42=="\t") {
                  /* null */
               }
                else if ($temp42=="\r") {
                  TTextBuffer.ConsumeCRLF(Self)               }
                else if ($temp42=="\n") {
                  TTextBuffer.ConsumeCRLF(Self)               }
                else if ($temp42=="") {
                  break;
               }
                else if ($temp42==";") {
                  Result = true;
                  break;
               }
                else if ($temp42=="\"") {
                  LValue = TTextBuffer.ReadQuotedString(Self);
                  if (LValue.length>0) {
                     List.v.push(LValue);
                     LValue = "";
                  }
               }
                else if ($temp42==",") {
                  LTemp$13 = Trim$_String_(LTemp$13);
                  if (LTemp$13.length>0) {
                     List.v.push(LTemp$13);
                     LTemp$13 = "";
                  }
               }
                else {
                  LTemp$13+=TTextBuffer.Current(Self);
               }
            }
            if (!TTextBuffer.Next$1(Self)) {
               break;
            }
         }
         if (LTemp$13.length>0) {
            List.v.push(LTemp$13);
         }
         Result = List.v.length>0;
      }
      return Result
   }
   /// function TTextBuffer.ReadQuotedString() : String
   ///  [line: 1713, column: 22, file: System.Text.Parser]
   ,ReadQuotedString:function(Self) {
      var Result = "";
      var TempChar = "";
      if (!TTextBuffer.Empty$1(Self)) {
         if (!TTextBuffer.EOF$4(Self)) {
            if (TTextBuffer.Current(Self)!="\"") {
               TW3ErrorObject.SetLastError$1(Self,"Failed to read quoted string, expected index on \" character error");
               return Result;
            }
            if (!TTextBuffer.NextNoCrLf(Self)) {
               TW3ErrorObject.SetLastError$1(Self,"Failed to skip initial \" character error");
               return Result;
            }
            while (!TTextBuffer.EOF$4(Self)) {
               TempChar = TTextBuffer.Current(Self);
               if (TempChar=="\"") {
                  if (!TTextBuffer.NextNoCrLf(Self)) {
                     TW3ErrorObject.SetLastError$1(Self,"failed to skip final \" character in string error");
                  }
                  break;
               }
               Result+=TempChar;
               if (!TTextBuffer.NextNoCrLf(Self)) {
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TTextBuffer.ReadTo(MatchText: String) : Boolean
   ///  [line: 822, column: 22, file: System.Text.Parser]
   ,ReadTo$2:function(Self, MatchText) {
      var Result = false;
      var MatchLen = 0,
         TempCache = {v:""};
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TTextBuffer.Empty$1(Self)) {
         Result = false;
         TW3ErrorObject.SetLastError$1(Self,$R[57]);
      } else {
         if (TTextBuffer.BOF$4(Self)) {
            if (!TTextBuffer.First$1(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$4(Self)) {
            TW3ErrorObject.SetLastError$1(Self,$R[59]);
            return Result;
         }
         MatchLen = MatchText.length;
         if (MatchLen>0) {
            MatchText = (MatchText).toLocaleLowerCase();
            do {
               TempCache.v = "";
               if (TTextBuffer.Peek$1(Self,MatchLen,TempCache)) {
                  TempCache.v = (TempCache.v).toLocaleLowerCase();
                  Result = SameText(TempCache.v,MatchText);
                  if (Result) {
                     break;
                  }
               }
               if (!TTextBuffer.Next$1(Self)) {
                  break;
               }
            } while (!TTextBuffer.EOF$4(Self));
         }
      }
      return Result
   }
   /// function TTextBuffer.ReadTo(const Resignators: TSysCharSet; var TextRead: String) : Boolean
   ///  [line: 769, column: 22, file: System.Text.Parser]
   ,ReadTo$1:function(Self, Resignators, TextRead$1) {
      var Result = false;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      SetLength(TextRead$1,0);
      if (TTextBuffer.Empty$1(Self)) {
         Result = false;
         TW3ErrorObject.SetLastError$1(Self,$R[57]);
      } else {
         if (TTextBuffer.BOF$4(Self)) {
            if (!TTextBuffer.First$1(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$4(Self)) {
            TW3ErrorObject.SetLastError$1(Self,$R[59]);
            return Result;
         }
         do {
            if (Resignators.indexOf(TTextBuffer.Current(Self))>=0) {
               break;
            } else {
               TextRead$1.v+=TTextBuffer.Current(Self);
            }
            if (!TTextBuffer.Next$1(Self)) {
               break;
            }
         } while (!TTextBuffer.EOF$4(Self));
         Result = TextRead$1.v.length>0;
      }
      return Result
   }
   /// function TTextBuffer.ReadToEOL() : Boolean
   ///  [line: 431, column: 22, file: System.Text.Parser]
   ,ReadToEOL:function(Self) {
      var Result = false;
      var LStart$2 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TTextBuffer.Empty$1(Self)) {
         TW3ErrorObject.SetLastError$1(Self,$R[57]);
      } else {
         if (TTextBuffer.BOF$4(Self)) {
            if (!TTextBuffer.First$1(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$4(Self)) {
            TW3ErrorObject.SetLastError$1(Self,$R[59]);
            return Result;
         }
         LStart$2 = Self.FOffset$3;
         do {
            if (Self.FData.charAt(Self.FOffset$3-1)=="\r"&&Self.FData.charAt(Self.FOffset$3)=="\n") {
               Result = true;
               break;
            } else {
               ++Self.FOffset$3;
               ++Self.FCol;
            }
         } while (!TTextBuffer.EOF$4(Self));
         if (!Result) {
            if (TTextBuffer.EOF$4(Self)) {
               if (LStart$2<Self.FOffset$3) {
                  Result = true;
               }
            }
         }
      }
      return Result
   }
   /// function TTextBuffer.ReadWord(var TextRead: String) : Boolean
   ///  [line: 1601, column: 22, file: System.Text.Parser]
   ,ReadWord$2:function(Self, TextRead$2) {
      var Result = false;
      var el$3 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      TextRead$2.v = "";
      if (TTextBuffer.Empty$1(Self)) {
         TW3ErrorObject.SetLastError$1(Self,"Failed to read word, buffer is empty error");
      } else {
         TTextBuffer.ConsumeJunk(Self);
         if (TTextBuffer.EOF$4(Self)) {
            TW3ErrorObject.SetLastError$1(Self,"Failed to read word, unexpected EOF");
         } else {
            do {
               el$3 = TTextBuffer.Current(Self);
               if ((((el$3>="A")&&(el$3<="Z"))||((el$3>="a")&&(el$3<="z"))||((el$3>="0")&&(el$3<="9"))||(el$3=="_")||(el$3=="-"))) {
                  TextRead$2.v+=el$3;
               } else {
                  break;
               }
               if (!TTextBuffer.NextNoCrLf(Self)) {
                  break;
               }
            } while (!TTextBuffer.EOF$4(Self));
            Result = TextRead$2.v.length>0;
         }
      }
      return Result
   }
   /// procedure TTextBuffer.Restore(const Mark: TTextBufferBookmark)
   ///  [line: 336, column: 23, file: System.Text.Parser]
   ,Restore:function(Self, Mark$1) {
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TTextBuffer.Empty$1(Self)) {
         throw Exception.Create($New(ETextBuffer),"Failed to restore bookmark, buffer is empty error");
      } else {
         if (Mark$1!==null) {
            Self.FOffset$3 = Mark$1.bbOffset;
            Self.FCol = Mark$1.bbCol;
            Self.FRow = Mark$1.bbRow;
            TObject.Free(Mark$1);
         } else {
            throw Exception.Create($New(ETextBuffer),"Failed to restore bookmark, object was nil error");
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
};
TTextBuffer.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TTbTagValue = record
///  [line: 48, column: 3, file: System.Text.Parser]
function Copy$TTbTagValue(s,d) {
   return d;
}
function Clone$TTbTagValue($) {
   return {

   }
}
/// TTbTagData = record
///  [line: 53, column: 3, file: System.Text.Parser]
function Copy$TTbTagData(s,d) {
   return d;
}
function Clone$TTbTagData($) {
   return {

   }
}
/// TParserModelObject = class (TObject)
///  [line: 197, column: 3, file: System.Text.Parser]
var TParserModelObject = {
   $ClassName:"TParserModelObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FChildren = [];
      $.FParent = null;
   }
   /// procedure TParserModelObject.Clear()
   ///  [line: 1779, column: 30, file: System.Text.Parser]
   ,Clear$9:function(Self) {
      var LItem$3 = null;
      var a$475 = 0;
      try {
         var a$476 = [];
         a$476 = Self.FChildren;
         var $temp43;
         for(a$475=0,$temp43=a$476.length;a$475<$temp43;a$475++) {
            LItem$3 = a$476[a$475];
            if (LItem$3!==null) {
               TObject.Free(LItem$3);
            }
         }
      } finally {
         Self.FChildren.length=0;
      }
   }
   /// constructor TParserModelObject.Create(const AParent: TParserModelObject)
   ///  [line: 1761, column: 32, file: System.Text.Parser]
   ,Create$81:function(Self, AParent) {
      TObject.Create(Self);
      Self.FParent = AParent;
      return Self
   }
   /// destructor TParserModelObject.Destroy()
   ///  [line: 1767, column: 31, file: System.Text.Parser]
   ,Destroy:function(Self) {
      TParserModelObject.Clear$9(Self);
      Self.FChildren.length=0;
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TParserContext = class (TW3ErrorObject)
///  [line: 167, column: 3, file: System.Text.Parser]
var TParserContext = {
   $ClassName:"TParserContext",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.FBuffer$3 = null;
      $.FStack$1 = [];
   }
   /// procedure TParserContext.ClearStack()
   ///  [line: 1865, column: 26, file: System.Text.Parser]
   ,ClearStack:function(Self) {
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      try {
         Self.FStack$1.length=0;
      } catch ($e) {
         var e$12 = $W($e);
         TW3ErrorObject.SetLastError$1(Self,("Internal error:"+e$12.FMessage))      }
   }
   /// constructor TParserContext.Create(const SourceCode: String)
   ///  [line: 1815, column: 28, file: System.Text.Parser]
   ,Create$82:function(Self, SourceCode) {
      TW3ErrorObject.Create$45(Self);
      Self.FBuffer$3 = TTextBuffer.Create$80($New(TTextBuffer),SourceCode);
      return Self
   }
   /// destructor TParserContext.Destroy()
   ///  [line: 1821, column: 27, file: System.Text.Parser]
   ,Destroy:function(Self) {
      TParserContext.ClearStack(Self);
      TObject.Free(Self.FBuffer$3);
      TW3ErrorObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
};
TParserContext.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TCustomParser = class (TW3ErrorObject)
///  [line: 184, column: 3, file: System.Text.Parser]
var TCustomParser = {
   $ClassName:"TCustomParser",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.FContext = null;
   }
   /// function TCustomParser.Parse() : Boolean
   ///  [line: 1887, column: 24, file: System.Text.Parser]
   ,Parse$2:function(Self) {
      var Result = false;
      Result = false;
      TW3ErrorObject.SetLastErrorF$1(Self,"No parser implemented for class %s",[TObject.ClassName(Self.ClassType)]);
      return Result
   }
   /// constructor TCustomParser.Create(const ParseContext: TParserContext)
   ///  [line: 1881, column: 27, file: System.Text.Parser]
   ,Create$83:function(Self, ParseContext) {
      TW3ErrorObject.Create$45(Self);
      Self.FContext = ParseContext;
      return Self
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,Parse$2$:function($){return $.ClassType.Parse$2($)}
};
TCustomParser.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// ETextBuffer = class (Exception)
///  [line: 33, column: 3, file: System.Text.Parser]
var ETextBuffer = {
   $ClassName:"ETextBuffer",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TSQLite3WriteTransaction = class (TW3WriteTransaction)
///  [line: 50, column: 3, file: SmartNJ.Sqlite3]
var TSQLite3WriteTransaction = {
   $ClassName:"TSQLite3WriteTransaction",$Parent:TW3WriteTransaction
   ,$Init:function ($) {
      TW3WriteTransaction.$Init($);
   }
   /// procedure TSQLite3WriteTransaction.Execute(CB: TW3DatabaseCBStandard)
   ///  [line: 151, column: 36, file: SmartNJ.Sqlite3]
   ,Execute$2:function(Self, CB$6) {
      var LStatement = null;
      TW3OwnedErrorObject.ClearLastError(Self);
      if (!Self.FHandle$4) {
         if (TW3Transaction.GetDatabase(Self)!==null) {
            try {
               TSQLite3Database.Prepare($As(TW3Transaction.GetDatabase(Self),TSQLite3Database),Self);
            } catch ($e) {
               var e$13 = $W($e);
               TW3OwnedErrorObject.SetLastError(Self,e$13.FMessage);
               if (CB$6) {
                  CB$6(Self,false);
               }
               return;
            }
            LStatement = Self.FHandle$4;
            LStatement.run(TW3TransactionParameters.GetValues(Self.FParams),function (Error$4) {
               if (Error$4!==null) {
                  TW3OwnedErrorObject.SetLastError(Self,Error$4.message);
                  if (CB$6) {
                     CB$6(Self,false);
                  }
                  return;
               }
               if (CB$6) {
                  CB$6(Self,true);
               }
            });
         }
      }
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$46:TW3Component.Create$46
   ,FinalizeObject:TW3Transaction.FinalizeObject
   ,InitializeObject:TW3Transaction.InitializeObject
   ,CanExecute:TW3SQLTransaction.CanExecute
   ,Execute$2$:function($){return $.ClassType.Execute$2.apply($.ClassType, arguments)}
};
TSQLite3WriteTransaction.$Intf={
   ISQLTransaction:[TW3SQLTransaction.SetHandle,TW3SQLTransaction.GetSQL,TW3SQLTransaction.SetSQL,TW3SQLTransaction.CanExecute]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TSQLite3ReadTransaction = class (TW3ReadTransaction)
///  [line: 45, column: 3, file: SmartNJ.Sqlite3]
var TSQLite3ReadTransaction = {
   $ClassName:"TSQLite3ReadTransaction",$Parent:TW3ReadTransaction
   ,$Init:function ($) {
      TW3ReadTransaction.$Init($);
   }
   /// procedure TSQLite3ReadTransaction.Execute(CB: TW3DatabaseCBStandard)
   ///  [line: 101, column: 35, file: SmartNJ.Sqlite3]
   ,Execute$2:function(Self, CB$7) {
      var LStatement$1 = null;
      TW3OwnedErrorObject.ClearLastError(Self);
      if (!Self.FHandle$4) {
         if (TW3Transaction.GetDatabase(Self)!==null) {
            try {
               TSQLite3Database.Prepare($As(TW3Transaction.GetDatabase(Self),TSQLite3Database),Self);
            } catch ($e) {
               var e$14 = $W($e);
               TW3OwnedErrorObject.SetLastError(Self,e$14.FMessage);
               if (CB$7) {
                  CB$7(Self,false);
               }
               return;
            }
            LStatement$1 = Self.FHandle$4;
            LStatement$1.all(TW3TransactionParameters.GetValues(Self.FParams),function (Error$5, Rows) {
               if (Error$5!==null) {
                  TW3OwnedErrorObject.SetLastError(Self,Error$5.message);
                  if (CB$7) {
                     CB$7(Self,false);
                  }
                  return;
               }
            });
         }
      }
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$46:TW3Component.Create$46
   ,FinalizeObject:TW3Transaction.FinalizeObject
   ,InitializeObject:TW3Transaction.InitializeObject
   ,CanExecute:TW3SQLTransaction.CanExecute
   ,Execute$2$:function($){return $.ClassType.Execute$2.apply($.ClassType, arguments)}
};
TSQLite3ReadTransaction.$Intf={
   ISQLTransaction:[TW3SQLTransaction.SetHandle,TW3SQLTransaction.GetSQL,TW3SQLTransaction.SetSQL,TW3SQLTransaction.CanExecute]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TSQLite3Database = class (TW3Database)
///  [line: 55, column: 3, file: SmartNJ.Sqlite3]
var TSQLite3Database = {
   $ClassName:"TSQLite3Database",$Parent:TW3Database
   ,$Init:function ($) {
      TW3Database.$Init($);
      $.FAccMode = 0;
      $.FHandle$5 = undefined;
   }
   /// function TSQLite3Database.AccessModeToAPIValue() : Integer
   ///  [line: 322, column: 27, file: SmartNJ.Sqlite3]
   ,AccessModeToAPIValue:function(Self) {
      var Result = 0;
      var LAccess = null;
      LAccess = SQLite3();
      switch (Self.FAccMode) {
         case 0 :
            Result = LAccess.OPEN_CREATE;
            break;
         case 1 :
            Result = LAccess.OPEN_READONLY;
            break;
         case 2 :
            Result = LAccess.OPEN_READWRITE;
            break;
         case 3 :
            Result = LAccess.OPEN_READWRITE|LAccess.OPEN_CREATE;
            break;
         case 4 :
            Result = LAccess.OPEN_READWRITE|LAccess.OPEN_CREATE;
            break;
      }
      return Result
   }
   /// procedure TSQLite3Database.apiClose(CB: TW3DatabaseCBStandard)
   ///  [line: 400, column: 28, file: SmartNJ.Sqlite3]
   ,apiClose:function(Self, CB$8) {
      var Access$6 = null;
      TW3OwnedErrorObject.ClearLastError(Self);
      if (TW3Database.GetActive$2(Self)) {
         Access$6 = TSQLite3Database.GetDBAPI(Self);
         Access$6.close(function (err) {
            if (err) {
               WriteLn("Failed to close: "+err.message);
               TW3OwnedErrorObject.SetLastError(Self,err.message);
            }
            try {
               TSQLite3Database.SetHandle$1(Self,null);
            } finally {
               if (CB$8) {
                  CB$8(Self,err===null);
               }
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastError(Self,"Database not active error");
         if (CB$8) {
            CB$8(Self,false);
         }
      }
   }
   /// procedure TSQLite3Database.apiOpen(CB: TW3DatabaseCBStandard)
   ///  [line: 367, column: 28, file: SmartNJ.Sqlite3]
   ,apiOpen:function(Self, CB$9) {
      var LDBRef = undefined;
      var LLibRef = null,
         LMode = 0,
         LFilename = "";
      LLibRef = SQLite3();
      LMode = TSQLite3Database.AccessModeToAPIValue(Self);
      LFilename = TW3Database.GetLocation(Self);
      try {
         LDBRef = new (LLibRef).Database(LFilename, LMode);
         TSQLite3Database.SetHandle$1(Self,LDBRef);
         if (CB$9) {
            CB$9(Self,true);
         }
      } catch ($e) {
         var e$15 = $W($e);
         TW3OwnedErrorObject.SetLastError(Self,e$15.FMessage);
         if (CB$9) {
            CB$9(Self,false);
         } else {
            throw $e;
         }
      }
   }
   /// function TSQLite3Database.CreateReadTransaction(var Transaction: TW3ReadTransaction) : Boolean
   ///  [line: 351, column: 27, file: SmartNJ.Sqlite3]
   ,CreateReadTransaction:function(Self, Transaction$1) {
      var Result = false;
      Transaction$1.v = TW3Component.Create$46$($New(TSQLite3ReadTransaction),Self);
      TW3Transaction.SetDatabase(Transaction$1.v,Self);
      TW3Database.RegisterTransaction(Self,Transaction$1.v);
      Result = true;
      return Result
   }
   /// function TSQLite3Database.CreateWriteTransaction(var Transaction: TW3WriteTransaction) : Boolean
   ///  [line: 359, column: 27, file: SmartNJ.Sqlite3]
   ,CreateWriteTransaction:function(Self, Transaction$2) {
      var Result = false;
      Transaction$2.v = TW3Component.Create$46$($New(TSQLite3WriteTransaction),Self);
      TW3Transaction.SetDatabase(Transaction$2.v,Self);
      TW3Database.RegisterTransaction(Self,Transaction$2.v);
      Result = true;
      return Result
   }
   /// function TSQLite3Database.GetDBAPI() : JSQLite3Database
   ///  [line: 236, column: 27, file: SmartNJ.Sqlite3]
   ,GetDBAPI:function(Self) {
      var Result = null;
      if (Self.FHandle$5) {
         Result = Self.FHandle$5;
      } else {
         Result = null;
      }
      return Result
   }
   /// procedure TSQLite3Database.InitializeObject()
   ///  [line: 202, column: 28, file: SmartNJ.Sqlite3]
   ,InitializeObject:function(Self) {
      TW3Component.InitializeObject(Self);
      Self.FAccMode = 3;
      Self.FHandle$5 = undefined;
      Self.FOptions$3.AutoWriteToConsole = true;
   }
   /// procedure TSQLite3Database.Prepare(const Transaction: TW3SQLTransaction)
   ///  [line: 215, column: 28, file: SmartNJ.Sqlite3]
   ,Prepare:function(Self, Transaction$3) {
      var API = null,
         res = null;
      if (Transaction$3!==null) {
         if (TW3Database.GetActive$2(Self)) {
            API = TSQLite3Database.GetDBAPI(Self);
            res = API.prepare(Transaction$3.FSQL);
            $AsIntf(Transaction$3,"ISQLTransaction")[0](res);
         } else {
            throw Exception.Create($New(ESQLite3Database),"Prepare failed, database not active error");
         }
      } else {
         throw Exception.Create($New(ESQLite3Database),"Prepare failed, transaction was nil error");
      }
   }
   /// procedure TSQLite3Database.SetHandle(NewHandle: THandle)
   ///  [line: 335, column: 28, file: SmartNJ.Sqlite3]
   ,SetHandle$1:function(Self, NewHandle$3) {
      Self.FHandle$5 = NewHandle$3;
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$46:TW3Component.Create$46
   ,FinalizeObject:TW3Component.FinalizeObject
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
   ,apiClose$:function($){return $.ClassType.apiClose.apply($.ClassType, arguments)}
   ,apiOpen$:function($){return $.ClassType.apiOpen.apply($.ClassType, arguments)}
   ,CreateReadTransaction$:function($){return $.ClassType.CreateReadTransaction.apply($.ClassType, arguments)}
   ,CreateWriteTransaction$:function($){return $.ClassType.CreateWriteTransaction.apply($.ClassType, arguments)}
};
TSQLite3Database.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TSQLite3AccessMode enumeration
///  [line: 30, column: 3, file: SmartNJ.Sqlite3]
var TSQLite3AccessMode = [ "sqaCreate", "sqaReadOnly", "sqaReadWrite", "sqaReadWriteCreate", "sqaDefault" ];
/// ESQLite3Database = class (EW3Database)
///  [line: 27, column: 3, file: SmartNJ.Sqlite3]
var ESQLite3Database = {
   $ClassName:"ESQLite3Database",$Parent:EW3Database
   ,$Init:function ($) {
      EW3Database.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SQLite3() {
   return require("sqlite3").verbose();
};
/// TW3Structure = class (TObject)
///  [line: 93, column: 3, file: System.Structure]
var TW3Structure = {
   $ClassName:"TW3Structure",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TW3Structure.ReadBool(const Name: String) : Boolean
   ///  [line: 207, column: 23, file: System.Structure]
   ,ReadBool$1:function(Self, Name$14) {
      return (TW3Structure.Read$6$(Self,Name$14)?true:false);
   }
   /// function TW3Structure.ReadBytes(const Name: String) : TByteArray
   ///  [line: 158, column: 23, file: System.Structure]
   ,ReadBytes$1:function(Self, Name$15) {
      return TW3Structure.Read$6$(Self,Name$15);
   }
   /// function TW3Structure.ReadDateTime(const Name: String) : TDateTime
   ///  [line: 217, column: 23, file: System.Structure]
   ,ReadDateTime$2:function(Self, Name$16) {
      return TW3Structure.Read$6$(Self,Name$16);
   }
   /// function TW3Structure.ReadFloat(const Name: String) : Float
   ///  [line: 212, column: 23, file: System.Structure]
   ,ReadFloat$1:function(Self, Name$17) {
      return Number(TW3Structure.Read$6$(Self,Name$17));
   }
   /// function TW3Structure.ReadInt(const Name: String) : Integer
   ///  [line: 202, column: 23, file: System.Structure]
   ,ReadInt$1:function(Self, Name$18) {
      return parseInt(TW3Structure.Read$6$(Self,Name$18),10);
   }
   /// function TW3Structure.ReadString(const Name: String; const Decode: Boolean) : String
   ///  [line: 194, column: 23, file: System.Structure]
   ,ReadString$3:function(Self, Name$19, Decode$3) {
      var Result = "";
      if (Decode$3) {
         Result = TString.DecodeBase64(TString,String(TW3Structure.Read$6$(Self,Name$19)));
      } else {
         Result = String(TW3Structure.Read$6$(Self,Name$19));
      }
      return Result
   }
   /// procedure TW3Structure.WriteBool(const Name: String; value: Boolean)
   ///  [line: 179, column: 24, file: System.Structure]
   ,WriteBool$1:function(Self, Name$20, value$2) {
      TW3Structure.Write$4$(Self,Name$20,value$2);
   }
   /// procedure TW3Structure.WriteBytes(const Name: String; const Value: TByteArray)
   ///  [line: 130, column: 24, file: System.Structure]
   ,WriteBytes$1:function(Self, Name$21, Value$60) {
      try {
         TW3Structure.Write$4$(Self,Name$21,TDatatype.BytesToBase64$1(TDatatype,Value$60));
      } catch ($e) {
         var e$16 = $W($e);
         throw EW3Exception.CreateFmt($New(EW3Structure),"Failed to write bytes to structure, system threw exception [%s] with message [%s]",[TObject.ClassName(e$16.ClassType), e$16.FMessage]);
      }
   }
   /// procedure TW3Structure.WriteDateTime(const Name: String; value: TDateTime)
   ///  [line: 189, column: 24, file: System.Structure]
   ,WriteDateTime$1:function(Self, Name$22, value$3) {
      TW3Structure.Write$4$(Self,Name$22,value$3);
   }
   /// procedure TW3Structure.WriteFloat(const Name: String; value: Float)
   ///  [line: 184, column: 24, file: System.Structure]
   ,WriteFloat:function(Self, Name$23, value$4) {
      TW3Structure.Write$4$(Self,Name$23,value$4);
   }
   /// procedure TW3Structure.WriteInt(const Name: String; value: Integer)
   ///  [line: 174, column: 24, file: System.Structure]
   ,WriteInt:function(Self, Name$24, value$5) {
      TW3Structure.Write$4$(Self,Name$24,value$5);
   }
   /// procedure TW3Structure.WriteStream(const Name: String; const Value: TStream)
   ///  [line: 142, column: 24, file: System.Structure]
   ,WriteStream:function(Self, Name$25, Value$61) {
      var LBytes$5 = [];
      if (Value$61!==null) {
         if (TStream.GetSize$(Value$61)>0) {
            TStream.SetPosition$(Value$61,0);
            LBytes$5 = TStream.Read(Value$61,TStream.GetSize$(Value$61));
         }
         TW3Structure.WriteBytes$1(Self,Name$25,LBytes$5);
      } else {
         throw Exception.Create($New(EW3Structure),"Failed to write stream to structure, stream was nil error");
      }
   }
   /// procedure TW3Structure.WriteString(Name: String; Value: String; const Encode: Boolean)
   ///  [line: 163, column: 24, file: System.Structure]
   ,WriteString$2:function(Self, Name$26, Value$62, Encode$3) {
      if (Encode$3) {
         Value$62 = TString.EncodeBase64(TString,Value$62);
      }
      TW3Structure.Write$4$(Self,Name$26,Value$62);
   }
   ,Destroy:TObject.Destroy
   ,Clear$10$:function($){return $.ClassType.Clear$10($)}
   ,Exists$2$:function($){return $.ClassType.Exists$2.apply($.ClassType, arguments)}
   ,LoadFromBuffer$1$:function($){return $.ClassType.LoadFromBuffer$1.apply($.ClassType, arguments)}
   ,LoadFromStream$6$:function($){return $.ClassType.LoadFromStream$6.apply($.ClassType, arguments)}
   ,Read$6$:function($){return $.ClassType.Read$6.apply($.ClassType, arguments)}
   ,SaveToBuffer$1$:function($){return $.ClassType.SaveToBuffer$1.apply($.ClassType, arguments)}
   ,SaveToStream$6$:function($){return $.ClassType.SaveToStream$6.apply($.ClassType, arguments)}
   ,Write$4$:function($){return $.ClassType.Write$4.apply($.ClassType, arguments)}
};
TW3Structure.$Intf={
   IW3StructureReadAccess:[TW3Structure.Exists$2,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TW3Structure.Read$6,TW3Structure.ReadBytes$1]
   ,IW3StructureWriteAccess:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.Write$4,TW3Structure.WriteBytes$1,TW3Structure.WriteStream]
   ,IW3Structure:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TW3Structure.Read$6,TW3Structure.Write$4]
}
/// EW3Structure = class (EW3Exception)
///  [line: 92, column: 3, file: System.Structure]
var EW3Structure = {
   $ClassName:"EW3Structure",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TJSONStructure = class (TW3Structure)
///  [line: 27, column: 3, file: System.Structure.JSON]
var TJSONStructure = {
   $ClassName:"TJSONStructure",$Parent:TW3Structure
   ,$Init:function ($) {
      TW3Structure.$Init($);
      $.FObj = null;
   }
   /// procedure TJSONStructure.Clear()
   ///  [line: 146, column: 26, file: System.Structure.JSON]
   ,Clear$10:function(Self) {
      TJSONObject.Clear$1(Self.FObj);
   }
   /// constructor TJSONStructure.Create()
   ///  [line: 64, column: 28, file: System.Structure.JSON]
   ,Create$84:function(Self) {
      TObject.Create(Self);
      Self.FObj = TJSONObject.Create$64($New(TJSONObject));
      return Self
   }
   /// destructor TJSONStructure.Destroy()
   ///  [line: 70, column: 27, file: System.Structure.JSON]
   ,Destroy:function(Self) {
      TObject.Free(Self.FObj);
      TObject.Destroy(Self);
   }
   /// function TJSONStructure.Exists(const Name: String) : Boolean
   ///  [line: 151, column: 25, file: System.Structure.JSON]
   ,Exists$2:function(Self, Name$27) {
      return TJSONObject.Exists$1(Self.FObj,Name$27);
   }
   /// procedure TJSONStructure.FromJSon(const Text: String)
   ///  [line: 93, column: 26, file: System.Structure.JSON]
   ,FromJSon:function(Self, Text$16) {
      var LOptions = [0];
      LOptions = [15];
      Self.FObj = TJSONObject.Create$66($New(TJSONObject),null,LOptions);
      TJSONObject.FromJSON(Self.FObj,Text$16);
   }
   /// procedure TJSONStructure.LoadFromBuffer(const Buffer: TBinaryData)
   ///  [line: 126, column: 26, file: System.Structure.JSON]
   ,LoadFromBuffer$1:function(Self, Buffer$7) {
      TJSONObject.LoadFromBuffer(Self.FObj,Buffer$7);
   }
   /// procedure TJSONStructure.LoadFromStream(const Stream: TStream)
   ///  [line: 116, column: 26, file: System.Structure.JSON]
   ,LoadFromStream$6:function(Self, Stream$4) {
      TJSONObject.LoadFromStream$1(Self.FObj,Stream$4);
   }
   /// function TJSONStructure.Read(const Name: String) : Variant
   ///  [line: 161, column: 25, file: System.Structure.JSON]
   ,Read$6:function(Self, Name$28) {
      var Result = {v:undefined};
      try {
         if (TJSONObject.Exists$1(Self.FObj,Name$28)) {
            TJSONObject.Read$3(Self.FObj,Name$28,Result);
         } else {
            Result.v = undefined;
         }
      } finally {return Result.v}
   }
   /// procedure TJSONStructure.SaveToBuffer(const Buffer: TBinaryData)
   ///  [line: 121, column: 26, file: System.Structure.JSON]
   ,SaveToBuffer$1:function(Self, Buffer$8) {
      TJSONObject.SaveToBuffer(Self.FObj,Buffer$8);
   }
   /// procedure TJSONStructure.SaveToStream(const Stream: TStream)
   ///  [line: 111, column: 26, file: System.Structure.JSON]
   ,SaveToStream$6:function(Self, Stream$5) {
      TJSONObject.SaveToStream$1(Self.FObj,Stream$5);
   }
   /// function TJSONStructure.ToJSonObject() : TJSONObject
   ///  [line: 102, column: 25, file: System.Structure.JSON]
   ,ToJSonObject:function(Self) {
      var Result = null;
      var LOptions$1 = [0];
      LOptions$1 = [15];
      Result = TJSONObject.Create$66($New(TJSONObject),null,LOptions$1);
      TJSONObject.FromJSON(Result,TJSONObject.ToJSON(Self.FObj));
      return Result
   }
   /// procedure TJSONStructure.Write(const Name: String; const Value: Variant)
   ///  [line: 156, column: 26, file: System.Structure.JSON]
   ,Write$4:function(Self, Name$29, Value$63) {
      TJSONObject.AddOrSet(Self.FObj,Name$29,Value$63);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Clear$10$:function($){return $.ClassType.Clear$10($)}
   ,Exists$2$:function($){return $.ClassType.Exists$2.apply($.ClassType, arguments)}
   ,LoadFromBuffer$1$:function($){return $.ClassType.LoadFromBuffer$1.apply($.ClassType, arguments)}
   ,LoadFromStream$6$:function($){return $.ClassType.LoadFromStream$6.apply($.ClassType, arguments)}
   ,Read$6$:function($){return $.ClassType.Read$6.apply($.ClassType, arguments)}
   ,SaveToBuffer$1$:function($){return $.ClassType.SaveToBuffer$1.apply($.ClassType, arguments)}
   ,SaveToStream$6$:function($){return $.ClassType.SaveToStream$6.apply($.ClassType, arguments)}
   ,Write$4$:function($){return $.ClassType.Write$4.apply($.ClassType, arguments)}
};
TJSONStructure.$Intf={
   IW3StructureReadAccess:[TJSONStructure.Exists$2,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TJSONStructure.Read$6,TW3Structure.ReadBytes$1]
   ,IW3StructureWriteAccess:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TJSONStructure.Write$4,TW3Structure.WriteBytes$1,TW3Structure.WriteStream]
   ,IW3Structure:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TJSONStructure.Read$6,TJSONStructure.Write$4]
}
/// TJSONObject = class (TObject)
///  [line: 37, column: 3, file: System.JSON]
var TJSONObject = {
   $ClassName:"TJSONObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FInstance = undefined;
      $.FOptions$6 = [0];
   }
   /// function TJSONObject.AddOrSet(const PropertyName: String; const Data: Variant) : TJSONObject
   ///  [line: 415, column: 22, file: System.JSON]
   ,AddOrSet:function(Self, PropertyName$19, Data$43) {
      var Result = null;
      Result = Self;
      if (TJSONObject.Exists$1(Self,PropertyName$19)) {
         if ($SetIn(Self.FOptions$6,3,0,4)) {
            Self.FInstance[PropertyName$19] = Data$43;
         } else {
            throw EW3Exception.CreateFmt($New(EJSONObject),"Failed to set value[%s], instance does not allow alteration",[PropertyName$19]);
         }
      } else if ($SetIn(Self.FOptions$6,1,0,4)) {
         Self.FInstance[PropertyName$19] = Data$43;
      } else {
         throw EW3Exception.CreateFmt($New(EJSONObject),"Failed to add value [%s], instance does not allow new properties",[PropertyName$19]);
      }
      return Result
   }
   /// procedure TJSONObject.Clear()
   ///  [line: 346, column: 23, file: System.JSON]
   ,Clear$1:function(Self) {
      Self.FInstance = TVariant.CreateObject();
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance; const Options: TJSONObjectOptions; Clone: Boolean)
   ///  [line: 185, column: 25, file: System.JSON]
   ,Create$67:function(Self, Instance$8, Options$5, Clone$1) {
      TObject.Create(Self);
      Self.FOptions$6 = Options$5.slice(0);
      if (TW3VariantHelper$Valid$2(Instance$8)) {
         if (TW3VariantHelper$IsObject(Instance$8)) {
            if (Clone$1) {
               Self.FInstance = TVariant.CreateObject();
               TVariant.ForEachProperty(Instance$8,function (Name$30, Data$44) {
                  var Result = 1;
                  TJSONObject.AddOrSet(Self,Name$30,Data$44);
                  Result = 1;
                  return Result
               });
            } else {
               Self.FInstance = Instance$8;
            }
         } else {
            throw Exception.Create($New(EJSONObject),"Failed to clone instance, reference is not an object");
         }
      } else {
         if ($SetIn(Self.FOptions$6,0,0,4)) {
            Self.FInstance = TVariant.CreateObject();
         } else {
            throw Exception.Create($New(EJSONObject),"Instance was nil, provided options does not allow initialization error");
         }
      }
      return Self
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance; const Options: TJSONObjectOptions)
   ///  [line: 161, column: 25, file: System.JSON]
   ,Create$66:function(Self, Instance$9, Options$6) {
      TObject.Create(Self);
      Self.FOptions$6 = Options$6.slice(0);
      if (TW3VariantHelper$Valid$2(Instance$9)&&TW3VariantHelper$IsObject(Instance$9)) {
         Self.FInstance = Instance$9;
      } else {
         if ($SetIn(Self.FOptions$6,0,0,4)) {
            Self.FInstance = TVariant.CreateObject();
         } else {
            throw Exception.Create($New(EJSONObject),"Instance was nil, provided options does not allow initialization error");
         }
      }
      return Self
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance)
   ///  [line: 142, column: 25, file: System.JSON]
   ,Create$65:function(Self, Instance$10) {
      TObject.Create(Self);
      Self.FOptions$6 = [15];
      if (Instance$10) {
         if (TW3VariantHelper$IsObject(Instance$10)) {
            Self.FInstance = Instance$10;
         } else {
            throw Exception.Create($New(EJSONObject),"Failed to attach to JSON instance, reference is not an object");
         }
      } else {
         Self.FInstance = TVariant.CreateObject();
      }
      return Self
   }
   /// constructor TJSONObject.Create()
   ///  [line: 135, column: 25, file: System.JSON]
   ,Create$64:function(Self) {
      TObject.Create(Self);
      Self.FOptions$6 = [15];
      Self.FInstance = TVariant.CreateObject();
      return Self
   }
   /// destructor TJSONObject.Destroy()
   ,Destroy$17:function(Self) {
      Self.FInstance = null;
      TObject.Destroy(Self);
   }
   /// function TJSONObject.Exists(const PropertyName: String) : Boolean
   ///  [line: 434, column: 22, file: System.JSON]
   ,Exists$1:function(Self, PropertyName$20) {
      return (Object.hasOwnProperty.call(Self.FInstance,PropertyName$20)?true:false);
   }
   /// procedure TJSONObject.FromJSON(const Text: String)
   ///  [line: 308, column: 23, file: System.JSON]
   ,FromJSON:function(Self, Text$17) {
      Self.FInstance = JSON.parse(Text$17);
   }
   /// procedure TJSONObject.LoadFromBuffer(const Buffer: TBinaryData)
   ///  [line: 320, column: 23, file: System.JSON]
   ,LoadFromBuffer:function(Self, Buffer$9) {
      var LBytes$6 = [],
         LText = "";
      LBytes$6 = TBinaryData.ToBytes(Buffer$9);
      LText = TString.DecodeUTF8(TString,LBytes$6);
      Self.FInstance = JSON.parse(LText);
   }
   /// procedure TJSONObject.LoadFromStream(const Stream: TStream)
   ///  [line: 334, column: 23, file: System.JSON]
   ,LoadFromStream$1:function(Self, Stream$6) {
      var LBytes$7 = [],
         LText$1 = "";
      LBytes$7 = TStream.Read(Stream$6,TStream.GetSize$(Stream$6));
      LText$1 = TString.DecodeUTF8(TString,LBytes$7);
      Self.FInstance = JSON.parse(LText$1);
   }
   /// function TJSONObject.Read(const PropertyName: String; var Data: Variant) : TJSONObject
   ///  [line: 391, column: 22, file: System.JSON]
   ,Read$3:function(Self, PropertyName$21, Data$45) {
      var Result = null;
      Result = Self;
      if (TJSONObject.Exists$1(Self,PropertyName$21)) {
         Data$45.v = Self.FInstance[PropertyName$21];
      } else {
         throw EW3Exception.CreateFmt($New(EJSONObject),"Failed to read value, property [%s] not found error",[PropertyName$21]);
      }
      return Result
   }
   /// procedure TJSONObject.SaveToBuffer(const Buffer: TBinaryData)
   ///  [line: 313, column: 23, file: System.JSON]
   ,SaveToBuffer:function(Self, Buffer$10) {
      var LText$2 = "",
         LBytes$8 = [];
      LText$2 = JSON.stringify(Self.FInstance)+"   ";
      LBytes$8 = TString.EncodeUTF8(TString,LText$2);
      TBinaryData.AppendBytes(Buffer$10,LBytes$8);
   }
   /// procedure TJSONObject.SaveToStream(const Stream: TStream)
   ///  [line: 327, column: 23, file: System.JSON]
   ,SaveToStream$1:function(Self, Stream$7) {
      var LText$3 = "",
         LBytes$9 = [];
      LText$3 = JSON.stringify(Self.FInstance)+"   ";
      LBytes$9 = TString.EncodeUTF8(TString,LText$3);
      TStream.Write$1(Stream$7,LBytes$9);
   }
   /// function TJSONObject.ToJSON() : String
   ///  [line: 303, column: 22, file: System.JSON]
   ,ToJSON:function(Self) {
      return JSON.stringify(Self.FInstance,null,2);
   }
   ,Destroy:TObject.Destroy
};
/// EJSONObject = class (EW3Exception)
///  [line: 35, column: 3, file: System.JSON]
var EJSONObject = {
   $ClassName:"EJSONObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function WriteLnF(Text$18, Data$46) {
   util().log(Format(String(Text$18),Data$46.slice(0)));
};
function WriteLn(Text$19) {
   util().log(String(Text$19));
};
/// TW3Win32DirectoryParser = class (TW3UnixDirectoryParser)
///  [line: 62, column: 3, file: SmartNJ.System]
var TW3Win32DirectoryParser = {
   $ClassName:"TW3Win32DirectoryParser",$Parent:TW3UnixDirectoryParser
   ,$Init:function ($) {
      TW3UnixDirectoryParser.$Init($);
   }
   /// function TW3Win32DirectoryParser.GetPathSeparator() : Char
   ///  [line: 609, column: 34, file: SmartNJ.System]
   ,GetPathSeparator:function(Self) {
      return "\\";
   }
   /// function TW3Win32DirectoryParser.GetRootMoniker() : String
   ///  [line: 614, column: 34, file: SmartNJ.System]
   ,GetRootMoniker:function(Self) {
      var Result = "";
      function GetDriveFrom(ThisPath) {
         var Result = "";
         var xpos$1 = 0;
         xpos$1 = (ThisPath.indexOf(":\\")+1);
         if (xpos$1>=2) {
            ++xpos$1;
            Result = ThisPath.substr(0,xpos$1);
         }
         return Result
      };
      Result = (GetDriveFrom(ParamStr$1(1))).toLocaleLowerCase();
      if (Result.length<2) {
         Result = GetDriveFrom(ParamStr$1(0));
         if (Result.length<2) {
            throw Exception.Create($New(Exception),"Failed to extract root moniker from script path error");
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt:TW3UnixDirectoryParser.ChangeFileExt
   ,ExcludeLeadingPathDelimiter:TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter
   ,ExcludeTrailingPathDelimiter:TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter
   ,GetDevice:TW3UnixDirectoryParser.GetDevice
   ,GetDirectoryName:TW3UnixDirectoryParser.GetDirectoryName
   ,GetExtension:TW3UnixDirectoryParser.GetExtension
   ,GetFileName:TW3UnixDirectoryParser.GetFileName
   ,GetFileNameWithoutExtension:TW3UnixDirectoryParser.GetFileNameWithoutExtension
   ,GetPathName:TW3UnixDirectoryParser.GetPathName
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars:TW3UnixDirectoryParser.HasValidFileNameChars
   ,HasValidPathChars:TW3UnixDirectoryParser.HasValidPathChars
   ,IncludeLeadingPathDelimiter:TW3UnixDirectoryParser.IncludeLeadingPathDelimiter
   ,IncludeTrailingPathDelimiter:TW3UnixDirectoryParser.IncludeTrailingPathDelimiter
   ,IsValidPath:TW3UnixDirectoryParser.IsValidPath
};
TW3Win32DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3Win32DirectoryParser.GetPathSeparator,TW3Win32DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TW3PosixDirectoryParser = class (TW3DirectoryParser)
///  [line: 68, column: 3, file: SmartNJ.System]
var TW3PosixDirectoryParser = {
   $ClassName:"TW3PosixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   /// function TW3PosixDirectoryParser.ChangeFileExt(const FilePath: String; NewExt: String) : String
   ///  [line: 479, column: 34, file: SmartNJ.System]
   ,ChangeFileExt:function(Self, FilePath$14, NewExt$1) {
      var Result = "";
      var LName$1 = "";
      var Separator$8 = "",
         x$17 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$14.length>0) {
         if ((FilePath$14.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$14]);
         } else {
            if (StrEndsWith(FilePath$14," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$14]);
            } else {
               Separator$8 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$1,Separator$8)) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Path (%s) has no filename error",[FilePath$14]);
               } else {
                  if ((FilePath$14.indexOf(Separator$8)+1)>0) {
                     LName$1 = TW3DirectoryParser.GetFileName$(Self,FilePath$14);
                     if (TW3ErrorObject.GetFailed$1(Self)) {
                        return Result;
                     }
                  } else {
                     LName$1 = FilePath$14;
                  }
                  if (LName$1.length>0) {
                     if ((LName$1.indexOf(".")+1)>0) {
                        for(x$17=LName$1.length;x$17>=1;x$17--) {
                           if (LName$1.charAt(x$17-1)==".") {
                              Result = LName$1.substr(0,(x$17-1))+NewExt$1;
                              break;
                           }
                        }
                     } else {
                        Result = LName$1+NewExt$1;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[FilePath$14]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.ExcludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 596, column: 34, file: SmartNJ.System]
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$15) {
      var Result = "";
      var Separator$9 = "";
      Separator$9 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$15,Separator$9)) {
         Result = FilePath$15.substr((1+Separator$9.length)-1);
      } else {
         Result = FilePath$15;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.ExcludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 577, column: 34, file: SmartNJ.System]
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$16) {
      var Result = "";
      var Separator$10 = "";
      Separator$10 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$16,Separator$10)) {
         Result = FilePath$16.substr(0,(FilePath$16.length-Separator$10.length));
      } else {
         Result = FilePath$16;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetDevice(const FilePath: String) : String
   ///  [line: 327, column: 34, file: SmartNJ.System]
   ,GetDevice:function(Self, FilePath$17) {
      var Result = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$17.length>0) {
         if (StrBeginsWith(FilePath$17,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetDirectoryName(const FilePath: String) : String
   ///  [line: 534, column: 34, file: SmartNJ.System]
   ,GetDirectoryName:function(Self, FilePath$18) {
      var Result = "";
      var Separator$11 = "",
         NameParts$1 = [];
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$18.length>0) {
         if ((FilePath$18.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$18]);
         } else {
            if (StrEndsWith(FilePath$18," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$18]);
            } else {
               Separator$11 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$18,Separator$11)) {
                  Result = FilePath$18;
                  return Result;
               }
               NameParts$1 = (FilePath$18).split(Separator$11);
               NameParts$1.splice((NameParts$1.length-1),1)
               ;
               Result = (NameParts$1).join(Separator$11)+Separator$11;
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[FilePath$18]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetExtension(const Filename: String) : String
   ///  [line: 419, column: 34, file: SmartNJ.System]
   ,GetExtension:function(Self, Filename$5) {
      var Result = "";
      var LName$2 = "";
      var Separator$12 = "",
         x$18 = 0;
      var dx$1 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Filename$5.length>0) {
         if ((Filename$5.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$5]);
         } else {
            if (StrEndsWith(Filename$5," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$5]);
            } else {
               Separator$12 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$2,Separator$12)) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Path (%s) has no filename error",[Filename$5]);
               } else {
                  if ((Filename$5.indexOf(Separator$12)+1)>0) {
                     LName$2 = TW3DirectoryParser.GetFileName$(Self,Filename$5);
                  } else {
                     LName$2 = Filename$5;
                  }
                  if (!TW3ErrorObject.GetFailed$1(Self)) {
                     for(x$18=Filename$5.length;x$18>=1;x$18--) {
                        {var $temp44 = Filename$5.charAt(x$18-1);
                           if ($temp44==".") {
                              dx$1 = Filename$5.length;
                              (dx$1-= x$18);
                              ++dx$1;
                              Result = RightStr(Filename$5,dx$1);
                              break;
                           }
                            else if ($temp44==Separator$12) {
                              break;
                           }
                        }
                     }
                     if (Result.length<1) {
                        TW3ErrorObject.SetLastErrorF$1(Self,"Failed to extract extension, filename (%s) contained no postfix",[TW3DirectoryParser.GetFileName$(Self,Filename$5)]);
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[Filename$5]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetFileName(const FilePath: String) : String
   ///  [line: 342, column: 34, file: SmartNJ.System]
   ,GetFileName:function(Self, FilePath$19) {
      var Result = "";
      var Separator$13 = "",
         x$19 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$19.length>0) {
         if ((FilePath$19.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$19]);
         } else {
            if (StrEndsWith(FilePath$19," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$19]);
            } else {
               Separator$13 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (!StrEndsWith(FilePath$19,Separator$13)) {
                  for(x$19=FilePath$19.length;x$19>=1;x$19--) {
                     if (FilePath$19.charAt(x$19-1)!=Separator$13) {
                        Result = FilePath$19.charAt(x$19-1)+Result;
                     } else {
                        break;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty path (%s) error",[FilePath$19]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetFileNameWithoutExtension(const Filename: String) : String
   ///  [line: 372, column: 34, file: SmartNJ.System]
   ,GetFileNameWithoutExtension:function(Self, Filename$6) {
      var Result = "";
      var Separator$14 = "",
         LName$3 = "";
      var x$20 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Filename$6.length>0) {
         if ((Filename$6.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$6]);
         } else {
            if (StrEndsWith(Filename$6," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$6]);
            } else {
               Separator$14 = TW3DirectoryParser.GetPathSeparator$(Self);
               if ((Filename$6.indexOf(Separator$14)+1)>0) {
                  LName$3 = TW3DirectoryParser.GetFileName$(Self,Filename$6);
               } else {
                  LName$3 = Filename$6;
               }
               if (!TW3ErrorObject.GetFailed$1(Self)) {
                  if (LName$3.length>0) {
                     if ((LName$3.indexOf(".")+1)>0) {
                        for(x$20=LName$3.length;x$20>=1;x$20--) {
                           if (LName$3.charAt(x$20-1)==".") {
                              Result = LName$3.substr(0,(x$20-1));
                              break;
                           }
                        }
                     } else {
                        Result = LName$3;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[Filename$6]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetPathName(const FilePath: String) : String
   ///  [line: 274, column: 34, file: SmartNJ.System]
   ,GetPathName:function(Self, FilePath$20) {
      var Result = "";
      var LParts = [],
         LTemp$14 = "";
      var Separator$15 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$20.length>0) {
         if ((FilePath$20.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$20]);
         } else {
            if (StrEndsWith(FilePath$20," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$20]);
            } else {
               Separator$15 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$20,Separator$15)) {
                  if (FilePath$20==TW3DirectoryParser.GetRootMoniker$(Self)) {
                     TW3ErrorObject.SetLastError$1(Self,"Failed to get directory name, path is root");
                     return Result;
                  }
                  LTemp$14 = (FilePath$20).substr(0,(FilePath$20.length-Separator$15.length));
                  LParts = (LTemp$14).split(Separator$15);
                  Result = LParts[(LParts.length-1)];
                  return Result;
               }
               LParts = (FilePath$20).split(Separator$15);
               if (LParts.length>1) {
                  Result = LParts[(LParts.length-1)-1];
               } else {
                  Result = LParts[(LParts.length-1)];
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty path (%s) error",[FilePath$20]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetPathSeparator() : Char
   ///  [line: 155, column: 34, file: SmartNJ.System]
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   /// function TW3PosixDirectoryParser.GetRootMoniker() : String
   ///  [line: 160, column: 35, file: SmartNJ.System]
   ,GetRootMoniker:function(Self) {
      return "\/";
   }
   /// function TW3PosixDirectoryParser.HasValidFileNameChars(FileName: String) : Boolean
   ///  [line: 165, column: 34, file: SmartNJ.System]
   ,HasValidFileNameChars:function(Self, FileName$1) {
      var Result = false;
      var el$4 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FileName$1.length>0) {
         if ((FileName$1.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FileName$1]);
         } else {
            for (var $temp45=0;$temp45<FileName$1.length;$temp45++) {
               el$4=$uniCharAt(FileName$1,$temp45);
               if (!el$4) continue;
               Result = (((el$4>="A")&&(el$4<="Z"))||((el$4>="a")&&(el$4<="z"))||((el$4>="0")&&(el$4<="9"))||(el$4=="-")||(el$4=="_")||(el$4==".")||(el$4==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el$4, FileName$1]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.HasValidPathChars(FolderName: String) : Boolean
   ///  [line: 194, column: 34, file: SmartNJ.System]
   ,HasValidPathChars:function(Self, FolderName$1) {
      var Result = false;
      var el$5 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FolderName$1.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in directory-name (\"%s\") error",[FolderName$1]);
      } else {
         if (FolderName$1.length>0) {
            for (var $temp46=0;$temp46<FolderName$1.length;$temp46++) {
               el$5=$uniCharAt(FolderName$1,$temp46);
               if (!el$5) continue;
               Result = (((el$5>="A")&&(el$5<="Z"))||((el$5>="a")&&(el$5<="z"))||((el$5>="0")&&(el$5<="9"))||(el$5=="-")||(el$5=="_")||(el$5==".")||(el$5==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$5, FolderName$1]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IncludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 587, column: 34, file: SmartNJ.System]
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$21) {
      var Result = "";
      var Separator$16 = "";
      Separator$16 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$21,Separator$16)) {
         Result = FilePath$21;
      } else {
         Result = Separator$16+FilePath$21;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IncludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 567, column: 34, file: SmartNJ.System]
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$22) {
      var Result = "";
      var Separator$17 = "";
      Separator$17 = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$22;
      if (!StrEndsWith(Result,Separator$17)) {
         Result+=Separator$17;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IsValidPath(FilePath: String) : Boolean
   ///  [line: 223, column: 34, file: SmartNJ.System]
   ,IsValidPath:function(Self, FilePath$23) {
      var Result = false;
      var Separator$18 = "",
         PathParts$1 = [],
         Index$5 = 0,
         a$477 = 0;
      var part$1 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FilePath$23.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$23]);
      } else {
         if (FilePath$23.length>0) {
            Separator$18 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts$1 = (FilePath$23).split(Separator$18);
            Index$5 = 0;
            var $temp47;
            for(a$477=0,$temp47=PathParts$1.length;a$477<$temp47;a$477++) {
               part$1 = PathParts$1[a$477];
               {var $temp48 = part$1;
                  if ($temp48=="") {
                     TW3ErrorObject.SetLastErrorF$1(Self,"Path has multiple separators (%s) error",[FilePath$23]);
                     return false;
                  }
                   else if ($temp48=="~") {
                     if (Index$5>0) {
                        TW3ErrorObject.SetLastErrorF$1(Self,"Path has misplaced root moniker (%s) error",[FilePath$23]);
                        return Result;
                     }
                  }
                   else {
                     if (Index$5==(PathParts$1.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part$1)) {
                           return Result;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part$1)) {
                        return Result;
                     }
                  }
               }
               Index$5+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3PosixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3PosixDirectoryParser.GetPathSeparator,TW3PosixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3PosixDirectoryParser.IsValidPath,TW3PosixDirectoryParser.HasValidPathChars,TW3PosixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3PosixDirectoryParser.GetFileNameWithoutExtension,TW3PosixDirectoryParser.GetPathName,TW3PosixDirectoryParser.GetDevice,TW3PosixDirectoryParser.GetFileName,TW3PosixDirectoryParser.GetExtension,TW3PosixDirectoryParser.GetDirectoryName,TW3PosixDirectoryParser.IncludeTrailingPathDelimiter,TW3PosixDirectoryParser.IncludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeTrailingPathDelimiter,TW3PosixDirectoryParser.ChangeFileExt]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TW3NodeSyncFileAPI = class (TObject)
///  [line: 44, column: 3, file: SmartNJ.System]
var TW3NodeSyncFileAPI = {
   $ClassName:"TW3NodeSyncFileAPI",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TW3NodeSyncFileAPI.DeleteFile(const FullPath: String) : Boolean
   ///  [line: 993, column: 35, file: SmartNJ.System]
   ,DeleteFile:function(FullPath) {
      var Result = false;
      NodeFsAPI().unlinkSync(FullPath);
      Result = true;
      return Result
   }
   /// function TW3NodeSyncFileAPI.ReadTextFile(const FullPath: String) : String
   ///  [line: 956, column: 35, file: SmartNJ.System]
   ,ReadTextFile:function(FullPath$1) {
      var Result = "";
      var Options$7 = {encoding:"",flag:""};
      Options$7.encoding = "utf8";
      Options$7.flag = "r";
      Result = String(NodeFsAPI().readFileSync(FullPath$1,Clone$TReadFileSyncOptions(Options$7)));
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TW3EnvVariables = class (TObject)
///  [line: 92, column: 3, file: SmartNJ.System]
var TW3EnvVariables = {
   $ClassName:"TW3EnvVariables",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TRunPlatform enumeration
///  [line: 35, column: 3, file: SmartNJ.System]
var TRunPlatform = [ "rpUnknown", "rpWindows", "rpLinux", "rpMac", "rpEspruino" ];
function PathSeparator() {
   return NodePathAPI().sep;
};
function ParamStr$1(index$3) {
   var Result = "";
   Result = process.argv[index$3];
   return Result
};
function IncludeTrailingPathDelimiter$4(PathName) {
   var Result = "";
   Result = Trim$_String_(PathName);
   if (!StrEndsWith(Result,PathSeparator())) {
      Result+=PathSeparator();
   }
   return Result
};
function GetPlatform() {
   var Result = 0;
   var token = "";
   token = process.platform;
   token = (Trim$_String_(token)).toLocaleLowerCase();
   {var $temp49 = token;
      if ($temp49=="darwin") {
         Result = 3;
      }
       else if ($temp49=="win32") {
         Result = 1;
      }
       else if ($temp49=="linux") {
         Result = 2;
      }
       else if ($temp49=="espruino") {
         Result = 4;
      }
       else {
         Result = 0;
      }
   }
   return Result
};
function FileExists$2(Filename$7) {
   var Result = false;
   var stats = null;
   if (NodeFsAPI().existsSync(Filename$7)) {
      try {
         stats = NodeFsAPI().lstatSync(Filename$7);
         Result = stats.isFile();
      } catch ($e) {
         var e$17 = $W($e);
         throw EW3Exception.CreateFmt($New(EW3FileOperationError),"%s failed, system threw exception %s with message: %s",["FileExists", TObject.ClassName(e$17.ClassType), e$17.FMessage]);
      }
   }
   return Result
};
/// EW3FileOperationError = class (EW3Exception)
///  [line: 32, column: 3, file: SmartNJ.System]
var EW3FileOperationError = {
   $ClassName:"EW3FileOperationError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TRagnarokServerBase = class (TObject)
///  [line: 20, column: 3, file: ragnarok.types]
var TRagnarokServerBase = {
   $ClassName:"TRagnarokServerBase",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TRagnarokServerBase.InitializeMessages()
   ///  [line: 32, column: 31, file: ragnarok.types]
   ,InitializeMessages:function(Self) {
      /* null */
   }
   ,Destroy:TObject.Destroy
   ,InitializeMessages$:function($){return $.ClassType.InitializeMessages($)}
};
/// TW3CustomDictionary = class (TObject)
///  [line: 23, column: 3, file: System.Dictionaries]
var TW3CustomDictionary = {
   $ClassName:"TW3CustomDictionary",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLUT = undefined;
   }
   /// procedure TW3CustomDictionary.Clear()
   ///  [line: 105, column: 31, file: System.Dictionaries]
   ,Clear$12:function(Self) {
      Self.FLUT = TVariant.CreateObject();
   }
   /// function TW3CustomDictionary.Contains(const ItemKey: String) : Boolean
   ///  [line: 165, column: 30, file: System.Dictionaries]
   ,Contains:function(Self, ItemKey) {
      var Result = false;
      if (ItemKey.length>0) {
         Result = (Self.FLUT.hasOwnProperty(ItemKey)?true:false);
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.Contains", TObject.ClassName(Self.ClassType), $R[29]]);
      }
      return Result
   }
   /// constructor TW3CustomDictionary.Create()
   ///  [line: 92, column: 33, file: System.Dictionaries]
   ,Create$87:function(Self) {
      TObject.Create(Self);
      Self.FLUT = TVariant.CreateObject();
      return Self
   }
   /// procedure TW3CustomDictionary.Delete(const ItemKey: String)
   ///  [line: 230, column: 31, file: System.Dictionaries]
   ,Delete$6:function(Self, ItemKey$1) {
      if (ItemKey$1.length>0) {
         try {
            delete (Self.FLUT[ItemKey$1]);
         } catch ($e) {
            var e$18 = $W($e);
            throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.Delete", TObject.ClassName(Self.ClassType), e$18.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.Delete", TObject.ClassName(Self.ClassType), $R[29]]);
      }
   }
   /// destructor TW3CustomDictionary.Destroy()
   ///  [line: 98, column: 32, file: System.Dictionaries]
   ,Destroy:function(Self) {
      TW3CustomDictionary.Clear$12(Self);
      Self.FLUT = undefined;
      TObject.Destroy(Self);
   }
   /// function TW3CustomDictionary.GetItem(const ItemKey: String) : Variant
   ///  [line: 198, column: 30, file: System.Dictionaries]
   ,GetItem$2:function(Self, ItemKey$2) {
      var Result = undefined;
      if (ItemKey$2.length>0) {
         try {
            Result = Self.FLUT[ItemKey$2];
         } catch ($e) {
            var e$19 = $W($e);
            throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.GetItem", TObject.ClassName(Self.ClassType), e$19.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.GetItem", TObject.ClassName(Self.ClassType), $R[29]]);
      }
      return Result
   }
   /// procedure TW3CustomDictionary.SetItem(const ItemKey: String; const KeyValue: Variant)
   ///  [line: 214, column: 31, file: System.Dictionaries]
   ,SetItem:function(Self, ItemKey$3, KeyValue) {
      if (ItemKey$3.length>0) {
         try {
            Self.FLUT[ItemKey$3] = KeyValue;
         } catch ($e) {
            var e$20 = $W($e);
            throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.SetItem", TObject.ClassName(Self.ClassType), e$20.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.SetItem", TObject.ClassName(Self.ClassType), $R[29]]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TW3VarDictionary = class (TW3CustomDictionary)
///  [line: 67, column: 3, file: System.Dictionaries]
var TW3VarDictionary = {
   $ClassName:"TW3VarDictionary",$Parent:TW3CustomDictionary
   ,$Init:function ($) {
      TW3CustomDictionary.$Init($);
   }
   ,Destroy:TW3CustomDictionary.Destroy
};
/// TW3ObjDictionary = class (TW3CustomDictionary)
///  [line: 46, column: 3, file: System.Dictionaries]
var TW3ObjDictionary = {
   $ClassName:"TW3ObjDictionary",$Parent:TW3CustomDictionary
   ,$Init:function ($) {
      TW3CustomDictionary.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 49, column: 13, file: System.Dictionaries]
   ,a$239:function(Self, ItemKey$4) {
      return TVariant.AsObject(TW3CustomDictionary.GetItem$2(Self,ItemKey$4));
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 50, column: 13, file: System.Dictionaries]
   ,a$238:function(Self, ItemKey$5, Value$64) {
      TW3CustomDictionary.SetItem(Self,ItemKey$5,Value$64);
   }
   ,Destroy:TW3CustomDictionary.Destroy
};
/// TQTXRoute = class (TObject)
///  [line: 28, column: 3, file: ragnarok.messages.base]
var TQTXRoute = {
   $ClassName:"TQTXRoute",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.TagValue = undefined;
      $.Destination = $.Channel = $.Source = "";
      $.FOwner$1 = null;
   }
   /// constructor TQTXRoute.Create(const Message: TQTXBaseMessage)
   ///  [line: 86, column: 23, file: ragnarok.messages.base]
   ,Create$98:function(Self, Message$2) {
      TObject.Create(Self);
      Self.FOwner$1 = Message$2;
      return Self
   }
   /// procedure TQTXRoute.ReadObjData(const Root: TQTXJSONObject)
   ///  [line: 100, column: 21, file: ragnarok.messages.base]
   ,ReadObjData$1:function(Self, Root) {
      Self.Channel = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$4(Root,"channel"));
      Self.Source = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$4(Root,"source"));
      Self.Destination = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$4(Root,"destination"));
      if (TQTXJSONObject.Locate(Root,"tagvalue",false)!==null) {
         Self.TagValue = TQTXJSONObject.ReadVariant$1(Root,"tagvalue");
      }
   }
   /// procedure TQTXRoute.WriteObjData(const Root: TQTXJSONObject)
   ///  [line: 110, column: 21, file: ragnarok.messages.base]
   ,WriteObjData$1:function(Self, Root$1) {
      TQTXJSONObject.WriteOrAdd(Root$1,"channel",TString.EncodeBase64(TString,Self.Channel));
      TQTXJSONObject.WriteOrAdd(Root$1,"source",TString.EncodeBase64(TString,Self.Source));
      TQTXJSONObject.WriteOrAdd(Root$1,"destination",TString.EncodeBase64(TString,Self.Destination));
      TQTXJSONObject.WriteVariant$1(Root$1,"tagvalue",Self.TagValue);
   }
   ,Destroy:TObject.Destroy
};
/// TQTXBaseMessage = class (TObject)
///  [line: 44, column: 3, file: ragnarok.messages.base]
var TQTXBaseMessage = {
   $ClassName:"TQTXBaseMessage",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.Ticket = "";
      $.SendTime = undefined;
      $.FBinary = $.FEnvelope = $.FRouting = null;
   }
   /// constructor TQTXBaseMessage.Create(const WithTicket: String)
   ///  [line: 130, column: 29, file: ragnarok.messages.base]
   ,Create$71:function(Self, WithTicket) {
      TQTXBaseMessage.Create$70$(Self);
      Self.Ticket = WithTicket;
      return Self
   }
   /// constructor TQTXBaseMessage.Create()
   ///  [line: 122, column: 29, file: ragnarok.messages.base]
   ,Create$70:function(Self) {
      TObject.Create(Self);
      Self.FBinary = TDataTypeConverter.Create$15$($New(TBinaryData));
      Self.FEnvelope = TQTXJSONObject.Create$99($New(TQTXJSONObject));
      Self.FRouting = TQTXRoute.Create$98($New(TQTXRoute),Self);
      return Self
   }
   /// destructor TQTXBaseMessage.Destroy()
   ///  [line: 136, column: 28, file: ragnarok.messages.base]
   ,Destroy:function(Self) {
      TObject.Free(Self.FBinary);
      TObject.Free(Self.FEnvelope);
      TObject.Free(Self.FRouting);
      TObject.Destroy(Self);
   }
   /// procedure TQTXBaseMessage.Parse(const ObjectString: String)
   ///  [line: 179, column: 27, file: ragnarok.messages.base]
   ,Parse:function(Self, ObjectString) {
      TQTXJSONObject.FromJSON$1(Self.FEnvelope,ObjectString);
      TQTXBaseMessage.ReadObjData$(Self,Self.FEnvelope);
   }
   /// function TQTXBaseMessage.QueryIdentifier(ObjectString: String) : String
   ///  [line: 156, column: 32, file: ragnarok.messages.base]
   ,QueryIdentifier:function(Self, ObjectString$1) {
      var Result = {v:""};
      try {
         var LEnvelope = null;
         LEnvelope = TQTXJSONObject.Create$99($New(TQTXJSONObject));
         try {
            try {
               TQTXJSONObject.FromJSON$1(LEnvelope,ObjectString$1);
               if (TQTXJSONObject.Exists$4(LEnvelope,"identifier")) {
                  Result.v = TQTXJSONObject.ReadString$4(LEnvelope,"identifier");
               }
            } catch ($e) {
               return Result.v;
            }
         } finally {
            TObject.Free(LEnvelope);
         }
      } finally {return Result.v}
   }
   /// procedure TQTXBaseMessage.ReadObjData(const Root: TQTXJSONObject)
   ///  [line: 185, column: 27, file: ragnarok.messages.base]
   ,ReadObjData:function(Self, Root$2) {
      var LRoute = null,
         LSource = null;
      if (TQTXJSONObject.ReadString$4(Root$2,"identifier")==TObject.ClassName(Self.ClassType)) {
         Self.Ticket = TQTXJSONObject.ReadString$4(Root$2,"ticket");
         LRoute = TQTXJSONObject.Locate(Root$2,"routing",false);
         if (LRoute!==null) {
            TQTXRoute.ReadObjData$1(Self.FRouting,LRoute);
         }
         LSource = TQTXJSONObject.Locate(Root$2,TObject.ClassName(Self.ClassType),false);
         if (LSource!==null) {
            Self.SendTime = TQTXJSONObject.ReadFloat$2(LSource,"sendtime");
            TBinaryData.FromBase64(Self.FBinary,String(TQTXJSONObject.Read$8(LSource,"attachment")));
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),"Expected identifier of [%s] error",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// function TQTXBaseMessage.Serialize() : String
   ///  [line: 173, column: 26, file: ragnarok.messages.base]
   ,Serialize:function(Self) {
      var Result = "";
      TQTXBaseMessage.WriteObjData$(Self,Self.FEnvelope);
      Result = TQTXJSONObject.ToJSON$4(Self.FEnvelope,4);
      return Result
   }
   /// procedure TQTXBaseMessage.WriteObjData(const Root: TQTXJSONObject)
   ///  [line: 205, column: 27, file: ragnarok.messages.base]
   ,WriteObjData:function(Self, Root$3) {
      var LRoute$1 = null,
         LTarget = null;
      TQTXJSONObject.WriteOrAdd(Root$3,"identifier",TObject.ClassName(Self.ClassType));
      TQTXJSONObject.WriteOrAdd(Root$3,"ticket",Self.Ticket);
      LRoute$1 = TQTXJSONObject.Branch(Root$3,"routing");
      TQTXRoute.WriteObjData$1(Self.FRouting,LRoute$1);
      Self.SendTime = Now();
      LTarget = TQTXJSONObject.Branch(Root$3,TObject.ClassName(Self.ClassType));
      TQTXJSONObject.WriteOrAdd(LTarget,"sendtime",Self.SendTime);
      TQTXJSONObject.WriteOrAdd(LTarget,"attachment",TBinaryData.ToBase64(Self.FBinary));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$70$:function($){return $.ClassType.Create$70($)}
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// EQTXMessage = class (EW3Exception)
///  [line: 23, column: 3, file: ragnarok.messages.base]
var EQTXMessage = {
   $ClassName:"EQTXMessage",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TQTXServerMessage = class (TQTXBaseMessage)
///  [line: 34, column: 3, file: ragnarok.messages.network]
var TQTXServerMessage = {
   $ClassName:"TQTXServerMessage",$Parent:TQTXBaseMessage
   ,$Init:function ($) {
      TQTXBaseMessage.$Init($);
      $.Code = 0;
      $.Response = "";
   }
   /// procedure TQTXServerMessage.ReadObjData(const Root: TQTXJSONObject)
   ///  [line: 382, column: 29, file: ragnarok.messages.network]
   ,ReadObjData:function(Self, Root$4) {
      var LParent = null,
         LSource$1 = null;
      TQTXBaseMessage.ReadObjData(Self,Root$4);
      LParent = TQTXJSONObject.Locate(Root$4,TObject.ClassName(Self.ClassType),false);
      if (LParent!==null) {
         LSource$1 = TQTXJSONObject.Locate(LParent,"response",false);
         if (LSource$1!==null) {
            Self.Code = TQTXJSONObject.ReadInteger$2(LSource$1,"code");
            Self.Response = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$4(LSource$1,"text"));
         } else {
            throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["response"]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXServerMessage.WriteObjData(const Root: TQTXJSONObject)
   ///  [line: 364, column: 29, file: ragnarok.messages.network]
   ,WriteObjData:function(Self, Root$5) {
      var LParent$1 = null,
         LTarget$1 = null;
      TQTXBaseMessage.WriteObjData(Self,Root$5);
      LParent$1 = TQTXJSONObject.Locate(Root$5,TObject.ClassName(Self.ClassType),false);
      if (LParent$1!==null) {
         LTarget$1 = TQTXJSONObject.Branch(LParent$1,"response");
         if (LTarget$1!==null) {
            TQTXJSONObject.WriteOrAdd(LTarget$1,"code",Self.Code);
            TQTXJSONObject.WriteOrAdd(LTarget$1,"text",TString.EncodeBase64(TString,Self.Response));
         } else {
            throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",["response"]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Create$70:TQTXBaseMessage.Create$70
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXServerError = class (TQTXServerMessage)
///  [line: 43, column: 3, file: ragnarok.messages.network]
var TQTXServerError = {
   $ClassName:"TQTXServerError",$Parent:TQTXServerMessage
   ,$Init:function ($) {
      TQTXServerMessage.$Init($);
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Create$70:TQTXBaseMessage.Create$70
   ,ReadObjData:TQTXServerMessage.ReadObjData
   ,WriteObjData:TQTXServerMessage.WriteObjData
};
/// TQTXClientMessage = class (TQTXBaseMessage)
///  [line: 31, column: 3, file: ragnarok.messages.network]
var TQTXClientMessage = {
   $ClassName:"TQTXClientMessage",$Parent:TQTXBaseMessage
   ,$Init:function ($) {
      TQTXBaseMessage.$Init($);
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Create$70:TQTXBaseMessage.Create$70
   ,ReadObjData:TQTXBaseMessage.ReadObjData
   ,WriteObjData:TQTXBaseMessage.WriteObjData
};
/// TQTXLoginResponse = class (TQTXServerMessage)
///  [line: 69, column: 3, file: ragnarok.messages.network]
var TQTXLoginResponse = {
   $ClassName:"TQTXLoginResponse",$Parent:TQTXServerMessage
   ,$Init:function ($) {
      TQTXServerMessage.$Init($);
      $.Username = $.SessionID = "";
   }
   /// procedure TQTXLoginResponse.ReadObjData(const Root: TQTXJSONObject)
   ///  [line: 305, column: 29, file: ragnarok.messages.network]
   ,ReadObjData:function(Self, Root$6) {
      var LParent$2 = null,
         LSource$2 = null;
      TQTXServerMessage.ReadObjData(Self,Root$6);
      LParent$2 = TQTXJSONObject.Locate(Root$6,TObject.ClassName(Self.ClassType),false);
      if (LParent$2!==null) {
         LSource$2 = TQTXJSONObject.Locate(LParent$2,"session",false);
         if (LSource$2!==null) {
            Self.Username = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$4(LSource$2,"username"));
            Self.SessionID = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$4(LSource$2,"sessionid"));
         } else {
            throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["session"]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXLoginResponse.WriteObjData(const Root: TQTXJSONObject)
   ///  [line: 288, column: 29, file: ragnarok.messages.network]
   ,WriteObjData:function(Self, Root$7) {
      var LParent$3 = null,
         LTarget$2 = null;
      TQTXServerMessage.WriteObjData(Self,Root$7);
      LParent$3 = TQTXJSONObject.Locate(Root$7,TObject.ClassName(Self.ClassType),false);
      if (LParent$3!==null) {
         LTarget$2 = TQTXJSONObject.Branch(LParent$3,"session");
         if (LTarget$2!==null) {
            TQTXJSONObject.WriteOrAdd(LTarget$2,"username",TString.EncodeBase64(TString,Self.Username));
            TQTXJSONObject.WriteOrAdd(LTarget$2,"sessionid",TString.EncodeBase64(TString,Self.SessionID));
         } else {
            throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",["session"]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Create$70:TQTXBaseMessage.Create$70
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXLoginRequest = class (TQTXClientMessage)
///  [line: 60, column: 3, file: ragnarok.messages.network]
var TQTXLoginRequest = {
   $ClassName:"TQTXLoginRequest",$Parent:TQTXClientMessage
   ,$Init:function ($) {
      TQTXClientMessage.$Init($);
      $.Username = $.Password = "";
   }
   /// procedure TQTXLoginRequest.ReadObjData(const Root: TQTXJSONObject)
   ///  [line: 343, column: 28, file: ragnarok.messages.network]
   ,ReadObjData:function(Self, Root$8) {
      var LParent$4 = null,
         LSource$3 = null;
      TQTXBaseMessage.ReadObjData(Self,Root$8);
      LParent$4 = TQTXJSONObject.Locate(Root$8,TObject.ClassName(Self.ClassType),false);
      if (LParent$4!==null) {
         LSource$3 = TQTXJSONObject.Locate(LParent$4,"credentials",false);
         if (LSource$3!==null) {
            Self.Username = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$4(LSource$3,"username"));
            Self.Password = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$4(LSource$3,"password"));
         } else {
            throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["credentials"]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXLoginRequest.WriteObjData(const Root: TQTXJSONObject)
   ///  [line: 326, column: 28, file: ragnarok.messages.network]
   ,WriteObjData:function(Self, Root$9) {
      var LParent$5 = null,
         LTarget$3 = null;
      TQTXBaseMessage.WriteObjData(Self,Root$9);
      LParent$5 = TQTXJSONObject.Locate(Root$9,TObject.ClassName(Self.ClassType),false);
      if (LParent$5!==null) {
         LTarget$3 = TQTXJSONObject.Branch(LParent$5,"credentials");
         if (LTarget$3!==null) {
            TQTXJSONObject.WriteOrAdd(LTarget$3,"username",TString.EncodeBase64(TString,Self.Username));
            TQTXJSONObject.WriteOrAdd(LTarget$3,"password",TString.EncodeBase64(TString,Self.Password));
         } else {
            throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",["credentials"]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Create$70:TQTXBaseMessage.Create$70
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXFileIOResponse = class (TQTXServerMessage)
///  [line: 87, column: 3, file: ragnarok.messages.network]
var TQTXFileIOResponse = {
   $ClassName:"TQTXFileIOResponse",$Parent:TQTXServerMessage
   ,$Init:function ($) {
      TQTXServerMessage.$Init($);
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Create$70:TQTXBaseMessage.Create$70
   ,ReadObjData:TQTXServerMessage.ReadObjData
   ,WriteObjData:TQTXServerMessage.WriteObjData
};
/// TQTXFileIORequest = class (TQTXClientMessage)
///  [line: 78, column: 3, file: ragnarok.messages.network]
var TQTXFileIORequest = {
   $ClassName:"TQTXFileIORequest",$Parent:TQTXClientMessage
   ,$Init:function ($) {
      TQTXClientMessage.$Init($);
      $.Command = $.SessionId = "";
   }
   /// procedure TQTXFileIORequest.ReadObjData(const Root: TQTXJSONObject)
   ///  [line: 267, column: 29, file: ragnarok.messages.network]
   ,ReadObjData:function(Self, Root$10) {
      var LParent$6 = null,
         LSource$4 = null;
      TQTXBaseMessage.ReadObjData(Self,Root$10);
      LParent$6 = TQTXJSONObject.Locate(Root$10,TObject.ClassName(Self.ClassType),false);
      if (LParent$6!==null) {
         LSource$4 = TQTXJSONObject.Locate(LParent$6,"filesystem",false);
         if (LSource$4!==null) {
            Self.Command = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$4(LSource$4,"command"));
            Self.SessionId = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$4(LSource$4,"sessionid"));
         } else {
            throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["filesystem"]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXFileIORequest.WriteObjData(const Root: TQTXJSONObject)
   ///  [line: 250, column: 29, file: ragnarok.messages.network]
   ,WriteObjData:function(Self, Root$11) {
      var LParent$7 = null,
         LTarget$4 = null;
      TQTXBaseMessage.WriteObjData(Self,Root$11);
      LParent$7 = TQTXJSONObject.Locate(Root$11,TObject.ClassName(Self.ClassType),false);
      if (LParent$7!==null) {
         LTarget$4 = TQTXJSONObject.Branch(LParent$7,"filesystem");
         if (LTarget$4!==null) {
            TQTXJSONObject.WriteOrAdd(LTarget$4,"command",TString.EncodeBase64(TString,Self.Command));
            TQTXJSONObject.WriteOrAdd(LTarget$4,"sessionid",TString.EncodeBase64(TString,Self.SessionId));
         } else {
            throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",["filesystem"]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Create$70:TQTXBaseMessage.Create$70
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXFileIODirResponse = class (TQTXFileIOResponse)
///  [line: 90, column: 3, file: ragnarok.messages.network]
var TQTXFileIODirResponse = {
   $ClassName:"TQTXFileIODirResponse",$Parent:TQTXFileIOResponse
   ,$Init:function ($) {
      TQTXFileIOResponse.$Init($);
      $.FDirList = null;
   }
   /// procedure TQTXFileIODirResponse.ReadObjData(const Root: TQTXJSONObject)
   ///  [line: 232, column: 33, file: ragnarok.messages.network]
   ,ReadObjData:function(Self, Root$12) {
      var LParent$8 = null;
      TQTXServerMessage.ReadObjData(Self,Root$12);
      LParent$8 = TQTXJSONObject.Locate(Root$12,TObject.ClassName(Self.ClassType),false);
      if (LParent$8!==null) {
         if (TQTXJSONObject.Exists$4(LParent$8,"filesystem")) {
            Self.FDirList = TVariant.AsObject(TQTXJSONObject.Read$8(LParent$8,"filesystem"));
         } else {
            throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["filesystem"]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXFileIODirResponse.WriteObjData(const Root: TQTXJSONObject)
   ///  [line: 222, column: 33, file: ragnarok.messages.network]
   ,WriteObjData:function(Self, Root$13) {
      var LParent$9 = null;
      TQTXServerMessage.WriteObjData(Self,Root$13);
      LParent$9 = TQTXJSONObject.Locate(Root$13,TObject.ClassName(Self.ClassType),false);
      if (LParent$9!==null) {
         TQTXJSONObject.WriteOrAdd(LParent$9,"filesystem",Self.FDirList);
      } else {
         throw EW3Exception.CreateFmt($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXFileIODirResponse.LoadDirListFromString(const Data: String)
   ///  [line: 214, column: 33, file: ragnarok.messages.network]
   ,LoadDirListFromString:function(Self, Data$47) {
      var LTemp$15;
      LTemp$15 = JSON.parse(Data$47);
      (Self.FDirList) = LTemp$15;
   }
   /// constructor TQTXFileIODirResponse.Create()
   ///  [line: 208, column: 35, file: ragnarok.messages.network]
   ,Create$70:function(Self) {
      TQTXBaseMessage.Create$70(Self);
      Self.FDirList = new TNJFileItemList();
      return Self
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Create$70$:function($){return $.ClassType.Create$70($)}
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TRagnarokMessageInfo = class (TObject)
///  [line: 34, column: 3, file: ragnarok.messages.dispatcher]
var TRagnarokMessageInfo = {
   $ClassName:"TRagnarokMessageInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.MessageClass$1 = null;
      $.MessageHandler$1 = null;
   }
   ,Destroy:TObject.Destroy
};
/// TRagnarokMessageDispatcher = class (TObject)
///  [line: 39, column: 3, file: ragnarok.messages.dispatcher]
var TRagnarokMessageDispatcher = {
   $ClassName:"TRagnarokMessageDispatcher",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FItems$3 = [];
      $.FNameLUT$1 = null;
   }
   /// procedure TRagnarokMessageDispatcher.Clear()
   ///  [line: 91, column: 38, file: ragnarok.messages.dispatcher]
   ,Clear$20:function(Self) {
      var a$478 = 0;
      var info$2 = null;
      try {
         var a$479 = [];
         a$479 = Self.FItems$3;
         var $temp50;
         for(a$478=0,$temp50=a$479.length;a$478<$temp50;a$478++) {
            info$2 = a$479[a$478];
            TObject.Free(info$2);
         }
      } finally {
         Self.FItems$3.length=0;
         TW3CustomDictionary.Clear$12(Self.FNameLUT$1);
      }
   }
   /// constructor TRagnarokMessageDispatcher.Create()
   ///  [line: 75, column: 40, file: ragnarok.messages.dispatcher]
   ,Create$107:function(Self) {
      TObject.Create(Self);
      Self.FNameLUT$1 = TW3CustomDictionary.Create$87($New(TW3ObjDictionary));
      return Self
   }
   /// destructor TRagnarokMessageDispatcher.Destroy()
   ///  [line: 82, column: 39, file: ragnarok.messages.dispatcher]
   ,Destroy:function(Self) {
      if (Self.FItems$3.length>0) {
         TRagnarokMessageDispatcher.Clear$20(Self);
      }
      TObject.Free(Self.FNameLUT$1);
      TObject.Destroy(Self);
   }
   /// function TRagnarokMessageDispatcher.GetMessageInfoForName(MessageClassName: String) : TRagnarokMessageInfo
   ///  [line: 121, column: 37, file: ragnarok.messages.dispatcher]
   ,GetMessageInfoForName$1:function(Self, MessageClassName$1) {
      var Result = null;
      MessageClassName$1 = (Trim$_String_(MessageClassName$1)).toLocaleLowerCase();
      if (MessageClassName$1.length>0) {
         if (TW3CustomDictionary.Contains(Self.FNameLUT$1,MessageClassName$1)) {
            Result = $As(TW3ObjDictionary.a$239(Self.FNameLUT$1,MessageClassName$1),TRagnarokMessageInfo);
         }
      }
      return Result
   }
   /// procedure TRagnarokMessageDispatcher.RegisterMessage(const MessageClass: TQTXMessageBaseClass; const Handler: TRRMessageHandler)
   ///  [line: 132, column: 38, file: ragnarok.messages.dispatcher]
   ,RegisterMessage$1:function(Self, MessageClass$3, Handler) {
      var LName$4 = "",
         Info$3 = null;
      if (MessageClass$3) {
         if (Handler) {
            LName$4 = (Trim$_String_(TObject.ClassName(MessageClass$3))).toLocaleLowerCase();
            if (TW3CustomDictionary.Contains(Self.FNameLUT$1,LName$4)) {
               throw Exception.Create($New(ERagnarokMessageDispatcher),$R[28]);
            } else {
               Info$3 = TObject.Create($New(TRagnarokMessageInfo));
               Info$3.MessageClass$1 = MessageClass$3;
               Info$3.MessageHandler$1 = Handler;
               Self.FItems$3.push(Info$3);
               TW3ObjDictionary.a$238(Self.FNameLUT$1,LName$4,Info$3);
            }
         } else {
            throw Exception.Create($New(ERagnarokMessageDispatcher),$R[27]);
         }
      } else {
         throw Exception.Create($New(ERagnarokMessageDispatcher),$R[26]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// ERagnarokMessageDispatcher = class (EW3Exception)
///  [line: 26, column: 3, file: ragnarok.messages.dispatcher]
var ERagnarokMessageDispatcher = {
   $ClassName:"ERagnarokMessageDispatcher",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TWbAccountState enumeration
///  [line: 45, column: 3, file: ragnarok.sessions]
var TWbAccountState = [ "asActive", "asLocked", "asUnConfirmed" ];
/// TWbAccountPrivilege enumeration
///  [line: 35, column: 3, file: ragnarok.sessions]
var TWbAccountPrivilege = [ "apExecute", "apRead", "apWrite", "apCreate", "apInstall", "apAppstore" ];
/// TUserSessionManager = class (TW3ErrorObject)
///  [line: 148, column: 3, file: ragnarok.sessions]
var TUserSessionManager = {
   $ClassName:"TUserSessionManager",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.OnPrivilegeGranted = null;
      $.OnPrivilegeRevoked = null;
      $.OnSessionClosed = null;
      $.OnSessionOpened = null;
      $.FIPLookup = $.FLookup = null;
      $.FSessions = [];
   }
   /// function TUserSessionManager.Add(FromThisIP: String) : TUserSession
   ///  [line: 371, column: 30, file: ragnarok.sessions]
   ,Add$5:function(Self, FromThisIP) {
      var Result = null;
      var LSessionID = "";
      TW3ErrorObject.ClearLastError$1(Self);
      FromThisIP = (Trim$_String_(FromThisIP)).toLocaleLowerCase();
      if (FromThisIP.length<1) {
         TW3ErrorObject.SetLastError$1(Self,"Failed to add session, remote IP was empty error");
      }
      if (TW3CustomDictionary.Contains(Self.FIPLookup,FromThisIP)) {
         Result = $As(TW3ObjDictionary.a$239(Self.FIPLookup,FromThisIP),TUserSession);
         return Result;
      }
      Result = TUserSession.Create$104($New(TUserSession),Self,FromThisIP);
      LSessionID = (TUserSession.GetIdentifier(Result)).toLocaleLowerCase();
      Self.FSessions.push(Result);
      TW3ObjDictionary.a$238(Self.FLookup,LSessionID,Result);
      TUserSessionManager.SessionAdded(Self,Result);
      TW3ObjDictionary.a$238(Self.FIPLookup,FromThisIP,Result);
      return Result
   }
   /// procedure TUserSessionManager.Clear()
   ///  [line: 220, column: 31, file: ragnarok.sessions]
   ,Clear$19:function(Self) {
      var a$480 = 0;
      var item$4 = null;
      try {
         var a$481 = [];
         a$481 = Self.FSessions;
         var $temp51;
         for(a$480=0,$temp51=a$481.length;a$480<$temp51;a$480++) {
            item$4 = a$481[a$480];
            TObject.Free(item$4);
         }
      } finally {
         Self.FSessions.length=0;
         TW3CustomDictionary.Clear$12(Self.FLookup);
      }
   }
   /// constructor TUserSessionManager.Create()
   ///  [line: 204, column: 33, file: ragnarok.sessions]
   ,Create$45:function(Self) {
      TW3ErrorObject.Create$45(Self);
      Self.FLookup = TW3CustomDictionary.Create$87($New(TW3ObjDictionary));
      Self.FIPLookup = TW3CustomDictionary.Create$87($New(TW3ObjDictionary));
      return Self
   }
   /// destructor TUserSessionManager.Destroy()
   ///  [line: 211, column: 32, file: ragnarok.sessions]
   ,Destroy:function(Self) {
      if (Self.FSessions.length>0) {
         TUserSessionManager.Clear$19(Self);
      }
      TObject.Free(Self.FLookup);
      TObject.Free(Self.FIPLookup);
      TW3ErrorObject.Destroy(Self);
   }
   /// function TUserSessionManager.GetSessionbyId(Identifier: String) : TUserSession
   ///  [line: 321, column: 30, file: ragnarok.sessions]
   ,GetSessionbyId:function(Self, Identifier$3) {
      var Result = null;
      TW3ErrorObject.ClearLastError$1(Self);
      Identifier$3 = Trim$_String_((Identifier$3).toLocaleLowerCase());
      if (Identifier$3.length>0) {
         if (TW3CustomDictionary.Contains(Self.FLookup,Identifier$3)) {
            Result = $As(TW3ObjDictionary.a$239(Self.FLookup,Identifier$3),TUserSession);
         } else {
            TW3ErrorObject.SetLastErrorF$1(Self,"Uknown session [%s] error",[Identifier$3]);
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Invalid session-id error");
      }
      return Result
   }
   /// procedure TUserSessionManager.PrivilegeGranted(Session: TUserSession; Privilege: TWbAccountPrivilege)
   ///  [line: 242, column: 31, file: ragnarok.sessions]
   ,PrivilegeGranted:function(Self, Session, Privilege) {
      if (!TUserSession.GetState$1(Session)) {
         if (Self.OnPrivilegeGranted) {
            Self.OnPrivilegeGranted(Self,Session,Privilege);
         }
      }
   }
   /// procedure TUserSessionManager.PrivilegeRevoked(Session: TUserSession; Privilege: TWbAccountPrivilege)
   ///  [line: 233, column: 31, file: ragnarok.sessions]
   ,PrivilegeRevoked:function(Self, Session$1, Privilege$1) {
      if (!TUserSession.GetState$1(Session$1)) {
         if (Self.OnPrivilegeRevoked) {
            Self.OnPrivilegeRevoked(Self,Session$1,Privilege$1);
         }
      }
   }
   /// procedure TUserSessionManager.SessionAdded(const Session: TUserSession)
   ///  [line: 251, column: 31, file: ragnarok.sessions]
   ,SessionAdded:function(Self, Session$2) {
      if (Self.OnSessionOpened) {
         Self.OnSessionOpened(Self,Session$2);
      }
   }
   /// procedure TUserSessionManager.SessionDeleted(const Session: TUserSession)
   ///  [line: 257, column: 31, file: ragnarok.sessions]
   ,SessionDeleted:function(Self, Session$3) {
      var x$21 = 0;
      var id$2 = "",
         LRemoteIP = "";
      if (Session$3!==null) {
         var $temp52;
         for(x$21=0,$temp52=Self.FSessions.length;x$21<$temp52;x$21++) {
            if (Self.FSessions[x$21]===Session$3) {
               Self.FSessions.splice(x$21,1)
               ;
               break;
            }
         }
         id$2 = (TUserSession.GetIdentifier(Session$3)).toLocaleLowerCase();
         if (TW3CustomDictionary.Contains(Self.FLookup,id$2)) {
            TW3CustomDictionary.Delete$6(Self.FLookup,id$2);
         }
         LRemoteIP = (Trim$_String_(Session$3.FRemoteIP)).toLocaleLowerCase();
         if (TW3CustomDictionary.Contains(Self.FIPLookup,LRemoteIP)) {
            TW3CustomDictionary.Delete$6(Self.FIPLookup,LRemoteIP);
         }
         if (Self.OnSessionClosed) {
            Self.OnSessionClosed(Self,Session$3);
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45$:function($){return $.ClassType.Create$45($)}
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
};
TUserSessionManager.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TUserSessionData = record
///  [line: 51, column: 3, file: ragnarok.sessions]
function Copy$TUserSessionData(s,d) {
   d.Identifier=s.Identifier;
   d.LoginTime=s.LoginTime;
   d.Password$1=s.Password$1;
   d.Privileges=s.Privileges.slice(0);
   d.RootFolder=s.RootFolder;
   d.State$2=s.State$2;
   d.Username=s.Username;
   d.Values$7=s.Values$7;
   return d;
}
function Clone$TUserSessionData($) {
   return {
      Identifier:$.Identifier,
      LoginTime:$.LoginTime,
      Password$1:$.Password$1,
      Privileges:$.Privileges.slice(0),
      RootFolder:$.RootFolder,
      State$2:$.State$2,
      Username:$.Username,
      Values$7:$.Values$7
   }
}
/// TUserSession = class (TW3OwnedErrorObject)
///  [line: 84, column: 3, file: ragnarok.sessions]
var TUserSession = {
   $ClassName:"TUserSession",$Parent:TW3OwnedErrorObject
   ,$Init:function ($) {
      TW3OwnedErrorObject.$Init($);
      $.FData$1 = {Identifier:"",LoginTime:undefined,Password$1:"",Privileges:[0],RootFolder:"",State$2:0,Username:"",Values$7:undefined};
      $.FRemoteIP = "";
      $.FValues = null;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 123, column: 66, file: ragnarok.sessions]
   ,a$425:function(Self, Value$65) {
      Self.FData$1.RootFolder = Value$65;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 123, column: 41, file: ragnarok.sessions]
   ,a$424:function(Self) {
      return Self.FData$1.RootFolder;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 122, column: 68, file: ragnarok.sessions]
   ,a$423:function(Self, Value$66) {
      Self.FData$1.LoginTime = Value$66;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 122, column: 44, file: ragnarok.sessions]
   ,a$422:function(Self) {
      return Self.FData$1.LoginTime;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 120, column: 64, file: ragnarok.sessions]
   ,a$419:function(Self, Value$67) {
      Self.FData$1.Username = Value$67;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 120, column: 41, file: ragnarok.sessions]
   ,a$418:function(Self) {
      return Self.FData$1.Username;
   }
   /// function TUserSession.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 469, column: 23, file: ragnarok.sessions]
   ,AcceptOwner:function(Self, CandidateObject$6) {
      return CandidateObject$6!==null&&$Is(CandidateObject$6,TUserSessionManager);
   }
   /// constructor TUserSession.Create(Manager: TUserSessionManager; UserIP: String)
   ///  [line: 443, column: 26, file: ragnarok.sessions]
   ,Create$104:function(Self, Manager, UserIP) {
      TW3OwnedErrorObject.Create$16(Self,Manager);
      Self.FValues = TJSONStructure.Create$84($New(TJSONStructure));
      TUserSession.Reset$5(Self);
      Self.FRemoteIP = Trim$_String_(UserIP);
      return Self
   }
   /// destructor TUserSession.Destroy()
   ///  [line: 451, column: 25, file: ragnarok.sessions]
   ,Destroy:function(Self) {
      if (TUserSession.GetOwner$8(Self)!==null) {
         TUserSessionManager.SessionDeleted(TUserSession.GetOwner$8(Self),Self);
      }
      TObject.Free(Self.FValues);
      TW3OwnedErrorObject.Destroy(Self);
   }
   /// procedure TUserSession.FromJSON(const Data: String)
   ///  [line: 633, column: 24, file: ragnarok.sessions]
   ,FromJSON$2:function(Self, Data$48) {
      var LValues = {Identifier:"",LoginTime:undefined,Password$1:"",Privileges:[0],RootFolder:"",State$2:0,Username:"",Values$7:undefined};
      LValues = JSON.parse(Data$48);
      Copy$TUserSessionData(LValues,Self.FData$1);
      TJSONStructure.FromJSon(Self.FValues,JSON.stringify(Self.FData$1.Values$7));
      Self.FData$1.Values$7 = undefined;
   }
   /// function TUserSession.GetIdentifier() : String
   ///  [line: 494, column: 23, file: ragnarok.sessions]
   ,GetIdentifier:function(Self) {
      return Self.FData$1.Identifier;
   }
   /// function TUserSession.GetOwner() : TUserSessionManager
   ///  [line: 474, column: 23, file: ragnarok.sessions]
   ,GetOwner$8:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TUserSessionManager);
   }
   /// function TUserSession.GetPrivileges() : TWbAccountPrivileges
   ///  [line: 522, column: 23, file: ragnarok.sessions]
   ,GetPrivileges:function(Self) {
      return Self.FData$1.Privileges.slice(0);
   }
   /// function TUserSession.GetRootFolder() : String
   ///  [line: 484, column: 23, file: ragnarok.sessions]
   ,GetRootFolder:function(Self) {
      return Self.FData$1.RootFolder;
   }
   /// function TUserSession.GetState() : TWbAccountState
   ///  [line: 509, column: 23, file: ragnarok.sessions]
   ,GetState$1:function(Self) {
      return Self.FData$1.State$2;
   }
   /// function TUserSession.GetValues() : IW3Structure
   ///  [line: 479, column: 23, file: ragnarok.sessions]
   ,GetValues$1:function(Self) {
      return $AsIntf(Self.FValues,"IW3Structure");
   }
   /// procedure TUserSession.Reset()
   ///  [line: 459, column: 24, file: ragnarok.sessions]
   ,Reset$5:function(Self) {
      Self.FData$1.Privileges = [0];
      Self.FData$1.State$2 = 2;
      Self.FData$1.Identifier = "Session"+TW3Identifiers.GenerateUniqueNumber(TW3Identifiers).toString();
      Self.FData$1.LoginTime = Now();
      Self.FData$1.Username = "";
      Self.FData$1.Password$1 = "";
   }
   /// procedure TUserSession.SetIdentifier(NewIdentifier: String)
   ///  [line: 499, column: 24, file: ragnarok.sessions]
   ,SetIdentifier:function(Self, NewIdentifier) {
      var old = "";
      NewIdentifier = Trim$_String_(NewIdentifier);
      if (NewIdentifier!=Self.FData$1.Identifier) {
         old = Self.FData$1.Identifier;
         Self.FData$1.Identifier = NewIdentifier;
      }
   }
   /// procedure TUserSession.SetPrivileges(const NewPrivileges: TWbAccountPrivileges)
   ///  [line: 527, column: 24, file: ragnarok.sessions]
   ,SetPrivileges:function(Self, NewPrivileges) {
      if ($SetIn(NewPrivileges,0,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),0,0,6))) {
         $SetInc(Self.FData$1.Privileges,0,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,0);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),0,0,6)&&(!$SetIn(NewPrivileges,0,0,6))) {
         $SetExc(Self.FData$1.Privileges,0,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,0);
      }
      if ($SetIn(NewPrivileges,1,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),1,0,6))) {
         $SetInc(Self.FData$1.Privileges,1,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,1);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),1,0,6)&&(!$SetIn(NewPrivileges,1,0,6))) {
         $SetExc(Self.FData$1.Privileges,1,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,1);
      }
      if ($SetIn(NewPrivileges,2,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),2,0,6))) {
         $SetInc(Self.FData$1.Privileges,2,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,2);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),2,0,6)&&(!$SetIn(NewPrivileges,2,0,6))) {
         $SetExc(Self.FData$1.Privileges,2,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,2);
      }
      if ($SetIn(NewPrivileges,3,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),3,0,6))) {
         $SetInc(Self.FData$1.Privileges,3,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,3);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),3,0,6)&&(!$SetIn(NewPrivileges,3,0,6))) {
         $SetExc(Self.FData$1.Privileges,3,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,3);
      }
      if ($SetIn(NewPrivileges,4,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),4,0,6))) {
         $SetInc(Self.FData$1.Privileges,4,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,4);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),4,0,6)&&(!$SetIn(NewPrivileges,4,0,6))) {
         $SetExc(Self.FData$1.Privileges,4,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,4);
      }
      if ($SetIn(NewPrivileges,5,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),5,0,6))) {
         $SetInc(Self.FData$1.Privileges,5,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,5);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),5,0,6)&&(!$SetIn(NewPrivileges,5,0,6))) {
         $SetExc(Self.FData$1.Privileges,5,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,5);
      }
   }
   /// procedure TUserSession.SetRootFolder(const NewRootFolder: String)
   ///  [line: 489, column: 24, file: ragnarok.sessions]
   ,SetRootFolder:function(Self, NewRootFolder) {
      Self.FData$1.RootFolder = NewRootFolder;
   }
   /// procedure TUserSession.SetState(const NewState: TWbAccountState)
   ///  [line: 514, column: 24, file: ragnarok.sessions]
   ,SetState$2:function(Self, NewState$3) {
      if (NewState$3!=Self.FData$1.State$2) {
         Self.FData$1.State$2 = NewState$3;
      }
   }
   /// function TUserSession.ToJSON() : String
   ///  [line: 627, column: 23, file: ragnarok.sessions]
   ,ToJSON$6:function(Self) {
      var Result = "";
      Self.FData$1.Values$7 = TJSONStructure.ToJSonObject(Self.FValues);
      Result = JSON.stringify(Self.FData$1);
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedErrorObject.Create$16
};
TUserSession.$Intf={
   IW3OwnedObjectAccess:[TUserSession.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IUserSession:[TUserSession.GetOwner$8,TUserSession.GetValues$1,TUserSession.GetIdentifier,TUserSession.SetIdentifier,TUserSession.GetPrivileges,TUserSession.SetPrivileges,TUserSession.GetState$1,TUserSession.SetState$2,TUserSession.GetRootFolder,TUserSession.SetRootFolder,TUserSession.Reset$5,TUserSession.ToJSON$6,TUserSession.FromJSON$2]
}
function SessionManager() {
   var Result = null;
   if (__SessionManager===null) {
      __SessionManager = TW3ErrorObject.Create$45$($New(TUserSessionManager));
   }
   Result = __SessionManager;
   return Result
};
/// TNJWebSocketSocket = class (TW3ErrorObject)
///  [line: 58, column: 3, file: SmartNJ.Server.WebSocket]
var TNJWebSocketSocket = {
   $ClassName:"TNJWebSocketSocket",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.RemoteAddress = "";
      $.FReq = $.FServer$1 = $.FSocket = null;
   }
   /// procedure TNJWebSocketSocket.Close()
   ///  [line: 833, column: 30, file: SmartNJ.Server.WebSocket]
   ,Close$2:function(Self) {
      if (TNJWebSocketSocket.GetReadyState(Self)!=3) {
         Self.FSocket.terminate();
      }
   }
   /// constructor TNJWebSocketSocket.Create(const Server: TNJRawWebSocketServer; const WsSocket: JWsSocket)
   ///  [line: 724, column: 32, file: SmartNJ.Server.WebSocket]
   ,Create$88:function(Self, Server$6, WsSocket) {
      TW3ErrorObject.Create$45(Self);
      if (Server$6!==null) {
         Self.FServer$1 = Server$6;
         if (WsSocket!==null) {
            Self.FSocket = WsSocket;
            Self.FOptions$4.AutoWriteToConsole = true;
            Self.FOptions$4.ThrowExceptions = true;
            Self.FSocket["name"] = TW3Identifiers.GenerateUniqueObjectId(TW3Identifiers);
         } else {
            throw Exception.Create($New(Exception),"Failed to create socket, socket was nil error");
         }
      } else {
         throw Exception.Create($New(Exception),"Failed to create socket, server was nil error");
      }
      return Self
   }
   /// destructor TNJWebSocketSocket.Destroy()
   ///  [line: 746, column: 31, file: SmartNJ.Server.WebSocket]
   ,Destroy:function(Self) {
      if (Self.FReq!==null) {
         TObject.Free(Self.FReq);
      }
      Self.FServer$1 = null;
      Self.FSocket = null;
      TW3ErrorObject.Destroy(Self);
   }
   /// function TNJWebSocketSocket.GetExceptionClass() : TW3ExceptionClass
   ///  [line: 756, column: 30, file: SmartNJ.Server.WebSocket]
   ,GetExceptionClass:function(Self) {
      return ENJWebSocketSocket;
   }
   /// function TNJWebSocketSocket.GetReadyState() : JWsReadyState
   ///  [line: 792, column: 29, file: SmartNJ.Server.WebSocket]
   ,GetReadyState:function(Self) {
      return (Self.FSocket)?Self.FSocket.readyState:3;
   }
   /// function TNJWebSocketSocket.GetSocketName() : String
   ///  [line: 844, column: 29, file: SmartNJ.Server.WebSocket]
   ,GetSocketName:function(Self) {
      return String(Self.FSocket["name"]);
   }
   /// procedure TNJWebSocketSocket.Send(const Text: String)
   ///  [line: 767, column: 30, file: SmartNJ.Server.WebSocket]
   ,Send$1:function(Self, Text$20) {
      if (Self.FSocket!==null) {
         Self.FSocket.send(Text$20);
      }
   }
   /// procedure TNJWebSocketSocket.SetSocketName(NewName: String)
   ///  [line: 800, column: 30, file: SmartNJ.Server.WebSocket]
   ,SetSocketName:function(Self, NewName) {
      var OldName = "";
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      NewName = Trim$_String_(NewName);
      OldName = TNJWebSocketSocket.GetSocketName(Self);
      if (NewName!=OldName) {
         if (NewName.length>0) {
            Self.FSocket["name"] = Trim$_String_(NewName);
            TNJRawWebSocketServer.DoClientNameChange(Self.FServer$1,Self,OldName);
         } else {
            TW3ErrorObject.SetLastError$1(Self,"Invalid socket rename, string was empty error");
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
};
TNJWebSocketSocket.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJCustomServer = class (TW3ErrorObject)
///  [line: 57, column: 3, file: SmartNJ.Server]
var TNJCustomServer = {
   $ClassName:"TNJCustomServer",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.OnBeforeServerStopped = null;
      $.OnBeforeServerStarted = null;
      $.OnAfterServerStopped = null;
      $.OnAfterServerStarted = null;
      $.FActive$3 = false;
      $.FHandle$6 = undefined;
      $.FPort$1 = 0;
   }
   /// procedure TNJCustomServer.AfterStart()
   ///  [line: 164, column: 27, file: SmartNJ.Server]
   ,AfterStart:function(Self) {
      if (Self.OnAfterServerStarted) {
         Self.OnAfterServerStarted(Self);
      }
   }
   /// procedure TNJCustomServer.AfterStop()
   ///  [line: 170, column: 27, file: SmartNJ.Server]
   ,AfterStop:function(Self) {
      if (Self.OnAfterServerStopped) {
         Self.OnAfterServerStopped(Self);
      }
   }
   /// procedure TNJCustomServer.BeforeStart()
   ///  [line: 158, column: 27, file: SmartNJ.Server]
   ,BeforeStart:function(Self) {
      if (Self.OnBeforeServerStarted) {
         Self.OnBeforeServerStarted(Self);
      }
   }
   /// procedure TNJCustomServer.BeforeStop()
   ///  [line: 152, column: 27, file: SmartNJ.Server]
   ,BeforeStop:function(Self) {
      if (Self.OnBeforeServerStopped) {
         Self.OnBeforeServerStopped(Self);
      }
   }
   /// function TNJCustomServer.GetActive() : Boolean
   ///  [line: 192, column: 26, file: SmartNJ.Server]
   ,GetActive$3:function(Self) {
      return Self.FActive$3;
   }
   /// function TNJCustomServer.GetHandle() : THandle
   ///  [line: 202, column: 26, file: SmartNJ.Server]
   ,GetHandle$1:function(Self) {
      return Self.FHandle$6;
   }
   /// function TNJCustomServer.GetPort() : Integer
   ///  [line: 176, column: 26, file: SmartNJ.Server]
   ,GetPort:function(Self) {
      return Self.FPort$1;
   }
   /// procedure TNJCustomServer.SetActive(const Value: Boolean)
   ///  [line: 197, column: 27, file: SmartNJ.Server]
   ,SetActive$3:function(Self, Value$68) {
      Self.FActive$3 = Value$68;
   }
   /// procedure TNJCustomServer.SetHandle(const Value: THandle)
   ///  [line: 207, column: 27, file: SmartNJ.Server]
   ,SetHandle$2:function(Self, Value$69) {
      Self.FHandle$6 = Value$69;
   }
   /// procedure TNJCustomServer.SetPort(const Value: Integer)
   ///  [line: 181, column: 27, file: SmartNJ.Server]
   ,SetPort$1:function(Self, Value$70) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$3(Self)) {
         TW3ErrorObject.SetLastError$1(Self,"Port cannot be altered while server is active error");
      } else {
         Self.FPort$1 = Value$70;
      }
   }
   /// procedure TNJCustomServer.Start()
   ///  [line: 118, column: 27, file: SmartNJ.Server]
   ,Start$1:function(Self) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$3(Self)) {
         TW3ErrorObject.SetLastError$1(Self,"Server already active error");
      } else {
         TNJCustomServer.BeforeStart(Self);
         TNJCustomServer.StartServer$(Self);
      }
   }
   /// procedure TNJCustomServer.StartServer()
   ///  [line: 142, column: 27, file: SmartNJ.Server]
   ,StartServer:function(Self) {
      TNJCustomServer.SetActive$3$(Self,true);
   }
   /// procedure TNJCustomServer.Stop()
   ///  [line: 130, column: 27, file: SmartNJ.Server]
   ,Stop$1:function(Self) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$3(Self)) {
         TNJCustomServer.BeforeStop(Self);
         TNJCustomServer.StopServer$(Self);
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Server not started error");
      }
   }
   /// procedure TNJCustomServer.StopServer()
   ///  [line: 147, column: 27, file: SmartNJ.Server]
   ,StopServer:function(Self) {
      TNJCustomServer.SetActive$3$(Self,false);
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,GetPort$:function($){return $.ClassType.GetPort($)}
   ,SetActive$3$:function($){return $.ClassType.SetActive$3.apply($.ClassType, arguments)}
   ,SetPort$1$:function($){return $.ClassType.SetPort$1.apply($.ClassType, arguments)}
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
};
TNJCustomServer.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJRawWebSocketServer = class (TNJCustomServer)
///  [line: 123, column: 3, file: SmartNJ.Server.WebSocket]
var TNJRawWebSocketServer = {
   $ClassName:"TNJRawWebSocketServer",$Parent:TNJCustomServer
   ,$Init:function ($) {
      TNJCustomServer.$Init($);
      $.OnBinMessage = null;
      $.OnTextMessage = null;
      $.OnClientDisconnected = null;
      $.OnClientConnected = null;
      $.OnError = null;
      $.FPath$1 = "";
      $.FPayload = 0;
      $.FSocketLUT = null;
      $.FSockets = [];
      $.FTrack = false;
   }
   /// procedure TNJRawWebSocketServer.ClearLookupTable()
   ///  [line: 653, column: 33, file: SmartNJ.Server.WebSocket]
   ,ClearLookupTable:function(Self) {
      TW3CustomDictionary.Clear$12(Self.FSocketLUT);
   }
   /// constructor TNJRawWebSocketServer.Create()
   ///  [line: 347, column: 35, file: SmartNJ.Server.WebSocket]
   ,Create$45:function(Self) {
      TW3ErrorObject.Create$45(Self);
      Self.FSocketLUT = TW3CustomDictionary.Create$87($New(TW3ObjDictionary));
      Self.FOptions$4.AutoWriteToConsole = true;
      Self.FOptions$4.ThrowExceptions = true;
      Self.FPayload = 41943040;
      return Self
   }
   /// destructor TNJRawWebSocketServer.Destroy()
   ///  [line: 359, column: 34, file: SmartNJ.Server.WebSocket]
   ,Destroy:function(Self) {
      TObject.Free(Self.FSocketLUT);
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TNJRawWebSocketServer.Dispatch(Socket: TNJWebSocketSocket; Data: TNJWebsocketMessage)
   ///  [line: 446, column: 33, file: SmartNJ.Server.WebSocket]
   ,Dispatch$2:function(Self, Socket$3, Data$49) {
      switch (Data$49.wiType) {
         case 0 :
            if (Self.OnTextMessage) {
               Self.OnTextMessage(Self,Socket$3,Clone$TNJWebsocketMessage(Data$49));
            }
            break;
         case 1 :
            if (Self.OnBinMessage) {
               Self.OnBinMessage(Self,Socket$3,Clone$TNJWebsocketMessage(Data$49));
            }
            break;
      }
   }
   /// procedure TNJRawWebSocketServer.DoClientConnected(Socket: THandle; Request: THandle)
   ///  [line: 533, column: 33, file: SmartNJ.Server.WebSocket]
   ,DoClientConnected:function(Self, Socket$4, Request$1) {
      var NJSocket = null;
      NJSocket = TNJWebSocketSocket.Create$88($New(TNJWebSocketSocket),Self,Socket$4);
      TW3ObjDictionary.a$238(Self.FSocketLUT,TNJWebSocketSocket.GetSocketName(NJSocket),NJSocket);
      Self.FSockets.push(NJSocket);
      if (Request$1) {
         if (Request$1.connection) {
            NJSocket.RemoteAddress = String(Request$1.connection.remoteAddress);
         }
      }
      if (Self.OnClientConnected) {
         Self.OnClientConnected(Self,NJSocket);
      }
      Socket$4.on("error",function (error$3) {
         var NJSocket$1 = null,
            SocketName;
         if (Self.OnError) {
            NJSocket$1 = null;
            SocketName = Socket$4["name"];
            if (TW3CustomDictionary.Contains(Self.FSocketLUT,(String(SocketName)))) {
               NJSocket$1 = $As(TW3ObjDictionary.a$239(Self.FSocketLUT,(String(SocketName))),TNJWebSocketSocket);
            } else {
               NJSocket$1 = TNJWebSocketSocket.Create$88($New(TNJWebSocketSocket),Self,Socket$4);
            }
            Self.OnError(Self,NJSocket$1,error$3);
         }
      });
      Socket$4.on("message",function (message$1) {
         var LInfo = {wiType:0,wiBuffer:null,wiText:""};
         var NJSocket$2 = null,
            SocketName$1;
         NJSocket$2 = null;
         if (TW3VariantHelper$IsUInt8Array(message$1)) {
            LInfo.wiType = 1;
            LInfo.wiBuffer = message$1;
         } else {
            LInfo.wiType = 0;
            LInfo.wiText = String(message$1);
         }
         SocketName$1 = Socket$4["name"];
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,(String(SocketName$1)))) {
            NJSocket$2 = $As(TW3ObjDictionary.a$239(Self.FSocketLUT,(String(SocketName$1))),TNJWebSocketSocket);
         } else {
            NJSocket$2 = TNJWebSocketSocket.Create$88($New(TNJWebSocketSocket),Self,Socket$4);
         }
         TNJRawWebSocketServer.Dispatch$2(Self,NJSocket$2,Clone$TNJWebsocketMessage(LInfo));
      });
      Socket$4.on("close",function (code$1, reason) {
         TNJRawWebSocketServer.DoClientDisconnected(Self,Socket$4,code$1,reason);
      });
   }
   /// procedure TNJRawWebSocketServer.DoClientDisconnected(Socket: THandle; Code: Integer; Reason: String)
   ///  [line: 606, column: 33, file: SmartNJ.Server.WebSocket]
   ,DoClientDisconnected:function(Self, Socket$5, Code$1, Reason) {
      var RawSocket$1 = null,
         SocketId = "",
         NJSocket$3 = null,
         index$4 = 0;
      RawSocket$1 = Socket$5;
      SocketId = TVariant.AsString(Socket$5["name"]);
      NJSocket$3 = null;
      if (SocketId.length>0) {
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,SocketId)) {
            WriteLn("Found in name lookup!");
            NJSocket$3 = $As(TW3ObjDictionary.a$239(Self.FSocketLUT,SocketId),TNJWebSocketSocket);
         }
      }
      try {
         if (Self.OnClientDisconnected) {
            Self.OnClientDisconnected(Self,NJSocket$3);
         }
      } finally {
         if (NJSocket$3!==null) {
            index$4 = Self.FSockets.indexOf(NJSocket$3);
            Self.FSockets.splice(index$4,1)
            ;
            TW3CustomDictionary.Delete$6(Self.FSocketLUT,SocketId);
            TObject.Free(NJSocket$3);
         }
      }
   }
   /// procedure TNJRawWebSocketServer.DoClientNameChange(Client: TNJWebSocketSocket; OldName: String)
   ///  [line: 505, column: 33, file: SmartNJ.Server.WebSocket]
   ,DoClientNameChange:function(Self, Client, OldName$1) {
      var NewName$1 = "";
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      NewName$1 = Trim$_String_(TNJWebSocketSocket.GetSocketName(Client));
      if (NewName$1.length>0) {
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,NewName$1)) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid socket rename, a socket with that name [%s] already exists",[NewName$1]);
            return;
         }
         OldName$1 = Trim$_String_(OldName$1);
         if (OldName$1.length>0) {
            if (TW3CustomDictionary.Contains(Self.FSocketLUT,OldName$1)) {
               TW3CustomDictionary.Delete$6(Self.FSocketLUT,OldName$1);
            }
         }
         TW3ObjDictionary.a$238(Self.FSocketLUT,TNJWebSocketSocket.GetSocketName(Client),Client);
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Invalid socket rename, string was empty errror");
      }
   }
   /// function TNJRawWebSocketServer.GetClient(const Index: Integer) : TNJWebSocketSocket
   ///  [line: 493, column: 32, file: SmartNJ.Server.WebSocket]
   ,GetClient:function(Self, Index$6) {
      var Result = null;
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$3(Self)) {
         if (Index$6>=0&&Index$6<Self.FSockets.length) {
            Result = Self.FSockets[Index$6];
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"GetClient failed, server is not active error");
      }
      return Result
   }
   /// function TNJRawWebSocketServer.GetCount() : Integer
   ///  [line: 478, column: 32, file: SmartNJ.Server.WebSocket]
   ,GetCount:function(Self) {
      var Result = 0;
      var LHandle = undefined;
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$3(Self)) {
         LHandle = TNJCustomServer.GetHandle$1(Self);
         if (LHandle.clients) {
            Result = parseInt(LHandle.clients.size,10);
         } else {
            TW3ErrorObject.SetLastError$1(Self,"Client tracking not enabled, client-count not available error");
         }
      } else {
         Result = -1;
      }
      return Result
   }
   /// function TNJRawWebSocketServer.GetExceptionClass() : TW3ExceptionClass
   ///  [line: 365, column: 32, file: SmartNJ.Server.WebSocket]
   ,GetExceptionClass:function(Self) {
      return ENJWebSocketServer;
   }
   /// procedure TNJRawWebSocketServer.InternalSetActive(const Value: Boolean)
   ///  [line: 648, column: 33, file: SmartNJ.Server.WebSocket]
   ,InternalSetActive:function(Self, Value$71) {
      TNJCustomServer.SetActive$3(Self,Value$71);
   }
   /// procedure TNJRawWebSocketServer.SetActive(const Value: Boolean)
   ///  [line: 420, column: 33, file: SmartNJ.Server.WebSocket]
   ,SetActive$3:function(Self, Value$72) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Value$72!=TNJCustomServer.GetActive$3(Self)) {
         TNJCustomServer.SetActive$3(Self,Value$72);
         try {
            if (TNJCustomServer.GetActive$3(Self)) {
               TNJCustomServer.StartServer$(Self);
            } else {
               TNJCustomServer.StopServer$(Self);
            }
         } catch ($e) {
            var e$21 = $W($e);
            TNJCustomServer.SetActive$3(Self,(!Value$72));
            TW3ErrorObject.SetLastErrorF$1(Self,"Failed to start server, system threw exception %s with message [%s]",[TObject.ClassName(e$21.ClassType), e$21.FMessage]);
         }
      }
   }
   /// procedure TNJRawWebSocketServer.SetPath(URLPath: String)
   ///  [line: 398, column: 33, file: SmartNJ.Server.WebSocket]
   ,SetPath:function(Self, URLPath) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$3(Self)) {
         TW3ErrorObject.SetLastError$1(Self,"Failed to set listen path, server is active error");
      } else {
         Self.FPath$1 = (Trim$_String_(URLPath)).toLocaleLowerCase();
      }
   }
   /// procedure TNJRawWebSocketServer.SetPayload(NewPayload: Integer)
   ///  [line: 388, column: 33, file: SmartNJ.Server.WebSocket]
   ,SetPayload:function(Self, NewPayload) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$3(Self)) {
         TW3ErrorObject.SetLastError$1(Self,"Failed to set payload, value must be 1024 or above");
      } else {
         Self.FPayload = (NewPayload<1024)?1024:NewPayload;
      }
   }
   /// procedure TNJRawWebSocketServer.SetTracking(Value: Boolean)
   ///  [line: 409, column: 33, file: SmartNJ.Server.WebSocket]
   ,SetTracking:function(Self, Value$73) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$3(Self)) {
         TW3ErrorObject.SetLastError$1(Self,"Failed to set client-tracking, server is active error");
      } else {
         Self.FTrack = Value$73;
      }
   }
   /// procedure TNJRawWebSocketServer.StartServer()
   ///  [line: 658, column: 33, file: SmartNJ.Server.WebSocket]
   ,StartServer:function(Self) {
      var LOptions$2,
         LServer;
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      TW3CustomDictionary.Clear$12(Self.FSocketLUT);
      LOptions$2 = TVariant.CreateObject();
      LOptions$2["clientTracking "] = Self.FTrack;
      if (Self.FPath$1.length>0) {
         LOptions$2["path"] = Self.FPath$1;
      }
      LOptions$2["port"] = TNJCustomServer.GetPort$(Self);
      LOptions$2["maxPayload"] = Self.FPayload;
      LServer = null;
      try {
         LServer = WebSocketCreateServer(LOptions$2);
      } catch ($e) {
         var e$22 = $W($e);
         TW3ErrorObject.SetLastErrorF$1(Self,"Failed to create websocket server object, system threw exception %s with message [%s]",[TObject.ClassName(e$22.ClassType), e$22.FMessage]);
         return;
      }
      TNJCustomServer.SetHandle$2(Self,LServer);
      LServer.on("error",function (error$4) {
         if (Self.OnError) {
            Self.OnError(Self,null,error$4);
         }
      });
      LServer.on("connection",$Event2(Self,TNJRawWebSocketServer.DoClientConnected));
      TNJCustomServer.AfterStart(Self);
   }
   /// procedure TNJRawWebSocketServer.StopServer()
   ///  [line: 706, column: 33, file: SmartNJ.Server.WebSocket]
   ,StopServer:function(Self) {
      var LHandle$1 = undefined;
      LHandle$1 = TNJCustomServer.GetHandle$1(Self);
      if (LHandle$1) {
         LHandle$1.close(function () {
            TNJRawWebSocketServer.InternalSetActive(Self,false);
            TNJCustomServer.AfterStop(Self);
            TW3CustomDictionary.Clear$12(Self.FSocketLUT);
         });
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45$:function($){return $.ClassType.Create$45($)}
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
   ,GetPort:TNJCustomServer.GetPort
   ,SetActive$3$:function($){return $.ClassType.SetActive$3.apply($.ClassType, arguments)}
   ,SetPort$1:TNJCustomServer.SetPort$1
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
};
TNJRawWebSocketServer.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJWebSocketHybridServer = class (TNJRawWebSocketServer)
///  [line: 197, column: 3, file: SmartNJ.Server.WebSocket]
var TNJWebSocketHybridServer = {
   $ClassName:"TNJWebSocketHybridServer",$Parent:TNJRawWebSocketServer
   ,$Init:function ($) {
      TNJRawWebSocketServer.$Init($);
   }
   ,Destroy:TNJRawWebSocketServer.Destroy
   ,Create$45:TNJRawWebSocketServer.Create$45
   ,GetExceptionClass:TNJRawWebSocketServer.GetExceptionClass
   ,GetPort:TNJCustomServer.GetPort
   ,SetActive$3:TNJRawWebSocketServer.SetActive$3
   ,SetPort$1:TNJCustomServer.SetPort$1
   ,StartServer:TNJRawWebSocketServer.StartServer
   ,StopServer:TNJRawWebSocketServer.StopServer
   ,GetHttpServer$:function($){return $.ClassType.GetHttpServer($)}
};
TNJWebSocketHybridServer.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJWebSocketServer = class (TNJWebSocketHybridServer)
///  [line: 205, column: 3, file: SmartNJ.Server.WebSocket]
var TNJWebSocketServer = {
   $ClassName:"TNJWebSocketServer",$Parent:TNJWebSocketHybridServer
   ,$Init:function ($) {
      TNJWebSocketHybridServer.$Init($);
      $.FServer$2 = null;
   }
   /// constructor TNJWebSocketServer.Create()
   ///  [line: 234, column: 32, file: SmartNJ.Server.WebSocket]
   ,Create$45:function(Self) {
      TNJRawWebSocketServer.Create$45(Self);
      Self.FServer$2 = TW3ErrorObject.Create$45$($New(TNJHTTPServer));
      Self.FOptions$4.AutoWriteToConsole = true;
      Self.FOptions$4.ThrowExceptions = true;
      return Self
   }
   /// destructor TNJWebSocketServer.Destroy()
   ///  [line: 243, column: 31, file: SmartNJ.Server.WebSocket]
   ,Destroy:function(Self) {
      TObject.Free(Self.FServer$2);
      TNJRawWebSocketServer.Destroy(Self);
   }
   /// function TNJWebSocketServer.GetHttpServer() : TNJCustomServer
   ///  [line: 249, column: 29, file: SmartNJ.Server.WebSocket]
   ,GetHttpServer:function(Self) {
      return Self.FServer$2;
   }
   /// function TNJWebSocketServer.GetPort() : Integer
   ///  [line: 323, column: 29, file: SmartNJ.Server.WebSocket]
   ,GetPort:function(Self) {
      var Result = 0;
      if (Self.FServer$2!==null) {
         Result = TNJCustomServer.GetPort$(Self.FServer$2);
      }
      return Result
   }
   /// procedure TNJWebSocketServer.SetPort(const Value: Integer)
   ///  [line: 329, column: 30, file: SmartNJ.Server.WebSocket]
   ,SetPort$1:function(Self, Value$74) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$3(Self)) {
         TW3ErrorObject.SetLastError$1(Self,"Port cannot be altered while server is active error");
      } else if (TNJCustomServer.GetPort$(Self.FServer$2)!=Value$74) {
         TNJCustomServer.SetPort$1$(Self.FServer$2,Value$74);
      }
   }
   /// procedure TNJWebSocketServer.StartServer()
   ///  [line: 254, column: 30, file: SmartNJ.Server.WebSocket]
   ,StartServer:function(Self) {
      var LOptions$3,
         LServer$1;
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      TNJRawWebSocketServer.ClearLookupTable(Self);
      TNJCustomServer.SetActive$3$(Self.FServer$2,true);
      LOptions$3 = TVariant.CreateObject();
      LOptions$3["server"] = TNJCustomServer.GetHandle$1(Self.FServer$2);
      LOptions$3["clientTracking "] = Self.FTrack;
      LOptions$3["maxPayload"] = 41943040;
      if (Self.FPath$1.length>0) {
         LOptions$3["path"] = Self.FPath$1;
      }
      LServer$1 = null;
      try {
         LServer$1 = WebSocketCreateServer(LOptions$3);
      } catch ($e) {
         var e$23 = $W($e);
         TW3ErrorObject.SetLastErrorF$1(Self,"Failed to create websocket server object, system threw exception %s with message [%s]",[TObject.ClassName(e$23.ClassType), e$23.FMessage]);
         return;
      }
      TNJCustomServer.SetHandle$2(Self,LServer$1);
      LServer$1.on("error",function (error$5) {
         if (Self.OnError) {
            Self.OnError(Self,null,error$5);
         }
      });
      LServer$1.on("connection",$Event2(Self,TNJRawWebSocketServer.DoClientConnected));
      TNJCustomServer.AfterStart(Self);
   }
   /// procedure TNJWebSocketServer.StopServer()
   ///  [line: 307, column: 30, file: SmartNJ.Server.WebSocket]
   ,StopServer:function(Self) {
      var LHandle$2 = undefined;
      LHandle$2 = TNJCustomServer.GetHandle$1(Self);
      if (LHandle$2) {
         LHandle$2.close(function () {
            TNJCustomServer.SetActive$3$(Self.FServer$2,false);
            TNJRawWebSocketServer.InternalSetActive(Self,false);
            TNJCustomServer.AfterStop(Self);
            TNJRawWebSocketServer.ClearLookupTable(Self);
         });
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45$:function($){return $.ClassType.Create$45($)}
   ,GetExceptionClass:TNJRawWebSocketServer.GetExceptionClass
   ,GetPort$:function($){return $.ClassType.GetPort($)}
   ,SetActive$3:TNJRawWebSocketServer.SetActive$3
   ,SetPort$1$:function($){return $.ClassType.SetPort$1.apply($.ClassType, arguments)}
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
   ,GetHttpServer$:function($){return $.ClassType.GetHttpServer($)}
};
TNJWebSocketServer.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJWebsocketMessageType enumeration
///  [line: 43, column: 3, file: SmartNJ.Server.WebSocket]
var TNJWebsocketMessageType = [ "mtText", "mtBinary" ];
/// TNJWebsocketMessage = record
///  [line: 52, column: 3, file: SmartNJ.Server.WebSocket]
function Copy$TNJWebsocketMessage(s,d) {
   d.wiType=s.wiType;
   d.wiBuffer=s.wiBuffer;
   d.wiText=s.wiText;
   return d;
}
function Clone$TNJWebsocketMessage($) {
   return {
      wiType:$.wiType,
      wiBuffer:$.wiBuffer,
      wiText:$.wiText
   }
}
/// JWsReadyState enumeration
///  [line: 45, column: 3, file: SmartNJ.Server.WebSocket]
var JWsReadyState = [ "rsConnecting", "rsOpen", "rsClosing", "rsClosed" ];
/// ENJWebSocketSocket = class (EW3Exception)
///  [line: 35, column: 3, file: SmartNJ.Server.WebSocket]
var ENJWebSocketSocket = {
   $ClassName:"ENJWebSocketSocket",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ENJWebSocketServer = class (EW3Exception)
///  [line: 36, column: 3, file: SmartNJ.Server.WebSocket]
var ENJWebSocketServer = {
   $ClassName:"ENJWebSocketServer",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TNJServerChildClass = class (TW3HandleBasedObject)
///  [line: 43, column: 3, file: SmartNJ.Server]
var TNJServerChildClass = {
   $ClassName:"TNJServerChildClass",$Parent:TW3HandleBasedObject
   ,$Init:function ($) {
      TW3HandleBasedObject.$Init($);
      $.FParent$1 = null;
   }
   /// constructor TNJServerChildClass.Create(Server: TNJCustomServer)
   ///  [line: 108, column: 33, file: SmartNJ.Server]
   ,Create$90:function(Self, Server$7) {
      TObject.Create(Self);
      Self.FParent$1 = Server$7;
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TNJCustomServerResponse = class (TNJServerChildClass)
///  [line: 54, column: 3, file: SmartNJ.Server]
var TNJCustomServerResponse = {
   $ClassName:"TNJCustomServerResponse",$Parent:TNJServerChildClass
   ,$Init:function ($) {
      TNJServerChildClass.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TNJCustomServerRequest = class (TNJServerChildClass)
///  [line: 51, column: 3, file: SmartNJ.Server]
var TNJCustomServerRequest = {
   $ClassName:"TNJCustomServerRequest",$Parent:TNJServerChildClass
   ,$Init:function ($) {
      TNJServerChildClass.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// ENJServerError = class (EW3Exception)
///  [line: 36, column: 3, file: SmartNJ.Server]
var ENJServerError = {
   $ClassName:"ENJServerError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ENJNetworkError = class (EW3Exception)
///  [line: 27, column: 3, file: SmartNJ.Network]
var ENJNetworkError = {
   $ClassName:"ENJNetworkError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TNJAddressBindings = class (TW3LockedObject)
///  [line: 78, column: 3, file: SmartNJ.Network.Bindings]
var TNJAddressBindings = {
   $ClassName:"TNJAddressBindings",$Parent:TW3LockedObject
   ,$Init:function ($) {
      TW3LockedObject.$Init($);
      $.FItems = [];
   }
   /// procedure TNJAddressBindings.Clear()
   ///  [line: 172, column: 30, file: SmartNJ.Network.Bindings]
   ,Clear$13:function(Self) {
      var x$22 = 0;
      if (TW3LockedObject.GetLockState$1(Self)) {
         throw Exception.Create($New(EW3LockError),"Bindings cannot be altered while active error");
      } else {
         try {
            var $temp53;
            for(x$22=0,$temp53=Self.FItems.length;x$22<$temp53;x$22++) {
               TObject.Free(Self.FItems[x$22]);
               Self.FItems[x$22]=null;
            }
         } finally {
            Self.FItems.length=0;
         }
      }
   }
   /// destructor TNJAddressBindings.Destroy()
   ///  [line: 114, column: 31, file: SmartNJ.Network.Bindings]
   ,Destroy:function(Self) {
      if (Self.FItems.length>0) {
         TNJAddressBindings.Clear$13(Self);
      }
      TObject.Destroy(Self);
   }
   /// procedure TNJAddressBindings.ObjectLocked()
   ///  [line: 243, column: 30, file: SmartNJ.Network.Bindings]
   ,ObjectLocked$1:function(Self) {
      var x$23 = 0;
      var LAccess$1 = null;
      var $temp54;
      for(x$23=0,$temp54=Self.FItems.length;x$23<$temp54;x$23++) {
         LAccess$1 = $AsIntf(Self.FItems[x$23],"IW3LockObject");
         LAccess$1[0]();
      }
   }
   /// procedure TNJAddressBindings.ObjectUnLocked()
   ///  [line: 252, column: 30, file: SmartNJ.Network.Bindings]
   ,ObjectUnLocked$1:function(Self) {
      var x$24 = 0;
      var LAccess$2 = null;
      var $temp55;
      for(x$24=0,$temp55=Self.FItems.length;x$24<$temp55;x$24++) {
         LAccess$2 = $AsIntf(Self.FItems[x$24],"IW3LockObject");
         LAccess$2[1]();
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,ObjectLocked$1$:function($){return $.ClassType.ObjectLocked$1($)}
   ,ObjectUnLocked$1$:function($){return $.ClassType.ObjectUnLocked$1($)}
};
TNJAddressBindings.$Intf={
   IW3LockObject:[TW3LockedObject.DisableAlteration$1,TW3LockedObject.EnableAlteration$1,TW3LockedObject.GetLockState$1]
}
/// TNJAddressBinding = class (TW3OwnedLockedObject)
///  [line: 42, column: 3, file: SmartNJ.Network.Bindings]
var TNJAddressBinding = {
   $ClassName:"TNJAddressBinding",$Parent:TW3OwnedLockedObject
   ,$Init:function ($) {
      TW3OwnedLockedObject.$Init($);
   }
   /// function TNJAddressBinding.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 270, column: 28, file: SmartNJ.Network.Bindings]
   ,AcceptOwner:function(Self, CandidateObject$7) {
      return (CandidateObject$7!==null)&&$Is(CandidateObject$7,TNJAddressBindings);
   }
   /// constructor TNJAddressBinding.Create(const AOwner: TNJAddressBindings)
   ///  [line: 265, column: 31, file: SmartNJ.Network.Bindings]
   ,Create$93:function(Self, AOwner$5) {
      TW3OwnedObject.Create$16(Self,AOwner$5);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3OwnedLockedObject.ObjectLocked
   ,ObjectUnLocked:TW3OwnedLockedObject.ObjectUnLocked
};
TNJAddressBinding.$Intf={
   IW3OwnedObjectAccess:[TNJAddressBinding.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
function util() {
   return require("util");
};
/// JServerCredentials = class (TObject)
///  [line: 71, column: 3, file: NodeJS.net]
var JServerCredentials = {
   $ClassName:"JServerCredentials",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// JServerAddressResult = class (TObject)
///  [line: 64, column: 3, file: NodeJS.net]
var JServerAddressResult = {
   $ClassName:"JServerAddressResult",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
function http() {
   return require("http");
};
/// TNJHTTPServer = class (TNJCustomServer)
///  [line: 104, column: 3, file: SmartNJ.Server.Http]
var TNJHTTPServer = {
   $ClassName:"TNJHTTPServer",$Parent:TNJCustomServer
   ,$Init:function ($) {
      TNJCustomServer.$Init($);
      $.OnRequest = null;
   }
   /// procedure TNJHTTPServer.Dispatch(request: JServerRequest; response: JServerResponse)
   ///  [line: 147, column: 25, file: SmartNJ.Server.Http]
   ,Dispatch$3:function(Self, request$2, response) {
      var LRequest = null,
         LResponse = null;
      if (Self.OnRequest) {
         LRequest = TNJHttpRequest.Create$92($New(TNJHttpRequest),Self,request$2);
         LResponse = TNJHttpResponse.Create$91($New(TNJHttpResponse),Self,response);
         try {
            Self.OnRequest(Self,LRequest,LResponse);
         } catch ($e) {
            var e$24 = $W($e);
            throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Dispatch failed, system threw exception %s with message [%s]",[TObject.ClassName(e$24.ClassType), e$24.FMessage]);
         }
         LResponse = null;
         LRequest = null;
      }
   }
   /// procedure TNJHTTPServer.InternalSetActive(const Value: Boolean)
   ///  [line: 212, column: 25, file: SmartNJ.Server.Http]
   ,InternalSetActive$1:function(Self, Value$75) {
      TNJCustomServer.SetActive$3(Self,Value$75);
   }
   /// procedure TNJHTTPServer.SetActive(const Value: Boolean)
   ///  [line: 127, column: 25, file: SmartNJ.Server.Http]
   ,SetActive$3:function(Self, Value$76) {
      if (Value$76!=TNJCustomServer.GetActive$3(Self)) {
         TNJCustomServer.SetActive$3(Self,Value$76);
         try {
            if (TNJCustomServer.GetActive$3(Self)) {
               TNJCustomServer.StartServer$(Self);
            } else {
               TNJCustomServer.StopServer$(Self);
            }
         } catch ($e) {
            var e$25 = $W($e);
            TNJCustomServer.SetActive$3(Self,(!Value$76))         }
      }
   }
   /// procedure TNJHTTPServer.StartServer()
   ///  [line: 178, column: 25, file: SmartNJ.Server.Http]
   ,StartServer:function(Self) {
      var LServer$2 = null;
      try {
         LServer$2 = http().createServer($Event2(Self,TNJHTTPServer.Dispatch$3));
      } catch ($e) {
         var e$26 = $W($e);
         throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Failed to create NodeJS server object, system threw exception %s with message [%s]",[TObject.ClassName(e$26.ClassType), e$26.FMessage]);
      }
      try {
         LServer$2.listen(TNJCustomServer.GetPort$(Self),"");
      } catch ($e) {
         var e$27 = $W($e);
         LServer$2 = null;
         throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Failed to start server, system threw exception %s with message %s",[TObject.ClassName(e$27.ClassType), e$27.FMessage]);
      }
      TNJCustomServer.SetHandle$2(Self,LServer$2);
      TNJCustomServer.AfterStart(Self);
   }
   /// procedure TNJHTTPServer.StopServer()
   ///  [line: 217, column: 25, file: SmartNJ.Server.Http]
   ,StopServer:function(Self) {
      var cb = null;
      cb = function () {
         TNJHTTPServer.InternalSetActive$1(Self,false);
         TNJCustomServer.AfterStop(Self);
      };
      TNJCustomServer.GetHandle$1(Self).close(cb);
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,GetPort:TNJCustomServer.GetPort
   ,SetActive$3$:function($){return $.ClassType.SetActive$3.apply($.ClassType, arguments)}
   ,SetPort$1:TNJCustomServer.SetPort$1
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
};
TNJHTTPServer.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJHttpResponse = class (TNJCustomServerResponse)
///  [line: 69, column: 3, file: SmartNJ.Server.Http]
var TNJHttpResponse = {
   $ClassName:"TNJHttpResponse",$Parent:TNJCustomServerResponse
   ,$Init:function ($) {
      TNJCustomServerResponse.$Init($);
   }
   /// constructor TNJHttpResponse.Create(const Server: TNJCustomServer; const ResponseObject: JServerResponse)
   ///  [line: 232, column: 29, file: SmartNJ.Server.Http]
   ,Create$91:function(Self, Server$8, ResponseObject) {
      TNJServerChildClass.Create$90(Self,Server$8);
      TW3HandleBasedObject.SetObjectHandle(Self,ResponseObject);
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TNJHttpRequest = class (TNJCustomServerRequest)
///  [line: 41, column: 3, file: SmartNJ.Server.Http]
var TNJHttpRequest = {
   $ClassName:"TNJHttpRequest",$Parent:TNJCustomServerRequest
   ,$Init:function ($) {
      TNJCustomServerRequest.$Init($);
   }
   /// constructor TNJHttpRequest.Create(const Server: TNJCustomServer; const RequestObject: JServerRequest)
   ///  [line: 324, column: 28, file: SmartNJ.Server.Http]
   ,Create$92:function(Self, Server$9, RequestObject) {
      TNJServerChildClass.Create$90(Self,Server$9);
      TW3HandleBasedObject.SetObjectHandle(Self,RequestObject);
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// ENJHttpServerError = class (ENJServerError)
///  [line: 38, column: 3, file: SmartNJ.Server.Http]
var ENJHttpServerError = {
   $ClassName:"ENJHttpServerError",$Parent:ENJServerError
   ,$Init:function ($) {
      ENJServerError.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function WebSocketCreateServer(Options$8) {
   var Result = undefined;
   var Access = null;
   Access = WebSocketAPI();
   Result = new (Access).Server(Options$8);
   return Result
};
function WebSocketAPI() {
   return require("ws");
};
/// JWsVerifyClientInfo = class (TObject)
///  [line: 44, column: 3, file: Nodejs.websocket]
var JWsVerifyClientInfo = {
   $ClassName:"JWsVerifyClientInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.origin = "";
      $.req = null;
      $.secure = false;
   }
   ,Destroy:TObject.Destroy
};
/// TWbCMDLineParser = class (TCustomParser)
///  [line: 38, column: 3, file: Ragnarok.Cmd.Parser]
var TWbCMDLineParser = {
   $ClassName:"TWbCMDLineParser",$Parent:TCustomParser
   ,$Init:function ($) {
      TCustomParser.$Init($);
      $.FItems$4 = [];
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 42, column: 56, file: Ragnarok.Cmd.Parser]
   ,a$447:function(Self, index$5) {
      return Clone$TWbCMDInfo(Self.FItems$4[index$5]);
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 43, column: 37, file: Ragnarok.Cmd.Parser]
   ,a$446:function(Self) {
      return Self.FItems$4.length;
   }
   /// procedure TWbCMDLineParser.Clear()
   ///  [line: 71, column: 28, file: Ragnarok.Cmd.Parser]
   ,Clear$21:function(Self) {
      Self.FItems$4.length=0;
   }
   /// constructor TWbCMDLineParser.Create()
   ///  [line: 56, column: 30, file: Ragnarok.Cmd.Parser]
   ,Create$108:function(Self) {
      TCustomParser.Create$83(Self,TParserContext.Create$82($New(TParserContext),""));
      Self.FOptions$4.AutoResetError = true;
      Self.FOptions$4.AutoWriteToConsole = true;
      Self.FOptions$4.ThrowExceptions = false;
      return Self
   }
   /// destructor TWbCMDLineParser.Destroy()
   ///  [line: 64, column: 29, file: Ragnarok.Cmd.Parser]
   ,Destroy:function(Self) {
      if (Self.FItems$4.length>0) {
         TWbCMDLineParser.Clear$21(Self);
      }
      TW3ErrorObject.Destroy(Self);
   }
   /// function TWbCMDLineParser.Parse() : Boolean
   ///  [line: 76, column: 27, file: Ragnarok.Cmd.Parser]
   ,Parse$2:function(Self) {
      var Result = false;
      var LBuffer$2 = null,
         LCMD = {v:""},
         LParams = {v:[]},
         Rec = {ciCommand:"",ciParams:[]};
      var a$482 = 0;
      var item$5 = "";
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      LBuffer$2 = Self.FContext.FBuffer$3;
      if (TTextBuffer.Empty$1(LBuffer$2)) {
         TW3ErrorObject.SetLastError$1(Self,"Parsing failed, buffer is empty error");
      } else {
         TTextBuffer.First$1(LBuffer$2);
         do {
            LCMD.v = "";
            LParams.v = [];
            TTextBuffer.ConsumeJunk(LBuffer$2);
            if (TTextBuffer.EOF$4(LBuffer$2)) {
               break;
            }
            if (!TTextBuffer.ReadWord$2(LBuffer$2,LCMD)) {
               TW3ErrorObject.SetLastError$1(Self,"Syntax error, failed to read command error");
               break;
            }
            Rec.ciCommand = LCMD.v;
            TTextBuffer.ConsumeJunk(LBuffer$2);
            if (TTextBuffer.EOF$4(LBuffer$2)||TTextBuffer.CrLf(LBuffer$2)) {
               Self.FItems$4.push(Clone$TWbCMDInfo(Rec));
               break;
            }
            if (!TTextBuffer.ReadCommaList(LBuffer$2,LParams)) {
               break;
            }
            var $temp56;
            for(a$482=0,$temp56=LParams.v.length;a$482<$temp56;a$482++) {
               item$5 = LParams.v[a$482];
               Rec.ciParams.push(item$5);
            }
            Self.FItems$4.push(Clone$TWbCMDInfo(Rec));
            if (!TTextBuffer.Next$1(LBuffer$2)) {
               break;
            }
         } while (!TTextBuffer.EOF$4(LBuffer$2));
         Result = !TW3ErrorObject.GetFailed$1(Self);
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45:TW3ErrorObject.Create$45
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,Parse$2$:function($){return $.ClassType.Parse$2($)}
};
TWbCMDLineParser.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TWbCMDInfo = record
///  [line: 32, column: 3, file: Ragnarok.Cmd.Parser]
function Copy$TWbCMDInfo(s,d) {
   d.ciCommand=s.ciCommand;
   d.ciParams=s.ciParams;
   return d;
}
function Clone$TWbCMDInfo($) {
   return {
      ciCommand:$.ciCommand,
      ciParams:$.ciParams
   }
}
/// TNameValuePairData = record
///  [line: 29, column: 3, file: System.NameValuePairs]
function Copy$TNameValuePairData(s,d) {
   d.pdName=s.pdName;
   d.pdValue=s.pdValue;
   return d;
}
function Clone$TNameValuePairData($) {
   return {
      pdName:$.pdName,
      pdValue:$.pdValue
   }
}
/// TNameValuePair = class (TObject)
///  [line: 39, column: 3, file: System.NameValuePairs]
var TNameValuePair = {
   $ClassName:"TNameValuePair",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FData$2 = {pdName:"",pdValue:undefined};
   }
   /// procedure TNameValuePair.Clear()
   ///  [line: 126, column: 26, file: System.NameValuePairs]
   ,Clear$22:function(Self) {
      Self.FData$2.pdName = "";
      Self.FData$2.pdValue = null;
   }
   /// constructor TNameValuePair.Create()
   ///  [line: 120, column: 28, file: System.NameValuePairs]
   ,Create$109:function(Self) {
      TObject.Create(Self);
      TNameValuePair.Clear$22(Self);
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TApplication = class (TObject)
///  [line: 29, column: 3, file: SmartNJ.Application]
var TApplication = {
   $ClassName:"TApplication",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TApplication.Terminate()
   ///  [line: 161, column: 24, file: SmartNJ.Application]
   ,Terminate:function(Self) {
      var access = null;
      access = process;
      if (access!==null) {
         access.exit(0);
      }
   }
   ,Destroy:TObject.Destroy
};
function Application() {
   var Result = null;
   if (__App===null) {
      __App = TObject.Create($New(TApplication));
   }
   Result = __App;
   return Result
};
/// TW3NodeStorageDevice = class (TW3StorageDevice)
///  [line: 62, column: 3, file: SmartNJ.Device.Storage]
var TW3NodeStorageDevice = {
   $ClassName:"TW3NodeStorageDevice",$Parent:TW3StorageDevice
   ,$Init:function ($) {
      TW3StorageDevice.$Init($);
      $.FCurrent$1 = $.FRootPath = "";
      $.FFileSys = null;
   }
   /// procedure TW3NodeStorageDevice.CdUp(CB: TW3DeviceCdUpCallback)
   ///  [line: 692, column: 32, file: SmartNJ.Device.Storage]
   ,CdUp:function(Self, CB$10) {
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         TW3StorageDevice.GetPath$1$(Self,function (Sender$5, Path$1, Success$1) {
            var xCurrentPath = "",
               DirParser = null,
               Moniker = "",
               Separator$19 = "",
               Parts$2 = [];
            if (Success$1) {
               xCurrentPath = Path$1;
               DirParser = GetDirectoryParser();
               Moniker = DirParser[1]();
               Separator$19 = DirParser[0]();
               xCurrentPath = TW3NodeStorageDevice.Normalize(Self,xCurrentPath);
               if ((xCurrentPath).toLocaleLowerCase()==(Moniker).toLocaleLowerCase()) {
                  if (CB$10) {
                     CB$10(Self,false);
                  }
                  return;
               }
               if (StrEndsWith(xCurrentPath,Separator$19)) {
                  xCurrentPath = (xCurrentPath).substr(0,(xCurrentPath.length-1));
               }
               Parts$2 = (xCurrentPath).split(Separator$19);
               Parts$2.splice((Parts$2.length-1),1)
               ;
               xCurrentPath = (Parts$2).join(Separator$19);
               TW3StorageDevice.ChDir$(Self,xCurrentPath,function (Sender$6, Path$2, Success$2) {
                  if (CB$10) {
                     CB$10(Self,Success$2);
                  }
               });
            } else if (CB$10) {
               CB$10(Self,false);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.CdUp"]);
         if (CB$10) {
            CB$10(Self,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.ChDir(FolderPath: String; CB: TW3DeviceChDirCallback)
   ///  [line: 375, column: 32, file: SmartNJ.Device.Storage]
   ,ChDir:function(Self, FolderPath, CB$11) {
      var Access$7 = null,
         workingpath = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         FolderPath = TW3NodeStorageDevice.Normalize(Self,FolderPath);
         Access$7 = process;
         if (Access$7!==null) {
            workingpath = "";
            try {
               Access$7.chdir(FolderPath);
               workingpath = Access$7.cwd();
            } catch ($e) {
               var e$28 = $W($e);
               TW3OwnedErrorObject.SetLastError(Self,e$28.FMessage);
               if (CB$11) {
                  CB$11(Self,FolderPath,false);
               }
               return;
            }
            Self.FCurrent$1 = workingpath;
            if (CB$11) {
               CB$11(Self,workingpath,workingpath.length>0);
            }
         } else {
            TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, unable to access process error",["TW3NodeStorageDevice.ChDir"]);
            if (CB$11) {
               CB$11(Self,FolderPath,false);
            }
         }
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.ChDir"]);
         if (CB$11) {
            CB$11(Self,FolderPath,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.DirExists(FolderName: String; CB: TW3DeviceGetFileExistsCallback)
   ///  [line: 532, column: 32, file: SmartNJ.Device.Storage]
   ,DirExists:function(Self, FolderName$2, CB$12) {
      var PROCNAME = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME = "TW3NodeStorageDevice.DirExists";
         FolderName$2 = TW3NodeStorageDevice.Normalize(Self,FolderName$2);
         Self.FFileSys.lstat(FolderName$2,function (err$1, stats$1) {
            if (err$1) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed with message %s",[PROCNAME, err$1.message]);
               if (CB$12) {
                  CB$12(Self,FolderName$2,false);
               }
               return;
            }
            if (CB$12) {
               CB$12(Self,FolderName$2,stats$1.isDirectory());
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.DirExists"]);
         if (CB$12) {
            CB$12(Self,FolderName$2,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.Examine(FolderPath: String; CB: TW3FileOperationExamineCallBack)
   ///  [line: 641, column: 32, file: SmartNJ.Device.Storage]
   ,Examine$1:function(Self, FolderPath$1, CB$13) {
      var LWalker = null;
      var PROCNAME$1 = "",
         LFiles = null;
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$1 = "TW3NodeStorageDevice.Examine";
         FolderPath$1 = TW3NodeStorageDevice.Normalize(Self,FolderPath$1);
         LWalker = TW3ErrorObject.Create$45$($New(TNJFileWalker));
         TNJFileWalker.Examine$4(LWalker,FolderPath$1,function (Sender$7, Success$3) {
            var LTemp$16 = null;
            var LTemp$17 = null;
            if (Success$3) {
               if (CB$13) {
                  LTemp$16.dlPath = FolderPath$1;
                  LTemp$16.dlItems = TNJFileWalker.ExtractAll(LWalker);
                  CB$13(Self,FolderPath$1,LTemp$16,true);
               }
            } else {
               if (CB$13) {
                  LTemp$17.dlPath = FolderPath$1;
                  CB$13(Self,FolderPath$1,LTemp$17,false);
               }
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Examine"]);
         if (CB$13) {
            LFiles.dlPath = FolderPath$1;
            CB$13(Self,FolderPath$1,LFiles,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.FileExists(Filename: String; CB: TW3DeviceGetFileExistsCallback)
   ///  [line: 426, column: 32, file: SmartNJ.Device.Storage]
   ,FileExists$1:function(Self, Filename$8, CB$14) {
      var PROCNAME$2 = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$2 = "TW3NodeStorageDevice.FileExists";
         Filename$8 = TW3NodeStorageDevice.Normalize(Self,Filename$8);
         Self.FFileSys.exists(Filename$8,function (exists$1) {
            if (exists$1) {
               Self.FFileSys.lstat(Filename$8,function (err$2, stats$2) {
                  if (err$2) {
                     TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed with message %s",[PROCNAME$2, err$2.message]);
                     if (CB$14) {
                        CB$14(Self,Filename$8,false);
                     }
                     return;
                  }
                  if (CB$14) {
                     CB$14(Self,Filename$8,stats$2.isFile());
                  }
               });
            } else if (CB$14) {
               CB$14(Self,Filename$8,false);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.FileExists"]);
         if (CB$14) {
            CB$14(Self,"",false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.GetFileSize(Filename: String; CB: TW3DeviceGetFileSizeCallback)
   ///  [line: 305, column: 32, file: SmartNJ.Device.Storage]
   ,GetFileSize:function(Self, Filename$9, CB$15) {
      var PROCNAME$3 = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$3 = "TW3NodeStorageDevice.GetFileSize";
         Filename$9 = TW3NodeStorageDevice.Normalize(Self,Filename$9);
         Self.FFileSys.lstat(Filename$9,function (err$3, stats$3) {
            if (err$3) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed with message %s",[PROCNAME$3, err$3.message]);
               if (CB$15) {
                  CB$15(Self,Filename$9,0,false);
               }
               return;
            }
            if (CB$15) {
               CB$15(Self,Filename$9,stats$3.size,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.GetFileSize"]);
         if (CB$15) {
            CB$15(Self,"",0,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.GetPath(CB: TW3DeviceGetPathCallback)
   ///  [line: 342, column: 32, file: SmartNJ.Device.Storage]
   ,GetPath$1:function(Self, CB$16) {
      var Access$8 = null;
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         try {
            Access$8 = process;
            if (Access$8!==null) {
               if (CB$16) {
                  CB$16(Self,Access$8.cwd(),true);
               }
            }
         } catch ($e) {
            var e$29 = $W($e);
            TW3OwnedErrorObject.SetLastError(Self,e$29.FMessage);
            CB$16(Self,"",false);
            return;
         }
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.GetPath"]);
         if (CB$16) {
            CB$16(Self,"",false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.GetStorageObjectType(ObjName: String; CB: TW3DeviceObjTypeCallback)
   ///  [line: 472, column: 32, file: SmartNJ.Device.Storage]
   ,GetStorageObjectType:function(Self, ObjName, CB$17) {
      var PROCNAME$4 = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$4 = "TW3NodeStorageDevice.GetStorageObjectType";
         ObjName = TW3NodeStorageDevice.Normalize(Self,ObjName);
         Self.FFileSys.lstat(ObjName,function (err$4, stats$4) {
            if (err$4) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed with message %s",[PROCNAME$4, err$4.message]);
               if (CB$17) {
                  CB$17(Self,ObjName,0,false);
               }
               return;
            }
            if (CB$17) {
               if (stats$4.isDirectory()) {
                  CB$17(Self,ObjName,2,true);
               } else if (stats$4.isFile()) {
                  CB$17(Self,ObjName,1,true);
               } else if (stats$4.isBlockDevice()) {
                  CB$17(Self,ObjName,3,true);
               } else if (stats$4.isCharacterDevice()) {
                  CB$17(Self,ObjName,4,true);
               } else if (stats$4.isSymbolicLink()) {
                  CB$17(Self,ObjName,5,true);
               } else if (stats$4.isFIFO()) {
                  CB$17(Self,ObjName,6,true);
               } else if (stats$4.isSocket()) {
                  CB$17(Self,ObjName,7,true);
               } else {
                  CB$17(Self,ObjName,0,false);
               }
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.GetStorageObjectType"]);
         if (CB$17) {
            CB$17(Self,ObjName,0,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.Load(Filename: String; TagValue: Variant; CB: TW3DeviceLoadCallback)
   ///  [line: 753, column: 32, file: SmartNJ.Device.Storage]
   ,Load:function(Self, Filename$10, TagValue$2, CB$18) {
      var PROCNAME$5 = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$5 = "TW3NodeStorageDevice.Load";
         Filename$10 = TW3NodeStorageDevice.Normalize(Self,Filename$10);
         Self.FFileSys.readFile(Filename$10,function (err$5, data) {
            var LTypedAccess$1 = null;
            var LBytes$10 = [],
               DataStream = null;
            if (err$5) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed: %s",[PROCNAME$5, err$5.message]);
               if (CB$18) {
                  CB$18(Self,TagValue$2,Filename$10,null,false);
               }
               return;
            }
            if (CB$18) {
               LTypedAccess$1 = new Uint8Array(data);
               LBytes$10 = TDatatype.TypedArrayToBytes$1(TDatatype,LTypedAccess$1);
               DataStream = TDataTypeConverter.Create$15$($New(TMemoryStream));
               if (LBytes$10.length>0) {
                  TStream.Write$1(DataStream,LBytes$10);
                  TStream.SetPosition$(DataStream,0);
               }
               CB$18(Self,TagValue$2,Filename$10,DataStream,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Load"]);
         if (CB$18) {
            CB$18(Self,TagValue$2,Filename$10,null,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.MakeDir(FolderName: String; Mode: TW3FilePermissionMask; CB: TW3DeviceMakeDirCallback)
   ///  [line: 570, column: 32, file: SmartNJ.Device.Storage]
   ,MakeDir:function(Self, FolderName$3, Mode$1, CB$19) {
      var PROCNAME$6 = "",
         ModeStr = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$6 = "TW3NodeStorageDevice.MakeDir";
         FolderName$3 = TW3NodeStorageDevice.Normalize(Self,FolderName$3);
         ModeStr = Mode$1.toString();
         Self.FFileSys.mkdir(FolderName$3,ModeStr,function (err$6) {
            if (err$6) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed with message: %s",[PROCNAME$6, err$6.message]);
               if (CB$19) {
                  CB$19(Self,FolderName$3,false);
               }
            } else if (CB$19) {
               CB$19(Self,FolderName$3,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.MakeDir"]);
         if (CB$19) {
            CB$19(Self,FolderName$3,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.Mount(Authentication: TW3DeviceAuthenticationData; CB: TW3StorageDeviceMountEvent)
   ///  [line: 245, column: 32, file: SmartNJ.Device.Storage]
   ,Mount:function(Self, Authentication, CB$20) {
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         TW3StorageDevice.UnMount$(Self,null);
      }
      Self.FFileSys = NodeFsAPI();
      try {
         Self.FRootPath = process.cwd();
      } catch ($e) {
         var e$30 = $W($e);
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Mount"]);
         if (CB$20) {
            CB$20(Self,false);
         }
         return;
      }
      Self.FCurrent$1 = Self.FRootPath;
      TW3StorageDevice.SetActive$1(Self,true);
      if (CB$20) {
         CB$20(Self,true);
      }
   }
   /// function TW3NodeStorageDevice.Normalize(NameOrPath: String) : String
   ///  [line: 300, column: 31, file: SmartNJ.Device.Storage]
   ,Normalize:function(Self, NameOrPath) {
      return NodePathAPI().normalize(NameOrPath);
   }
   /// procedure TW3NodeStorageDevice.RemoveDir(FolderName: String; CB: TW3DeviceRemoveDirCallback)
   ///  [line: 607, column: 32, file: SmartNJ.Device.Storage]
   ,RemoveDir:function(Self, FolderName$4, CB$21) {
      /* null */
   }
   /// procedure TW3NodeStorageDevice.Save(Filename: String; TagValue: Variant; Data: TStream; CB: TW3DeviceSaveCallback)
   ///  [line: 800, column: 32, file: SmartNJ.Device.Storage]
   ,Save:function(Self, Filename$11, TagValue$3, Data$50, CB$22) {
      var PROCNAME$7 = "",
         buffer$2 = null;
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$7 = "TW3NodeStorageDevice.Save";
         Filename$11 = TW3NodeStorageDevice.Normalize(Self,Filename$11);
         TStream.SetPosition$(Data$50,0);
         buffer$2 = Buffer.from(TStream.Read(Data$50,TStream.GetSize$(Data$50)));
         Self.FFileSys.writeFile(Filename$11,buffer$2,function (err$7) {
            if (err$7) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed: %s",[PROCNAME$7, err$7.message]);
               if (CB$22) {
                  CB$22(Self,TagValue$3,Filename$11,false);
               }
               return;
            }
            if (CB$22) {
               CB$22(Self,TagValue$3,Filename$11,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Save"]);
         if (CB$22) {
            CB$22(Self,TagValue$3,Filename$11,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.UnMount(CB: TW3StorageDeviceUnMountEvent)
   ///  [line: 281, column: 32, file: SmartNJ.Device.Storage]
   ,UnMount:function(Self, CB$23) {
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         try {
            TW3StorageDevice.SetActive$1(Self,false);
            if (CB$23) {
               CB$23(Self,true);
            }
         } finally {
            Self.FFileSys = null;
            Self.FRootPath = "";
            Self.FCurrent$1 = "";
         }
      }
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,Create$46:TW3Component.Create$46
   ,FinalizeObject:TW3StorageDevice.FinalizeObject
   ,InitializeObject:TW3StorageDevice.InitializeObject
   ,CdUp$:function($){return $.ClassType.CdUp.apply($.ClassType, arguments)}
   ,ChDir$:function($){return $.ClassType.ChDir.apply($.ClassType, arguments)}
   ,DirExists$:function($){return $.ClassType.DirExists.apply($.ClassType, arguments)}
   ,Examine$1$:function($){return $.ClassType.Examine$1.apply($.ClassType, arguments)}
   ,FileExists$1$:function($){return $.ClassType.FileExists$1.apply($.ClassType, arguments)}
   ,GetFileSize$:function($){return $.ClassType.GetFileSize.apply($.ClassType, arguments)}
   ,GetPath$1$:function($){return $.ClassType.GetPath$1.apply($.ClassType, arguments)}
   ,GetStorageObjectType$:function($){return $.ClassType.GetStorageObjectType.apply($.ClassType, arguments)}
   ,Load$:function($){return $.ClassType.Load.apply($.ClassType, arguments)}
   ,MakeDir$:function($){return $.ClassType.MakeDir.apply($.ClassType, arguments)}
   ,Mount$:function($){return $.ClassType.Mount.apply($.ClassType, arguments)}
   ,RemoveDir$:function($){return $.ClassType.RemoveDir.apply($.ClassType, arguments)}
   ,Save$:function($){return $.ClassType.Save.apply($.ClassType, arguments)}
   ,UnMount$:function($){return $.ClassType.UnMount.apply($.ClassType, arguments)}
};
TW3NodeStorageDevice.$Intf={
   IW3StorageDevice:[TW3StorageDevice.GetActive,TW3StorageDevice.GetName,TW3StorageDevice.GetDeviceOptions,TW3StorageDevice.GetDeviceId,TW3NodeStorageDevice.GetPath$1,TW3NodeStorageDevice.GetFileSize,TW3NodeStorageDevice.FileExists$1,TW3NodeStorageDevice.DirExists,TW3NodeStorageDevice.MakeDir,TW3NodeStorageDevice.RemoveDir,TW3NodeStorageDevice.Examine$1,TW3NodeStorageDevice.ChDir,TW3NodeStorageDevice.CdUp,TW3NodeStorageDevice.GetStorageObjectType,TW3NodeStorageDevice.Load,TW3NodeStorageDevice.Save]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TWriteFileOptions = record
///  [line: 71, column: 3, file: NodeJS.fs]
function Copy$TWriteFileOptions(s,d) {
   return d;
}
function Clone$TWriteFileOptions($) {
   return {

   }
}
/// TWatchOptions = record
///  [line: 93, column: 3, file: NodeJS.fs]
function Copy$TWatchOptions(s,d) {
   return d;
}
function Clone$TWatchOptions($) {
   return {

   }
}
/// TWatchFileOptions = record
///  [line: 88, column: 3, file: NodeJS.fs]
function Copy$TWatchFileOptions(s,d) {
   return d;
}
function Clone$TWatchFileOptions($) {
   return {

   }
}
/// TWatchFileListenerObject = record
///  [line: 83, column: 3, file: NodeJS.fs]
function Copy$TWatchFileListenerObject(s,d) {
   return d;
}
function Clone$TWatchFileListenerObject($) {
   return {

   }
}
/// TReadFileSyncOptions = record
///  [line: 66, column: 3, file: NodeJS.fs]
function Copy$TReadFileSyncOptions(s,d) {
   d.encoding=s.encoding;
   d.flag=s.flag;
   return d;
}
function Clone$TReadFileSyncOptions($) {
   return {
      encoding:$.encoding,
      flag:$.flag
   }
}
/// TReadFileOptions = record
///  [line: 61, column: 3, file: NodeJS.fs]
function Copy$TReadFileOptions(s,d) {
   return d;
}
function Clone$TReadFileOptions($) {
   return {

   }
}
/// TCreateWriteStreamOptions = class (TObject)
///  [line: 122, column: 3, file: NodeJS.fs]
var TCreateWriteStreamOptions = {
   $ClassName:"TCreateWriteStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.flags = "w";
   }
   ,Destroy:TObject.Destroy
};
/// TCreateWriteStreamOptionsEx = class (TCreateWriteStreamOptions)
///  [line: 131, column: 3, file: NodeJS.fs]
var TCreateWriteStreamOptionsEx = {
   $ClassName:"TCreateWriteStreamOptionsEx",$Parent:TCreateWriteStreamOptions
   ,$Init:function ($) {
      TCreateWriteStreamOptions.$Init($);
      $.start = 0;
   }
   ,Destroy:TObject.Destroy
};
/// TCreateReadStreamOptions = class (TObject)
///  [line: 102, column: 3, file: NodeJS.fs]
var TCreateReadStreamOptions = {
   $ClassName:"TCreateReadStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TCreateReadStreamOptionsEx = class (TCreateReadStreamOptions)
///  [line: 112, column: 3, file: NodeJS.fs]
var TCreateReadStreamOptionsEx = {
   $ClassName:"TCreateReadStreamOptionsEx",$Parent:TCreateReadStreamOptions
   ,$Init:function ($) {
      TCreateReadStreamOptions.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TAppendFileOptions = record
///  [line: 77, column: 3, file: NodeJS.fs]
function Copy$TAppendFileOptions(s,d) {
   return d;
}
function Clone$TAppendFileOptions($) {
   return {

   }
}
function NodeFsAPI() {
   return require("fs");
};
function NodePathAPI() {
   return require("path");
};
/// JPathParseData = record
///  [line: 20, column: 3, file: NodeJS.Path]
function Copy$JPathParseData(s,d) {
   return d;
}
function Clone$JPathParseData($) {
   return {

   }
}
/// TNJFileWalker = class (TW3ErrorObject)
///  [line: 28, column: 3, file: SmartNJ.FileWalker]
var TNJFileWalker = {
   $ClassName:"TNJFileWalker",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.OnIncludeFile = null;
      $.OnAfterWalk = null;
      $.OnBeforeWalk = null;
      $.FBusy = $.FCancel = false;
      $.FCurrent$2 = $.FPath = "";
      $.FDelay$2 = 1;
      $.FFinished = null;
      $.FItemStack = [];
      $.FListData = null;
   }
   /// procedure TNJFileWalker.Cancel()
   ///  [line: 129, column: 25, file: SmartNJ.FileWalker]
   ,Cancel:function(Self) {
      if (Self.FBusy) {
         Self.FCancel = true;
         Self.FItemStack.length=0;
      }
   }
   /// procedure TNJFileWalker.Clear()
   ///  [line: 117, column: 25, file: SmartNJ.FileWalker]
   ,Clear$2:function(Self) {
      Self.FListData.dlPath = "";
      Self.FListData.dlItems.length=0;
      Self.FListData = new TNJFileItemList();
      Self.FItemStack.length=0;
      Self.FBusy = false;
      Self.FCancel = false;
      Self.FFinished = null;
      Self.FPath = "";
   }
   /// constructor TNJFileWalker.Create()
   ///  [line: 89, column: 27, file: SmartNJ.FileWalker]
   ,Create$45:function(Self) {
      TW3ErrorObject.Create$45(Self);
      Self.FListData = new TNJFileItemList();
      return Self
   }
   /// destructor TNJFileWalker.Destroy()
   ///  [line: 95, column: 26, file: SmartNJ.FileWalker]
   ,Destroy:function(Self) {
      try {
         try {
            if (Self.FBusy) {
               TNJFileWalker.Cancel(Self);
            }
         } finally {
            Self.FItemStack.length=0;
            Self.FListData.dlPath = "";
            Self.FListData.dlItems.length=0;
            Self.FListData = null;
            Self.FFinished = null;
         }
      } catch ($e) {
         var e$31 = $W($e);
         /* null */
      }
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TNJFileWalker.DoAfterWalk()
   ///  [line: 167, column: 25, file: SmartNJ.FileWalker]
   ,DoAfterWalk:function(Self) {
      try {
         if (Self.FFinished) {
            Self.FFinished(Self,true);
         }
      } finally {
         if (Self.OnAfterWalk) {
            Self.OnAfterWalk(Self);
         }
      }
   }
   /// procedure TNJFileWalker.DoBeforeWalk()
   ///  [line: 161, column: 25, file: SmartNJ.FileWalker]
   ,DoBeforeWalk:function(Self) {
      if (Self.OnBeforeWalk) {
         Self.OnBeforeWalk(Self);
      }
   }
   /// procedure TNJFileWalker.Examine(Path: String; CB: TNJFileWalkerCallback)
   ///  [line: 342, column: 25, file: SmartNJ.FileWalker]
   ,Examine$4:function(Self, Path$3, CB$24) {
      var FFileSys$1 = null;
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Self.FBusy) {
         TW3ErrorObject.SetLastError$1(Self,"Filewalker instance is busy error");
         if (CB$24) {
            CB$24(Self,false);
         }
         return;
      }
      TNJFileWalker.Clear$2(Self);
      Self.FBusy = true;
      Self.FFinished = CB$24;
      Self.FPath = Path$3;
      TNJFileWalker.DoBeforeWalk(Self);
      FFileSys$1 = NodeFsAPI();
      FFileSys$1.readdir(Path$3,function (err$8, files) {
         var a$483 = 0;
         var fsName = "";
         if (err$8) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Failed to examine [%s], path does not exist error",[Self.FPath]);
            if (CB$24) {
               CB$24(Self,false);
            }
         } else {
            Self.FListData.dlPath = Path$3;
            var $temp57;
            for(a$483=0,$temp57=files.length;a$483<$temp57;a$483++) {
               fsName = files[a$483];
               Self.FItemStack.push(fsName);
            }
            TW3Dispatch.Execute$1(TW3Dispatch,$Event0(Self,TNJFileWalker.NextOrDone),Self.FDelay$2);
         }
      });
   }
   /// function TNJFileWalker.ExtractAll() : TNJFileItems
   ///  [line: 285, column: 24, file: SmartNJ.FileWalker]
   ,ExtractAll:function(Self) {
      var Result = [];
      var a$484 = 0;
      var xItem = null;
      if (Self.FBusy) {
         Result = [];
      } else {
         var a$485 = [];
         a$485 = Self.FListData.dlItems;
         var $temp58;
         for(a$484=0,$temp58=a$485.length;a$484<$temp58;a$484++) {
            xItem = a$485[a$484];
            Result.push(xItem);
         }
      }
      return Result
   }
   /// procedure TNJFileWalker.NextOrDone()
   ///  [line: 192, column: 25, file: SmartNJ.FileWalker]
   ,NextOrDone:function(Self) {
      function SignalDone() {
         Self.FBusy = false;
         TW3Dispatch.Execute$1(TW3Dispatch,$Event0(Self,TNJFileWalker.DoAfterWalk),Self.FDelay$2);
      };
      if (Self.FCancel) {
         SignalDone();
         return;
      }
      if (Self.FItemStack.length>0) {
         TW3Dispatch.Execute$1(TW3Dispatch,$Event0(Self,TNJFileWalker.ProcessFromStack),Self.FDelay$2);
      } else {
         SignalDone();
      }
   }
   /// procedure TNJFileWalker.ProcessFromStack()
   ///  [line: 215, column: 25, file: SmartNJ.FileWalker]
   ,ProcessFromStack:function(Self) {
      var LKeep = {v:false},
         FileObjName = "";
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Self.FCancel) {
         TNJFileWalker.NextOrDone(Self);
         return;
      }
      if (Self.FItemStack.length>0) {
         LKeep.v = true;
         FileObjName = Self.FItemStack.pop();
         Self.FCurrent$2 = NodePathAPI().normalize(IncludeTrailingPathDelimiter$4(Self.FPath)+FileObjName);
         NodeFsAPI().stat(Self.FCurrent$2,function (err$9, stats$5) {
            var LItem$4 = null,
               LTemp$18 = "";
            if (err$9) {
               TW3ErrorObject.SetLastError$1(Self,err$9.message);
               TNJFileWalker.NextOrDone(Self);
               return;
            }
            LItem$4 = new TNJFileItem();
            LItem$4.diFileName = FileObjName;
            LItem$4.diFileType = (stats$5.isFile())?0:1;
            LItem$4.diFileSize = (stats$5.isFile())?stats$5.size:0;
            LItem$4.diCreated = (stats$5.ctime!==null)?JDateToDateTime(stats$5.ctime):Now();
            LItem$4.diModified = (stats$5.mtime!==null)?JDateToDateTime(stats$5.mtime):Now();
            if (stats$5.isFile()) {
               LTemp$18 = "";
               LTemp$18 = '0' + ((stats$5).mode & parseInt('777', 8)).toString(8);
               LItem$4.diFileMode = LTemp$18;
            }
            if (Self.OnIncludeFile) {
               Self.OnIncludeFile(Self,LItem$4,LKeep);
            }
            if (LKeep.v) {
               Self.FListData.dlItems.push(LItem$4);
            } else {
               LItem$4 = null;
            }
            Self.FCurrent$2 = "";
            TW3Dispatch.Execute$1(TW3Dispatch,$Event0(Self,TNJFileWalker.NextOrDone),Self.FDelay$2);
         });
      } else {
         TNJFileWalker.NextOrDone(Self);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45$:function($){return $.ClassType.Create$45($)}
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
};
TNJFileWalker.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// Tspawn_options_object = class (TObject)
///  [line: 38, column: 3, file: NodeJS.Child_Process]
var Tspawn_options_object = {
   $ClassName:"Tspawn_options_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// Tfork_options_object = class (TObject)
///  [line: 68, column: 3, file: NodeJS.Child_Process]
var Tfork_options_object = {
   $ClassName:"Tfork_options_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// Texec_options_object = class (TObject)
///  [line: 46, column: 3, file: NodeJS.Child_Process]
var Texec_options_object = {
   $ClassName:"Texec_options_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TexecFile_options_object = class (TObject)
///  [line: 57, column: 3, file: NodeJS.Child_Process]
var TexecFile_options_object = {
   $ClassName:"TexecFile_options_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
function child_process() {
   return require("child_process");
};
function NodeJSOsAPI() {
   return require("os");
};
/// Tlisteners_result_object = class (TObject)
///  [line: 36, column: 3, file: NodeJS.cluster]
var Tlisteners_result_object = {
   $ClassName:"Tlisteners_result_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
function NodeJSClusterAPI() {
   return require("cluster");
};
/// TNJHttpsServerMode enumeration
///  [line: 89, column: 3, file: SmartNJ.Server.Https]
var TNJHttpsServerMode = [ "ssmCertificate", "ssmPfxKey" ];
/// TNJHTTPSServer = class (TNJCustomServer)
///  [line: 124, column: 3, file: SmartNJ.Server.Https]
var TNJHTTPSServer = {
   $ClassName:"TNJHTTPSServer",$Parent:TNJCustomServer
   ,$Init:function ($) {
      TNJCustomServer.$Init($);
      $.OnRequest = null;
      $.FCerts = $.FCiphers = null;
      $.FMode = 0;
      $.FOptions$7 = [0];
   }
   /// constructor TNJHTTPSServer.Create()
   ///  [line: 329, column: 28, file: SmartNJ.Server.Https]
   ,Create$45:function(Self) {
      TW3ErrorObject.Create$45(Self);
      Self.FCiphers = TNJHttpsCiphers.Create$95($New(TNJHttpsCiphers),Self);
      Self.FCerts = TNJHTTPSCertificates.Create$96($New(TNJHTTPSCertificates),Self);
      Self.FOptions$7 = [4];
      Self.FMode = 0;
      return Self
   }
   /// destructor TNJHTTPSServer.Destroy()
   ///  [line: 338, column: 27, file: SmartNJ.Server.Https]
   ,Destroy:function(Self) {
      TObject.Free(Self.FCiphers);
      TObject.Free(Self.FCerts);
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TNJHTTPSServer.Dispatch(request: JServerRequest; response: JServerResponse)
   ///  [line: 365, column: 26, file: SmartNJ.Server.Https]
   ,Dispatch$4:function(Self, request$3, response$1) {
      var LRequest$1 = null,
         LResponse$1 = null;
      if (Self.OnRequest) {
         LRequest$1 = TNJHttpRequest.Create$92($New(TNJHttpRequest),Self,request$3);
         try {
            LResponse$1 = TNJHttpResponse.Create$91($New(TNJHttpResponse),Self,response$1);
            try {
               try {
                  Self.OnRequest(Self,LRequest$1,LResponse$1);
               } catch ($e) {
                  var e$32 = $W($e);
                  throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Dispatch failed, system threw exception %s with message [%s]",[TObject.ClassName(e$32.ClassType), e$32.FMessage]);
               }
            } finally {
               TObject.Free(LResponse$1);
            }
         } finally {
            TObject.Free(LRequest$1);
         }
      }
   }
   /// procedure TNJHTTPSServer.InternalSetActive(const Value: Boolean)
   ///  [line: 475, column: 26, file: SmartNJ.Server.Https]
   ,InternalSetActive$2:function(Self, Value$77) {
      TNJCustomServer.SetActive$3(Self,Value$77);
   }
   /// procedure TNJHTTPSServer.SetActive(const Value: Boolean)
   ///  [line: 345, column: 26, file: SmartNJ.Server.Https]
   ,SetActive$3:function(Self, Value$78) {
      if (Value$78!=TNJCustomServer.GetActive$3(Self)) {
         TNJCustomServer.SetActive$3(Self,Value$78);
         try {
            if (TNJCustomServer.GetActive$3(Self)) {
               TNJCustomServer.StartServer$(Self);
            } else {
               TNJCustomServer.StopServer$(Self);
            }
         } catch ($e) {
            var e$33 = $W($e);
            TNJCustomServer.SetActive$3(Self,(!Value$78))         }
      }
   }
   /// procedure TNJHTTPSServer.SetOptions(const NewPrefs: TNJHttpsServerOptions)
   ///  [line: 401, column: 26, file: SmartNJ.Server.Https]
   ,SetOptions:function(Self, NewPrefs) {
      if (TNJCustomServer.GetActive$3(Self)) {
         throw Exception.Create($New(ENJHttpServerError),"Server options cannot be altered while active error");
      } else {
         Self.FOptions$7 = NewPrefs.slice(0);
      }
   }
   /// procedure TNJHTTPSServer.StartServer()
   ///  [line: 410, column: 26, file: SmartNJ.Server.Https]
   ,StartServer:function(Self) {
      var LServer$3 = null;
      var opts;
      opts = TVariant.CreateObject();
      switch (Self.FMode) {
         case 0 :
            opts["cert"] = Self.FCerts.FCert;
            opts["ca"] = Self.FCerts.FCa;
            opts["key"] = Self.FCerts.FKey;
            break;
         case 1 :
            opts["pfx"] = Self.FCerts.FPFX;
            opts["passphrase"] = Self.FCerts.FPFXPass;
            break;
      }
      opts["requestCert"] = $SetIn(Self.FOptions$7,0,0,4);
      opts["rejectUnauthorized"] = $SetIn(Self.FOptions$7,1,0,4);
      if ($SetIn(Self.FOptions$7,3,0,4)) {
         if (!TNJHttpsCiphers.a$321(Self.FCiphers)) {
            opts["ciphers"] = TNJHttpsCiphers.ToString$9(Self.FCiphers);
            opts["honorCipherOrder"] = $SetIn(Self.FOptions$7,2,0,4);
         }
      }
      try {
         LServer$3 = https().createServer(opts,$Event2(Self,TNJHTTPSServer.Dispatch$4));
      } catch ($e) {
         var e$34 = $W($e);
         throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Failed to create NodeJS server object, system threw exception %s with message [%s]",[TObject.ClassName(e$34.ClassType), e$34.FMessage]);
      }
      try {
         LServer$3.listen(TNJCustomServer.GetPort$(Self),"");
      } catch ($e) {
         var e$35 = $W($e);
         LServer$3 = null;
         throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Failed to start server, system threw exception %s with message %s",[TObject.ClassName(e$35.ClassType), e$35.FMessage]);
      }
      TNJCustomServer.SetHandle$2(Self,LServer$3);
      TNJCustomServer.AfterStart(Self);
   }
   /// procedure TNJHTTPSServer.StopServer()
   ///  [line: 480, column: 26, file: SmartNJ.Server.Https]
   ,StopServer:function(Self) {
      var cb$1 = null;
      cb$1 = function () {
         TNJHTTPSServer.InternalSetActive$2(Self,false);
         TNJCustomServer.AfterStop(Self);
      };
      TNJCustomServer.GetHandle$1(Self).close(cb$1);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45$:function($){return $.ClassType.Create$45($)}
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,GetPort:TNJCustomServer.GetPort
   ,SetActive$3$:function($){return $.ClassType.SetActive$3.apply($.ClassType, arguments)}
   ,SetPort$1:TNJCustomServer.SetPort$1
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
};
TNJHTTPSServer.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJHttpsCiphers = class (TW3OwnedObject)
///  [line: 57, column: 3, file: SmartNJ.Server.Https]
var TNJHttpsCiphers = {
   $ClassName:"TNJHttpsCiphers",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.Separator = "";
      $.FItems$1 = [];
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 66, column: 38, file: SmartNJ.Server.Https]
   ,a$321:function(Self) {
      return Self.FItems$1.length<1;
   }
   /// constructor TNJHttpsCiphers.Create(const AOwner: TNJHTTPSServer)
   ///  [line: 230, column: 29, file: SmartNJ.Server.Https]
   ,Create$95:function(Self, AOwner$6) {
      TW3OwnedObject.Create$16(Self,AOwner$6);
      Self.Separator = ":";
      return Self
   }
   /// function TNJHttpsCiphers.GetOwner() : TNJHTTPSServer
   ///  [line: 236, column: 26, file: SmartNJ.Server.Https]
   ,GetOwner$6:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TNJHTTPSServer);
   }
   /// function TNJHttpsCiphers.ToString() : String
   ///  [line: 252, column: 26, file: SmartNJ.Server.Https]
   ,ToString$9:function(Self) {
      var Result = "";
      if (Self.FItems$1.length>0) {
         if (!$SetIn(TNJHttpsCiphers.GetOwner$6(Self).FOptions$7,2,0,4)) {
            Self.FItems$1.sort();
         }
         if (Self.Separator.length<1) {
            Self.Separator = ":";
         }
         Result = (Self.FItems$1).join(Self.Separator);
      }
      return Result
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
};
TNJHttpsCiphers.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TNJHTTPSCertificates = class (TW3OwnedObject)
///  [line: 94, column: 3, file: SmartNJ.Server.Https]
var TNJHTTPSCertificates = {
   $ClassName:"TNJHTTPSCertificates",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FCa = $.FCert = $.FKey = $.FPFX = $.FPFXPass = "";
   }
   /// constructor TNJHTTPSCertificates.Create(const AOwner: TNJHTTPSServer)
   ///  [line: 187, column: 34, file: SmartNJ.Server.Https]
   ,Create$96:function(Self, AOwner$7) {
      TW3OwnedObject.Create$16(Self,AOwner$7);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
};
TNJHTTPSCertificates.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// JServerOptions = class (TObject)
///  [line: 23, column: 3, file: NodeJS.https]
var JServerOptions = {
   $ClassName:"JServerOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
function https() {
   return require("https");
};
/// TNJWebSocketServerSecure = class (TNJWebSocketHybridServer)
///  [line: 85, column: 3, file: SmartNJ.Server.WebSocketSecure]
var TNJWebSocketServerSecure = {
   $ClassName:"TNJWebSocketServerSecure",$Parent:TNJWebSocketHybridServer
   ,$Init:function ($) {
      TNJWebSocketHybridServer.$Init($);
      $.FHttps = null;
   }
   /// constructor TNJWebSocketServerSecure.Create()
   ///  [line: 110, column: 38, file: SmartNJ.Server.WebSocketSecure]
   ,Create$45:function(Self) {
      TNJRawWebSocketServer.Create$45(Self);
      Self.FHttps = TW3ErrorObject.Create$45$($New(TNJHTTPSServer));
      return Self
   }
   /// destructor TNJWebSocketServerSecure.Destroy()
   ///  [line: 116, column: 37, file: SmartNJ.Server.WebSocketSecure]
   ,Destroy:function(Self) {
      TObject.Free(Self.FHttps);
      TNJRawWebSocketServer.Destroy(Self);
   }
   /// function TNJWebSocketServerSecure.GetHttpServer() : TNJCustomServer
   ///  [line: 122, column: 35, file: SmartNJ.Server.WebSocketSecure]
   ,GetHttpServer:function(Self) {
      return Self.FHttps;
   }
   /// function TNJWebSocketServerSecure.GetPort() : Integer
   ///  [line: 127, column: 35, file: SmartNJ.Server.WebSocketSecure]
   ,GetPort:function(Self) {
      return TNJCustomServer.GetPort$(Self.FHttps);
   }
   /// procedure TNJWebSocketServerSecure.SetPort(const Value: Integer)
   ///  [line: 132, column: 36, file: SmartNJ.Server.WebSocketSecure]
   ,SetPort$1:function(Self, Value$79) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$3(Self)) {
         TW3ErrorObject.SetLastError$1(Self,"Port cannot be altered while server is active error");
      } else {
         TNJCustomServer.SetPort$1$(Self.FHttps,Value$79);
      }
   }
   /// procedure TNJWebSocketServerSecure.StartServer()
   ///  [line: 143, column: 36, file: SmartNJ.Server.WebSocketSecure]
   ,StartServer:function(Self) {
      var LOptions$4,
         LServer$4;
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      TNJRawWebSocketServer.ClearLookupTable(Self);
      LOptions$4 = TVariant.CreateObject();
      LOptions$4["server"] = TNJCustomServer.GetHandle$1(Self.FHttps);
      LOptions$4["clientTracking "] = Self.FTrack;
      LOptions$4["maxPayload"] = Self.FPayload;
      if (Self.FPath$1.length>0) {
         LOptions$4["path"] = Self.FPath$1;
      }
      LServer$4 = null;
      try {
         LServer$4 = WebSocketCreateServer(LOptions$4);
      } catch ($e) {
         var e$36 = $W($e);
         TW3ErrorObject.SetLastErrorF$1(Self,"Failed to create websocket server object, system threw exception %s with message [%s]",[TObject.ClassName(e$36.ClassType), e$36.FMessage]);
         return;
      }
      TNJCustomServer.SetHandle$2(Self,LServer$4);
      LServer$4.on("error",function (error$6) {
         if (Self.OnError) {
            Self.OnError(Self,null,error$6);
         }
      });
      LServer$4.on("connection",$Event2(Self,TNJRawWebSocketServer.DoClientConnected));
      TNJCustomServer.AfterStart(Self);
   }
   /// procedure TNJWebSocketServerSecure.StopServer()
   ///  [line: 195, column: 36, file: SmartNJ.Server.WebSocketSecure]
   ,StopServer:function(Self) {
      var LHandle$3 = undefined;
      LHandle$3 = TNJCustomServer.GetHandle$1(Self);
      if (LHandle$3) {
         LHandle$3.close(function () {
            TNJCustomServer.SetActive$3$(Self.FHttps,false);
            TNJRawWebSocketServer.InternalSetActive(Self,false);
            TNJCustomServer.AfterStop(Self);
            TNJRawWebSocketServer.ClearLookupTable(Self);
         });
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$45$:function($){return $.ClassType.Create$45($)}
   ,GetExceptionClass:TNJRawWebSocketServer.GetExceptionClass
   ,GetPort$:function($){return $.ClassType.GetPort($)}
   ,SetActive$3:TNJRawWebSocketServer.SetActive$3
   ,SetPort$1$:function($){return $.ClassType.SetPort$1.apply($.ClassType, arguments)}
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
   ,GetHttpServer$:function($){return $.ClassType.GetHttpServer($)}
};
TNJWebSocketServerSecure.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TQTXUserSession = class (TW3OwnedObject)
///  [line: 44, column: 3, file: ragnarok.dbsessons]
var TQTXUserSession = {
   $ClassName:"TQTXUserSession",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
   }
   /// function TQTXUserSession.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 159, column: 26, file: ragnarok.dbsessons]
   ,AcceptOwner:function(Self, CandidateObject$8) {
      return CandidateObject$8!==null&&$Is(CandidateObject$8,TQTXUserSession);
   }
   /// constructor TQTXUserSession.Create(const Manager: TQTXSessionManager)
   ///  [line: 149, column: 29, file: ragnarok.dbsessons]
   ,Create$110:function(Self, Manager$1) {
      TW3OwnedObject.Create$16(Self,Manager$1);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   // IGNORED: TQTXUserSession.GetIdentifier
   // IGNORED: TQTXUserSession.GetPrivileges
   // IGNORED: TQTXUserSession.GetRemoteHost
   // IGNORED: TQTXUserSession.GetRootFolder
   // IGNORED: TQTXUserSession.GetState
   // IGNORED: TQTXUserSession.SetIdentifier
   // IGNORED: TQTXUserSession.SetPrivileges
   // IGNORED: TQTXUserSession.SetRemoteHost
   // IGNORED: TQTXUserSession.SetRootFolder
   // IGNORED: TQTXUserSession.SetState
};
TQTXUserSession.$Intf={
   IW3OwnedObjectAccess:[TQTXUserSession.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TQTXSessionManager = class (TObject)
///  [line: 82, column: 3, file: ragnarok.dbsessons]
var TQTXSessionManager = {
   $ClassName:"TQTXSessionManager",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
   // IGNORED: TQTXSessionManager.FindSessionByHost
   // IGNORED: TQTXSessionManager.FindSessionbyId
   // IGNORED: TQTXSessionManager.FlushSessions
   // IGNORED: TQTXSessionManager.GetSessionByIndex
   // IGNORED: TQTXSessionManager.GetSessionCount
   // IGNORED: TQTXSessionManager.Register
   // IGNORED: TQTXSessionManager.Register
};
/// TQTXAccountState enumeration
///  [line: 38, column: 3, file: ragnarok.dbsessons]
var TQTXAccountState = [ "asActive", "asLocked", "asUnConfirmed" ];
/// TQTXAccountPrivilege enumeration
///  [line: 28, column: 3, file: ragnarok.dbsessons]
var TQTXAccountPrivilege = [ "apExecute", "apRead", "apWrite", "apCreate", "apInstall", "apAppstore" ];
/// TQTXMessageInfo = class (TObject)
///  [line: 29, column: 3, file: service.dispatcher]
var TQTXMessageInfo = {
   $ClassName:"TQTXMessageInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.MessageClass = null;
      $.MessageHandler = null;
   }
   ,Destroy:TObject.Destroy
};
/// TQTXMessageDispatcher = class (TObject)
///  [line: 34, column: 3, file: service.dispatcher]
var TQTXMessageDispatcher = {
   $ClassName:"TQTXMessageDispatcher",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FItems$2 = [];
      $.FNameLUT = null;
   }
   /// procedure TQTXMessageDispatcher.Clear()
   ///  [line: 85, column: 33, file: service.dispatcher]
   ,Clear$16:function(Self) {
      var a$486 = 0;
      var info$3 = null;
      try {
         var a$487 = [];
         a$487 = Self.FItems$2;
         var $temp59;
         for(a$486=0,$temp59=a$487.length;a$486<$temp59;a$486++) {
            info$3 = a$487[a$486];
            TObject.Free(info$3);
         }
      } finally {
         Self.FItems$2.length=0;
         TW3CustomDictionary.Clear$12(Self.FNameLUT);
      }
   }
   /// constructor TQTXMessageDispatcher.Create()
   ///  [line: 69, column: 35, file: service.dispatcher]
   ,Create$97:function(Self) {
      TObject.Create(Self);
      Self.FNameLUT = TW3CustomDictionary.Create$87($New(TW3ObjDictionary));
      return Self
   }
   /// destructor TQTXMessageDispatcher.Destroy()
   ///  [line: 76, column: 34, file: service.dispatcher]
   ,Destroy:function(Self) {
      if (Self.FItems$2.length>0) {
         TQTXMessageDispatcher.Clear$16(Self);
      }
      TObject.Free(Self.FNameLUT);
      TObject.Destroy(Self);
   }
   /// function TQTXMessageDispatcher.GetMessageInfoForClass(const MessageClass: TQTXMessageBaseClass) : TQTXMessageInfo
   ///  [line: 99, column: 32, file: service.dispatcher]
   ,GetMessageInfoForClass:function(Self, MessageClass$4) {
      var Result = null;
      var a$488 = 0;
      var Info$4 = null;
      if (MessageClass$4) {
         var a$489 = [];
         a$489 = Self.FItems$2;
         var $temp60;
         for(a$488=0,$temp60=a$489.length;a$488<$temp60;a$488++) {
            Info$4 = a$489[a$488];
            if (Info$4.MessageClass==MessageClass$4) {
               Result = Info$4;
               break;
            }
         }
      }
      return Result
   }
   /// procedure TQTXMessageDispatcher.RegisterMessage(const MessageClass: TQTXMessageBaseClass; const Handler: TQTXDispatchHandler)
   ///  [line: 126, column: 33, file: service.dispatcher]
   ,RegisterMessage:function(Self, MessageClass$5, Handler$1) {
      var LName$5 = "",
         Info$5 = null;
      if (MessageClass$5) {
         if (Handler$1) {
            LName$5 = (Trim$_String_(TObject.ClassName(MessageClass$5))).toLocaleLowerCase();
            if (TW3CustomDictionary.Contains(Self.FNameLUT,LName$5)) {
               throw Exception.Create($New(EQTXMessageDispatcher),$R[25]);
            } else {
               Info$5 = TObject.Create($New(TQTXMessageInfo));
               Info$5.MessageClass = MessageClass$5;
               Info$5.MessageHandler = Handler$1;
               Self.FItems$2.push(Info$5);
               TW3ObjDictionary.a$238(Self.FNameLUT,LName$5,Info$5);
            }
         } else {
            throw Exception.Create($New(EQTXMessageDispatcher),$R[24]);
         }
      } else {
         throw Exception.Create($New(EQTXMessageDispatcher),$R[23]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// EQTXMessageDispatcher = class (EW3Exception)
///  [line: 22, column: 3, file: service.dispatcher]
var EQTXMessageDispatcher = {
   $ClassName:"EQTXMessageDispatcher",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TSessionService = class (TRagnarokService)
///  [line: 69, column: 3, file: service.session]
var TSessionService = {
   $ClassName:"TSessionService",$Parent:TRagnarokService
   ,$Init:function ($) {
      TRagnarokService.$Init($);
   }
   /// procedure TSessionService.HandleGetSession(Socket: TNJWebSocketSocket; Request: TQTXBaseMessage)
   ///  [line: 119, column: 27, file: service.session]
   ,HandleGetSession:function(Self, Socket$6, Request$2) {
      /* null */
   }
   /// procedure TSessionService.HandlePutSession(Socket: TNJWebSocketSocket; Request: TQTXBaseMessage)
   ///  [line: 123, column: 27, file: service.session]
   ,HandlePutSession:function(Self, Socket$7, Request$3) {
      /* null */
   }
   /// procedure TSessionService.HandleDestroySession(Socket: TNJWebSocketSocket; Request: TQTXBaseMessage)
   ///  [line: 127, column: 27, file: service.session]
   ,HandleDestroySession:function(Self, Socket$8, Request$4) {
      /* null */
   }
   /// procedure TSessionService.AfterServerStarted()
   ///  [line: 89, column: 27, file: service.session]
   ,AfterServerStarted:function(Self) {
      TRagnarokService.AfterServerStarted(Self);
      TQTXMessageDispatcher.RegisterMessage(Self.FDispatch,TQTXGetSessionDataMessage,$Event2(Self,TSessionService.HandleGetSession));
      TQTXMessageDispatcher.RegisterMessage(Self.FDispatch,TQTXPutSessionDataMessage,$Event2(Self,TSessionService.HandlePutSession));
      TQTXMessageDispatcher.RegisterMessage(Self.FDispatch,TQTXDestroySessionMessage,$Event2(Self,TSessionService.HandleDestroySession));
   }
   /// procedure TSessionService.BeforeServerStopped()
   ///  [line: 97, column: 27, file: service.session]
   ,BeforeServerStopped:function(Self) {
      TRagnarokService.BeforeServerStopped(Self);
   }
   /// procedure TSessionService.Dispatch(Socket: TNJWebSocketSocket; Message: TQTXBaseMessage)
   ///  [line: 102, column: 27, file: service.session]
   ,Dispatch:function(Self, Socket$9, Message$3) {
      var LInfo$1 = null;
      LInfo$1 = TQTXMessageDispatcher.GetMessageInfoForClass(Self.FDispatch,$ToClassType(Message$3));
      if (LInfo$1!==null) {
         try {
            LInfo$1.MessageHandler(Socket$9,Message$3);
         } catch ($e) {
            var e$37 = $W($e);
            WriteLn(e$37.FMessage)         }
      }
   }
   ,Destroy:TRagnarokService.Destroy
   ,AfterServerStarted$:function($){return $.ClassType.AfterServerStarted($)}
   ,BeforeServerStopped$:function($){return $.ClassType.BeforeServerStopped($)}
   ,Dispatch$:function($){return $.ClassType.Dispatch.apply($.ClassType, arguments)}
};
/// TQTXPutSessionDataMessage = class (TQTXBaseMessage)
///  [line: 53, column: 3, file: service.session]
var TQTXPutSessionDataMessage = {
   $ClassName:"TQTXPutSessionDataMessage",$Parent:TQTXBaseMessage
   ,$Init:function ($) {
      TQTXBaseMessage.$Init($);
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Create$70:TQTXBaseMessage.Create$70
   ,ReadObjData:TQTXBaseMessage.ReadObjData
   ,WriteObjData:TQTXBaseMessage.WriteObjData
};
/// TQTXGetSessionDataMessage = class (TQTXBaseMessage)
///  [line: 44, column: 3, file: service.session]
var TQTXGetSessionDataMessage = {
   $ClassName:"TQTXGetSessionDataMessage",$Parent:TQTXBaseMessage
   ,$Init:function ($) {
      TQTXBaseMessage.$Init($);
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Create$70:TQTXBaseMessage.Create$70
   ,ReadObjData:TQTXBaseMessage.ReadObjData
   ,WriteObjData:TQTXBaseMessage.WriteObjData
};
/// TQTXDestroySessionMessage = class (TQTXBaseMessage)
///  [line: 62, column: 3, file: service.session]
var TQTXDestroySessionMessage = {
   $ClassName:"TQTXDestroySessionMessage",$Parent:TQTXBaseMessage
   ,$Init:function ($) {
      TQTXBaseMessage.$Init($);
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Create$70:TQTXBaseMessage.Create$70
   ,ReadObjData:TQTXBaseMessage.ReadObjData
   ,WriteObjData:TQTXBaseMessage.WriteObjData
};
/// TCustomCodec = class (TObject)
///  [line: 134, column: 3, file: System.Codec]
var TCustomCodec = {
   $ClassName:"TCustomCodec",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FBindings = [];
      $.FCodecInfo = null;
   }
   /// constructor TCustomCodec.Create()
   ///  [line: 290, column: 26, file: System.Codec]
   ,Create$28:function(Self) {
      TObject.Create(Self);
      Self.FCodecInfo = TCustomCodec.MakeCodecInfo$(Self);
      if (Self.FCodecInfo===null) {
         throw EW3Exception.Create$27($New(ECodecError),"TCustomCodec.Create",Self,"Internal codec error, failed to obtain registration info error");
      }
      return Self
   }
   /// destructor TCustomCodec.Destroy()
   ///  [line: 299, column: 25, file: System.Codec]
   ,Destroy:function(Self) {
      TObject.Free(Self.FCodecInfo);
      TObject.Destroy(Self);
   }
   /// function TCustomCodec.MakeCodecInfo() : TCodecInfo
   ///  [line: 305, column: 23, file: System.Codec]
   ,MakeCodecInfo:function(Self) {
      return null;
   }
   /// procedure TCustomCodec.RegisterBinding(const Binding: TCodecBinding)
   ///  [line: 310, column: 24, file: System.Codec]
   ,RegisterBinding:function(Self, Binding) {
      if (Self.FBindings.indexOf(Binding)<0) {
         Self.FBindings.push(Binding);
      } else {
         throw EW3Exception.Create$27($New(ECodecError),"TCustomCodec.RegisterBinding",Self,"Binding already connected to codec error");
      }
   }
   /// procedure TCustomCodec.UnRegisterBinding(const Binding: TCodecBinding)
   ///  [line: 319, column: 24, file: System.Codec]
   ,UnRegisterBinding:function(Self, Binding$1) {
      var LIndex = 0;
      LIndex = Self.FBindings.indexOf(Binding$1);
      if (LIndex>=0) {
         Self.FBindings.splice(LIndex,1)
         ;
      } else {
         throw EW3Exception.Create$27($New(ECodecError),"TCustomCodec.UnRegisterBinding",Self,"Binding not connected to this codec error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,DecodeData$:function($){return $.ClassType.DecodeData.apply($.ClassType, arguments)}
   ,EncodeData$:function($){return $.ClassType.EncodeData.apply($.ClassType, arguments)}
   ,MakeCodecInfo$:function($){return $.ClassType.MakeCodecInfo($)}
};
TCustomCodec.$Intf={
   ICodecBinding:[TCustomCodec.RegisterBinding,TCustomCodec.UnRegisterBinding]
   ,ICodecProcess:[TCustomCodec.EncodeData,TCustomCodec.DecodeData]
}
/// TUTF8Codec = class (TCustomCodec)
///  [line: 23, column: 3, file: System.codec.utf8]
var TUTF8Codec = {
   $ClassName:"TUTF8Codec",$Parent:TCustomCodec
   ,$Init:function ($) {
      TCustomCodec.$Init($);
   }
   /// function TUTF8Codec.CanUseClampedArray() : Boolean
   ///  [line: 67, column: 27, file: System.codec.utf8]
   ,CanUseClampedArray:function() {
      var Result = {v:false};
      try {
         var LTemp$19 = undefined;
         try {
            LTemp$19 = new Uint8ClampedArray(10);
         } catch ($e) {
            var e$38 = $W($e);
            return Result.v;
         }
         if (LTemp$19) {
            Result.v = true;
         }
      } finally {return Result.v}
   }
   /// procedure TUTF8Codec.EncodeData(const Source: IBinaryTransport; const Target: IBinaryTransport)
   ///  [line: 211, column: 22, file: System.codec.utf8]
   ,EncodeData:function(Self, Source$5, Target$3) {
      var LReader = null,
         LWriter = null,
         BytesToRead$3 = 0,
         LCache$1 = [],
         Text$21 = "";
      LReader = TW3CustomReader.Create$39($New(TStreamReader),Source$5);
      try {
         LWriter = TW3CustomWriter.Create$29($New(TStreamWriter),Target$3);
         try {
            BytesToRead$3 = TW3CustomReader.GetTotalSize$2(LReader)-TW3CustomReader.GetReadOffset(LReader);
            if (BytesToRead$3>0) {
               LCache$1 = TW3CustomReader.Read$1(LReader,BytesToRead$3);
               Text$21 = TDatatype.BytesToString$1(TDatatype,LCache$1);
               LCache$1.length=0;
               LCache$1 = TUTF8Codec.Encode(Self,Text$21);
               TW3CustomWriter.Write(LWriter,LCache$1);
            }
         } finally {
            TObject.Free(LWriter);
         }
      } finally {
         TObject.Free(LReader);
      }
   }
   /// procedure TUTF8Codec.DecodeData(const Source: IBinaryTransport; const Target: IBinaryTransport)
   ///  [line: 241, column: 22, file: System.codec.utf8]
   ,DecodeData:function(Self, Source$6, Target$4) {
      var LReader$1 = null,
         LWriter$1 = null,
         BytesToRead$4 = 0,
         LCache$2 = [],
         Text$22 = "";
      LReader$1 = TW3CustomReader.Create$39($New(TReader),Source$6);
      try {
         LWriter$1 = TW3CustomWriter.Create$29($New(TWriter),Target$4);
         try {
            BytesToRead$4 = TW3CustomReader.GetTotalSize$2(LReader$1)-TW3CustomReader.GetReadOffset(LReader$1);
            if (BytesToRead$4>0) {
               LCache$2 = TW3CustomReader.Read$1(LReader$1,BytesToRead$4);
               Text$22 = TUTF8Codec.Decode(Self,LCache$2);
               LCache$2.length=0;
               LCache$2 = TDatatype.StringToBytes$1(TDatatype,Text$22);
               TW3CustomWriter.Write(LWriter$1,LCache$2);
            }
         } finally {
            TObject.Free(LWriter$1);
         }
      } finally {
         TObject.Free(LReader$1);
      }
   }
   /// function TUTF8Codec.MakeCodecInfo() : TCodecInfo
   ///  [line: 46, column: 21, file: System.codec.utf8]
   ,MakeCodecInfo:function(Self) {
      var Result = null;
      var LAccess$3 = null;
      var LVersion = {viMajor:0,viMinor:0,viRevision:0};
      Result = TObject.Create($New(TCodecInfo));
      LVersion.viMajor = 0;
      LVersion.viMinor = 2;
      LVersion.viRevision = 0;
      LAccess$3 = $AsIntf(Result,"ICodecInfo");
      LAccess$3[0]("UTF8Codec");
      LAccess$3[1]("text\/utf8");
      LAccess$3[2](LVersion);
      LAccess$3[3]([6]);
      LAccess$3[5](1);
      LAccess$3[6](0);
      return Result
   }
   /// function TUTF8Codec.Encode(TextToEncode: String) : TByteArray
   ///  [line: 80, column: 21, file: System.codec.utf8]
   ,Encode:function(Self, TextToEncode$2) {
      var Result = {v:[]};
      try {
         var LEncoder = null;
         var LClip = null;
         var LTemp$20 = null;
         var n = 0;
         var LTyped = null;
         try {
            LEncoder = new TextEncoder();
         } catch ($e) {
            if (TextToEncode$2.length>0) {
               if (TUTF8Codec.CanUseClampedArray()) {
                  LClip = new Uint8ClampedArray(1);
                  LTemp$20 = new Uint8ClampedArray(1);
               } else {
                  LClip = new Uint8Array(1);
                  LTemp$20 = new Uint8Array(1);
               }
               var $temp61;
               for(n=1,$temp61=TextToEncode$2.length;n<=$temp61;n++) {
                  LClip[0]=TString.CharCodeFor(TString,TextToEncode$2.charAt(n-1));
                  if (LClip[0]<128) {
                     Result.v.push(LClip[0]);
                  } else if (LClip[0]>127&&LClip[0]<2048) {
                     LTemp$20[0]=((LClip[0]>>>6)|192);
                     Result.v.push(LTemp$20[0]);
                     LTemp$20[0]=((LClip[0]&63)|128);
                     Result.v.push(LTemp$20[0]);
                  } else {
                     LTemp$20[0]=((LClip[0]>>>12)|224);
                     Result.v.push(LTemp$20[0]);
                     LTemp$20[0]=(((LClip[0]>>>6)&63)|128);
                     Result.v.push(LTemp$20[0]);
                     Result.v.push((LClip[0]&63)|128);
                     Result.v.push(LTemp$20[0]);
                  }
               }
            }
            return Result.v;
         }
         try {
            LTyped = LEncoder.encode(TextToEncode$2);
            Result.v = TDatatype.TypedArrayToBytes$1(TDatatype,LTyped);
         } finally {
            LEncoder = null;
         }
      } finally {return Result.v}
   }
   /// function TUTF8Codec.Decode(const BytesToDecode: TByteArray) : String
   ///  [line: 145, column: 21, file: System.codec.utf8]
   ,Decode:function(Self, BytesToDecode$1) {
      var Result = {v:""};
      try {
         var LDecoder = null;
         var bytelen = 0,
            i$2 = 0,
            c = 0,
            c2 = 0,
            c2$1 = 0,
            c3 = 0,
            c4 = 0,
            u = 0,
            c2$2 = 0,
            c3$1 = 0,
            LTyped$1 = undefined;
         try {
            LDecoder = new TextDecoder();
         } catch ($e) {
            bytelen = BytesToDecode$1.length;
            if (bytelen>0) {
               i$2 = 0;
               while (i$2<bytelen) {
                  c = BytesToDecode$1[i$2];
                  ++i$2;
                  if (c<128) {
                     Result.v+=TString.FromCharCode(TString,c);
                  } else if (c>191&&c<224) {
                     c2 = BytesToDecode$1[i$2];
                     ++i$2;
                     Result.v+=TString.FromCharCode(TString,(((c&31)<<6)|(c2&63)));
                  } else if (c>239&&c<365) {
                     c2$1 = BytesToDecode$1[i$2];
                     ++i$2;
                     c3 = BytesToDecode$1[i$2];
                     ++i$2;
                     c4 = BytesToDecode$1[i$2];
                     ++i$2;
                     u = (((((c&7)<<18)|((c2$1&63)<<12))|((c3&63)<<6))|(c4&63))-65536;
                     Result.v+=TString.FromCharCode(TString,(55296+(u>>>10)));
                     Result.v+=TString.FromCharCode(TString,(56320+(u&1023)));
                  } else {
                     c2$2 = BytesToDecode$1[i$2];
                     ++i$2;
                     c3$1 = BytesToDecode$1[i$2];
                     ++i$2;
                     Result.v+=TString.FromCharCode(TString,(((c&15)<<12)|(((c2$2&63)<<6)|(c3$1&63))));
                  }
               }
            }
            return Result.v;
         }
         try {
            LTyped$1 = TDatatype.BytesToTypedArray$1(TDatatype,BytesToDecode$1);
            Result.v = LDecoder.decode(LTyped$1);
         } finally {
            LDecoder = null;
         }
      } finally {return Result.v}
   }
   ,Destroy:TCustomCodec.Destroy
   ,DecodeData$:function($){return $.ClassType.DecodeData.apply($.ClassType, arguments)}
   ,EncodeData$:function($){return $.ClassType.EncodeData.apply($.ClassType, arguments)}
   ,MakeCodecInfo$:function($){return $.ClassType.MakeCodecInfo($)}
};
TUTF8Codec.$Intf={
   ICodecProcess:[TUTF8Codec.EncodeData,TUTF8Codec.DecodeData]
   ,ICodecBinding:[TCustomCodec.RegisterBinding,TCustomCodec.UnRegisterBinding]
}
/// TCodecVersionInfo = record
///  [line: 38, column: 3, file: System.Codec]
function Copy$TCodecVersionInfo(s,d) {
   d.viMajor=s.viMajor;
   d.viMinor=s.viMinor;
   d.viRevision=s.viRevision;
   return d;
}
function Clone$TCodecVersionInfo($) {
   return {
      viMajor:$.viMajor,
      viMinor:$.viMinor,
      viRevision:$.viRevision
   }
}
/// TCodecManager = class (TObject)
///  [line: 159, column: 3, file: System.Codec]
var TCodecManager = {
   $ClassName:"TCodecManager",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodecs = [];
   }
   /// function TCodecManager.CodecByClass(const ClsType: TCodecClass) : TCustomCodec
   ///  [line: 249, column: 24, file: System.Codec]
   ,CodecByClass:function(Self, ClsType) {
      var Result = null;
      var a$490 = 0;
      var LItem$5 = null;
      var a$491 = [];
      Result = null;
      a$491 = Self.FCodecs;
      var $temp62;
      for(a$490=0,$temp62=a$491.length;a$490<$temp62;a$490++) {
         LItem$5 = a$491[a$490];
         if (TObject.ClassType(LItem$5.ClassType)==ClsType) {
            Result = LItem$5;
            break;
         }
      }
      return Result
   }
   /// destructor TCodecManager.Destroy()
   ///  [line: 193, column: 26, file: System.Codec]
   ,Destroy:function(Self) {
      TObject.Destroy(Self);
   }
   /// procedure TCodecManager.RegisterCodec(const CodecClass: TCodecClass)
   ///  [line: 262, column: 25, file: System.Codec]
   ,RegisterCodec:function(Self, CodecClass) {
      var LItem$6 = null;
      LItem$6 = TCodecManager.CodecByClass(Self,CodecClass);
      if (LItem$6===null) {
         LItem$6 = TCustomCodec.Create$28($NewDyn(CodecClass,""));
         Self.FCodecs.push(LItem$6);
      } else {
         throw EW3Exception.Create$27($New(ECodecManager),"TCodecManager.RegisterCodec",Self,"Codec already registered error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TCodecInfo = class (TObject)
///  [line: 71, column: 3, file: System.Codec]
var TCodecInfo = {
   $ClassName:"TCodecInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.ciAbout = $.ciMime = $.ciName = "";
      $.ciDataFlow = [0];
      $.ciInput = 0;
      $.ciOutput = 0;
      $.ciVersion = {viMajor:0,viMinor:0,viRevision:0};
   }
   /// procedure TCodecInfo.SetDataFlow(const coFlow: TCodecDataFlow)
   ///  [line: 494, column: 22, file: System.Codec]
   ,SetDataFlow:function(Self, coFlow) {
      Self.ciDataFlow = coFlow.slice(0);
   }
   /// procedure TCodecInfo.SetDescription(const coInfo: String)
   ///  [line: 489, column: 22, file: System.Codec]
   ,SetDescription:function(Self, coInfo) {
      Self.ciAbout = coInfo;
   }
   /// procedure TCodecInfo.SetInput(const InputType: TCodecDataFormat)
   ///  [line: 479, column: 22, file: System.Codec]
   ,SetInput:function(Self, InputType) {
      Self.ciInput = InputType;
   }
   /// procedure TCodecInfo.SetMime(const coMime: String)
   ///  [line: 467, column: 22, file: System.Codec]
   ,SetMime:function(Self, coMime) {
      Self.ciMime = coMime;
   }
   /// procedure TCodecInfo.SetName(const coName: String)
   ///  [line: 462, column: 22, file: System.Codec]
   ,SetName:function(Self, coName) {
      Self.ciName = coName;
   }
   /// procedure TCodecInfo.SetOutput(const OutputType: TCodecDataFormat)
   ///  [line: 484, column: 22, file: System.Codec]
   ,SetOutput:function(Self, OutputType) {
      Self.ciOutput = OutputType;
   }
   /// procedure TCodecInfo.SetVersion(const coVersion: TCodecVersionInfo)
   ///  [line: 472, column: 22, file: System.Codec]
   ,SetVersion:function(Self, coVersion) {
      Self.ciVersion.viMajor = coVersion.viMajor;
      Self.ciVersion.viMinor = coVersion.viMinor;
      Self.ciVersion.viRevision = coVersion.viRevision;
   }
   ,Destroy:TObject.Destroy
};
TCodecInfo.$Intf={
   ICodecInfo:[TCodecInfo.SetName,TCodecInfo.SetMime,TCodecInfo.SetVersion,TCodecInfo.SetDataFlow,TCodecInfo.SetDescription,TCodecInfo.SetInput,TCodecInfo.SetOutput]
}
/// TCodecDataFormat enumeration
///  [line: 52, column: 3, file: System.Codec]
var TCodecDataFormat = [ "cdBinary", "cdText" ];
/// function TCodecDataFlowHelper.Ordinal(const Self: TCodecDataFlow) : Integer
///  [line: 426, column: 31, file: System.Codec]
function TCodecDataFlowHelper$Ordinal(Self$12) {
   var Result = 0;
   Result = 0;
   if ($SetIn(Self$12,1,0,3)) {
      ++Result;
   }
   if ($SetIn(Self$12,2,0,3)) {
      (Result+= 2);
   }
   return Result
}
/// TCodecDataDirection enumeration
///  [line: 47, column: 3, file: System.Codec]
var TCodecDataDirection = { 1:"cdRead", 2:"cdWrite" };
/// TCodecBinding = class (TObject)
///  [line: 102, column: 3, file: System.Codec]
var TCodecBinding = {
   $ClassName:"TCodecBinding",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodec = null;
   }
   /// constructor TCodecBinding.Create(const EndPoint: TCustomCodec)
   ///  [line: 334, column: 27, file: System.Codec]
   ,Create$41:function(Self, EndPoint) {
      var LAccess$4 = null;
      TObject.Create(Self);
      if (EndPoint!==null) {
         Self.FCodec = EndPoint;
         LAccess$4 = $AsIntf(Self.FCodec,"ICodecBinding");
         LAccess$4[0](Self);
      } else {
         throw EW3Exception.Create$27($New(ECodecBinding),"TCodecBinding.Create",Self,"Binding failed, invalid endpoint error");
      }
      return Self
   }
   /// destructor TCodecBinding.Destroy()
   ///  [line: 349, column: 26, file: System.Codec]
   ,Destroy:function(Self) {
      var LAccess$5 = null;
      LAccess$5 = $AsIntf(Self.FCodec,"ICodecBinding");
      LAccess$5[1](Self);
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// ECodecManager = class (EW3Exception)
///  [line: 29, column: 3, file: System.Codec]
var ECodecManager = {
   $ClassName:"ECodecManager",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ECodecError = class (EW3Exception)
///  [line: 30, column: 3, file: System.Codec]
var ECodecError = {
   $ClassName:"ECodecError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ECodecBinding = class (EW3Exception)
///  [line: 28, column: 3, file: System.Codec]
var ECodecBinding = {
   $ClassName:"ECodecBinding",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function CodecManager() {
   var Result = null;
   if (!__Manager) {
      __Manager = TObject.Create($New(TCodecManager));
   }
   Result = __Manager;
   return Result
};
var __CSUniqueId = 0;
var __UNIQUE = 0;
var a$7 = 0;
var a$10 = 0;
var a$9 = 0;
var a$8 = 0;
var a$11 = 0;
var a$12 = 0;
var a$30 = null;
var CRC_Table_Ready = false;
var CRC_Table = function () {
      for (var r=[],i=0; i<513; i++) r.push(0);
      return r
   }();
var a$89 = null;
var NodeProgram = null,
   _Common_CIPHERS = ["","","","","","","","","","","",""],
   _Common_BlackList = ["","",""];
var _Common_CIPHERS = ["TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384", "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384", "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA", "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256", "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384", "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256", "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA", "HIGH"];
var _Common_BlackList = ["!RC4", "!MD5", "!aNULL"];
var __App = null;
var __SessionManager = null;
var __RESERVED = [];
var __RESERVED = ["$ClassName", "$Parent", "$Init", "toString", "toLocaleString", "valueOf", "indexOf", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "constructor", "destructor"].slice();
var _OperandLUT,
   _StatementLUT,
   _Reserved = [],
   __Parser = null;
var __Def_Converter = null;
var __CONV_BUFFER = null;
var __CONV_VIEW = null;
var __CONV_ARRAY = null;
var __SIZES = [0,0,0,0,0,0,0,0,0,0,0],
   _NAMES = ["","","","","","","","","","",""],
   __B64_Lookup = function () {
      for (var r=[],i=0; i<257; i++) r.push("");
      return r
   }(),
   __B64_RevLookup = function () {
      for (var r=[],i=0; i<257; i++) r.push(0);
      return r
   }(),
   CNT_B64_CHARSET = "";
var __SIZES = [0, 1, 1, 2, 2, 4, 2, 4, 4, 8, 8];
var _NAMES = ["Unknown", "Boolean", "Byte", "Char", "Word", "Longword", "Smallint", "Integer", "Single", "Double", "String"];
var CNT_B64_CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+\/";
var __UniqueNumber = 0;
var __TYPE_MAP = {Boolean:undefined,Number$1:undefined,String$1:undefined,Object$2:undefined,Undefined:undefined,Function$1:undefined};
var pre_binary = [0,0],
   pre_OnOff = ["",""],
   pre_YesNo = ["",""],
   pre_StartStop = ["",""],
   pre_RunPause = ["",""];
var pre_binary = [0, 1];
var pre_OnOff = ["off", "on"];
var pre_YesNo = ["no", "yes"];
var pre_StartStop = ["stop", "start"];
var pre_RunPause = ["paused", "running"];
var __Manager = null;
var __Def_Converter = TDataTypeConverter.Create$15$($New(TDataTypeConverter));
SetupConversionLUT();
SetupBase64();
SetupTypeLUT();
TCodecManager.RegisterCodec(CodecManager(),TUTF8Codec);
SetupFilterLUT();
if (typeof btoa === 'undefined') {
      global.btoa = function (str) {
        return new Buffer(str).toString('base64');
      };
    }

    if (typeof atob === 'undefined') {
      global.atob = function (b64Encoded) {
        return new Buffer(b64Encoded, 'base64').toString();
      };
    }
;
switch (GetPlatform()) {
   case 1 :
      InstallDirectoryParser(TW3ErrorObject.Create$45$($New(TW3Win32DirectoryParser)));
      break;
   case 2 :
      InstallDirectoryParser(TW3ErrorObject.Create$45$($New(TW3PosixDirectoryParser)));
      break;
   default :
      throw Exception.Create($New(Exception),"Unsupported OS, no directory-parser for platform error");
}
;
var $Application = function() {
   NodeProgram = TNodeProgram.Create$3($New(TNodeProgram));
   TNodeProgram.Execute(NodeProgram);
}
$Application();

